package com.jjlee.wedqueen.rest.recommend.controller;

import com.jjlee.wedqueen.rest.recommend.model.RecommendResponse;
import com.jjlee.wedqueen.rest.recommend.service.RecommendService;
import com.jjlee.wedqueen.rest.region.model.RegionResponse;
import com.jjlee.wedqueen.rest.region.service.RegionService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class RecommendController extends RetrofitController {
    private RecommendService recommendService;

    public RecommendController() {
        super();
        recommendService = RETROFIT.create(RecommendService.class);
    }

    public Call<RecommendResponse> getRecommendUsers() {
        return recommendService.getRecommendUsers();
    }

    public Call<RecommendResponse> getHomeRecommendUsers() {
        return recommendService.getHomeRecommendUsers( "dday" );
    }

}
