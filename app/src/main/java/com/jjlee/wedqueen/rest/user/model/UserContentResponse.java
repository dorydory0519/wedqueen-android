package com.jjlee.wedqueen.rest.user.model;

import com.jjlee.wedqueen.rest.content.model.Content;

import java.util.List;

public class UserContentResponse {

    private List<Content> contents;

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }
}
