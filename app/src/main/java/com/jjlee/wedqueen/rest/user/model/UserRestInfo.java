package com.jjlee.wedqueen.rest.user.model;

import java.util.List;

/**
 * Created by leejungyi on 2017. 10. 22..
 */

public class UserRestInfo {
    private UserRestDto user;
    String marriageDate;
    private List<RegionDto> regions;
    private List<WeddingStyleDto> weddingStyles;
    private List<TabCategoryDto> tabcategories;
    private int followerCount;
    private int followingCount;

    public UserRestInfo() {
    }

    public UserRestDto getUser() {
        return user;
    }
    public void setUser(UserRestDto user) {
        this.user = user;
    }
    public String getMarriageDate() {
        return marriageDate;
    }
    public void setMarriageDate(String marriageDate) {
        this.marriageDate = marriageDate;
    }
    public List<RegionDto> getRegions() {
        return regions;
    }
    public void setRegions(List<RegionDto> regions) {
        this.regions = regions;
    }
    public List<WeddingStyleDto> getWeddingStyles() {
        return weddingStyles;
    }
    public void setWeddingStyles(List<WeddingStyleDto> weddingStyles) {
        this.weddingStyles = weddingStyles;
    }
    public List<TabCategoryDto> getTabcategories() {
        return tabcategories;
    }
    public void setTabcategories(List<TabCategoryDto> tabcategories) {
        this.tabcategories = tabcategories;
    }
    public int getFollowerCount() {
        return followerCount;
    }
    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }
    public int getFollowingCount() {
        return followingCount;
    }
    public void setFollowingCount(int followingCount) {
        this.followingCount = followingCount;
    }
}
