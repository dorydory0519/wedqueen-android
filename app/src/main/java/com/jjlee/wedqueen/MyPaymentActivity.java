package com.jjlee.wedqueen;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.CustomViews.CustomPaymentModel;
import com.jjlee.wedqueen.CustomViews.CustomReservModel;
import com.jjlee.wedqueen.rest.payment.controller.PaymentController;
import com.jjlee.wedqueen.rest.payment.model.Payment;
import com.jjlee.wedqueen.rest.payment.model.PaymentResponse;
import com.jjlee.wedqueen.rest.survey.controller.SurveyController;
import com.jjlee.wedqueen.rest.survey.model.SurveyAnswer;
import com.jjlee.wedqueen.rest.survey.model.SurveyResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPaymentActivity extends AppCompatActivity {

    private LinearLayout paymentWrapper;

    private Dialog pointHelpDlg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypayment);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "my_payment" ) );

        initPointHelpDlg();

        PaymentController paymentController = new PaymentController();
        paymentWrapper = (LinearLayout)findViewById( R.id.payment_wrapper );

        Call<PaymentResponse> paymentCall = paymentController.getPayments();
        paymentCall.enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    PaymentResponse paymentResponse = response.body();

                    if( !ObjectUtils.isEmpty( paymentResponse.getPayments() ) && paymentResponse.getPayments().size() > 0 ) {
                        List<Payment> payments = paymentResponse.getPayments();

                        for( Payment payment : payments ) {
                            appendPaymentModel( payment, true );
                        }
                    } else {
                        appendPaymentModel( null, false );
                    }
                } else {
                    appendPaymentModel( null, false );
                }
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                appendPaymentModel( null, false );
            }
        });
    }


    private void appendPaymentModel( Payment payment, boolean paymentOn ) {

        if( paymentOn ) {
            CustomPaymentModel customPaymentModel = new CustomPaymentModel( MyPaymentActivity.this );
            customPaymentModel.setPaymentOnOff( true );
            customPaymentModel.setPaymentCompanyThumb( payment.getCompanyThumbnailUrl() );
            customPaymentModel.setPaymentCompanyName( payment.getCompanyName() + " | " + payment.getProductName() );
            customPaymentModel.setPaymentAmount( payment.getAmount() );
            customPaymentModel.setPaymentTime( payment.getUpdationTime() );
            customPaymentModel.setPaymentStatus( payment.getStatus() );
//            customPaymentModel.setPaymentStatusColorCode( payment.getStatusColorCode() );
            customPaymentModel.setReviewParam( payment.getId() );

            customPaymentModel.setContract( payment.isContract(), payment.getContractPoint() );
            if( !payment.isContract() ) {
                customPaymentModel.getContractSection().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showPaymentHelpDlg( "contract", "" );
                    }
                });
            }

            customPaymentModel.setReview( payment.isReview(), payment.getReviewPoint() );
            if( !payment.isReview() ) {
                customPaymentModel.getReviewSection().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showPaymentHelpDlg( "review", v.getTag() + "" );
                    }
                });
            }

            customPaymentModel.setTag( payment.getLinkUrl() );
            customPaymentModel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startFullscreenActivity( v.getTag() + "" );
                }
            });

            paymentWrapper.addView( customPaymentModel );
        } else {

            CustomPaymentModel customPaymentModel = new CustomPaymentModel( MyPaymentActivity.this );
            customPaymentModel.setPaymentOnOff( false );
            paymentWrapper.addView( customPaymentModel );
        }
    }


    private void initPointHelpDlg() {
        pointHelpDlg = new Dialog( this );
        pointHelpDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pointHelpDlg.setContentView(R.layout.dialog_pointhelp);

        pointHelpDlg.findViewById( R.id.pointhelp_close ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pointHelpDlg.dismiss();
            }
        });
    }


    private void showPaymentHelpDlg( String type, String paymentReviewParam ) {
        if( !ObjectUtils.isEmpty( type ) ) {
            TextView pointHelpLabel = (TextView)pointHelpDlg.findViewById( R.id.pointhelp_label );
            LinearLayout pointHelpTextWrapper = (LinearLayout)pointHelpDlg.findViewById( R.id.pointhelp_text_wrapper );
            pointHelpTextWrapper.removeAllViews();

            TextView pointStartBtn = (TextView)pointHelpDlg.findViewById( R.id.point_start_btn );

            TextView helpTextView1 = new TextView( MyPaymentActivity.this );
            helpTextView1.setPadding( 0, 10, 0, 10 );
            helpTextView1.setLineSpacing( 0, 1.2f );

            TextView helpTextView2 = new TextView( MyPaymentActivity.this );
            helpTextView2.setPadding( 0, 10, 0, 10 );
            helpTextView2.setLineSpacing( 0, 1.2f );

            if( type.equals( "contract" ) ) {
                pointHelpLabel.setText( "결제 포인트" );

                helpTextView1.setText( "∙ 상품 결제시 포인트가 자동으로 적립됩니다." );
                pointHelpTextWrapper.addView( helpTextView1 );

                helpTextView2.setText( "∙ 포인트 적립에 오류가 있을 경우 아래의 버튼을 클릭하여 말씀해주세요." );
                pointHelpTextWrapper.addView( helpTextView2 );

                pointStartBtn.setText( "'웨딩의 여신' 플러스 친구와 대화하기" );
                pointStartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startFullscreenActivity( "http://pf.kakao.com/_exjFpl" );
                    }
                });

            } else if( type.equals( "review" ) ) {
                pointHelpLabel.setText( "후기 포인트" );

                helpTextView1.setText( "∙ 지금 바로 아래의 버튼을 눌러 후기를 작성해주세요." );
                pointHelpTextWrapper.addView( helpTextView1 );

                helpTextView2.setText( "∙ 1,000P를 적립해드립니다!" );
                pointHelpTextWrapper.addView( helpTextView2 );

                pointStartBtn.setText( "후기쓰러 가기" );
                pointStartBtn.setTag( paymentReviewParam );
                pointStartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pointHelpDlg.dismiss();
                        Intent intent = new Intent( MyPaymentActivity.this, ContentEditActivity.class );
                        intent.putExtra( "paymentReviewParam", v.getTag() + "" );
                        startActivityForResult( intent, Constants.REQ_CODE_OPEN_CONTENTEDIT_ACTIVITY );
                    }
                });
            }
            pointHelpDlg.show();
        }
    }


    private void startFullscreenActivity(String url) {
        Intent intent = new Intent( this, FullscreenActivity.class );
        intent.putExtra( "URL", url );
        startActivityForResult( intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY );
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult( RESULT_OK, intent );
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
