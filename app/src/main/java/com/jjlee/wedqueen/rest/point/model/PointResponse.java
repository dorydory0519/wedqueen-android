package com.jjlee.wedqueen.rest.point.model;

/**
 * Created by esmac on 2018. 1. 31..
 */

public class PointResponse {
    private int point;

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
}
