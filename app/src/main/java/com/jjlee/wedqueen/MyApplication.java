package com.jjlee.wedqueen;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.jjlee.wedqueen.config.KakaoSdkAdapter;
import com.jjlee.wedqueen.rest.account.controller.AccountController;
import com.jjlee.wedqueen.rest.account.model.NotificationCountResponse;
import com.jjlee.wedqueen.rest.app.model.TabCategory;
import com.jjlee.wedqueen.rest.dday.model.DDayDto;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.MyCookieJar;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.kakao.auth.KakaoSDK;
import com.tsengvn.typekit.Typekit;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

//import io.airbridge.AirBridge;
import io.fabric.sdk.android.Fabric;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

//import io.userhabit.service.Userhabit;
import me.leolin.shortcutbadger.ShortcutBadger;
import okhttp3.Cookie;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.converter.gson.GsonConverterFactory;


import retrofit2.Retrofit;

/**
 * Created by JJLEE on 2016. 3. 31..
 */
public class MyApplication extends Application {

    private static final Object lock = new Object();

    private static MyApplication instance;
    private static Retrofit retrofit;
    private static Retrofit logRetrofit;
    private static Activity currentActivity;

    private int badgeCount = 0;
    private DDayDto dDayDto;
    private Calendar dDayDate;
    private boolean isLoggedIn = false;
    private boolean isLoginContentDetail = true;
    private String userId = null;
    private String userImageUrl = null;
    private String userNickName = null;
    private int userPoint = 0;
    private boolean showInviteCode = false;

    // AccountActivity에서 로그인이나 로그아웃하면 이게 false로 세팅됨.
    // 추후 MainActivity로 돌아오면 로그인/로그아웃 상태를 Sync 하면서 true로 세팅.
    // 즉,
    // MainActivity -> AccountActivity(login/logout) 해도 싱크가 맞춰지고,
    // MainActivity -> FullscreenActivity -> FullscreenActivity -> AccountActivity(login/logout)
    //                                    -> FullscreenActivity -> homeAsUp(); 해도 싱크가 맞춰짐.
    private boolean isLoginSynchronized = true;
    private boolean isLatestVersion;

    public MyApplication() {
        instance = this;
    }

    public void onCreate()
    {
        super.onCreate();

//        AirBridge.init( this, Constants.airBridgeAppName, Constants.airBridgeAppToken );
//        IgawCommon.autoSessionTracking( this );

//        Userhabit.setSessionEndTime( 10 );
//        Userhabit.start( this );

        // Custom Font 일괄적용
        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this, "NanumSquareOTFRegular.otf"))
                .addItalic(Typekit.createFromAsset(this, "NanumSquareOTFBold.otf"))
                .addBold(Typekit.createFromAsset(this, "NanumSquareOTFExtraBold.otf"));

        TwitterAuthConfig authConfig = new TwitterAuthConfig(
                getResources().getString(R.string.twitter_consumer_key),
                getResources().getString(R.string.twitter_consumer_secret));
        Fabric.with(this, new Crashlytics(), new Twitter(authConfig));
        logUser("Anonymous");

        KakaoSDK.init(new KakaoSdkAdapter());
        FacebookSdk.sdkInitialize(this);

        increaseLaunchCount();

//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        Request request = original.newBuilder()
                                .header("User-Agent", "android")
                                .header("deviceType", "android")
                                .method(original.method(), original.body())
                                .build();

                        return chain.proceed(request);
                    }
                })

                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)

//                .addInterceptor(logging)
                .cookieJar(new MyCookieJar())
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        logRetrofit = new Retrofit.Builder()
                .baseUrl(Constants.LOG_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

    }

    public void logUser(String userId) {
        Crashlytics.setUserIdentifier(userId);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    /**
     *
     * KAKAO SDK 요구사항
     * Activity가 올라올때마다 Activity의 onCreate에서
     * getGlobalAppicationContext.setCurrentActivity를 호출해줘야한다.
     *
     * 내려갈 때마다 onDestroy에서 null로 꼭 바꿔줘야 할듯? ( MEMORYLEAK 우려 )
     *
     */
    public static Activity getCurrentActivity() {
        return currentActivity;
    }

    public static void setCurrentActivity(Activity currentActivity) {
        MyApplication.currentActivity = currentActivity;
    }

    public static MyApplication getGlobalApplicationContext() {
        return instance;
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }

    public static Retrofit getLogRetrofit() {
        return logRetrofit;
    }

    public int getBadgeCount() {
        return badgeCount;
    }

    public void setBadgeCount(int badgeCount) {
        this.badgeCount = badgeCount;
        ShortcutBadger.applyCount(this, badgeCount);
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setIsLoggedIn(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public boolean isLoginContentDetail() {
        Prefs prefs = new Prefs( this );
        return prefs.isLoginContentDetail();
    }

    public void setLoginContentDetail(boolean loginContentDetail) {
        Prefs prefs = new Prefs( this );
        prefs.setLoginContentDetail( loginContentDetail );
        isLoginContentDetail = loginContentDetail;
    }

    // DetailContent를 Load가능한지 login 여부 및 loginContentDetail 변수를 체크하는 메서드
    public boolean canLoadDetailContent() {

        boolean canLoad = false;

        // 로그인되어있거나 isLoginContentDetail이 false로 설정되어있는 상태
        if( isLoggedIn || !isLoginContentDetail() ) {
            canLoad = true;
        }

        return canLoad;
    }

    public boolean isShowInviteCode() {
        return showInviteCode;
    }

    public void setShowInviteCode(boolean showInviteCode) {
        this.showInviteCode = showInviteCode;
    }

    public boolean isLoginSynchronized() {
        return isLoginSynchronized;
    }

    public void setIsLoginSynchronized(boolean isLoginSynchronized) {
        this.isLoginSynchronized = isLoginSynchronized;
    }

    public String getVersionName()
    {
        try
        {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        }
        catch(PackageManager.NameNotFoundException e)
        {
            return null;
        }
    }

    public boolean isLatestVersion() {
        return isLatestVersion;
    }

    public void setIsLatestVersion(boolean isLatestVersion) {
        this.isLatestVersion = isLatestVersion;
    }

    public void increaseLaunchCount() {
        Prefs prefs = new Prefs(this);
        int launchCount = prefs.getLaunchCount();
        prefs.setLaunchCount(launchCount+1);
    }

    public String getUserId() {

        if( ObjectUtils.isEmpty( userId ) ) {
            Prefs prefs = new Prefs( this );
            userId = prefs.getUserId();
        }

        return userId;
    }

    public void setUserId(String userId) {

        Prefs prefs = new Prefs( this );
        prefs.setUserId( userId );

        this.userId = userId;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }

    public String getUserNickName() {

        if( ObjectUtils.isEmpty( userNickName ) ) {
            Prefs prefs = new Prefs( this );
            userNickName = prefs.getUserNickname();
        }

        return userNickName;
    }

    public void setUserNickName(String userNickName) {

        Prefs prefs = new Prefs( this );
        prefs.setUserNickname( userNickName );

        this.userNickName = userNickName;
    }
    public int getUserPoint() {

        Prefs prefs = new Prefs( this );
        userPoint = prefs.getUserPoint();

        return userPoint;
    }
    public void setUserPoint(int userPoint) {
        Prefs prefs = new Prefs( this );
        prefs.setUserPoint( userPoint );
        this.userPoint = userPoint;
    }

    public void setCookieData(List<Cookie> cookieData ) {
        Prefs prefs = new Prefs( this );
        prefs.setCookieData( cookieData );
    }
    public List<Cookie> getCookieData() {
        Prefs prefs = new Prefs( this );
        List<Cookie> cookieData = prefs.getCookieData();
        return cookieData;
    }

//    public void setTabCategories( HashMap<String, ArrayList<TabCategory>> tabCategories ) {
//        Prefs prefs = new Prefs(this);
//        prefs.setCurrentTabCategories( tabCategories );
//    }

//    public HashMap<String, ArrayList<TabCategory>> getTabCategories() {
//        Prefs prefs = new Prefs(this);
//        HashMap<String, ArrayList<TabCategory>> tabCategories = prefs.getCurrentTabCategories();
//        return tabCategories;
//    }

    public static boolean isScreenOn(Context context) {
        if (android.os.Build.VERSION.SDK_INT >= 20) {
            return ((PowerManager)context.getSystemService(Context.POWER_SERVICE)).isInteractive();
        } else {
            return ((PowerManager)context.getSystemService(Context.POWER_SERVICE)).isScreenOn();
        }

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public DDayDto getdDayDto() {
        return dDayDto;
    }

    public void setdDayDto(DDayDto dDayDto) {
        this.dDayDto = dDayDto;
    }

    public Calendar getdDayDate() {
        return dDayDate;
    }

    public void setdDayDate(Calendar dDayDate) {
        this.dDayDate = dDayDate;
    }

    public static void requestCountDownSingleNoti(String notiId) {
        if(notiId != null) {
            AccountController accountController = new AccountController();
            Call<NotificationCountResponse> call = accountController.countDownSingleNotification(notiId);
            call.enqueue(new Callback<NotificationCountResponse>() {
                @Override
                public void onResponse(Call<NotificationCountResponse> call, retrofit2.Response<NotificationCountResponse> response) {
                    MyApplication app = MyApplication.getGlobalApplicationContext();
                    int beforeBadgeCount = app.getBadgeCount();
                    if(response.body().getNotificationCount() == null) {
                        app.setBadgeCount(beforeBadgeCount - 1);
                    } else {
                        app.setBadgeCount(response.body().getNotificationCount());
                    }

                }

                @Override
                public void onFailure(Call<NotificationCountResponse> call, Throwable t) {
                    MyApplication app = MyApplication.getGlobalApplicationContext();
                    int beforeBadgeCount = app.getBadgeCount();
                    if(beforeBadgeCount > 0) {
                        app.setBadgeCount(beforeBadgeCount - 1);
                    }
                }
            });
        }
    }
}
