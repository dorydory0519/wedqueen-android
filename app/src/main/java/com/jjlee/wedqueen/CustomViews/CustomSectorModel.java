package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;


public class CustomSectorModel extends LinearLayout {

    private TextView sectorTitle;
    private CustomHorizontalScrollView sectorScrollview;
    private LinearLayout sectorWrapper;
    private ImageView bannerImage;

    private String type;
    private int bannerImgSize;
    private long sectorItemId;

    public CustomSectorModel(Context context) {
        super(context);
        initView();
    }

    public CustomSectorModel(Context context, int bannerImgSize) {
        super(context);
        this.bannerImgSize = bannerImgSize;
        initView();
    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_sector, this, false);
        addView(v);

        sectorTitle = (TextView)findViewById( R.id.sector_title );
        sectorScrollview = (CustomHorizontalScrollView)findViewById( R.id.sector_scrollview );
        sectorWrapper = (LinearLayout)findViewById( R.id.sector_wrapper );
        bannerImage = (ImageView)findViewById( R.id.banner_image );

    }

    public void setSectorTitle( String resString ) {
        if( ObjectUtils.isEmpty( resString ) ) {
            sectorTitle.setVisibility( View.GONE );
        } else {
            sectorTitle.setText( resString );
        }
    }

    public void setType(String type) {
        this.type = type;
    }

    public LinearLayout getSectorWrapper() {
        return sectorWrapper;
    }

    public CustomHorizontalScrollView getSectorScrollview() {
        return sectorScrollview;
    }

    public void setSectorScrollview(CustomHorizontalScrollView sectorScrollview) {
        this.sectorScrollview = sectorScrollview;
    }

    public void setSectorWrapper(LinearLayout sectorWrapper) {
        this.sectorWrapper = sectorWrapper;
    }

    public ImageView getBannerImage() {
        return bannerImage;
    }

    public void setBannerImage(ImageView bannerImage) {
        this.bannerImage = bannerImage;
    }

    public long getSectorItemId() {
        return sectorItemId;
    }

    public void setSectorItemId(long sectorItemId) {
        this.sectorItemId = sectorItemId;
    }
}
