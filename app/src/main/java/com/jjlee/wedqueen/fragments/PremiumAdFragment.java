package com.jjlee.wedqueen.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.premium.model.PremiumAd;
import com.jjlee.wedqueen.utils.ObjectUtils;

public class PremiumAdFragment extends Fragment {

    private String imageUrl;
    private String title;
    private String auxName;

    private ImageView adImg;
    private TextView adTitle;
    private TextView adAux;

    public static PremiumAdFragment newInstance( PremiumAd premiumAd ) {
        PremiumAdFragment fragment = new PremiumAdFragment();
        Bundle args = new Bundle();
        if( !ObjectUtils.isEmpty( premiumAd ) ) {
            args.putString( "imageUrl", premiumAd.getImage() );
            args.putString( "title", premiumAd.getTitle() );
            args.putString( "auxName", premiumAd.getAuxName() );
        }
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        imageUrl = args.getString( "imageUrl" );
        title = args.getString( "title" );
        auxName = args.getString( "auxName" );
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_premium_ad, container, false);

        adImg = (ImageView)view.findViewById( R.id.ad_img );
        adTitle = (TextView)view.findViewById( R.id.ad_title );
        adAux = (TextView)view.findViewById( R.id.ad_aux );

        if( !ObjectUtils.isEmpty( imageUrl ) ) {
            Glide.with( getContext() ).load( imageUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( adImg );
        }
        adTitle.setText( title );
        adAux.setText( auxName );

        return view;
    }
}
