package com.jjlee.wedqueen.rest.app.model;

import java.io.Serializable;

/**
 * Created by JJLEE on 2016. 4. 23..
 */
public class TabCategory implements Serializable {
    private Long id;
    private String categoryNameKor;
    private String categoryName;
    private String tabType;
    private Long position;
    private boolean isTipOpen;
    private String iconUrl;

    public TabCategory( com.jjlee.wedqueen.rest.tabCategory.model.TabCategory tabCategory ) {
        this.id = (long)tabCategory.getId();
        this.categoryNameKor = tabCategory.getCategoryNameKor();
        this.categoryName = tabCategory.getCategoryName();
        this.tabType = tabCategory.getTabType();
        this.position = Long.parseLong( tabCategory.getPosition() );
        this.isTipOpen = true;
        this.iconUrl = tabCategory.getIconUrl();
    }

    public TabCategory( Long id, String categoryNameKor, String categoryName, String tabType, Long position, boolean isTipOpen, String iconUrl ) {
        this.id = id;
        this.categoryNameKor = categoryNameKor;
        this.categoryName = categoryName;
        this.tabType = tabType;
        this.position = position;
        this.isTipOpen = isTipOpen;
        this.iconUrl = iconUrl;
    }

    public TabCategory(Long id, String categoryNameKor, String categoryName, String tabType, Long position, String iconUrl) {
        this( id, categoryNameKor, categoryName, tabType, position, true, iconUrl );
    }

    public TabCategory(Long id, String categoryNameKor, String categoryName, String tabType, Long position) {
        this( id, categoryNameKor, categoryName, tabType, position, true, "" );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryNameKor() {
        return categoryNameKor;
    }

    public void setCategoryNameKor(String categoryNameKor) {
        this.categoryNameKor = categoryNameKor;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTabType() {
        return tabType;
    }

    public void setTabType(String tabType) {
        this.tabType = tabType;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public boolean isTipOpen() {
        return isTipOpen;
    }

    public void setTipOpen(boolean tipOpen) {
        isTipOpen = tipOpen;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
