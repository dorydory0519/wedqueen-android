package com.jjlee.wedqueen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.CustomViews.CustomDiaryModel;
import com.jjlee.wedqueen.rest.content.model.Content;
import com.jjlee.wedqueen.rest.subscription.controller.SubscriptionController;
import com.jjlee.wedqueen.rest.subscription.model.SubscriptionResponse;
import com.jjlee.wedqueen.rest.user.controller.UserController;
import com.jjlee.wedqueen.rest.user.model.UserResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by esmac on 2017. 10. 19..
 */

public class OtherUserDiaryActivity extends AppCompatActivity {

    private RequestManager mGlideRequestManager;

    private ImageView userImg;
    private TextView userNickname;
    private TextView userDday;
    private LinearLayout followLink;
    private ImageView followImg;
    private TextView followerCnt;

    // 추천유저에서 선택한 userId
    private int targetUserId;
    private boolean isFollow = false;
    private SubscriptionController subscriptionController;

    private LinearLayout diaryWrapper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otheruser_diary);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "other_userinfo" ) );

        mGlideRequestManager = Glide.with( this );

        targetUserId = getIntent().getIntExtra( "userId", -1 );

        userImg = (ImageView)findViewById(R.id.user_img);
        userNickname = (TextView)findViewById(R.id.user_nickname);
        userDday = (TextView)findViewById(R.id.user_dday);
        followLink = (LinearLayout) findViewById( R.id.follow_link );
        followImg = (ImageView) findViewById( R.id.follow_img );
        followerCnt = (TextView) findViewById( R.id.follower_cnt );

        diaryWrapper = (LinearLayout)findViewById(R.id.diary_wrapper);

        findViewById(R.id.close_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics( displayMetrics );
        userNickname.setMaxWidth( displayMetrics.widthPixels - SizeConverter.dpToPx( 220 ) );

        UserController userController = new UserController();
        Call<UserResponse> userCall = userController.getUser( targetUserId );
        userCall.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    UserResponse userResponse = response.body();

                    if( !ObjectUtils.isEmpty( OtherUserDiaryActivity.this ) ) {
                        mGlideRequestManager.load( userResponse.getUser().getImageUrl() ).bitmapTransform(new CropCircleTransformation( OtherUserDiaryActivity.this )).placeholder( R.drawable.default_user_icon ).into( userImg );
                        userNickname.setText( userResponse.getUser().getNickname() );

                        SpannableString spannableDday = new SpannableString( userResponse.getDdayToString() );
                        spannableDday.setSpan(new UnderlineSpan(), 0, spannableDday.length(), 0);
                        userDday.setText( spannableDday );

                        followerCnt.setText( "팔로워 " + userResponse.getFollowerCount() + "명" );

                        if( userResponse.getHasSubscription() ) {
                            followLink.setBackgroundResource( R.drawable.green_rounding );
                            followImg.setImageResource( R.drawable.follow_on );
                            isFollow = true;
                        } else {
                            followLink.setBackgroundResource( R.drawable.red_rounding );
                            followImg.setImageResource( R.drawable.follow_off );
                            isFollow = false;
                        }

                        List<Content> contents = userResponse.getContents();

                        for( Content content : contents ) {

                            CustomDiaryModel customDiaryModel = new CustomDiaryModel( OtherUserDiaryActivity.this, mGlideRequestManager );
                            customDiaryModel.setContentId( content.getId() );
                            customDiaryModel.setDiaryDday( content.getDdayToString() );
                            customDiaryModel.setDiaryState( "" );
                            customDiaryModel.setDiaryCategory( content.getTypeInKorean() );
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT );
                            layoutParams.setMargins( 80, 0, 0, 0 );
                            customDiaryModel.setLayoutParams( layoutParams );
                            if( !ObjectUtils.isEmpty( content.getImageUrl() ) ) {
                                customDiaryModel.setDiaryImg( content.getImageUrl() );
                            } else {
                                customDiaryModel.setDiaryImg( "http://jcleeco.godohosting.com/content/8170/1500893496179.jpg" );
                            }
                            customDiaryModel.setDiaryViewcount( content.getViewCount().toString() );
                            customDiaryModel.setDiaryTitle( content.getTitle() );
                            customDiaryModel.setTag( content.getUrl() );
                            customDiaryModel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startFullscreenActivity( "" + v.getTag() );
                                }
                            });

                            diaryWrapper.addView( customDiaryModel );
                        }

                        if( contents.size() < 1 ) {
                            TextView textView = new TextView( OtherUserDiaryActivity.this );
                            textView.setText( "작성하신 리얼웨딩 컨텐츠가 없으시네요 " );
                            textView.setTextSize( TypedValue.COMPLEX_UNIT_DIP, 14 );
                            textView.setGravity( Gravity.CENTER );
                            textView.setLayoutParams( new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, SizeConverter.dpToPx( 250 ) ) );
                            diaryWrapper.addView( textView );
                        }
                    }

                } else {
                    Log.e( "WQTEST", "response body is null" );
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.e( "WQTEST", "userCall onFailure" );
                t.printStackTrace();
            }
        });

        subscriptionController = new SubscriptionController();
        followLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
                    if( isFollow ) {
                        Call<SubscriptionResponse> subscriptionCall = subscriptionController.delSubscription( "user", targetUserId );
                        subscriptionCall.enqueue(new Callback<SubscriptionResponse>() {
                            @Override
                            public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                                Log.e( "WQTEST", "subscriptionCall onResponse" );
                                if( !ObjectUtils.isEmpty( response.body() ) ) {
                                    SubscriptionResponse subscriptionResponse = response.body();
                                    if( subscriptionResponse.getCode() == 200 ) {
                                        Toast.makeText( OtherUserDiaryActivity.this, "내 구독 리스트에서 해당 유저를 삭제하였습니다!", Toast.LENGTH_SHORT).show();
                                        followLink.setBackgroundResource( R.drawable.red_rounding );
                                        followImg.setImageResource( R.drawable.follow_off );
                                        isFollow = false;
                                    } else {
                                        Toast.makeText( OtherUserDiaryActivity.this, subscriptionResponse.getMsg(), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Log.e( "WQTEST", "response body is null" );
                                }
                            }

                            @Override
                            public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                                Log.e( "WQTEST", "subscriptionCall onFailure" );
                                t.printStackTrace();
                            }
                        });
                    } else {
                        Call<SubscriptionResponse> subscriptionCall = subscriptionController.postSubscription( "user", targetUserId );
                        subscriptionCall.enqueue(new Callback<SubscriptionResponse>() {
                            @Override
                            public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                                Log.e( "WQTEST", "subscriptionCall onResponse" );
                                if( !ObjectUtils.isEmpty( response.body() ) ) {
                                    SubscriptionResponse subscriptionResponse = response.body();
                                    if( subscriptionResponse.getCode() == 200 ) {
                                        Toast.makeText( OtherUserDiaryActivity.this, "내 구독 리스트에 해당 유저를 추가하였습니다!", Toast.LENGTH_SHORT).show();
                                        followLink.setBackgroundResource( R.drawable.green_rounding );
                                        followImg.setImageResource( R.drawable.follow_on );
                                        isFollow = true;
                                    } else {
                                        Toast.makeText( OtherUserDiaryActivity.this, subscriptionResponse.getMsg(), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Log.e( "WQTEST", "response body is null" );
                                }
                            }

                            @Override
                            public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                                Log.e( "WQTEST", "subscriptionCall onFailure" );
                                t.printStackTrace();
                            }
                        });
                    }
                } else {
                    Intent intent = new Intent( OtherUserDiaryActivity.this, AccountActivity.class );
                    intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
                    startActivity( intent );
                }
            }
        });

    }

    private void startFullscreenActivity(String url) {
        if( MyApplication.getGlobalApplicationContext().canLoadDetailContent() ) {
            Intent intent = new Intent( this, FullscreenActivity.class );
            intent.putExtra( "URL", url );
            startActivityForResult( intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY );
        } else {
            Intent intent = new Intent( this, AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            startActivity( intent );
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
