package com.jjlee.wedqueen.rest.premium.controller;

import com.jjlee.wedqueen.rest.premium.model.PremiumAd;
import com.jjlee.wedqueen.rest.premium.model.PremiumAdLog;
import com.jjlee.wedqueen.rest.premium.model.PremiumAdLogResponse;
import com.jjlee.wedqueen.rest.premium.service.PremiumLogService;
import com.jjlee.wedqueen.rest.premium.service.PremiumService;
import com.jjlee.wedqueen.support.RetrofitController;
import com.jjlee.wedqueen.utils.MapUtil;

import java.util.List;
import java.util.Map;

import retrofit2.Call;

public class PremiumController extends RetrofitController {
    private PremiumService premiumService;
    private PremiumLogService premiumLogService;

    public PremiumController() {
        super();
        premiumService = RETROFIT.create( PremiumService.class );
        premiumLogService = LOGRETROFIT.create( PremiumLogService.class );
    }

    public Call<PremiumAd> getPremiumAd() {
        return premiumService.getPremiumAd();
    }

    public Call<List<PremiumAd>> getPremiumAds() {
        return premiumService.getPremiumAds();
    }

    public Call<PremiumAdLogResponse> postPremiumAdLog( PremiumAdLog premiumAdLog ) {
        Map<String, String> map = MapUtil.getMapFromObject( premiumAdLog );
        return premiumLogService.postPremiumAdLog( map );
    }
}
