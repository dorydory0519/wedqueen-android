package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.HorizontalScrollView;

import com.jjlee.wedqueen.utils.ObjectUtils;

/**
 * Created by esmac on 2017. 9. 5..
 */

public class CustomHorizontalScrollView extends HorizontalScrollView {

    OnBottomReachedListener mListener;

    public CustomHorizontalScrollView(Context context) {
        super(context);
    }

    public CustomHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setSmoothScrollingEnabled(boolean smoothScrollingEnabled) {
        super.setSmoothScrollingEnabled(smoothScrollingEnabled);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {

        View view = (View) getChildAt(getChildCount()-1);
        int diff = view.getRight()-(getWidth()+getScrollX());

        if( !ObjectUtils.isEmpty( mListener ) && diff == 0 ) {
            mListener.onBottomReached();
        }

        super.onScrollChanged(l, t, oldl, oldt);
    }

    public OnBottomReachedListener getOnBottomReachedListener() {
        return mListener;
    }

    public void setOnBottomReachedListener(
            OnBottomReachedListener onBottomReachedListener) {
        mListener = onBottomReachedListener;
    }

    public interface OnBottomReachedListener{
        public void onBottomReached();
    }
}
