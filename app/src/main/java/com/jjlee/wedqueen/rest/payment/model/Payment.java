package com.jjlee.wedqueen.rest.payment.model;

/**
 * Created by esmac on 2018. 2. 19..
 */

public class Payment {
    private int id;
    private String companyThumbnailUrl;
    private String companyName;
    private String productName;
    private String amount;
    private String linkUrl;
    private String status;
    private String statusEng;
    private String statusColorCode;
    private String creationTime;
    private String updationTime;

    private boolean contract;
    private boolean review;

    private String contractPoint;
    private String reviewPoint;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompanyThumbnailUrl() {
        return companyThumbnailUrl;
    }

    public void setCompanyThumbnailUrl(String companyThumbnailUrl) {
        this.companyThumbnailUrl = companyThumbnailUrl;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusEng() {
        return statusEng;
    }

    public void setStatusEng(String statusEng) {
        this.statusEng = statusEng;
    }

    public String getStatusColorCode() {
        return statusColorCode;
    }

    public void setStatusColorCode(String statusColorCode) {
        this.statusColorCode = statusColorCode;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getUpdationTime() {
        return updationTime;
    }

    public void setUpdationTime(String updationTime) {
        this.updationTime = updationTime;
    }

    public boolean isContract() {
        return contract;
    }

    public void setContract(boolean contract) {
        this.contract = contract;
    }

    public boolean isReview() {
        return review;
    }

    public void setReview(boolean review) {
        this.review = review;
    }

    public String getContractPoint() {
        return contractPoint;
    }

    public void setContractPoint(String contractPoint) {
        this.contractPoint = contractPoint;
    }

    public String getReviewPoint() {
        return reviewPoint;
    }

    public void setReviewPoint(String reviewPoint) {
        this.reviewPoint = reviewPoint;
    }
}
