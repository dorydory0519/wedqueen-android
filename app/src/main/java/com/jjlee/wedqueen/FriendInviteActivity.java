package com.jjlee.wedqueen;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.rest.invitation.controller.InvitationController;
import com.jjlee.wedqueen.rest.invitation.model.InvitationResponse;
import com.jjlee.wedqueen.rest.section.controller.SectionController;
import com.jjlee.wedqueen.rest.section.model.Section;
import com.jjlee.wedqueen.rest.section.model.SectionResponse;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by esmac on 2018. 3. 19..
 */

public class FriendInviteActivity extends AppCompatActivity {

    private TextView myInviteCode;
    private TextView myInviteCount;
    private TextView myInvitePoint;

    private ImageView inviteGuideImg;
    private TextView inviteGuideMent;
    private TextView invitePointGuideMent;
    private ImageView inviteTipImg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_invite);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "invite_friend" ) );

        myInviteCode = (TextView) findViewById( R.id.my_invite_code );
        myInviteCount = (TextView) findViewById( R.id.my_invite_count );
        myInvitePoint = (TextView) findViewById( R.id.my_invite_point );

        inviteGuideImg = (ImageView) findViewById( R.id.invite_guide_img );
        inviteGuideMent = (TextView) findViewById( R.id.invite_guide_ment );
        invitePointGuideMent = (TextView) findViewById( R.id.invite_point_guide_ment );
        inviteTipImg = (ImageView) findViewById( R.id.invite_tip_img );

        findViewById( R.id.back_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        InvitationController invitationController = new InvitationController();
        Call<InvitationResponse> invitationCall = invitationController.getMyInvitationInfo();
        invitationCall.enqueue(new Callback<InvitationResponse>() {
            @Override
            public void onResponse(Call<InvitationResponse> call, Response<InvitationResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    InvitationResponse invitationResponse = response.body();
                    String strCode = invitationResponse.getCode();
                    if( !ObjectUtils.isEmpty( strCode ) ) {
                        myInviteCode.setText( strCode );
                        findViewById( R.id.copy_invite_code ).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ClipboardManager clipboardManager = (ClipboardManager) getSystemService( CLIPBOARD_SERVICE );
                                ClipData clip = ClipData.newPlainText( "inviteCode", myInviteCode.getText() );
                                clipboardManager.setPrimaryClip( clip );

                                Toast.makeText(FriendInviteActivity.this, "초대코드가 복사되었습니다 ( " + myInviteCode.getText() + " )", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    myInviteCount.setText( invitationResponse.getInvitationCount() + "명" );
                    myInvitePoint.setText( invitationResponse.getInvitePoint() + "P" );

                }
            }

            @Override
            public void onFailure(Call<InvitationResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });

        SectionController sectionController = new SectionController();
        Call<SectionResponse> sectionCall = sectionController.getSections( "point" );
        sectionCall.enqueue(new Callback<SectionResponse>() {
            @Override
            public void onResponse(Call<SectionResponse> call, Response<SectionResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    SectionResponse sectionResponse = response.body();

                    if( !ObjectUtils.isEmpty( sectionResponse.getSection() ) && sectionResponse.getSection().size() > 0 ) {
                        List<Section> sections = sectionResponse.getSection();

                        for( Section section : sections ) {

                            String sectionType = section.getType();
                            int sectionPosition = section.getPosition();
                            String sectionName = section.getName();

                            if( ObjectUtils.isEmpty( sectionType ) || ObjectUtils.isEmpty( sectionPosition ) ) {
                                continue;
                            }

                            if( !ObjectUtils.isEmpty( FriendInviteActivity.this ) ) {
                                if( sectionType.equals( "image" ) && section.getPosition() == 0 ) {
                                    Glide.with( FriendInviteActivity.this ).load( sectionName ).placeholder( R.color.colorWhite ).into( inviteGuideImg );
                                } else if( sectionType.equals( "text" ) && section.getPosition() == 1 ) {
                                    sectionName = sectionName.replaceAll( "\\\\n", "\\\n" );
                                    inviteGuideMent.setText( sectionName );
                                } else if( sectionType.equals( "text" ) && section.getPosition() == 2 ) {
                                    sectionName = sectionName.replaceAll( "\\\\n", "\\\n" );
                                    invitePointGuideMent.setText( sectionName );
                                } else if( sectionType.equals( "image" ) && section.getPosition() == 3 ) {
                                    Glide.with( FriendInviteActivity.this ).load( sectionName ).placeholder( R.color.colorWhite ).into( inviteTipImg );
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SectionResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

}
