package com.jjlee.wedqueen.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Layout;
import android.text.ParcelableSpan;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.AlignmentSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.ContentEditActivity;
import com.jjlee.wedqueen.CustomViews.CustomColorPickerModel;
import com.jjlee.wedqueen.CustomViews.CustomContentImageWrapper;
import com.jjlee.wedqueen.CustomViews.CustomTextStyleEditModel;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.content.model.Content;
import com.jjlee.wedqueen.rest.content.model.ContentForFragment;
import com.jjlee.wedqueen.rest.content.model.ContentFragment;
import com.jjlee.wedqueen.rest.content.model.PostContent;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.BitmapUtil;
import com.jjlee.wedqueen.utils.ExifUtils;
import com.jjlee.wedqueen.utils.KeyboardUtil;
import com.jjlee.wedqueen.utils.ObjectUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditMainFragment extends Fragment {

    private TextView editGuideLabel;

    private final int TEXTSTYLE_COLORIDX = 0;
    private final int TEXTSTYLE_BOLDIDX = 1;
    private final int TEXTSTYLE_UNDERLINEIDX = 2;
    private final int TEXTSTYLE_SIZEIDX = 3;

    private final int DEFAULT_TEXTSIZE = 15;
    private final String DEFAULT_TEXTCOLOR = "black";

    // 이모지 String 임시 변환 문구
    private final String DEFAULT_WEDQUEEN_EMOJI = "DEFAULT_WEDQUEEN_EMOJI";

    private final int CONTENTIMG_HEIGHT = 500;
    private final int CHECKCONTENT_TEXT_COUNT = 200;
    private int checkedTextDiameter = 0;

    private InputMethodManager inputMethodManager;

    private LinearLayout layoutTextstyle;
    private boolean isTextStyleOn = false;
    private ImageView closeTextstyleLayout;

    private List<CustomTextStyleEditModel> textStyleEditModels = new ArrayList<CustomTextStyleEditModel>();
    private List<CustomColorPickerModel> textColorModels = new ArrayList<CustomColorPickerModel>();
    private List<CustomTextStyleEditModel> textSizeModels = new ArrayList<CustomTextStyleEditModel>();

    private String selectedTextColor = DEFAULT_TEXTCOLOR;
    private String selectedTextStyleMode;
    private int selectedTextSize = DEFAULT_TEXTSIZE;
    private boolean editTextIsBold = false;
    private boolean editTextIsUnderline = false;

    private LinearLayout layoutColorPicker;
    private LinearLayout layoutSizePicker;

    private String selectedThumbnail;
    private ImageView imgSelector;
    private TextView imgLabel;
    private EditText editTitle;

    private LinearLayout editorWrapper;
    private List<EditText> editContents = new ArrayList<EditText>();
//    private EditText editContent;
    private List<Bitmap> bitmaps = new ArrayList<Bitmap>();
    private int addedImgIdx = 0;
    private boolean isAlignCenter = false;

    private ImageView targetImageView;

    private ImageView alignText;

    private LinearLayout layoutEmpty;
    private LinearLayout layoutTextToolbar;
    private LinearLayout layoutImageToolbar;
    private ImageView btnRemoveImg;

    private TextView saveBtn;

    private boolean isLoadingContent = false;
    private Dialog textCountCheckDlg;
    private TextView textCountCheckDlgOk;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_editmain, container, false);

        initTextCountCheckDlg();

        // 닫기 버튼 Click Action
        (view.findViewById(R.id.clear_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ContentEditActivity)getActivity()).showCloseDlg();
            }
        });

        // 임시저장 버튼 Click Action
        saveBtn = (TextView)view.findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 작성한 내용들 임시저장 Action
                ((ContentEditActivity)getActivity()).postTempContent();
            }
        });

        // 다음 버튼 Click Action
        (view.findViewById(R.id.next_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Move Next Fragment
                ((ContentEditActivity)getActivity()).moveFragment( 1 );
            }
        });


        imgSelector = (ImageView)view.findViewById(R.id.img_selector);
        imgSelector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        && ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                            Constants.REQ_EXTERNAL_STORAGE_PERMISSION);
                } else {
                    openFileChooser( Constants.REQ_CODE_OPEN_THUMBNAIL_IMG, Intent.EXTRA_LOCAL_ONLY );
                }
            }
        });
        imgLabel = (TextView)view.findViewById(R.id.img_label);
        imgLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        && ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                            Constants.REQ_EXTERNAL_STORAGE_PERMISSION);
                } else {
                    openFileChooser( Constants.REQ_CODE_OPEN_THUMBNAIL_IMG, Intent.EXTRA_LOCAL_ONLY );
                }
            }
        });


        editTitle = (EditText)view.findViewById(R.id.edit_title);
        editTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                ((ContentEditActivity)getActivity()).setSaved( false );
            }
        });


        layoutEmpty = (LinearLayout) view.findViewById(R.id.layout_empty);
        layoutEmpty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                removeImageFocusing( true );

                // 빈 영역을 클릭하면
                // wrapper의 가장 아래에 이미지(LinearLayout으로 감싸져있음)가 있으면 이미지 아래에 새로운 EditText를 추가
                if( editorWrapper.getChildAt( editorWrapper.getChildCount() - 1 ) instanceof LinearLayout ) {
                    addEditContent( false );
                }

                // 가장 아래의 EditText로 포커싱 이동
                if( editContents.size() > 0 ) {
                    editContents.get( editContents.size() - 1 ).requestFocus();
                    KeyboardUtil.showKeyBoard( getActivity(), editContents.get( editContents.size() - 1 ) );
                }
            }
        });
        layoutTextToolbar = (LinearLayout) view.findViewById(R.id.layout_texttoolbar);
        layoutImageToolbar = (LinearLayout) view.findViewById(R.id.layout_imagetoolbar);
        btnRemoveImg = (ImageView) view.findViewById(R.id.btn_remove_img);
        btnRemoveImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for( int i = 0; i < editorWrapper.getChildCount(); i++ ) {
                    if( editorWrapper.getChildAt( i ) instanceof CustomContentImageWrapper ) {
                        CustomContentImageWrapper contentImageWrapper = (CustomContentImageWrapper) editorWrapper.getChildAt( i );

                        if( contentImageWrapper.getSelectedBorder().getVisibility() == View.VISIBLE ) {
                            editorWrapper.removeView( contentImageWrapper );
                            layoutImageToolbar.setVisibility( View.GONE );
                            layoutTextToolbar.setVisibility( View.VISIBLE );
                            break;
                        }
                    }
                }
            }
        });


        layoutTextstyle = (LinearLayout)view.findViewById(R.id.layout_textstyle);
        layoutTextstyle.setVisibility(View.GONE);
        closeTextstyleLayout = (ImageView)view.findViewById(R.id.close_textstyle_layout);
        closeTextstyleLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( isTextStyleOn ) {
                    layoutTextstyle.setVisibility(View.GONE);
                    editContents.get( editContents.size() - 1 ).requestFocus();
                    KeyboardUtil.showKeyBoard( getActivity(), editContents.get( editContents.size() - 1 ) );
                } else {
                    layoutTextstyle.setVisibility(View.VISIBLE);
                }
                isTextStyleOn = !isTextStyleOn;
            }
        });


        inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);


        layoutColorPicker = (LinearLayout)view.findViewById(R.id.layout_colorpicker);
        for( int i = 0; i < layoutColorPicker.getChildCount(); i++ ) {
            textColorModels.add( (CustomColorPickerModel)layoutColorPicker.getChildAt( i ) );
        }
        for( CustomColorPickerModel textColorModel : textColorModels ) {
            textColorModel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedTextColor = ((CustomColorPickerModel)v).getColorCode();

                    for( EditText editContent : editContents ) {
                        int startIdx = editContent.getSelectionStart();
                        int endIdx = editContent.getSelectionEnd();
                        // 범위 선택된 부분이 있으면
                        if( startIdx != endIdx ) {
                            ForegroundColorSpan[] foregroundColorSpens = editContent.getText().getSpans( startIdx, endIdx, ForegroundColorSpan.class );
                            for( ForegroundColorSpan foregroundColorSpan : foregroundColorSpens ) {
                                removeSpans( foregroundColorSpan, editContent.getText(), startIdx, endIdx );
                            }
                            if( !selectedTextColor.equals( DEFAULT_TEXTCOLOR ) ) {
                                editContent.getText().setSpan( new ForegroundColorSpan( BitmapUtil.getColorCodeFromString( getContext(), selectedTextColor ) ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                            }
                        }
                    }

                    setTextColorModelClear();
                    v.setSelected( true );
                    textStyleEditModels.get( TEXTSTYLE_COLORIDX ).setPresentColor( selectedTextColor );
                }
            });
        }


        layoutSizePicker = (LinearLayout)view.findViewById(R.id.layout_sizepicker);
        for( int i = 0; i < layoutSizePicker.getChildCount(); i++ ) {
            textSizeModels.add( (CustomTextStyleEditModel) layoutSizePicker.getChildAt( i ) );
        }
        for( CustomTextStyleEditModel textSizeModel : textSizeModels ) {
            textSizeModel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedTextSize = ((CustomTextStyleEditModel)v).getTextSize();

                    for( EditText editContent : editContents ) {
                        int startIdx = editContent.getSelectionStart();
                        int endIdx = editContent.getSelectionEnd();
                        // 범위 선택된 부분이 있으면
                        if( startIdx != endIdx ) {
                            AbsoluteSizeSpan[] absoluteSizeSpens = editContent.getText().getSpans( startIdx, endIdx, AbsoluteSizeSpan.class );
                            for( AbsoluteSizeSpan absoluteSizeSpan : absoluteSizeSpens ) {
                                removeSpans( absoluteSizeSpan, editContent.getText(), startIdx, endIdx );
                            }
                            if( selectedTextSize != DEFAULT_TEXTSIZE ) {
                                editContent.getText().setSpan( new AbsoluteSizeSpan( selectedTextSize, true ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                            }
                        }
                    }

                    setTextSizeModelClear();
                    v.setSelected( true );
                }
            });
        }


        LinearLayout layoutTextstyleSelector = (LinearLayout)view.findViewById(R.id.layout_textstyle_selector);
        for( int i = 0; i < layoutTextstyleSelector.getChildCount(); i++ ) {
            textStyleEditModels.add( (CustomTextStyleEditModel)layoutTextstyleSelector.getChildAt( i ) );
        }
        // 이벤트 등록 Action
        for( CustomTextStyleEditModel customTextStyleEditModel : textStyleEditModels ) {
            customTextStyleEditModel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedTextStyleMode = ((CustomTextStyleEditModel)v).getTextStyleMode();
                    if( selectedTextStyleMode.equals( "color" ) ) {
                        textStyleEditModels.get( TEXTSTYLE_SIZEIDX ).setSelected( false );
                        layoutColorPicker.setVisibility(View.VISIBLE);
                        layoutSizePicker.setVisibility(View.GONE);
                        v.setSelected( true );
                    } else if( selectedTextStyleMode.equals( "bold" ) ) {

                        if( editTextIsBold ) {
                            v.setSelected( false );
                        } else {
                            v.setSelected( true );
                        }
                        editTextIsBold = !editTextIsBold;

                        for( EditText editContent : editContents ) {
                            int startIdx = editContent.getSelectionStart();
                            int endIdx = editContent.getSelectionEnd();
                            // 범위 선택된 부분이 있으면
                            if( startIdx != endIdx ) {
                                StyleSpan[] styleSpens = editContent.getText().getSpans( startIdx, endIdx, StyleSpan.class );
                                for( StyleSpan styleSpan : styleSpens ) {
                                    removeSpans( styleSpan, editContent.getText(), startIdx, endIdx );
                                }

                                if( editTextIsBold ) {
                                    editContent.getText().setSpan( new StyleSpan( Typeface.BOLD ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                                }
                            }
                        }

                    } else if( selectedTextStyleMode.equals( "underline" ) ) {
                        if( editTextIsUnderline ) {
                            v.setSelected( false );
                        } else {
                            v.setSelected( true );
                        }
                        editTextIsUnderline = !editTextIsUnderline;

                        for( EditText editContent : editContents ) {
                            int startIdx = editContent.getSelectionStart();
                            int endIdx = editContent.getSelectionEnd();
                            // 범위 선택된 부분이 있으면
                            if( startIdx != endIdx ) {
                                UnderlineSpan[] underlineSpens = editContent.getText().getSpans( startIdx, endIdx, UnderlineSpan.class );
                                for( UnderlineSpan underlineSpan : underlineSpens ) {
                                    removeSpans( underlineSpan, editContent.getText(), startIdx, endIdx );
//                                editContent.getText().removeSpan( underlineSpan );
                                }

                                if( editTextIsUnderline ) {
                                    editContent.getText().setSpan( new UnderlineSpan(), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                                }
                            }
                        }

                    } else if( selectedTextStyleMode.equals( "size" ) ) {
                        textStyleEditModels.get( TEXTSTYLE_COLORIDX ).setSelected( false );
                        layoutColorPicker.setVisibility(View.GONE);
                        layoutSizePicker.setVisibility(View.VISIBLE);
                        v.setSelected( true );
                    }
                }
            });
        }


        editGuideLabel = (TextView)view.findViewById(R.id.edit_guide_label);

        editorWrapper = (LinearLayout)view.findViewById(R.id.editor_wrapper);
//        EditText editContent = (EditText)view.findViewById(R.id.edit_content);
        if( ((ContentEditActivity)getActivity()).getContentId() < 0 ) {
            addEditContent( true );
        }


        // 컨텐트 수정 Action Listener
        view.findViewById(R.id.modify_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( isTextStyleOn ) {
                    layoutTextstyle.setVisibility(View.GONE);
                } else {
                    KeyboardUtil.hideKeyBoard( getActivity(), editContents.get( editContents.size() - 1 ) );
                    layoutTextstyle.setVisibility(View.VISIBLE);
                }
                isTextStyleOn = !isTextStyleOn;
            }
        });
        view.findViewById(R.id.add_img).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        && ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                            Constants.REQ_EXTERNAL_STORAGE_PERMISSION);
                } else {
                    openFileChooser( Constants.REQ_CODE_OPEN_CONTENT_IMG, Intent.EXTRA_ALLOW_MULTIPLE );
                }
            }
        });
        view.findViewById(R.id.add_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "동영상 업로드 기능은 아직 준비중입니다^^", Toast.LENGTH_LONG).show();
            }
        });
//        view.findViewById(R.id.share_content).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
        alignText = (ImageView)view.findViewById(R.id.align_text);
        alignText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int gravity;
                Layout.Alignment alignment;

                if( isAlignCenter ) {
                    alignText.setImageResource(R.drawable.edit_alignleft_icon);
                    alignment = Layout.Alignment.ALIGN_NORMAL;
                    gravity = Gravity.NO_GRAVITY;
                } else {
                    alignText.setImageResource(R.drawable.edit_aligncenter_icon);
                    alignment = Layout.Alignment.ALIGN_CENTER;
                    gravity = Gravity.CENTER_HORIZONTAL;
                }

                for( EditText editContent : editContents ) {
                    editContent.getEditableText().setSpan( new AlignmentSpan.Standard( alignment ), 0, editContent.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
                    editContent.setGravity( gravity );
                    editContent.setSelection( editContent.length() );
                }

                isAlignCenter = !isAlignCenter;
            }
        });

        return view;
    }


    private void setContentEditEvent( EditText editContent ) {
        editContent.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if( hasFocus ) {
                    editGuideLabel.setVisibility( View.GONE );
                    removeImageFocusing( true );
                }
//                else {
//                    if( editContent.length() < 1 ) {
//                        editGuideLabel.setVisibility(View.VISIBLE);
//                    }
//                }
            }
        });
        editContent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if( isTextStyleOn ) {
                    layoutTextstyle.setVisibility(View.GONE);
                }
                removeImageFocusing( true );
                return false;
            }
        });
        editContent.addTextChangedListener(new TextWatcher() {

            private int startIdx;
            private int endIdx;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                startIdx = start;
                endIdx = start + count;

                checkTextCount();
            }

            @Override
            public void afterTextChanged(Editable s) {

                ((ContentEditActivity)getActivity()).setSaved( false );

                if( !selectedTextColor.equals( DEFAULT_TEXTCOLOR ) ) {
                    ForegroundColorSpan[] foregroundColorSpens = s.getSpans( startIdx, endIdx, ForegroundColorSpan.class );
                    for( ForegroundColorSpan foregroundColorSpan : foregroundColorSpens ) {
                        removeSpans( foregroundColorSpan, s, startIdx, endIdx );
                    }

                    s.setSpan( new ForegroundColorSpan( BitmapUtil.getColorCodeFromString( getContext(), selectedTextColor ) ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                }

                if( editTextIsBold ) {
                    StyleSpan[] styleSpens = s.getSpans( startIdx, endIdx, StyleSpan.class );
                    for( StyleSpan styleSpan : styleSpens ) {
                        removeSpans( styleSpan, s, startIdx, endIdx );
                    }

                    s.setSpan( new StyleSpan( Typeface.BOLD ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                }

                if( editTextIsUnderline ) {
                    UnderlineSpan[] underlineSpens = s.getSpans( startIdx, endIdx, UnderlineSpan.class );
                    for( UnderlineSpan underlineSpan : underlineSpens ) {
                        removeSpans( underlineSpan, s, startIdx, endIdx );
                    }

                    s.setSpan( new UnderlineSpan(), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                }

                if( selectedTextSize != DEFAULT_TEXTSIZE ) {
                    AbsoluteSizeSpan[] absoluteSizeSpens = s.getSpans( startIdx, endIdx, AbsoluteSizeSpan.class );
                    for( AbsoluteSizeSpan absoluteSizeSpan : absoluteSizeSpens ) {
                        removeSpans( absoluteSizeSpan, s, startIdx, endIdx );
                    }

                    s.setSpan( new AbsoluteSizeSpan( selectedTextSize, true ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                }

            }
        });
        editContent.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if( ((EditText)v).length() < 1 ) {
                    if( keyCode == KeyEvent.KEYCODE_DEL && editContents.size() > 1 ) {
                        editContents.remove( v );
                        editorWrapper.removeView( v );
                        editContents.get( editContents.size() - 1 ).requestFocus();
                    }
                }

                return false;
            }
        });
    }


    private void removeSpans( ParcelableSpan spanType, Editable editable, int startIdx, int endIdx ) {

        int spanStartIdx = editable.getSpanStart( spanType );
        int spanEndIdx = editable.getSpanEnd( spanType );

        editable.removeSpan( spanType );

        ParcelableSpan prevParcelableSpan = spanType;
        ParcelableSpan nextParcelableSpan = spanType;
        if( spanType instanceof ForegroundColorSpan ) {
            prevParcelableSpan = new ForegroundColorSpan( ((ForegroundColorSpan) prevParcelableSpan).getForegroundColor() );
            nextParcelableSpan = new ForegroundColorSpan( ((ForegroundColorSpan) nextParcelableSpan).getForegroundColor() );
        } else if( spanType instanceof StyleSpan ) {
            prevParcelableSpan = new StyleSpan( ((StyleSpan) prevParcelableSpan).getStyle() );
            nextParcelableSpan = new StyleSpan( ((StyleSpan) nextParcelableSpan).getStyle() );
        } else if( spanType instanceof UnderlineSpan ) {
            prevParcelableSpan = new UnderlineSpan();
            nextParcelableSpan = new UnderlineSpan();
        } else if ( spanType instanceof AbsoluteSizeSpan ) {
            prevParcelableSpan = new AbsoluteSizeSpan( ((AbsoluteSizeSpan) prevParcelableSpan).getSize(), ((AbsoluteSizeSpan) prevParcelableSpan).getDip() );
            nextParcelableSpan = new AbsoluteSizeSpan( ((AbsoluteSizeSpan) nextParcelableSpan).getSize(), ((AbsoluteSizeSpan) nextParcelableSpan).getDip() );
        }

        // startIdx와 endIdx 사이에 있지 않은 Span이지만 remove 된 경우 앞뒤로 새로 붙여줌
        if( spanStartIdx < startIdx ) {
            editable.setSpan( prevParcelableSpan, spanStartIdx, startIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
        }
        if( endIdx < spanEndIdx ) {
            editable.setSpan( nextParcelableSpan, endIdx, spanEndIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
        }
    }


    private void setTextColorModelClear() {
        for( CustomColorPickerModel textColorModel : textColorModels ) {
            if( !textColorModel.getColorCode().equals( selectedTextColor ) ) {
                textColorModel.setSelected( false );
            }
        }
    }
    private void setTextSizeModelClear() {
        for( CustomTextStyleEditModel textSizeModel : textSizeModels ) {
            if( textSizeModel.getTextSize() != selectedTextSize ) {
                textSizeModel.setSelected( false );
            }
        }
    }

    public void setThumbnailImg( Uri uri, String serverFileName ) {
        selectedThumbnail = serverFileName;
        imgSelector.setImageURI( uri );
        imgSelector.setPadding( 0, 0, 0, 0 );
    }


    // 본문이미지 ImageView 추가
    public void addContentImg( String filePath, String serverFileName ) {

        if( editorWrapper.getChildCount() < 2 && editorWrapper.getChildAt( 0 ) instanceof EditText ) {
            editGuideLabel.setVisibility( View.GONE );
            ((EditText)editorWrapper.getChildAt( 0 )).setHint( null );
        }

        CustomContentImageWrapper customContentImageWrapper = new CustomContentImageWrapper( getActivity() );
        customContentImageWrapper.setTag( serverFileName );

        ImageView contentImage = customContentImageWrapper.getContentImg();

        // Bitmap 데이터를 메모리할당 없이 불러와서 크기체크 ( inJustDecodeBounds true )
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile( filePath, options );

        // 메모리에 할당하기 적당한 크기로 실제 decode
        int targetWidth = editorWrapper.getWidth() - 10;
        options.inSampleSize = calculateInSampleSize( options.outWidth, targetWidth );

        Log.e( "WQTEST3", "options.outWidth : " + options.outWidth );
        Log.e( "WQTEST3", "targetWidth : " + targetWidth );
        Log.e( "WQTEST3", "Bitmap inSampleSize : " + options.inSampleSize );
        options.inJustDecodeBounds = false;
        Bitmap targetImg = BitmapFactory.decodeFile( filePath, options );

        // Rotate, Resize 처리
        targetImg = ExifUtils.rotateResizeBitmap( filePath, targetImg, targetWidth );

        Log.e( "WQTEST3", "targetImg.getWidth() : " + targetImg.getWidth() );
        Log.e( "WQTEST3", "targetImg.getHeight() : " + targetImg.getHeight() );

        contentImage.setImageBitmap( targetImg );
        customContentImageWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                KeyboardUtil.hideKeyBoard( getActivity(), editContents.get( editContents.size() - 1 ) );

                // 나머지 ImageView 중에 선택된 것들 제거
                removeImageFocusing( false );
                v.requestFocus();

                ((CustomContentImageWrapper)v).getContentImg().setColorFilter( Color.parseColor( "#BDBDBD" ), PorterDuff.Mode.MULTIPLY );
                ((CustomContentImageWrapper)v).getSelectedBorder().setVisibility( View.VISIBLE );
            }
        });

        int targetPosition = editorWrapper.getChildCount() - 1;
        EditText detachEditText = null;
        int selectionStart = -1;
        for( int i = 0; i < editorWrapper.getChildCount(); i++ ) {
            if( editorWrapper.getChildAt( i ).hasFocus() && editorWrapper.getChildAt( i ) instanceof EditText ) {

                selectionStart = ((EditText) editorWrapper.getChildAt( i )).getSelectionStart();
                int editContentLength = ((EditText) editorWrapper.getChildAt( i )).length();


                if( selectionStart < editContentLength ) {
                    detachEditText = (EditText) editorWrapper.getChildAt( i );
                }

                targetPosition = i;
                break;
            }
        }

        editorWrapper.addView( customContentImageWrapper, targetPosition + 1 );

        if( !ObjectUtils.isEmpty( detachEditText ) ) {

            EditText newEditText = addEditContent( targetPosition + 2, editContents.indexOf( detachEditText ) );
            newEditText.setText( detachEditText.getEditableText() );

            Editable prevEditable = detachEditText.getEditableText();
            Editable nextEditable = newEditText.getEditableText();

            prevEditable.delete( selectionStart, prevEditable.length() );
            nextEditable.delete( 0, selectionStart );
        }

        addEditContent( false );

    }


    // 본문이미지( from Server ) ImageView 추가
    public void addContentImg( String serverFileName, float imageRatio ) {

        CustomContentImageWrapper customContentImageWrapper = new CustomContentImageWrapper( getActivity() );
        customContentImageWrapper.setTag( serverFileName.substring( serverFileName.lastIndexOf( "/" ) + 1 ) );

        ImageView contentImage = customContentImageWrapper.getContentImg();
        customContentImageWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                KeyboardUtil.hideKeyBoard( getActivity(), editContents.get( editContents.size() - 1 ) );

                // 나머지 ImageView 중에 선택된 것들 제거
                removeImageFocusing( false );
                v.requestFocus();

                ((CustomContentImageWrapper)v).getContentImg().setColorFilter( Color.parseColor( "#BDBDBD" ), PorterDuff.Mode.MULTIPLY );
                ((CustomContentImageWrapper)v).getSelectedBorder().setVisibility( View.VISIBLE );
            }
        });
        editorWrapper.addView( customContentImageWrapper );

        int targetWidth = editorWrapper.getWidth() - 10;
        int targetHeight = (int) ( targetWidth * imageRatio );

        Glide.with( getActivity() ).load( serverFileName ).override( targetWidth, targetHeight ).into( contentImage );

    }


    private void removeImageFocusing( boolean setTextToolbar ) {
        for( int i = 0; i < editorWrapper.getChildCount(); i++ ) {
            if( editorWrapper.getChildAt( i ) instanceof CustomContentImageWrapper ) {

                CustomContentImageWrapper customContentImageWrapper = ((CustomContentImageWrapper) editorWrapper.getChildAt( i ));

                if( customContentImageWrapper.getSelectedBorder().getVisibility() == View.VISIBLE ) {
                    customContentImageWrapper.getSelectedBorder().setVisibility( View.GONE );
                    customContentImageWrapper.getContentImg().setColorFilter( null );
                    break;
                }

//                ImageView contentImageView = ((CustomContentImageWrapper) editorWrapper.getChildAt( i )).getContentImg();
//                if( !ObjectUtils.isEmpty( contentImageView.getBackground() ) ) {
//                    contentImageView.setBackground( null );
//                    contentImageView.setColorFilter( null );
//                    break;
//                }
            }
        }

        if( setTextToolbar ) {
            layoutImageToolbar.setVisibility( View.GONE );
            layoutTextToolbar.setVisibility( View.VISIBLE );
        } else {
            layoutTextToolbar.setVisibility( View.GONE );
            layoutImageToolbar.setVisibility( View.VISIBLE );
        }
    }


    // editContent를 wrapper에 추가
    private EditText addEditContent( boolean existHint ) {
        EditText editContent = new EditText( getActivity() );
        editContent.setBackgroundColor( Color.TRANSPARENT );
        editContent.setTextSize( 15 );
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT );
        editContent.setLayoutParams( layoutParams );
        editContent.setTypeface( Typeface.createFromAsset( getActivity().getAssets(), "NanumSquareOTFRegular.otf" ) );
        editContent.setPadding( 2, 2, 2, 2 );
        editContent.setInputType( InputType.TYPE_TEXT_FLAG_MULTI_LINE );
        if( existHint ) {
            editContent.setHint( "글을 써주세요    " );
        }
        editContent.setHorizontallyScrolling( false );
        editContent.setSingleLine( false );
        if( isAlignCenter ) {
            editContent.setGravity( Gravity.CENTER_HORIZONTAL );
        } else {
            editContent.setGravity( Gravity.NO_GRAVITY );
        }
        setContentEditEvent( editContent );
        editorWrapper.addView( editContent );
        editContents.add( editContent );

        return editContent;
    }

    // editContent를 wrapper에 추가
    private EditText addEditContent( int editorWrapperPos, int editContentsPos ) {
        EditText editContent = new EditText( getActivity() );
        editContent.setBackgroundColor( Color.TRANSPARENT );
        editContent.setTextSize( 15 );
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT );
        editContent.setLayoutParams( layoutParams );
        editContent.setTypeface( Typeface.createFromAsset( getActivity().getAssets(), "NanumSquareOTFRegular.otf" ) );
        editContent.setPadding( 2, 2, 2, 2 );
        editContent.setInputType( InputType.TYPE_TEXT_FLAG_MULTI_LINE );
        editContent.setHorizontallyScrolling( false );
        editContent.setSingleLine( false );
        if( isAlignCenter ) {
            editContent.setGravity( Gravity.CENTER_HORIZONTAL );
        } else {
            editContent.setGravity( Gravity.NO_GRAVITY );
        }
        setContentEditEvent( editContent );
        editorWrapper.addView( editContent, editorWrapperPos ); // 실제로 wrapper에는 image가 들어가있으므로 +1된 position을 넣음
        editContents.add( editContentsPos, editContent );

        return editContent;
    }

    // 서버에서 가져온 ContentImg를 Editing Area에 Setting
//    public void setContentImg( Drawable d, String serverFileName ) {
//        String issuedImgStr = "img" + addedImgIdx;
//        SpannableString spannableString = new SpannableString( issuedImgStr );
//        d.setBounds( 0, 0, editContent.getWidth(), CONTENTIMG_HEIGHT );
//        CustomImageSpan imageSpan = new CustomImageSpan( d, ImageSpan.ALIGN_BOTTOM, serverFileName );
//
//        contentImages.add( new ContentImage( addedImgIdx++, issuedImgStr, imageSpan ) );
//        spannableString.setSpan( imageSpan, 0, issuedImgStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
//        editContent.getEditableText().append( spannableString );
//        editContent.setSelection( editContent.length() );
//    }

    // 갤러리에서 가져온 ContentImg를 Editing Area에 Setting
//    public void setContentImg( String filePath, String serverFileName ) {
//
//        String issuedImgStr = "img" + addedImgIdx;
//        SpannableString spannableString = new SpannableString( issuedImgStr );
//
//        // Bitmap 데이터를 메모리할당 없이 불러와서 크기체크 ( inJustDecodeBounds true )
//        BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile( filePath, options );
//
//        // 메모리에 할당하기 적당한 크기로 실제 decode
//        options.inSampleSize = calculateInSampleSize( options.outWidth, editContent.getWidth() );
//        options.inJustDecodeBounds = false;
//        Bitmap targetImg = BitmapFactory.decodeFile( filePath, options );
//
//        // Rotate, Resize 처리
//        targetImg = ExifUtils.rotateResizeBitmap( filePath, targetImg, editContent.getWidth() );
//
//        CustomImageSpan imageSpan = new CustomImageSpan( getActivity(), targetImg, ImageSpan.ALIGN_BOTTOM, serverFileName );
//        contentImages.add( new ContentImage( addedImgIdx++, issuedImgStr, imageSpan ) );
//
//        spannableString.setSpan( imageSpan, 0, issuedImgStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE );
//        editContent.append( "\n" );
//        editContent.append( "\n" );
//        editContent.getEditableText().append( spannableString );
//        editContent.append( "\n" );
//        editContent.setSelection( editContent.length() );
//
//    }


    public static int calculateInSampleSize( int realWidth, int reqWidth ) {

        int inSampleSize = 1;

        if( realWidth > reqWidth) {

            final int halfWidth = realWidth / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while( ( halfWidth / inSampleSize ) >= reqWidth ) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public void openFileChooser( int openType, String selectType  ) {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra( selectType, true );
        getActivity().startActivityForResult( Intent.createChooser( intent, "파일을 고르세요" ), openType );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.e( "WQTEST", "EditMainFragment[onRequestPermissionsResult]" );

        if (requestCode == Constants.REQ_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                openFileChooser( Constants.REQ_CODE_OPEN_CONTENT_IMG, Intent.EXTRA_ALLOW_MULTIPLE );
            } else {
                Toast.makeText(getActivity(), "권한을 허가하서야 파일을 올리실 수 있습니다", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void setGetContentData( ContentForFragment getContent ) {

        if( !getContent.getState().equals( "temp" ) ) {
            // 임시저장된 상태가 아니라면 임시저장 문구를 '수정'으로 변경
            saveBtn.setText( "수정" );
            saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 수정메서드로 binding
                    ((ContentEditActivity)getActivity()).putModifyContent();
                }
            });

            textCountCheckDlgOk.setText( "수정" );
            textCountCheckDlgOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 수정메서드로 binding
                    ((ContentEditActivity)getActivity()).putModifyContent();
                    textCountCheckDlg.dismiss();
                }
            });
        }

        editGuideLabel.setVisibility( View.GONE );

        // 대표이미지 Setting
        selectedThumbnail = getContent.getImageUrl().substring( getContent.getImageUrl().lastIndexOf( "/" ) + 1 );
        imgSelector.setPadding( 0, 0, 0, 0 );
        Glide.with( getActivity() ).load( getContent.getImageUrl() ).placeholder( R.color.image_placeholder_color ).into( imgSelector );

        // 제목 Setting
        editTitle.setText( getContent.getTitle() );

        isLoadingContent = true;

        // 본문 Setting
        List<ContentFragment> fragments = getContent.getFragments();
        for( ContentFragment fragment : fragments ) {
            if( !ObjectUtils.isEmpty( fragment.getType() ) && fragment.getType().equals( "text" ) ) {
                String source = fragment.getSource();

                // font-size 제대로 fromHTML 하지 못하는 부분 replace 처리
                source = source.replaceAll( "font-size:", "color:#" ).replaceAll( "px\"", "0000;\"" );

                // br태그 인식하는 것이 다른 부분 replace 처리
                source = source.replaceAll( "<br/>", "" );

                EditText addedEditText = addEditContent( false );
                SpannableString addedSpannableString = new SpannableString( Html.fromHtml( source ) );

                // Size값을 인식하지 못하므로 임시 변환처리
                ForegroundColorSpan[] foregroundColorSpans = addedSpannableString.getSpans( 0, addedSpannableString.length(), ForegroundColorSpan.class );
                for( ForegroundColorSpan foregroundColorSpan : foregroundColorSpans ) {
                    // 11px AbsoluteSize
                    if( foregroundColorSpan.getForegroundColor() == Color.parseColor( "#110000" ) ) {
                        int startIdx = addedSpannableString.getSpanStart( foregroundColorSpan );
                        int endIdx = addedSpannableString.getSpanEnd( foregroundColorSpan );
                        addedSpannableString.removeSpan( foregroundColorSpan );
                        addedSpannableString.setSpan( new AbsoluteSizeSpan( 11, true ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                    }
                    // 17px AbsoluteSize
                    else if( foregroundColorSpan.getForegroundColor() == Color.parseColor( "#170000" ) ) {
                        int startIdx = addedSpannableString.getSpanStart( foregroundColorSpan );
                        int endIdx = addedSpannableString.getSpanEnd( foregroundColorSpan );
                        addedSpannableString.removeSpan( foregroundColorSpan );
                        addedSpannableString.setSpan( new AbsoluteSizeSpan( 17, true ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                    }
                    // 19px AbsoluteSize
                    else if( foregroundColorSpan.getForegroundColor() == Color.parseColor( "#190000" ) ) {
                        int startIdx = addedSpannableString.getSpanStart( foregroundColorSpan );
                        int endIdx = addedSpannableString.getSpanEnd( foregroundColorSpan );
                        addedSpannableString.removeSpan( foregroundColorSpan );
                        addedSpannableString.setSpan( new AbsoluteSizeSpan( 19, true ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                    }
                    // 20px AbsoluteSize
                    else if( foregroundColorSpan.getForegroundColor() == Color.parseColor( "#200000" ) ) {
                        int startIdx = addedSpannableString.getSpanStart( foregroundColorSpan );
                        int endIdx = addedSpannableString.getSpanEnd( foregroundColorSpan );
                        addedSpannableString.removeSpan( foregroundColorSpan );
                        addedSpannableString.setSpan( new AbsoluteSizeSpan( 20, true ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                    }
                    // 23px AbsoluteSize
                    else if( foregroundColorSpan.getForegroundColor() == Color.parseColor( "#230000" ) ) {
                        int startIdx = addedSpannableString.getSpanStart( foregroundColorSpan );
                        int endIdx = addedSpannableString.getSpanEnd( foregroundColorSpan );
                        addedSpannableString.removeSpan( foregroundColorSpan );
                        addedSpannableString.setSpan( new AbsoluteSizeSpan( 23, true ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                    }
                }

                addedEditText.getEditableText().append( addedSpannableString );
            } else if( !ObjectUtils.isEmpty( fragment.getType() ) && fragment.getType().equals( "image" ) ) {
                addContentImg( fragment.getSource(), fragment.getImageRatio() );
            }
        }

        isLoadingContent = false;

    }


    public void setPostContentData(PostContent postContent) {
        if( !ObjectUtils.isEmpty( selectedThumbnail ) ) {
            postContent.setThumbnail( selectedThumbnail );
        }
        postContent.setTitle( editTitle.getText().toString() );

        // Span들 묶어주기 ( 작업중 )
        for( EditText editContent : editContents ) {

            Editable editable = editContent.getEditableText();
            List<List<ParcelableSpan>> linkedSpans = new ArrayList<>();

            ForegroundColorSpan[] foregroundColorSpans = editable.getSpans( 0, editable.length(), ForegroundColorSpan.class );
            formLinkedSpans( editable, foregroundColorSpans, linkedSpans );

            StyleSpan[] styleSpans  = editable.getSpans( 0, editable.length(), StyleSpan.class );
            formLinkedSpans( editable, styleSpans, linkedSpans );

            UnderlineSpan[] underlineSpans = editable.getSpans( 0, editable.length(), UnderlineSpan.class );
            formLinkedSpans( editable, underlineSpans, linkedSpans );

            AbsoluteSizeSpan[] absoluteSizeSpans = editable.getSpans( 0, editable.length(), AbsoluteSizeSpan.class );
            formLinkedSpans( editable, absoluteSizeSpans, linkedSpans );

            for( List<ParcelableSpan> linkedSpan : linkedSpans ) {
                int startIdx = 0;
                for( int i = 0; i < linkedSpan.size(); i++ ) {
                    ParcelableSpan parcelableSpan = linkedSpan.get( i );
                    if( i == 0 ) {
                        startIdx = editable.getSpanStart( parcelableSpan );
                    }
                    if( i == linkedSpan.size() - 1 ) {
                        int endIdx = editable.getSpanEnd( parcelableSpan );
                        if( parcelableSpan instanceof ForegroundColorSpan ) {
                            editable.setSpan( new ForegroundColorSpan( ((ForegroundColorSpan) parcelableSpan).getForegroundColor() ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                        } else if( parcelableSpan instanceof StyleSpan ) {
                            editable.setSpan( new StyleSpan( ((StyleSpan) parcelableSpan).getStyle() ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                        } else if( parcelableSpan instanceof UnderlineSpan ) {
                            editable.setSpan( new UnderlineSpan(), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                        } else if( parcelableSpan instanceof AbsoluteSizeSpan ) {
                            editable.setSpan( new AbsoluteSizeSpan( ((AbsoluteSizeSpan) parcelableSpan).getSize(), true ), startIdx, endIdx, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
                        }
                    }

                    editable.removeSpan( parcelableSpan );
                }
            }

        }


        List<ContentFragment> fragments = new ArrayList<ContentFragment>();
        for( int i = 0; i < editorWrapper.getChildCount(); i++ ) {
            View targetView = editorWrapper.getChildAt( i );
            if( targetView instanceof LinearLayout ) {
                fragments.add( new ContentFragment( "image", targetView.getTag() + "" ) );
            } else {

                Editable editable = ((EditText) targetView).getText();
                String text = ((EditText) targetView).getText().toString();
                List<String> emojiStringList = new ArrayList<>();

                Matcher matchEmo = Pattern.compile( "[\\uD83C-\\uDBFF\\udc00-\\uDFFF]+" ).matcher( text );
                while( matchEmo.find() ) {
                    int matchIdx = editable.toString().indexOf( matchEmo.group() );
                    editable.replace( matchIdx, matchIdx + matchEmo.group().length(), DEFAULT_WEDQUEEN_EMOJI );

                    ForegroundColorSpan[] foregroundColorSpans = editable.getSpans( matchIdx, matchIdx + DEFAULT_WEDQUEEN_EMOJI.length(), ForegroundColorSpan.class );
                    for( ForegroundColorSpan foregroundColorSpan : foregroundColorSpans ) {
                        editable.removeSpan( foregroundColorSpan );
                    }

                    StyleSpan[] styleSpans = editable.getSpans( matchIdx, matchIdx + DEFAULT_WEDQUEEN_EMOJI.length(), StyleSpan.class );
                    for( StyleSpan styleSpan : styleSpans ) {
                        editable.removeSpan( styleSpan );
                    }

                    UnderlineSpan[] underlineSpans = editable.getSpans( matchIdx, matchIdx + DEFAULT_WEDQUEEN_EMOJI.length(), UnderlineSpan.class );
                    for( UnderlineSpan underlineSpan : underlineSpans ) {
                        editable.removeSpan( underlineSpan );
                    }

                    AbsoluteSizeSpan[] absoluteSizeSpans = editable.getSpans( matchIdx, matchIdx + DEFAULT_WEDQUEEN_EMOJI.length(), AbsoluteSizeSpan.class );
                    for( AbsoluteSizeSpan absoluteSizeSpan : absoluteSizeSpans ) {
                        editable.removeSpan( absoluteSizeSpan );
                    }

                    emojiStringList.add( matchEmo.group() );
                }

//                Log.e( "WQTEST3", "after Converting Emoji to Temp : " + editable.toString() );

                String fragmentText = Html.toHtml( editable );

//                Log.e( "WQTEST3", "after String to Html : " + fragmentText );

                for( int j = 0; j < emojiStringList.size(); j++ ) {
                    fragmentText = fragmentText.replaceFirst( DEFAULT_WEDQUEEN_EMOJI, emojiStringList.get( j ) );
                    int matchIdx = editable.toString().indexOf( DEFAULT_WEDQUEEN_EMOJI );
                    editable.replace( matchIdx, matchIdx + DEFAULT_WEDQUEEN_EMOJI.length(), emojiStringList.get( j ) );

                    ForegroundColorSpan[] foregroundColorSpans = editable.getSpans( matchIdx, matchIdx + emojiStringList.get( j ).length(), ForegroundColorSpan.class );
                    for( ForegroundColorSpan foregroundColorSpan : foregroundColorSpans ) {
                        editable.removeSpan( foregroundColorSpan );
                    }

                    StyleSpan[] styleSpans = editable.getSpans( matchIdx, matchIdx + emojiStringList.get( j ).length(), StyleSpan.class );
                    for( StyleSpan styleSpan : styleSpans ) {
                        editable.removeSpan( styleSpan );
                    }

                    UnderlineSpan[] underlineSpans = editable.getSpans( matchIdx, matchIdx + emojiStringList.get( j ).length(), UnderlineSpan.class );
                    for( UnderlineSpan underlineSpan : underlineSpans ) {
                        editable.removeSpan( underlineSpan );
                    }

                    AbsoluteSizeSpan[] absoluteSizeSpans = editable.getSpans( matchIdx, matchIdx + emojiStringList.get( j ).length(), AbsoluteSizeSpan.class );
                    for( AbsoluteSizeSpan absoluteSizeSpan : absoluteSizeSpans ) {
                        editable.removeSpan( absoluteSizeSpan );
                    }
                }

//                Log.e( "WQTEST3", "after Converting Temp to Emoji : " + fragmentText );

                // 변환된 String을 원래대로 Convert
                Matcher matchCode = Pattern.compile( "(&#)([0-9]{2,})(;)" ).matcher( fragmentText );
                while( matchCode.find() ) {
                    fragmentText = fragmentText.replaceAll( matchCode.group(), Character.toString( (char)Integer.parseInt( matchCode.group( 2 ) ) ) );
                }

//                Log.e( "WQTEST3", "after Char int to String : " + fragmentText );

                fragmentText = fragmentText.replaceAll( "</p>", "</p><br/>" );

                Matcher matchTag = Pattern.compile( "(<br>)+(</p>)" ).matcher( fragmentText );
                while( matchTag.find() ) {
                    String replaceText = "</p>" + matchTag.group().replace( "</p>", "" );
                    fragmentText = fragmentText.replaceFirst( matchTag.group(), replaceText );
                }

//                Log.e( "WQTEST3", "after TAG replace : " + fragmentText );

                fragments.add( new ContentFragment( "text", fragmentText ) );

            }
        }

        postContent.setFragments( fragments );
    }


    private void formLinkedSpans( Editable editable, ParcelableSpan[] arrParcelableSpan, List<List<ParcelableSpan>> linkedSpans ) {

        Map<ParcelableSpan, Integer> tmpMapForSort = new HashMap<ParcelableSpan, Integer>();
        for( ParcelableSpan parcelableSpan : arrParcelableSpan ) {
            tmpMapForSort.put( parcelableSpan, editable.getSpanStart( parcelableSpan ) );
        }
        List<ParcelableSpan> sortedSpans = sortByValue( tmpMapForSort );

        List<ParcelableSpan> parcelableSpans = null;
        ParcelableSpan prevParcelableSpan = null;
        for( ParcelableSpan parcelableSpan : sortedSpans ) {
            int startIdx = editable.getSpanStart( parcelableSpan );

            if( !ObjectUtils.isEmpty( prevParcelableSpan )
                    && prevParcelableSpan instanceof ForegroundColorSpan
                    && ((ForegroundColorSpan)prevParcelableSpan).getForegroundColor() == ((ForegroundColorSpan)parcelableSpan).getForegroundColor()
                    && editable.getSpanEnd( prevParcelableSpan ) == startIdx ) {
                // 같은 Span이 붙어있는 경우
                if( !ObjectUtils.isEmpty( parcelableSpans ) ) {
                    parcelableSpans.add( parcelableSpan );
                }
            } else if( !ObjectUtils.isEmpty( prevParcelableSpan )
                    && prevParcelableSpan instanceof StyleSpan
                    && ((StyleSpan)prevParcelableSpan).getStyle() == ((StyleSpan)parcelableSpan).getStyle()
                    && editable.getSpanEnd( prevParcelableSpan ) == startIdx ) {
                // 같은 Span이 붙어있는 경우
                if( !ObjectUtils.isEmpty( parcelableSpans ) ) {
                    parcelableSpans.add( parcelableSpan );
                }
            } else if( !ObjectUtils.isEmpty( prevParcelableSpan )
                    && prevParcelableSpan instanceof UnderlineSpan
                    && editable.getSpanEnd( prevParcelableSpan ) == startIdx ) {
                // 같은 Span이 붙어있는 경우
                if( !ObjectUtils.isEmpty( parcelableSpans ) ) {
                    parcelableSpans.add( parcelableSpan );
                }
            } else if( !ObjectUtils.isEmpty( prevParcelableSpan )
                    && prevParcelableSpan instanceof AbsoluteSizeSpan
                    && ((AbsoluteSizeSpan)prevParcelableSpan).getSize() == ((AbsoluteSizeSpan)parcelableSpan).getSize()
                    && editable.getSpanEnd( prevParcelableSpan ) == startIdx ) {
                // 같은 Span이 붙어있는 경우
                if( !ObjectUtils.isEmpty( parcelableSpans ) ) {
                    parcelableSpans.add( parcelableSpan );
                }
            } else {
                // 다른 Span이 붙어있는 경우
                if( !ObjectUtils.isEmpty( parcelableSpans ) ) {
                    linkedSpans.add( parcelableSpans );     // 진행중이던 parcelableSpans는 add
                }
                parcelableSpans = new ArrayList<>();
                parcelableSpans.add( parcelableSpan );
            }

            prevParcelableSpan = parcelableSpan;
        }

        if( !ObjectUtils.isEmpty( parcelableSpans ) ) {
            linkedSpans.add( parcelableSpans );             // 마지막 parcelableSpans도 add 해줌
        }

    }


    public boolean checkMainDataSet() {

        if( ObjectUtils.isEmpty( editTitle.getText().toString() ) ) {
            Toast.makeText( getActivity(), "제목을 채워주세요!", Toast.LENGTH_SHORT).show();
            if( editTitle.getHint().toString().length() > 12 ) {
                editTitle.setHint( "제목을 써주세요" );
            } else {
                editTitle.setHint( editTitle.getHint() + "!" );
            }
            return false;
        }

        if( ObjectUtils.isEmpty( selectedThumbnail ) ) {
            Toast.makeText( getActivity(), "대표이미지를 넣어주세요!", Toast.LENGTH_SHORT).show();
            if( imgLabel.getText().toString().length() > 15 || imgLabel.getText().toString().length() < 7 ) {
                imgLabel.setText( "대표이미지를 넣어주세요" );
            } else {
                imgLabel.setText( imgLabel.getText() + "!" );
            }
            return false;
        }

        return true;

    }


    public List sortByValue( final Map map ) {

        List<ParcelableSpan> list = new ArrayList<>();
        list.addAll( map.keySet() );

        Collections.sort(list, new Comparator<ParcelableSpan>() {
            @Override
            public int compare(ParcelableSpan o1, ParcelableSpan o2) {

                Object v1 = map.get(o1);
                Object v2 = map.get(o2);

                return ((Comparable) v2).compareTo(v1);
            }
        });

        Collections.reverse( list ); // 주석시 오름차순

        return list;
    }


    // 리얼웨딩 Text를 체크해서 특정 COUNT 이상일 경우 임시저장 권장 Dialog를 띄워줌
    private void checkTextCount() {

        int totalTextCount = 0;

        if( !ObjectUtils.isEmpty( editContents ) ) {
            for( EditText editContent : editContents ) {
                totalTextCount += editContent.length();
            }

            if( totalTextCount > ( CHECKCONTENT_TEXT_COUNT * ( checkedTextDiameter + 1 ) ) ) {
                if( !isLoadingContent ) {
                    textCountCheckDlg.show();
                }
                checkedTextDiameter = totalTextCount / CHECKCONTENT_TEXT_COUNT;
            }
        }
    }


    public void initTextCountCheckDlg() {
        textCountCheckDlg = new Dialog( getActivity() );
        textCountCheckDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        textCountCheckDlg.setContentView(R.layout.dialog_save);

        TextView textToShow = (TextView) textCountCheckDlg.findViewById(R.id.text_to_show);
        textToShow.setText( "잠깐! 임시저장을 잊지마세요 :D" );

        textCountCheckDlgOk = (TextView) textCountCheckDlg.findViewById(R.id.ok_btn);
        textCountCheckDlgOk.setText( "임시저장" );
        textCountCheckDlgOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 작성한 내용들 임시저장 Action
                ((ContentEditActivity)getActivity()).postTempContent();
                textCountCheckDlg.dismiss();
            }
        });

        TextView cancelBtn = (TextView) textCountCheckDlg.findViewById(R.id.cancel_btn);
        cancelBtn.setText( "취소" );
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textCountCheckDlg.dismiss();
            }
        });

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if( isVisibleToUser ) {

            Answers.getInstance().logContentView(new ContentViewEvent()
                    .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                    .putContentType( "Fragment" )
                    .putContentName( "edit_main" ) );

        }
    }
}
