package com.jjlee.wedqueen.view;

import android.util.Log;

import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import com.jjlee.wedqueen.rest.dday.model.CategoryDto;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ExampleDataProvider extends AbstractDataProvider {
    private List<ConcreteData> mData;
    private ConcreteData mLastRemovedData;
    private int mLastRemovedPosition = -1;

    public ExampleDataProvider( List<CategoryDto> categoryDtos ) {

        mData = new LinkedList<>();

        for( int i = 0; i < categoryDtos.size(); i++ ) {
            mData.add( new ConcreteData( i, categoryDtos.get( i ).getTitle(), categoryDtos.get( i ).getcBudget(), categoryDtos.get( i ).getExpense() ) );
        }
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Data getItem(int index) {
        if (index < 0 || index >= getCount()) {
            throw new IndexOutOfBoundsException("index = " + index);
        }

        return mData.get(index);
    }

    @Override
    public int undoLastRemoval() {
        if (mLastRemovedData != null) {
            int insertedPosition;
            if (mLastRemovedPosition >= 0 && mLastRemovedPosition < mData.size()) {
                insertedPosition = mLastRemovedPosition;
            } else {
                insertedPosition = mData.size();
            }

            mData.add(insertedPosition, mLastRemovedData);

            mLastRemovedData = null;
            mLastRemovedPosition = -1;

            return insertedPosition;
        } else {
            return -1;
        }
    }

    @Override
    public void moveItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        final ConcreteData item = mData.remove(fromPosition);

        mData.add(toPosition, item);
        mLastRemovedPosition = -1;
    }

    @Override
    public void swapItem(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        Collections.swap(mData, toPosition, fromPosition);
        mLastRemovedPosition = -1;
    }

    @Override
    public void removeItem(int position) {
        //noinspection UnnecessaryLocalVariable
        final ConcreteData removedItem = mData.remove(position);

        mLastRemovedData = removedItem;
        mLastRemovedPosition = position;
    }

    public static final class ConcreteData extends Data {

        private final long mId;
        private String mText;
        private final int mViewType;
        private boolean mPinned;

        private String mTitle;
        private long mBudget;
        private long mExpense;

        ConcreteData( long id, String title, long budget, long expense ) {
            mId = id;
            mViewType = 0;
            mTitle = title;
            mBudget = budget;
            mExpense = expense;
        }

        ConcreteData( String title ) {
            mId = 0;
            mViewType = 0;
            mText = title;
            mTitle = title;
        }

        @Override
        public boolean isSectionHeader() {
            return false;
        }

        @Override
        public int getViewType() {
            return mViewType;
        }

        @Override
        public long getId() {
            return mId;
        }

        @Override
        public String toString() {
            return mText;
        }

        @Override
        public String getText() {
            return mText;
        }

        @Override
        public boolean isPinned() {
            return mPinned;
        }

        @Override
        public void setPinned(boolean pinned) {
            mPinned = pinned;
        }

        @Override
        public String getTitle() {
            return mTitle;
        }

        @Override
        public long getBudget() {
            return mBudget;
        }

        @Override
        public long getExpense() {
            return mExpense;
        }
    }
}
