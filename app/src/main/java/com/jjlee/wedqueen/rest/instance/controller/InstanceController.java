package com.jjlee.wedqueen.rest.instance.controller;

import com.jjlee.wedqueen.rest.instance.model.InstanceResponse;
import com.jjlee.wedqueen.rest.instance.service.InstanceService;
import com.jjlee.wedqueen.rest.tutorial.model.TutorialResponse;
import com.jjlee.wedqueen.rest.tutorial.service.TutorialService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class InstanceController extends RetrofitController {
    private InstanceService instanceService;

    public InstanceController() {
        super();
        instanceService = RETROFIT.create(InstanceService.class);
    }

    public Call<InstanceResponse> getInstances() {
        return instanceService.getInstances( "android" );
    }

}
