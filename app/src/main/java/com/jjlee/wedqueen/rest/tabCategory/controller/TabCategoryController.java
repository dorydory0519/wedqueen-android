package com.jjlee.wedqueen.rest.tabCategory.controller;

import com.jjlee.wedqueen.rest.tabCategory.model.TabCategoryLog;
import com.jjlee.wedqueen.rest.tabCategory.model.TabCategoryLogResponse;
import com.jjlee.wedqueen.rest.tabCategory.model.TabCategoryResponse;
import com.jjlee.wedqueen.rest.tabCategory.service.TabCategoryLogService;
import com.jjlee.wedqueen.rest.tabCategory.service.TabCategoryService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class TabCategoryController extends RetrofitController {
    private TabCategoryService tabCategoryService;
    private TabCategoryLogService tabCategoryLogService;

    public TabCategoryController() {
        super();
        tabCategoryService = RETROFIT.create(TabCategoryService.class);
        tabCategoryLogService = LOGRETROFIT.create(TabCategoryLogService.class);
    }

    public Call<TabCategoryResponse> getTabCategories() {
        return tabCategoryService.getTabCategories();
    }

    public Call<TabCategoryLogResponse> postTabCategoryLog( TabCategoryLog tabCategoryLog ) {
        return tabCategoryLogService.postTabCategoryLog( tabCategoryLog );
    }

}
