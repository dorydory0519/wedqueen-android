package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jjlee.wedqueen.R;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by esmac on 2017. 9. 9..
 */

public class CustomHomeRecommendModel extends LinearLayout {

    private ImageView homeRecommendImg;
    private TextView homeRecommendDday;
    private TextView homeRecommendNickName;

    private LinearLayout homeRecommendFollow;
    private ImageView homerecommendFollowImg;

    public CustomHomeRecommendModel(Context context) {
        super(context);
        initView();

    }

    public CustomHomeRecommendModel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomHomeRecommendModel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_homerecommend, this, false);
        addView(v);

        homeRecommendImg = (ImageView) findViewById( R.id.homerecommend_img );
        homeRecommendDday = (TextView) findViewById( R.id.homerecommend_dday );
        homeRecommendNickName = (TextView) findViewById( R.id.homerecommend_nickname );
        homeRecommendFollow = (LinearLayout) findViewById( R.id.homerecommend_follow );
        homerecommendFollowImg = (ImageView) findViewById( R.id.homerecommend_follow_img );
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.HomeRecommendModel);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.HomeRecommendModel, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {

        String homeRecommendImg_resID = typedArray.getString(R.styleable.HomeRecommendModel_homerecommend_img);
        setHomeRecommendImg( homeRecommendImg_resID );

        String homeRecommendDday_resID = typedArray.getString(R.styleable.HomeRecommendModel_homerecommend_dday);
        setHomeRecommendDday( homeRecommendDday_resID );

        String homeRecommendNickName_resID = typedArray.getString(R.styleable.HomeRecommendModel_homerecommend_nickname);
        setHomeRecommendNickName( homeRecommendNickName_resID );

        typedArray.recycle();
    }

    public void setHomeRecommendImg(String resUrl) {
        Glide.with( getContext() ).load( resUrl ).bitmapTransform( new CropCircleTransformation( getContext() ) ).placeholder( R.drawable.default_user_icon ).into( homeRecommendImg );
    }
    public void setHomeRecommendDday(String resString) {
        homeRecommendDday.setText( resString );
    }
    public void setHomeRecommendNickName(String resString) {
        homeRecommendNickName.setText( resString );
    }
    public void setFollow( boolean isFollow ) {

        homeRecommendFollow.setTag( isFollow );

        if( isFollow ) {
            homeRecommendFollow.setBackgroundResource( R.drawable.green_rounding );
            homerecommendFollowImg.setImageResource( R.drawable.follow_on );
        } else {
            homeRecommendFollow.setBackgroundResource( R.drawable.red_rounding );
            homerecommendFollowImg.setImageResource( R.drawable.follow_off );
        }
    }
    public LinearLayout getHomeRecommendFollow() {
        return homeRecommendFollow;
    }

}
