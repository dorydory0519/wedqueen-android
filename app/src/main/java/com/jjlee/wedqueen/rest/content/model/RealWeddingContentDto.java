package com.jjlee.wedqueen.rest.content.model;

public class RealWeddingContentDto {

    private Long id;
    private Long companyId;

    private Double companyLatitude;
    private Double companyLongitude;

    private String imageUrl;
    private String title;
    private String strEvent;
    private String writer;
    private String viewCountToString;
    private String url;

    private boolean loved;
    private boolean paymentRequired;
    private boolean inquiryRequired;
    private boolean surveyRequired;

    private String type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Double getCompanyLatitude() {
        return companyLatitude;
    }

    public void setCompanyLatitude(Double companyLatitude) {
        this.companyLatitude = companyLatitude;
    }

    public Double getCompanyLongitude() {
        return companyLongitude;
    }

    public void setCompanyLongitude(Double companyLongitude) {
        this.companyLongitude = companyLongitude;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStrEvent() {
        return strEvent;
    }

    public void setStrEvent(String strEvent) {
        this.strEvent = strEvent;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getViewCountToString() {
        return viewCountToString;
    }

    public void setViewCountToString(String viewCountToString) {
        this.viewCountToString = viewCountToString;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isLoved() {
        return loved;
    }

    public void setLoved(boolean loved) {
        this.loved = loved;
    }

    public boolean isPaymentRequired() {
        return paymentRequired;
    }

    public void setPaymentRequired(boolean paymentRequired) {
        this.paymentRequired = paymentRequired;
    }

    public boolean isInquiryRequired() {
        return inquiryRequired;
    }

    public void setInquiryRequired(boolean inquiryRequired) {
        this.inquiryRequired = inquiryRequired;
    }

    public boolean isSurveyRequired() {
        return surveyRequired;
    }

    public void setSurveyRequired(boolean surveyRequired) {
        this.surveyRequired = surveyRequired;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
