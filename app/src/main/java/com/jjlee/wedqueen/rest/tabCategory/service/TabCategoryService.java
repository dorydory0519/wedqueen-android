package com.jjlee.wedqueen.rest.tabCategory.service;

import com.jjlee.wedqueen.rest.tabCategory.model.TabCategoryResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface TabCategoryService {
    @Headers("Accept: application/json")
    @GET("api/v2/tabcategories")
    Call<TabCategoryResponse> getTabCategories();
}
