package com.jjlee.wedqueen.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.Prefs;

/**
 * Created by JJLEE on 2016. 3. 28..
 *
 * SectionPager 의 Page가 되는 프래그먼트.
 * SectionPager 의 각 페이지는 본 클래스를 상속받아 구현한다
 *
 */

public abstract class PageFragment extends Fragment {

    protected ContentLoadingProgressBar mProgressBar;
    protected View mNetworkErrorView;
    protected SwipeRefreshLayout mSwipeRefreshLayout;
    protected Prefs prefs;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prefs = new Prefs(getContext());
    }

    /**
     *
     * @param rootView
     * 기본적으로 모든 탭에 있는 요소들 초기화
     * 자식 프래그먼트의 onCreateView 에서 호출하는게 좋음.
     */
    protected void initiateNecessaryComponents(ViewGroup rootView) {
        // 네트워크 에러부분 구현
        mNetworkErrorView = rootView.findViewById(R.id.network_error);
        mNetworkErrorView.bringToFront();

        // 아래로 땡겨서 리프레쉬 하는 부분 구현
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh_layout);
        // mSwipeRefreshLayout.setProgressViewOffset(false, 0, 200);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimaryDark, R.color.colorPrimaryDark,
                R.color.colorPrimaryDark, R.color.colorAccent);

        // 빙글빙글 돌아가는 로딩바 부분 구현
        mProgressBar = (ContentLoadingProgressBar) rootView.findViewById(R.id.progress_bar);
        mProgressBar.getIndeterminateDrawable().setColorFilter(0xFFAF9A5E, android.graphics.PorterDuff.Mode.MULTIPLY);
        mProgressBar.bringToFront();
    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }
    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void showNetworkError() {
        mNetworkErrorView.setVisibility(View.VISIBLE);
    }
    public void hideNetworkError() {
        mNetworkErrorView.setVisibility(View.GONE);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.destroyDrawingCache();
            mSwipeRefreshLayout.clearAnimation();
        }
    }

    abstract public void refresh();
}
