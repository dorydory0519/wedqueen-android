package com.jjlee.wedqueen.rest.sector.model;

import java.util.List;

public class Sector {

    private Long id;
    private String title;
    private int position;
    private List<SectorItem> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public List<SectorItem> getItems() {
        return items;
    }

    public void setItems(List<SectorItem> items) {
        this.items = items;
    }
}
