package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;

public class CustomPremiumAdModel extends RelativeLayout {

    RequestManager mRequestManager;

    private ImageView adImg;
    private TextView adTitle;
    private TextView adAux;

    private long premiumAdId;

    private RelativeLayout relativeLayout;

    public CustomPremiumAdModel(Context context) {
        super(context);
        initView();
    }

    public CustomPremiumAdModel(Context context, RequestManager requestManager) {
        super(context);
        mRequestManager = requestManager;
        initView();
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService( infService );
        View v = li.inflate( R.layout.layout_premium_ad, this, false );
        addView(v);

        relativeLayout = (RelativeLayout) v;
        adImg = (ImageView) findViewById( R.id.ad_img );
        adTitle = (TextView) findViewById( R.id.ad_title );
        adAux = (TextView) findViewById( R.id.ad_aux );
    }

    public void setAdImg( String resUrl ) {
        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load(resUrl).centerCrop().placeholder(R.color.image_placeholder_color).into(adImg);
        } else {
            Glide.with( getContext() ).load( resUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( adImg );
        }
    }

    public void setRelativeLayoutPadding( int resInt ) {
        relativeLayout.setPadding( resInt, resInt, resInt, resInt );
    }

    public void setAdTitle( String resString ) {
        adTitle.setText( resString );
    }

    public void setAdAux( String resString ) {
        adAux.setText( resString );
    }

    public long getPremiumAdId() {
        return premiumAdId;
    }

    public void setPremiumAdId(long premiumAdId) {
        this.premiumAdId = premiumAdId;
    }
}
