package com.jjlee.wedqueen.rest.company.service;

import com.jjlee.wedqueen.rest.company.model.CompanyResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by LeeJungYi on 2016-04-01.
 */
public interface CompanyService {
    @Headers("Accept: application/json")
    @GET("api/v1/premium/{tab}")
    Call<CompanyResponse> getPremiumsInTab(@Path("tab") String tab, @Query("category") String category, @Query("page") int page);

    @GET("api/v3/premium")
    Call<CompanyResponse> getPremiums(@Query("category") String category, @Query("page") int page, @Query("contentCount") int contentCount);

    @GET("api/v1/company")
    Call<CompanyResponse> getCompanies(@Query("category") String category, @Query("page") int page);

}
