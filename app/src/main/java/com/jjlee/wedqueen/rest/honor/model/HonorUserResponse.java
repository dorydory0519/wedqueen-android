package com.jjlee.wedqueen.rest.honor.model;

import com.jjlee.wedqueen.rest.user.model.UserRestDto;

import java.util.List;

public class HonorUserResponse {
    private List<UserRestDto> users;

    public List<UserRestDto> getUsers() {
        return users;
    }

    public void setUsers(List<UserRestDto> users) {
        this.users = users;
    }
}
