package com.jjlee.wedqueen.rest.account.model;

import com.jjlee.wedqueen.rest.app.model.TabCategory;
import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.rest.dday.model.DDayDto;

import java.util.List;

/**
 * Created by JJLEE on 2016. 4. 4..
 */
public class LoginCommonResponse extends CommonResponse {
    private List<TabCategory> tabCategories;
    private List<ConstantDto> constants;
    private DDayDto dday;
    private String userImageUrl;
    private String nickname;
    private int point;
    private int notificationCount;
    private String userId;
    private String firstLogin;

    public LoginCommonResponse(String msg) {
        this.msg = msg;
    }

    public List<TabCategory> getTabCategories() {
        return tabCategories;
    }

    public void setTabCategories(List<TabCategory> tabCategories) {
        this.tabCategories = tabCategories;
    }

    public List<ConstantDto> getConstants() {
        return constants;
    }

    public void setConstants(List<ConstantDto> constants) {
        this.constants = constants;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }

    public int getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(int notificationCount) {
        this.notificationCount = notificationCount;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public DDayDto getDday() {
        return dday;
    }

    public void setDday(DDayDto dday) {
        this.dday = dday;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(String firstLogin) {
        this.firstLogin = firstLogin;
    }
}
