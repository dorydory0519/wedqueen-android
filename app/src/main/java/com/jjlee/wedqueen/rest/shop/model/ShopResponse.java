package com.jjlee.wedqueen.rest.shop.model;

import java.util.List;

/**
 * Created by esmac on 2018. 1. 11..
 */

public class ShopResponse {
    private List<Gift> gifts;

    public List<Gift> getGifts() {
        return gifts;
    }

    public void setGifts(List<Gift> gifts) {
        this.gifts = gifts;
    }
}
