package com.jjlee.wedqueen.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.AccountActivity;
import com.jjlee.wedqueen.CustomViews.CustomCompanyModel;
import com.jjlee.wedqueen.CustomViews.CustomHomeRecommendModel;
import com.jjlee.wedqueen.CustomViews.CustomHorizontalScrollView;
import com.jjlee.wedqueen.CustomViews.CustomImageGoodsModel;
import com.jjlee.wedqueen.CustomViews.CustomRealWeddingModel;
import com.jjlee.wedqueen.CustomViews.CustomRecentRealWeddingModel;
import com.jjlee.wedqueen.CustomViews.CustomSectorModel;
import com.jjlee.wedqueen.CustomViews.CustomWeddingTipModel;
import com.jjlee.wedqueen.DetailCompanyActivity;
import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.OtherUserDiaryActivity;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.RealWeddingActivity;
import com.jjlee.wedqueen.rest.company.controller.CompanyController;
import com.jjlee.wedqueen.rest.company.model.Company;
import com.jjlee.wedqueen.rest.company.model.CompanyResponse;
import com.jjlee.wedqueen.rest.content.controller.ContentController;
import com.jjlee.wedqueen.rest.content.model.Content;
import com.jjlee.wedqueen.rest.content.model.ContentResponse;
import com.jjlee.wedqueen.rest.recommend.controller.RecommendController;
import com.jjlee.wedqueen.rest.recommend.model.RecommendResponse;
import com.jjlee.wedqueen.rest.recommend.model.User;
import com.jjlee.wedqueen.rest.sector.controller.SectorController;
import com.jjlee.wedqueen.rest.sector.model.Sector;
import com.jjlee.wedqueen.rest.sector.model.SectorItem;
import com.jjlee.wedqueen.rest.sector.model.SectorItemAux;
import com.jjlee.wedqueen.rest.sector.model.SectorItemLog;
import com.jjlee.wedqueen.rest.sector.model.SectorItemLogResponse;
import com.jjlee.wedqueen.rest.sector.model.SectorResponse;
import com.jjlee.wedqueen.rest.subscription.controller.SubscriptionController;
import com.jjlee.wedqueen.rest.subscription.model.SubscriptionResponse;
import com.jjlee.wedqueen.rest.tabCategory.controller.TabCategoryController;
import com.jjlee.wedqueen.rest.tabCategory.model.TabCategoryLog;
import com.jjlee.wedqueen.rest.tabCategory.model.TabCategoryLogResponse;
import com.jjlee.wedqueen.rest.tip.controller.TipController;
import com.jjlee.wedqueen.rest.tip.model.Tip;
import com.jjlee.wedqueen.rest.tip.model.TipResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.CustomScrollView;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by esmac on 2017. 9. 6..
 */

public class CategoryFragment extends Fragment {

    private RequestManager mGlideRequestManager;

    private String categoryName;
    private String categoryNameKor;
    private int position;
    private long categoryId;
    private boolean isTipOpen;

    private SwipeRefreshLayout swipeCategory;
    private Fragment targetFragment;

    private LinearLayout companiesWrapper;
    private LinearLayout recentRealWeddingWrapper;
    private GridLayout realweddingWrapper;

    private TipController tipController;
    private RecommendController recommendController;
    private SubscriptionController subscriptionController;
    private SectorController sectorController;
    private TabCategoryController tabCategoryController;
    private ContentController contentController;
    private CompanyController companyController;

    private CustomHorizontalScrollView weddingTip;
    private LinearLayout weddingtipsWrapper;
    private TextView guideText;
    private String weddingTipText;
    private ImageView guideImg;

    private LinearLayout homeRecommendWrapper;

    private int displayWidth;
    private int realweddingImgSize;
    private int bannerImgSize;

    private LinearLayout sectorSection;

    private LinearLayout preminumSection;
    private boolean isLoadLastPremiums = false;

    private LinearLayout recentRealWeddingSection;
    private LinearLayout homeRecommendSection;

    private boolean isLoadingRealweddingData;

    private int targetTime;
    private int realWeddingPage;
    private int premiumPage;
    private int tipPage;

    private final int DEFAULT_IMAGEGOODS_HEIGHT = 180;

    public static CategoryFragment newInstance( String categoryName, String categoryNameKor, int position, long categoryId, boolean isTipOpen ) {
        CategoryFragment fragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putString( "categoryName", categoryName );
        args.putString( "categoryNameKor", categoryNameKor );
        args.putInt( "position", position );
        args.putLong( "categoryId", categoryId );
        args.putBoolean( "isTipOpen", isTipOpen );
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        targetFragment = this;
        mGlideRequestManager = Glide.with( this );

        tabCategoryController = new TabCategoryController();

        Bundle args = getArguments();
        categoryName = args.getString( "categoryName" );
        categoryNameKor = args.getString( "categoryNameKor" );
        position = args.getInt( "position" );
        categoryId = args.getLong( "categoryId" );
        isTipOpen = args.getBoolean( "isTipOpen" );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_category, container, false);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics( displayMetrics );
        displayWidth = displayMetrics.widthPixels;

        isLoadLastPremiums = false;
        isLoadingRealweddingData = false;
        realWeddingPage = 1;
        premiumPage = 1;
        tipPage = 1;
        targetTime = new Date().getSeconds();

        weddingTip = (CustomHorizontalScrollView) view.findViewById(R.id.weddingtip_scrollview);
        guideImg = (ImageView) view.findViewById(R.id.guide_img);

        weddingTipText = "웨딩";
        if( !ObjectUtils.isEmpty( categoryName ) ) {
            weddingTipText = categoryNameKor;
        }
        guideText = (TextView) view.findViewById(R.id.guide_text);

        if( !isTipOpen ) {
            weddingTip.setVisibility(View.GONE);
            guideImg.setImageResource(R.drawable.down_arrow_nor);
            guideText.setText( weddingTipText + " 가이드 받기" );
        } else {
            weddingTip.setVisibility(View.VISIBLE);
            guideImg.setImageResource(R.drawable.up_arrow);
            guideText.setText( weddingTipText + " 가이드 접기" );
        }
        view.findViewById(R.id.guide_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( isTipOpen ) {
                    weddingTip.setVisibility(View.GONE);
                    guideImg.setImageResource(R.drawable.down_arrow_nor);
                    guideText.setText( weddingTipText + " 가이드 받기" );
                } else {
                    weddingTip.setVisibility(View.VISIBLE);
                    guideImg.setImageResource(R.drawable.up_arrow);
                    guideText.setText( weddingTipText + " 가이드 접기" );
                }
                isTipOpen = !isTipOpen;
            }
        });


        // 웨딩Tip Setting
        weddingtipsWrapper = (LinearLayout) view.findViewById(R.id.weddingtips_wrapper);
        tipController = new TipController();
        getWeddingTipFromServer();
        weddingTip.setOnBottomReachedListener(new CustomHorizontalScrollView.OnBottomReachedListener() {
            @Override
            public void onBottomReached() {
                getWeddingTipFromServer();
            }
        });


        // 추천업체 Setting
        preminumSection = (LinearLayout) view.findViewById(R.id.premium_section);
        companiesWrapper = (LinearLayout)view.findViewById(R.id.companies_wrapper);
        view.findViewById(R.id.preminum_more).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(), DetailCompanyActivity.class );
                intent.putExtra( "categoryName", categoryName );
                getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_DETAILCOMPANY_ACTIVITY );
            }
        });
        companyController = new CompanyController();
        getPremiumsFromServer();
        ((CustomHorizontalScrollView)view.findViewById(R.id.premium_scrollview)).setOnBottomReachedListener(new CustomHorizontalScrollView.OnBottomReachedListener() {
            @Override
            public void onBottomReached() {
                getPremiumsFromServer();
            }
        });



        // 추천유저 Setting
        homeRecommendSection = (LinearLayout)view.findViewById( R.id.homerecommend_section );
        homeRecommendWrapper = (LinearLayout)view.findViewById( R.id.homerecommend_wrapper );
        recommendController = new RecommendController();
        subscriptionController = new SubscriptionController();
        getHomeRecommendFromServer();



        // 최신리얼웨딩 Setting
        recentRealWeddingSection = (LinearLayout) view.findViewById( R.id.recent_realwedding_section );
        recentRealWeddingWrapper = (LinearLayout) view.findViewById( R.id.recent_realwedding_wrapper );
        view.findViewById( R.id.realwedding_more ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(), RealWeddingActivity.class );
                intent.putExtra( "categoryName", categoryName );
                getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_REALWEDDING_ACTIVITY );
            }
        });
        contentController = new ContentController();
        getRecentRealWeddingFromServer();



        // Sector Setting ( 여러개의 Sector가 추가될 수 있음 )
        bannerImgSize = displayWidth; // - SizeConverter.dpToPx( 10 );
        sectorSection = (LinearLayout) view.findViewById( R.id. sector_section );
        sectorController = new SectorController();
        getSectorsFromServer();


        // 리얼웨딩 Setting
        realweddingWrapper = (GridLayout)view.findViewById(R.id.realwedding_wrapper);
        realweddingImgSize = ( displayWidth - SizeConverter.dpToPx( 14 ) ) / 2;
        getContentsFromServer();


        ((CustomScrollView)view.findViewById(R.id.hometab_scrollview)).setOnBottomReachedListener(new CustomScrollView.OnBottomReachedListener() {
            @Override
            public void onBottomReached() {
                if( !isLoadingRealweddingData ) {
                    getContentsFromServer();
                }
            }
        });


        swipeCategory = (SwipeRefreshLayout)view.findViewById(R.id.swipe_category);
        swipeCategory.setColorSchemeResources(
                R.color.colorPrimaryDark, R.color.colorAccent,
                R.color.colorPrimaryDark, R.color.colorAccent);
        swipeCategory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                tipPage = 1;
                getWeddingTipFromServer();

                premiumPage = 1;
                getPremiumsFromServer();

                getHomeRecommendFromServer();

                getRecentRealWeddingFromServer();

                getSectorsFromServer();

                realWeddingPage = 1;
                realweddingWrapper.removeAllViews();
                getContentsFromServer();

                swipeCategory.setRefreshing(false);
            }
        });

        return view;
    }


    private void getPremiumsFromServer() {

        // 첫 Loading or Refresh
        if( premiumPage == 1 ) {
            companiesWrapper.removeAllViews();
        }
        preminumSection.setVisibility( View.VISIBLE );

        Call<CompanyResponse> premiumCall = companyController.getPremiumsInTab( "home", categoryName, premiumPage++ );
        premiumCall.enqueue(new Callback<CompanyResponse>() {
            @Override
            public void onResponse(Call<CompanyResponse> call, Response<CompanyResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    CompanyResponse companyResponse = response.body();

                    if( !ObjectUtils.isEmpty( companyResponse.getCompanies() ) && companyResponse.getCompanies().size() > 0 ) {
                        List<Company> companies = companyResponse.getCompanies();

                        for( Company company : companies ) {

                            if( !ObjectUtils.isEmpty( getActivity() ) ) {
                                CustomCompanyModel customCompanyModel = new CustomCompanyModel( getActivity(), mGlideRequestManager );
                                customCompanyModel.setCompanyThumbnail( company.getThumbnailUrl() );
                                customCompanyModel.setCompanyName( company.getName() );
                                customCompanyModel.setCompanyPostScript( company.getAuxName() );
//                             customCompanyModel.setContentPrice( company.getContent().getReducedCost() );
                                customCompanyModel.setRealWeddingCnt( company.getRealWeddingCount() );
                                customCompanyModel.setSurveyCnt( company.getSurveyCount() );
                                customCompanyModel.setPaymentCnt( company.getPaymentCount() );
                                List<com.jjlee.wedqueen.rest.company.model.Content> contents = company.getContents();
                                if( !ObjectUtils.isEmpty( contents ) && contents.size() > 0 ) {
                                    customCompanyModel.setContentTitle( contents.get( 0 ).getTitle() );
                                    customCompanyModel.setContentImg( contents.get( 0 ).getImageUrl() );
                                    customCompanyModel.getCompanyContentSection().setTag( contents.get( 0 ).getUrl() );
                                    customCompanyModel.getCompanyContentSection().setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            startFullscreenActivity( "" + v.getTag() );
                                        }
                                    });
                                }

                                customCompanyModel.setTag( company.getUrl() );
                                customCompanyModel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startFullscreenActivity( "" + v.getTag() );
                                    }
                                });

                                companiesWrapper.addView( customCompanyModel );
                            }

                        }

                        isLoadLastPremiums = companyResponse.getLastPage();

                    } else {

                        // 추천업체가 존재하지 않는 경우
                        if( !isLoadLastPremiums && premiumPage < 3 ) {
                            preminumSection.setVisibility( View.GONE );
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<CompanyResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private void getRecentRealWeddingFromServer() {

        // 전체 카테고리에서만 해당 Section VISIBLE
        if( categoryId == 9 ) {
            recentRealWeddingSection.setVisibility( View.VISIBLE );

            recentRealWeddingWrapper.removeAllViews();

            Call<ContentResponse> contentCall = contentController.getContents( categoryName, 1, targetTime );
            contentCall.enqueue(new Callback<ContentResponse>() {
                @Override
                public void onResponse(Call<ContentResponse> call, Response<ContentResponse> response) {
                    if( !ObjectUtils.isEmpty( response.body() ) ) {
                        ContentResponse contentResponse = response.body();

                        if (contentResponse.getContents().size() > 0) {
                            List<Content> contents = contentResponse.getContents();

                            for( Content content : contents ) {
                                if( !ObjectUtils.isEmpty( getActivity() ) ) {

                                    CustomRecentRealWeddingModel customRecentRealWeddingModel = new CustomRecentRealWeddingModel( getActivity() );
                                    if( !ObjectUtils.isEmpty( content.getUser().getImageUrl() ) ) {
                                        customRecentRealWeddingModel.setWriterThumbnail( content.getUser().getImageUrl() );
                                        customRecentRealWeddingModel.setWriterName( content.getUser().getNickname() );
                                        customRecentRealWeddingModel.setTag( content.getUser().getId().intValue() );
                                        customRecentRealWeddingModel.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                Intent intent = new Intent( getActivity(), OtherUserDiaryActivity.class);
                                                intent.putExtra( "userId", (int)v.getTag() );
                                                getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_OTHERUSERDIARY_ACTIVITY );
                                            }
                                        });
                                    } else {
                                        customRecentRealWeddingModel.setWriterThumbnail( content.getCompany().getThumbnailUrl() );
                                        customRecentRealWeddingModel.setWriterName( content.getCompany().getName() );
                                        customRecentRealWeddingModel.setTag( content.getCompany().getUrl() );
                                        customRecentRealWeddingModel.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                startFullscreenActivity( v.getTag() + "" );
                                            }
                                        });
                                    }

                                    customRecentRealWeddingModel.setRealWeddingCnt( content.getContentCount() );
                                    customRecentRealWeddingModel.setFollowCnt( content.getSubscriberCount() );

                                    customRecentRealWeddingModel.setContentTitle( content.getTitle() );
                                    customRecentRealWeddingModel.setContentImg( content.getImageUrl() );
                                    customRecentRealWeddingModel.getRealweddingContentSection().setTag( content.getUrl() );
                                    customRecentRealWeddingModel.getRealweddingContentSection().setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            startFullscreenActivity( "" + v.getTag() );
                                        }
                                    });

                                    recentRealWeddingWrapper.addView( customRecentRealWeddingModel );
                                }
                            }
                        }
                    } else {
                        Log.e( "WQTEST3", "recent realwedding response body is null" );
                    }
                }

                @Override
                public void onFailure(Call<ContentResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            recentRealWeddingSection.setVisibility( View.GONE );
        }
    }


    private void getHomeRecommendFromServer() {

        // 전체 카테고리에서만 해당 Section VISIBLE
        if( categoryId == 9 ) {
            homeRecommendSection.setVisibility( View.VISIBLE );

            homeRecommendWrapper.removeAllViews();

            Call<RecommendResponse> recommendCall = recommendController.getHomeRecommendUsers();
            recommendCall.enqueue(new Callback<RecommendResponse>() {
                @Override
                public void onResponse(Call<RecommendResponse> call, Response<RecommendResponse> response) {
                    if( !ObjectUtils.isEmpty( response.body() ) ) {
                        RecommendResponse recommendResponse = response.body();

                        if (recommendResponse != null && recommendResponse.getUsers().size() > 0) {
                            List<User> users = recommendResponse.getUsers();

                            for( User user : users ) {
                                if( !ObjectUtils.isEmpty( getActivity() ) ) {

                                    CustomHomeRecommendModel customHomeRecommendModel = new CustomHomeRecommendModel( getActivity() );
                                    customHomeRecommendModel.setHomeRecommendImg( user.getImageUrl() );
                                    customHomeRecommendModel.setHomeRecommendNickName( user.getNickname() );
                                    customHomeRecommendModel.setHomeRecommendDday( user.getDdayToString() );
                                    customHomeRecommendModel.setFollow( user.isHasSubscription() );
                                    customHomeRecommendModel.setTag( user.getId() );
                                    customHomeRecommendModel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            // 해당 유저 Diary 확인 Activity 오픈
                                            Intent intent = new Intent( getActivity(), OtherUserDiaryActivity.class);
                                            intent.putExtra( "userId", (int)v.getTag() );
                                            getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_OTHERUSERDIARY_ACTIVITY );
                                        }
                                    });
                                    customHomeRecommendModel.getHomeRecommendFollow().setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {

                                                boolean isFollow = (boolean)v.getTag();
                                                CustomHomeRecommendModel targetModel = (CustomHomeRecommendModel)v.getParent().getParent();
                                                int targetUserId = (int) targetModel.getTag();

                                                setFollowUser( isFollow, targetUserId );
                                                ((CustomHomeRecommendModel)v.getParent().getParent()).setFollow( !isFollow );
                                            } else {
                                                Intent intent = new Intent( getActivity(), AccountActivity.class );
                                                intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
                                                intent.putExtra( Constants.LOGIN_FROM, "home_btn_follow" );
                                                getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
                                            }
                                        }
                                    });
                                    homeRecommendWrapper.addView( customHomeRecommendModel );
                                }
                            }
                        }
                    } else {
                        Log.e( "WQTEST3", "home recommend response body is null" );
                    }
                }

                @Override
                public void onFailure(Call<RecommendResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });

        } else {
            homeRecommendSection.setVisibility( View.GONE );
        }
    }


    private void getSectorsFromServer() {

        sectorSection.removeAllViews();

        Call<SectorResponse> sectorCall = sectorController.getSectors( String.valueOf( categoryId ) );
        sectorCall.enqueue(new Callback<SectorResponse>() {
            @Override
            public void onResponse(Call<SectorResponse> call, Response<SectorResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    SectorResponse sectorResponse = response.body();

                    if( !ObjectUtils.isEmpty( sectorResponse.getSectors() ) && sectorResponse.getSectors().size() > 0 ) {
                        List<Sector> sectors = sectorResponse.getSectors();

                        for( Sector sector : sectors ) {
                            if( !ObjectUtils.isEmpty( getActivity() ) ) {

                                List<SectorItem> sectorItems = sector.getItems();

                                if( !ObjectUtils.isEmpty( sectorItems ) && sectorItems.size() > 0 ) {
                                    CustomSectorModel customSectorModel = new CustomSectorModel( getActivity(), bannerImgSize );
                                    String type = sectorItems.get( 0 ).getType();
                                    LinearLayout sectorWrapper = customSectorModel.getSectorWrapper();

                                    customSectorModel.setType( type );
                                    customSectorModel.setSectorTitle( sector.getTitle() );
//                                    customSectorModel.setSectorItems( sectorItems );

                                    for( SectorItem sectorItem : sectorItems ) {
                                        List<String> eventTags = new ArrayList<String>();
                                        List<SectorItemAux> itemAuxes = sectorItem.getItemAux();
                                        for( SectorItemAux itemAux : itemAuxes ) {
                                            if( itemAux.getAuxKey().equals( "eventTag" ) ) {
                                                eventTags.add( itemAux.getAuxValue() );
                                            }
                                        }

                                        if( type.equals( "image" ) ) {

                                            CustomImageGoodsModel customImageGoodsModel = new CustomImageGoodsModel( getActivity() );
                                            customImageGoodsModel.setSectorItemId( sectorItem.getId() );
                                            customImageGoodsModel.setGoodsTitle( sectorItem.getTitle() );
                                            customImageGoodsModel.setGoodsImg( sectorItem.getImageUrl(), SizeConverter.dpToPx( DEFAULT_IMAGEGOODS_HEIGHT ), sectorItem.getImageRatio() );
                                            customImageGoodsModel.setGoodsHashTagWrapper( eventTags );
                                            customImageGoodsModel.setTag( sectorItem.getLinkUrl() );
                                            customImageGoodsModel.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    postSectorItemLogToServer( ((CustomImageGoodsModel)v).getSectorItemId() );
                                                    startFullscreenActivity( "" + v.getTag() );
                                                }
                                            });

                                            sectorWrapper.addView( customImageGoodsModel );
                                        } else if( type.equals( "content" ) ) {

                                            CustomCompanyModel customCompanyModel = new CustomCompanyModel( getActivity() );
                                            customCompanyModel.setSectorItemId( sectorItem.getId() );
                                            customCompanyModel.setContentTitle( sectorItem.getTitle() );
                                            customCompanyModel.setContentImg( sectorItem.getImageUrl() );
                                            customCompanyModel.setContentHashTagWrapper( eventTags );

                                            // 이곳부터 작업 - Company 붙이기
                                            com.jjlee.wedqueen.rest.sector.model.Company itemCompany = sectorItem.getCompany();
                                            if( !ObjectUtils.isEmpty( itemCompany ) && !ObjectUtils.isEmpty( itemCompany.getName() ) ) {
                                                customCompanyModel.setCompanyName( itemCompany.getName() );
                                                customCompanyModel.setCompanyThumbnail( itemCompany.getThumbnailUrl() );
                                                customCompanyModel.setCompanyPostScript( itemCompany.getDescription() );
                                                customCompanyModel.setRealWeddingCnt( itemCompany.getRealWeddingCount() );
                                                customCompanyModel.setSurveyCnt( itemCompany.getSurveyCount() );
                                                customCompanyModel.setPaymentCnt( itemCompany.getPaymentCount() );

                                                customCompanyModel.getCompanyContentSection().setTag( sectorItem.getLinkUrl() );
                                                customCompanyModel.getCompanyContentSection().setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        startFullscreenActivity( "" + v.getTag() );
                                                    }
                                                });
                                                customCompanyModel.setTag( Constants.BASE_URL + itemCompany.getUrl() );
                                            } else {
                                                customCompanyModel.setCompanyInfoGone();
                                                customCompanyModel.setTag( sectorItem.getLinkUrl() );
                                            }
                                            customCompanyModel.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    postSectorItemLogToServer( ((CustomCompanyModel)v).getSectorItemId() );
                                                    startFullscreenActivity( "" + v.getTag() );
                                                }
                                            });

                                            sectorWrapper.addView( customCompanyModel );
                                        } else if( type.equals( "banner" ) ) {

                                            customSectorModel.setSectorItemId( sectorItem.getId() );
                                            customSectorModel.getSectorScrollview().setVisibility( View.GONE );
                                            ImageView bannerImage = customSectorModel.getBannerImage();
                                            bannerImage.setVisibility( View.VISIBLE );

                                            bannerImage.getLayoutParams().width = bannerImgSize;
                                            bannerImage.getLayoutParams().height = (int)( sectorItem.getImageRatio() * bannerImgSize );
                                            Glide.with( getActivity() ).load( sectorItem.getImageUrl() ).centerCrop().placeholder( R.color.image_placeholder_color ).into( bannerImage );
                                            customSectorModel.setTag( sectorItem.getLinkUrl() );
                                            customSectorModel.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    postSectorItemLogToServer( ((CustomSectorModel)v).getSectorItemId() );
                                                    startFullscreenActivity( "" + v.getTag() );
                                                }
                                            });

                                        }
                                    }

                                    sectorSection.addView( customSectorModel );

                                }
                            }
                        }
                    }
                } else {
                    Log.e( "WQTEST3", "sector response body is null" );
                }
            }

            @Override
            public void onFailure(Call<SectorResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private void getContentsFromServer() {

        // 첫 Loading or Refresh
        if( realWeddingPage == 1 ) {
            realweddingWrapper.removeAllViews();
        }

        isLoadingRealweddingData = true;

        Call<ContentResponse> contentCall = contentController.getContents( categoryName, realWeddingPage++, targetTime );
        contentCall.enqueue(new Callback<ContentResponse>() {
            @Override
            public void onResponse(Call<ContentResponse> call, Response<ContentResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    ContentResponse contentResponse = response.body();

                    if( contentResponse.getContents().size() > 0 ) {
                        List<Content> contents = contentResponse.getContents();

                        for( Content content : contents ) {

                            if( !ObjectUtils.isEmpty( getActivity() ) ) {
                                CustomRealWeddingModel customRealWeddingModel = new CustomRealWeddingModel( getActivity() );
                                customRealWeddingModel.setRealweddingSize( realweddingImgSize );
                                customRealWeddingModel.setRealweddingImg( content.getImageUrl() );
                                customRealWeddingModel.setRealweddingEditor( "by " + content.getCompany().getName() );
                                customRealWeddingModel.setRealweddingTitle( content.getTitle() );
                                customRealWeddingModel.setTag( content.getUrl() );
                                customRealWeddingModel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startFullscreenActivity( "" + v.getTag() );
                                    }
                                });
                                realweddingWrapper.addView( customRealWeddingModel );
                            }
                        }
                    }
                } else {
                    Log.e( "WQTEST", "response body is null" );
                }

                isLoadingRealweddingData = false;
            }

            @Override
            public void onFailure(Call<ContentResponse> call, Throwable t) {
                t.printStackTrace();
                isLoadingRealweddingData = false;
            }
        });
    }


    private void getWeddingTipFromServer() {

        // 첫 Loading or Refresh
        if( tipPage == 1 ) {
            weddingtipsWrapper.removeAllViews();
        }

        Call<TipResponse> tipCall = tipController.getTip( "home", tipPage++, String.valueOf( categoryId ) );
        tipCall.enqueue(new Callback<TipResponse>() {
            @Override
            public void onResponse(Call<TipResponse> call, Response<TipResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    TipResponse tipResponse = response.body();

                    Tip tip = tipResponse.getTip();
                    if( !ObjectUtils.isEmpty( tip ) && !ObjectUtils.isEmpty( tip.getContent() ) && tip.getContent().size() > 0 ) {
                        List<com.jjlee.wedqueen.rest.tip.model.Content> contents = tip.getContent();
                        for( com.jjlee.wedqueen.rest.tip.model.Content content : contents ) {

                            if( !ObjectUtils.isEmpty( getActivity() ) ) {
                                CustomWeddingTipModel customWeddingTipModel = new CustomWeddingTipModel( getActivity() );
                                customWeddingTipModel.setWeddingtipTitle( content.getTitle() );
                                customWeddingTipModel.setWeddingtipImg( content.getBgImage() );
                                customWeddingTipModel.setTag( content.getUrl() );
                                customWeddingTipModel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startFullscreenActivity( "" + v.getTag() );
                                    }
                                });

                                weddingtipsWrapper.addView( customWeddingTipModel );
                            }
                        }
                    }

                } else {
                    Log.e( "WQTEST", "response body is null" );
                }
            }

            @Override
            public void onFailure(Call<TipResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private void setFollowUser( boolean isFollow, int targetUserId ) {
        if( isFollow ) {
            Call<SubscriptionResponse> subscriptionCall = subscriptionController.delSubscription( "user", targetUserId );
            subscriptionCall.enqueue(new Callback<SubscriptionResponse>() {
                @Override
                public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                }

                @Override
                public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                }
            });
        } else {
            Call<SubscriptionResponse> subscriptionCall = subscriptionController.postSubscription( "user", targetUserId );
            subscriptionCall.enqueue(new Callback<SubscriptionResponse>() {
                @Override
                public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                }

                @Override
                public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                }
            });
        }
    }


    private void postSectorItemLogToServer( long sectorItemId ) {

        String userId = MyApplication.getGlobalApplicationContext().getUserId();
        if( ObjectUtils.isEmpty( userId ) ) {
            userId = "-1";
        }

        SectorItemLog sectorItemLog = new SectorItemLog( sectorItemId, "click", Long.parseLong( userId ) );
        Call<SectorItemLogResponse> sectorCall = sectorController.postSectorItemLog( sectorItemLog );
        sectorCall.enqueue(new Callback<SectorItemLogResponse>() {
            @Override
            public void onResponse(Call<SectorItemLogResponse> call, Response<SectorItemLogResponse> response) {
            }

            @Override
            public void onFailure(Call<SectorItemLogResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void postTabCategoryLogToServer() {
        String userId = MyApplication.getGlobalApplicationContext().getUserId();
        if( ObjectUtils.isEmpty( userId ) ) {
            userId = "-1";
        }

        TabCategoryLog tabCategoryLog = new TabCategoryLog( categoryId, "android", Long.parseLong( userId ) );
        Call<TabCategoryLogResponse> tabCategoryLogCall = tabCategoryController.postTabCategoryLog( tabCategoryLog );
        tabCategoryLogCall.enqueue(new Callback<TabCategoryLogResponse>() {
            @Override
            public void onResponse(Call<TabCategoryLogResponse> call, Response<TabCategoryLogResponse> response) {
            }

            @Override
            public void onFailure(Call<TabCategoryLogResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
//        recycleAllImageBitmap( realweddingWrapper );
        Runtime.getRuntime().gc();
        super.onDestroyView();
//        onDestroy();
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.with(targetFragment).onLowMemory();
    }

    public void setTipOpen( boolean isTipOpen ) {
        this.isTipOpen = isTipOpen;
    }

    public int getPosition() {
        return position;
    }

    public String getCategoryName() {
        return categoryName;
    }

    // 현재 카테고리 Fragment에서 웨딩팁이 접혀있는지 Check
    public boolean getWeddingTipIsOpen() {
        return isTipOpen;
    }

    private void startFullscreenActivity(String url) {

        if( MyApplication.getGlobalApplicationContext().canLoadDetailContent() ) {
            Intent intent = new Intent(getActivity(), FullscreenActivity.class);
            intent.putExtra("URL", url);
            getActivity().startActivityForResult(intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY);
        } else {
            Intent intent = new Intent( getActivity(), AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            intent.putExtra( Constants.LOGIN_FROM, "home_other_content" );
            getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if( isVisibleToUser ) {
            // user에게 보이는 화면에서만 tabCategoryLog를 전송
            postTabCategoryLogToServer();
        }
    }
}
