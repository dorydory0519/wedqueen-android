package com.jjlee.wedqueen.rest.tutorial.model;

public class TutorialResponse {
    private Tutorial tutorial;


    public Tutorial getTutorial() {
        return tutorial;
    }

    public void setTutorial(Tutorial tutorial) {
        this.tutorial = tutorial;
    }
}

