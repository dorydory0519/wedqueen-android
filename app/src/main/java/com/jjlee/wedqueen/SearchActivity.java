package com.jjlee.wedqueen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.SearchView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.InviteEvent;
import com.crashlytics.android.answers.SearchEvent;
import com.jjlee.wedqueen.CustomViews.CustomHorizontalScrollView;
import com.jjlee.wedqueen.CustomViews.CustomPopularImgModel;
import com.jjlee.wedqueen.CustomViews.CustomRecommendUserModel;
import com.jjlee.wedqueen.CustomViews.CustomWeddingTipModel;
import com.jjlee.wedqueen.rest.autocomplete.model.AutoCompleteSet;
import com.jjlee.wedqueen.rest.autocomplete.model.Company;
import com.jjlee.wedqueen.rest.autocomplete.model.HashTag;
import com.jjlee.wedqueen.rest.content.controller.ContentController;
import com.jjlee.wedqueen.rest.content.model.Image;
import com.jjlee.wedqueen.rest.content.model.ImageResponse;
import com.jjlee.wedqueen.rest.guide.controller.GuideController;
import com.jjlee.wedqueen.rest.guide.model.GuideResponse;
import com.jjlee.wedqueen.rest.recommend.controller.RecommendController;
import com.jjlee.wedqueen.rest.recommend.model.RecommendResponse;
import com.jjlee.wedqueen.rest.recommend.model.User;
import com.jjlee.wedqueen.rest.tip.controller.TipController;
import com.jjlee.wedqueen.rest.tip.model.Tip;
import com.jjlee.wedqueen.rest.tip.model.TipResponse;
import com.jjlee.wedqueen.support.AutoCompleteAdapter;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.CustomScrollView;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    private RequestManager mGlideRequestManager;

    private android.widget.SearchView searchView;
    private GuideController guideController;

    private SwipeRefreshLayout swipeSearch;
    private CustomScrollView searchtabScrollview;

    private RecommendController recommendController;
    private LinearLayout recommendWrapper;

    private TipController tipController;
    private LinearLayout adWrapper;
    private int tipPage = 1;

    private ContentController contentController;
    private GridLayout popularWrapper;
    private int popularImgSize;

    private int popularImgPage = 1;

    private boolean isLoadingImageData;

    private AutoCompleteAdapter autoCompleteAdapter;
    private AutoCompleteTextView autoCompleteTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_search);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "search" ) );

        mGlideRequestManager = Glide.with( this );

        isLoadingImageData = false;

        searchtabScrollview = (CustomScrollView)findViewById(R.id.searchtab_scrollview);

        searchView = (android.widget.SearchView)findViewById(R.id.search_view);
        autoCompleteTextView = getAutoCompleteTextViewInSearchView( searchView );
        autoCompleteTextView.setTextSize( TypedValue.COMPLEX_UNIT_DIP, 12 );

        autoCompleteAdapter = new AutoCompleteAdapter( this, R.layout.layout_search_dropdown, "search" );
        autoCompleteTextView.setAdapter( autoCompleteAdapter );
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                AutoCompleteSet autoCompleteSet = autoCompleteAdapter.getItem( position );
                String selectedName = "";
                if( autoCompleteSet instanceof Company) {
                    selectedName = ((Company) autoCompleteSet).getName();
                    autoCompleteTextView.setText( selectedName );
                    startFullscreenActivity( ((Company) autoCompleteSet).getUrl() );
                } else if( autoCompleteSet instanceof HashTag) {
                    selectedName = ((HashTag) autoCompleteSet).getName();
                    autoCompleteTextView.setText( selectedName );
                    startFullscreenActivity( ((HashTag) autoCompleteSet).getUrl() );
                } else if( autoCompleteSet instanceof com.jjlee.wedqueen.rest.autocomplete.model.User ) {
                    selectedName = ((com.jjlee.wedqueen.rest.autocomplete.model.User) autoCompleteSet).getName();
                    autoCompleteTextView.setText( selectedName );
                    int targetUserId = ((com.jjlee.wedqueen.rest.autocomplete.model.User) autoCompleteSet).getUserId();
                    startOtherUserDiaryActivity( targetUserId );
                }

            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                startFullscreenActivity( Constants.SEARCH_URL + "?query=" + query );
                Answers.getInstance().logSearch( new SearchEvent().putQuery( query ) );
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


        guideController = new GuideController();
        Call<GuideResponse> guideCall = guideController.getKeywordGuide();
        guideCall.enqueue(new Callback<GuideResponse>() {
            @Override
            public void onResponse(Call<GuideResponse> call, Response<GuideResponse> response) {
                if( response.body() != null ) {
                    GuideResponse guideResponse = response.body();

                    if (guideResponse != null && guideResponse.getKeywordGuide() != null ) {
                        searchView.setQueryHint( guideResponse.getKeywordGuide().getKeyword() );
                    }
                }
            }

            @Override
            public void onFailure(Call<GuideResponse> call, Throwable t) {

            }
        });


        recommendWrapper = (LinearLayout)findViewById(R.id.recommend_wrapper);
        recommendController = new RecommendController();
        getRecommendUserFromServer();


        // 이미지 모음 Label Add
        contentController = new ContentController();
        popularWrapper = (GridLayout)findViewById(R.id.popular_wrapper);
        popularImgSize = ( ( getWindowManager().getDefaultDisplay().getWidth() - SizeConverter.dpToPx( 8 ) ) / 3 );
        getImagesFromServer();

        ((CustomScrollView)findViewById(R.id.searchtab_scrollview)).setOnBottomReachedListener(new CustomScrollView.OnBottomReachedListener() {
            @Override
            public void onBottomReached() {
                if( !isLoadingImageData ) {
                    getImagesFromServer();
                }
            }
        });


        // 웨딩Tip Setting
        adWrapper = (LinearLayout)findViewById(R.id.ad_wrapper);
        tipController = new TipController();
        getWeddingTipFromServer();

        ((CustomHorizontalScrollView)findViewById(R.id.ad_section)).setOnBottomReachedListener(new CustomHorizontalScrollView.OnBottomReachedListener() {
            @Override
            public void onBottomReached() {
                getWeddingTipFromServer();
            }
        });


        swipeSearch = (SwipeRefreshLayout)findViewById(R.id.swipe_search);
        swipeSearch.setColorSchemeResources(
                R.color.colorPrimaryDark, R.color.colorAccent,
                R.color.colorPrimaryDark, R.color.colorAccent);
        swipeSearch.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getRecommendUserFromServer();

                popularImgPage = 1;
                getImagesFromServer();

                tipPage = 1;
                getWeddingTipFromServer();

                swipeSearch.setRefreshing(false);
            }
        });
    }


    private void getImagesFromServer() {

        // 첫 Loading or Refresh
        if( popularImgPage == 1 ) {
            popularWrapper.removeAllViews();
        }

        isLoadingImageData = true;

        Call<ImageResponse> imageCall = contentController.getImages( popularImgPage++ );
        imageCall.enqueue(new Callback<ImageResponse>() {
            @Override
            public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    ImageResponse imageResponse = response.body();

                    if( imageResponse != null && imageResponse.getImages().size() > 0) {
                        List<Image> images = imageResponse.getImages();

                        for( Image image : images ) {
                            CustomPopularImgModel customPopularImgModel = new CustomPopularImgModel( SearchActivity.this, mGlideRequestManager );
                            customPopularImgModel.setPopularImgImg( image.getSource() );
                            List<String> hashTags = image.getHashTags();
                            String strHashTag = "";
                            for( String hashTag : hashTags ) {
                                strHashTag += "#" + hashTag + "  ";
                            }
                            customPopularImgModel.setPopularImgHashTag( strHashTag );
                            customPopularImgModel.setPopularImgSize( popularImgSize );
                            customPopularImgModel.setTag( image.getUrl() );
//                                Log.e( "WQTEST3", "contentId : " + image.getContentId() );
                            customPopularImgModel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startFullscreenActivity( "" + v.getTag() );
                                }
                            });
                            popularWrapper.addView( customPopularImgModel );
                        }
                    }
                } else {
                    Log.e( "WQTEST", "response body is null" );
                }
                isLoadingImageData = false;
            }

            @Override
            public void onFailure(Call<ImageResponse> call, Throwable t) {
                t.printStackTrace();
                isLoadingImageData = false;
            }
        });
    }

    private void getWeddingTipFromServer() {

        // 첫 Loading or Refresh
        if( tipPage == 1 ) {
            adWrapper.removeAllViews();
        }

        Call<TipResponse> tipCall = tipController.getTip( "search", tipPage++, "" );
        tipCall.enqueue(new Callback<TipResponse>() {
            @Override
            public void onResponse(Call<TipResponse> call, Response<TipResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    TipResponse tipResponse = response.body();

                    Tip tip = tipResponse.getTip();
                    if( !ObjectUtils.isEmpty( tip ) && !ObjectUtils.isEmpty( tip.getContent() ) && tip.getContent().size() > 0 ) {
                        List<com.jjlee.wedqueen.rest.tip.model.Content> contents = tip.getContent();
                        for( com.jjlee.wedqueen.rest.tip.model.Content content : contents ) {
                            CustomWeddingTipModel customWeddingTipModel = new CustomWeddingTipModel( SearchActivity.this );
                            customWeddingTipModel.setWeddingtipTitle( content.getTitle() );
                            customWeddingTipModel.setWeddingtipImg( content.getBgImage() );
                            adWrapper.addView( customWeddingTipModel );
                        }
                    }

                } else {
                    Log.e( "WQTEST", "response body is null" );
                }
            }

            @Override
            public void onFailure(Call<TipResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void getRecommendUserFromServer() {

        recommendWrapper.removeAllViews();

        Call<RecommendResponse> recommendCall = recommendController.getRecommendUsers();
        recommendCall.enqueue(new Callback<RecommendResponse>() {
            @Override
            public void onResponse(Call<RecommendResponse> call, Response<RecommendResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    RecommendResponse recommendResponse = response.body();

                    if (recommendResponse != null && recommendResponse.getUsers().size() > 0) {
                        List<User> users = recommendResponse.getUsers();

                        for( User user : users ) {
                            CustomRecommendUserModel customRecommendUserModel = new CustomRecommendUserModel( SearchActivity.this, mGlideRequestManager );
                            customRecommendUserModel.setRecommendUserImg( user.getImageUrl() );
                            customRecommendUserModel.setRecommendUserNickName( user.getNickname() );
                            customRecommendUserModel.setTag( user.getId() );
                            customRecommendUserModel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // 해당 유저 Diary 확인 Activity 오픈
                                    startOtherUserDiaryActivity( (int)v.getTag() );
                                }
                            });
                            recommendWrapper.addView( customRecommendUserModel );
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<RecommendResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private AutoCompleteTextView getAutoCompleteTextViewInSearchView(android.widget.SearchView searchView) {
        try {
            AutoCompleteTextView autoCompleteTextViewSearch = (AutoCompleteTextView) searchView.findViewById(searchView.getContext().getResources().getIdentifier("app:id/search_src_text", null, null));
            if (autoCompleteTextViewSearch != null) {
                return autoCompleteTextViewSearch;
            } else {
                LinearLayout linearLayout1 = (LinearLayout) searchView.getChildAt(0);
                LinearLayout linearLayout2 = (LinearLayout) linearLayout1.getChildAt(2);
                LinearLayout linearLayout3 = (LinearLayout) linearLayout2.getChildAt(1);
                AutoCompleteTextView autoComplete = (AutoCompleteTextView) linearLayout3.getChildAt(0);
                return autoComplete;
            }
        } catch (Exception e) {
            LinearLayout linearLayout1 = (LinearLayout) searchView.getChildAt(0);
            LinearLayout linearLayout2 = (LinearLayout) linearLayout1.getChildAt(2);
            LinearLayout linearLayout3 = (LinearLayout) linearLayout2.getChildAt(1);
            AutoCompleteTextView autoComplete = (AutoCompleteTextView) linearLayout3.getChildAt(0);
            return autoComplete;
        }
    }

    private void startFullscreenActivity(String url) {

        if( MyApplication.getGlobalApplicationContext().canLoadDetailContent() ) {
            Intent intent = new Intent( this, FullscreenActivity.class );
            intent.putExtra( "URL", url );
            startActivity( intent );
        } else {
            Intent intent = new Intent( this, AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            startActivity( intent );
        }

    }

    private void startOtherUserDiaryActivity(int userId) {
        Intent intent = new Intent( this, OtherUserDiaryActivity.class);
        intent.putExtra( "userId", userId );
        startActivityForResult( intent, Constants.REQ_CODE_OPEN_OTHERUSERDIARY_ACTIVITY );
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
