package com.jjlee.wedqueen;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.ValueCallback;
import android.widget.ImageButton;
import android.widget.Toast;

import com.jjlee.wedqueen.rest.image.controller.ImageController;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.ProgressRequestBody;

import org.json.JSONArray;

import java.util.HashMap;

/**
 * Created by JJLEE on 2016. 4. 5..
 */
public class WebviewContainingActivity extends AppCompatActivity {
    public String mCallbackFunctionName;
    public String mRequestPageType; // image/upload?type=mRequestPageType
    public String mMimeType;

    public JSONArray toolbarItemsJson;

    public ImageButton backButton;
    public ImageButton homeButton;

    public ImageButton shareButton;
    public ImageButton lovedOnButton;
    public ImageButton lovedOffButton;
    public ImageButton editButton;
    public ImageButton deleteButton;
    public ImageButton editContentButton;
    public ImageButton deleteContentButton;
    public ImageButton cancelContentButton;

    public ImageButton phoneButton;
    public ImageButton talkButton;
    public ImageButton locationButton;
    public ImageButton homepageButton;

    public ImageController imageController;

//    public FloatingActionButton fabButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null) {
            MyApplication application = MyApplication.getGlobalApplicationContext();

            String userImageUrl = savedInstanceState.getString("userImageUrl");
            int badgeCount = savedInstanceState.getInt("badgeCount");
            boolean isLoggedIn = savedInstanceState.getBoolean("isLoggedIn");
            boolean isLatestVersion = savedInstanceState.getBoolean("isLatestVersion");


            application.setUserImageUrl(userImageUrl);
            if(MainActivity.instance != null) {
                MainActivity.instance.setMypageIcon();
            }
            application.setBadgeCount(badgeCount);
            application.setIsLoggedIn(isLoggedIn);
            application.setIsLatestVersion(isLatestVersion);

        }

        imageController = new ImageController();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        MyApplication application = MyApplication.getGlobalApplicationContext();
//        outState.putSerializable("tabCategories", Constants.TAB_CATEGORIES_NAME);
        outState.putString("userImageUrl", application.getUserImageUrl());
        outState.putInt("badgeCount", application.getBadgeCount());
        outState.putBoolean("isLoggedIn", application.isLoggedIn());
        outState.putBoolean("isLatestVersion", application.isLatestVersion());
        super.onSaveInstanceState(outState);
    }

    public void tryOpenGallery(String callbackFunctionName, String requestPageType, String mimeType) {

        this.mCallbackFunctionName = callbackFunctionName;
        this.mRequestPageType = requestPageType;
        this.mMimeType = mimeType;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                    Constants.REQ_EXTERNAL_STORAGE_PERMISSION);
        } else {
            openFileChooserManually();
        }
    }

    public void openFileChooserManually() {
        Intent intent = new Intent();
        if(mRequestPageType.equals("weddingTalk") || mRequestPageType.equals("profile")) {
            intent.setType("image/*");
        } else {
            if(mMimeType == null) {
                intent.setType("*/*");
            } else if(mMimeType.equals("image")) {
                intent.setType("image/*");
            } else if(mMimeType.equals("video/mp4")) {
                intent.setType("video/mp4");
            } else if(mMimeType.equals("video")) {
                intent.setType("video/*");
            } else {
                intent.setType("*/*");
            }

        }

        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        this.startActivityForResult(Intent.createChooser(intent, "파일을 고르세요"), Constants.FILE_CHOOSE);
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions, @NonNull final int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.REQ_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                openFileChooserManually ();
            } else {
                Toast.makeText(this, "권한을 허가하서야 파일을 올리실 수 있습니다", Toast.LENGTH_LONG).show();
            }
        }
    }
}
