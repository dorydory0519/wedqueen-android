package com.jjlee.wedqueen.rest.section.controller;

import com.jjlee.wedqueen.rest.section.model.Section;
import com.jjlee.wedqueen.rest.section.model.SectionResponse;
import com.jjlee.wedqueen.rest.section.service.SectionService;
import com.jjlee.wedqueen.rest.sector.model.SectorItemLog;
import com.jjlee.wedqueen.rest.sector.model.SectorItemLogResponse;
import com.jjlee.wedqueen.rest.sector.model.SectorResponse;
import com.jjlee.wedqueen.rest.sector.service.SectorLogService;
import com.jjlee.wedqueen.rest.sector.service.SectorService;
import com.jjlee.wedqueen.support.RetrofitController;

import java.util.List;

import retrofit2.Call;

public class SectionController extends RetrofitController {
    private SectionService sectionService;

    public SectionController() {
        super();
        sectionService = RETROFIT.create(SectionService.class);
    }

    public Call<SectionResponse> getSections( String tabType ) {
        return sectionService.getSection( tabType );
    }

}
