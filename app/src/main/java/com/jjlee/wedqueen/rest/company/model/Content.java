package com.jjlee.wedqueen.rest.company.model;

import java.io.Serializable;

/**
 * Created by LeeJungYi on 2016-04-01.
 */
public class Content implements Serializable {

    private int id;
    private String title;
//    private int reducedCost;
    private String imageUrl;
    private String url;

    private String viewCountToString;
    private boolean loved;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    public int getReducedCost() {
//        return reducedCost;
//    }
//
//    public void setReducedCost(int reducedCost) {
//        this.reducedCost = reducedCost;
//    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getViewCountToString() {
        return viewCountToString;
    }

    public void setViewCountToString(String viewCountToString) {
        this.viewCountToString = viewCountToString;
    }

    public boolean isLoved() {
        return loved;
    }

    public void setLoved(boolean loved) {
        this.loved = loved;
    }
}
