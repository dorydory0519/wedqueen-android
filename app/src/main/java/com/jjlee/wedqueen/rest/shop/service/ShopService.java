package com.jjlee.wedqueen.rest.shop.service;

import com.jjlee.wedqueen.rest.sector.model.SectorResponse;
import com.jjlee.wedqueen.rest.shop.model.ShopResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ShopService {
    @Headers("Accept: application/json")
    @GET("api/v1/shop/gift")
    Call<ShopResponse> getGifts(@Query("filter") String filter);

    @Headers("Accept: application/json")
    @GET("api/v1/shop/premium")
    Call<SectorResponse> getPremiumGifts();

}
