package com.jjlee.wedqueen.rest.recommend.model;

import java.io.Serializable;

public class User implements Serializable {

    private int id;
    private String nickname;
    private String imageUrl;
    private String level;
    private boolean hasSubscription;
    private String ddayToString;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public boolean isHasSubscription() {
        return hasSubscription;
    }

    public void setHasSubscription(boolean hasSubscription) {
        this.hasSubscription = hasSubscription;
    }

    public String getDdayToString() {
        return ddayToString;
    }

    public void setDdayToString(String ddayToString) {
        this.ddayToString = ddayToString;
    }
}
