package com.jjlee.wedqueen.rest.popup.model;

import java.util.List;

public class PopupResponse {
    private List<Popup> popups;
    private String ment;
    private int term;

    public List<Popup> getPopups() {
        return popups;
    }

    public void setPopups(List<Popup> popups) {
        this.popups = popups;
    }

    public String getMent() {
        return ment;
    }

    public void setMent(String ment) {
        this.ment = ment;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }
}

