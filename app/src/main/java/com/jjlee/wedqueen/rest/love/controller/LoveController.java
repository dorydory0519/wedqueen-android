package com.jjlee.wedqueen.rest.love.controller;


import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.rest.love.service.LoveService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

/**
 * Created by JJLEE on 2016. 5. 7..
 */
public class LoveController extends RetrofitController {
    private LoveService loveService;

    public LoveController() {
        super();
        loveService = RETROFIT.create(LoveService.class);
    }

    public Call<CommonResponse> loveOn(Long lovableId, String lovableType) {
        return loveService.loveRequest(lovableId, lovableType, "POST");
    }

    public Call<CommonResponse> loveOff(Long lovableId, String lovableType) {
        return loveService.loveRequest(lovableId, lovableType, "DELETE");
    }
}
