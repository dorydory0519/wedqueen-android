package com.jjlee.wedqueen.rest.tutorial.service;

import com.jjlee.wedqueen.rest.tutorial.model.TutorialResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface TutorialService {

    @Headers("Accept: application/json")
    @GET("api/v1/tutorial/{userAgent}")
    Call<TutorialResponse> getTutorial(@Path("userAgent") String userAgent);

}
