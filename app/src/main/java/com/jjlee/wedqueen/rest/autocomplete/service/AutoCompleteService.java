package com.jjlee.wedqueen.rest.autocomplete.service;

import com.jjlee.wedqueen.rest.autocomplete.model.AutoCompleteResponse;
import com.jjlee.wedqueen.rest.company.model.CompanyResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by LeeJungYi on 2016-04-01.
 */
public interface AutoCompleteService {
    @Headers("Accept: application/json")

    @GET("api/v1/company/autocomplete")
    Call<AutoCompleteResponse> getAutoCompletedCompanies(@Query("keyword") String keyword);

}
