package com.jjlee.wedqueen.rest.user.service;

import com.jjlee.wedqueen.rest.common.model.AppResponse;
import com.jjlee.wedqueen.rest.user.model.GetUserResponse;
import com.jjlee.wedqueen.rest.user.model.InvitationResponse;
import com.jjlee.wedqueen.rest.user.model.PostUserInfoResponse;
import com.jjlee.wedqueen.rest.user.model.UserContentResponse;
import com.jjlee.wedqueen.rest.user.model.UserInfoForm;
import com.jjlee.wedqueen.rest.user.model.UserInfoResponse;
import com.jjlee.wedqueen.rest.user.model.UserResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserService {
    @Headers("Accept: application/json")
    @GET("api/v1/user/content")
    Call<UserContentResponse> getContents();

    @Headers("Accept: application/json")
    @GET("api/v1/user")
    Call<GetUserResponse> getUser(@Query("_") String timeMilli);

    @Headers("Accept: application/json")
    @GET("api/v1/user/{id}")
    Call<UserResponse> getUser(@Path("id") long id);

    @Headers("Accept: application/json")
    @GET("api/v1/user/info")
    Call<UserInfoResponse> getUserInfo();

    @Headers("Accept: application/json")
    @POST("api/v1/user/info")
    Call<PostUserInfoResponse> postUserInfo(@Body UserInfoForm userInfoForm);

    @Headers("Accept: application/json")
    @PUT("api/v1/user/info")
    Call<PostUserInfoResponse> putUserInfo(@Body UserInfoForm userInfoForm);

    @GET("/user/invitation")
    Call<InvitationResponse> getCoupleInvitationCode();

    @POST("/user/invitation/match")
    @FormUrlEncoded
    Call<AppResponse> matchCoupleInvitation(@FieldMap Map<String, String> map);

    @POST("/user/invitation/disconnect")
    Call<AppResponse> disconnectCouple();
}
