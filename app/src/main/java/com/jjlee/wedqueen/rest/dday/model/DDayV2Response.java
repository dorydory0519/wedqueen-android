package com.jjlee.wedqueen.rest.dday.model;

public class DDayV2Response {
    private DDayDto ddayData;

    public DDayDto getDdayData() {
        return ddayData;
    }

    public void setDdayData(DDayDto ddayData) {
        this.ddayData = ddayData;
    }
}
