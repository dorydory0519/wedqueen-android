package com.jjlee.wedqueen.rest.app.model;

/**
 * Created by JJLEE on 2016. 4. 12..
 */
public class VersionResponse {
    private boolean mandatory;
    private boolean latest;
    private boolean loggedIn;

    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public boolean isLatest() {
        return latest;
    }

    public void setLatest(boolean latest) {
        this.latest = latest;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

}
