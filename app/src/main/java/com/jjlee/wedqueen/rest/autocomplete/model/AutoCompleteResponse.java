package com.jjlee.wedqueen.rest.autocomplete.model;

import java.util.List;

/**
 * Created by esmac on 2017. 9. 21..
 */

public class AutoCompleteResponse {
    List<Company> companies;

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }
}
