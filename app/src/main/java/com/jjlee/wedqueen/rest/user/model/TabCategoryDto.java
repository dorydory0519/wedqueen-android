package com.jjlee.wedqueen.rest.user.model;

/**
 * Created by leejungyi on 2017. 10. 22..
 */

public class TabCategoryDto {

    private Long id;
    private Long position;

    private String categoryNameKor;
    private String categoryName;
    private String tabType;

    private boolean appVisible;
    private boolean selected;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public String getCategoryNameKor() {
        return categoryNameKor;
    }

    public void setCategoryNameKor(String categoryNameKor) {
        this.categoryNameKor = categoryNameKor;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTabType() {
        return tabType;
    }

    public void setTabType(String tabType) {
        this.tabType = tabType;
    }

    public boolean isAppVisible() {
        return appVisible;
    }

    public void setAppVisible(boolean appVisible) {
        this.appVisible = appVisible;
    }

    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
