package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jjlee.wedqueen.R;

/**
 * Created by esmac on 2017. 9. 9..
 */

public class CustomWeddingTipModel extends RelativeLayout {

    ImageView weddingtipImg;
    TextView weddingtipTitle;

    public CustomWeddingTipModel(Context context) {
        super(context);
        initView();

    }

    public CustomWeddingTipModel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomWeddingTipModel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_weddingtips, this, false);
        addView(v);

        weddingtipImg = (ImageView)findViewById(R.id.weddingtip_img);
        weddingtipTitle = (TextView)findViewById(R.id.weddingtip_title);

    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.WeddingTipModel);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.WeddingTipModel, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {

        String weddingtipImg_resID = typedArray.getString(R.styleable.WeddingTipModel_weddingtip_img);
        setWeddingtipImg( weddingtipImg_resID );

        String weddingtipTitle_resID = typedArray.getString(R.styleable.WeddingTipModel_weddingtip_title);
        setWeddingtipTitle( weddingtipTitle_resID );

        typedArray.recycle();
    }

    public void setWeddingtipImg(String resUrl) {
        Glide.with(getContext()).load(resUrl).centerCrop().placeholder( R.color.image_placeholder_color ).into( weddingtipImg );
        weddingtipImg.setColorFilter(Color.parseColor("#BDBDBD"), PorterDuff.Mode.MULTIPLY);
    }
    public void setWeddingtipTitle(String resString) {
        weddingtipTitle.setText( resString );
    }
}
