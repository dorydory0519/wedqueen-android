package com.jjlee.wedqueen.rest.sector.controller;

import com.jjlee.wedqueen.rest.sector.model.SectorItemLog;
import com.jjlee.wedqueen.rest.sector.model.SectorItemLogResponse;
import com.jjlee.wedqueen.rest.sector.model.SectorResponse;
import com.jjlee.wedqueen.rest.sector.service.SectorLogService;
import com.jjlee.wedqueen.rest.sector.service.SectorService;
import com.jjlee.wedqueen.support.RetrofitController;

import java.util.List;

import retrofit2.Call;

public class SectorController extends RetrofitController {
    private SectorService sectorService;
    private SectorLogService sectorLogService;

    public SectorController() {
        super();
        sectorService = RETROFIT.create(SectorService.class);
        sectorLogService = LOGRETROFIT.create(SectorLogService.class);
    }

    public Call<SectorResponse> getSectors( String tabCategoryId ) {
        return sectorService.getSectors( tabCategoryId );
    }

    public Call<SectorResponse> getInviteSectors() {
        return sectorService.getInviteSectors();
    }

    public Call<SectorItemLogResponse> postSectorItemLog( SectorItemLog sectorItemLog ) {
        return sectorLogService.postSectorItemLog( sectorItemLog );
    }

}
