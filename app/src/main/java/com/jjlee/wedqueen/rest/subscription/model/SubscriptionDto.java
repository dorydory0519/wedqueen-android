package com.jjlee.wedqueen.rest.subscription.model;

/**
 * Created by esmac on 2018. 3. 12..
 */

public class SubscriptionDto {
    private int subscriptionableId;
    private String thumbnailUrl;
    private String url;
    private String uri;
    private String name;
    private boolean hasSubscription;
    private String ddayToString;

    public int getSubscriptionableId() {
        return subscriptionableId;
    }

    public void setSubscriptionableId(int subscriptionableId) {
        this.subscriptionableId = subscriptionableId;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHasSubscription() {
        return hasSubscription;
    }

    public void setHasSubscription(boolean hasSubscription) {
        this.hasSubscription = hasSubscription;
    }

    public String getDdayToString() {
        return ddayToString;
    }

    public void setDdayToString(String ddayToString) {
        this.ddayToString = ddayToString;
    }
}
