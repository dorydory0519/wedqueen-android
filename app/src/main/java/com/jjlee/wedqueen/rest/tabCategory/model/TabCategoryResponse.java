package com.jjlee.wedqueen.rest.tabCategory.model;

import java.util.List;

public class TabCategoryResponse {
    private List<TabCategory> tabcategories;

    public List<TabCategory> getTabCategories() {
        return tabcategories;
    }

    public void setTabCategories(List<TabCategory> tabcategories) {
        this.tabcategories = tabcategories;
    }
}
