package com.jjlee.wedqueen.rest.user.model;

public class GetUserResponse {
    private boolean isCouple;
    private User user;

    public boolean isCouple() {
        return isCouple;
    }

    public void setCouple(boolean couple) {
        isCouple = couple;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
