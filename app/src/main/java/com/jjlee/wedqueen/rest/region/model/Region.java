package com.jjlee.wedqueen.rest.region.model;

import java.io.Serializable;

public class Region implements Serializable {

    private int index;
    private String korRegion;
    private String engRegion;
    private String korPrecinct;
    private String engPrecinct;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getKorRegion() {
        return korRegion;
    }

    public void setKorRegion(String korRegion) {
        this.korRegion = korRegion;
    }

    public String getEngRegion() {
        return engRegion;
    }

    public void setEngRegion(String engRegion) {
        this.engRegion = engRegion;
    }

    public String getKorPrecinct() {
        return korPrecinct;
    }

    public void setKorPrecinct(String korPrecinct) {
        this.korPrecinct = korPrecinct;
    }

    public String getEngPrecinct() {
        return engPrecinct;
    }

    public void setEngPrecinct(String engPrecinct) {
        this.engPrecinct = engPrecinct;
    }
}
