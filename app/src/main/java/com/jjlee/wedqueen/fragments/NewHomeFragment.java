package com.jjlee.wedqueen.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.jjlee.wedqueen.AccountActivity;
import com.jjlee.wedqueen.CategoryHomeActivity;
import com.jjlee.wedqueen.CompanyMapActivity;
import com.jjlee.wedqueen.CustomViews.CustomCategoryIcon;
import com.jjlee.wedqueen.CustomViews.CustomEventModel;
import com.jjlee.wedqueen.CustomViews.CustomHonorUserModel;
import com.jjlee.wedqueen.CustomViews.CustomPremiumAdModel;
import com.jjlee.wedqueen.CustomViews.CustomRealReviewModel;
import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MainActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.OtherUserDiaryActivity;
import com.jjlee.wedqueen.PremiumAdsActivity;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.RealWeddingActivity;
import com.jjlee.wedqueen.SearchActivity;
import com.jjlee.wedqueen.rest.app.model.TabCategory;
import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.rest.company.controller.CompanyController;
import com.jjlee.wedqueen.rest.content.controller.ContentController;
import com.jjlee.wedqueen.rest.content.model.Content;
import com.jjlee.wedqueen.rest.content.model.ContentResponse;
import com.jjlee.wedqueen.rest.content.model.RealWeddingContentDto;
import com.jjlee.wedqueen.rest.dday.controller.DDayController;
import com.jjlee.wedqueen.rest.dday.model.DDayDto;
import com.jjlee.wedqueen.rest.dday.model.DDayV2Response;
import com.jjlee.wedqueen.rest.honor.controller.HonorController;
import com.jjlee.wedqueen.rest.honor.model.HonorUserResponse;
import com.jjlee.wedqueen.rest.love.controller.LoveController;
import com.jjlee.wedqueen.rest.premium.controller.PremiumController;
import com.jjlee.wedqueen.rest.premium.model.PremiumAd;
import com.jjlee.wedqueen.rest.premium.model.PremiumAdLog;
import com.jjlee.wedqueen.rest.premium.model.PremiumAdLogResponse;
import com.jjlee.wedqueen.rest.section.controller.SectionController;
import com.jjlee.wedqueen.rest.section.model.Section;
import com.jjlee.wedqueen.rest.section.model.SectionResponse;
import com.jjlee.wedqueen.rest.subscription.controller.SubscriptionController;
import com.jjlee.wedqueen.rest.subscription.model.SubscriptionResponse;
import com.jjlee.wedqueen.rest.tabCategory.controller.TabCategoryController;
import com.jjlee.wedqueen.rest.tabCategory.model.TabCategoryResponse;
import com.jjlee.wedqueen.rest.user.model.UserRestDto;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.utils.CalendarUtil;
import com.jjlee.wedqueen.utils.DecimalUtils;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class NewHomeFragment extends Fragment {

    private int displayWidth;
    private Prefs prefs;
    private RequestManager mGlideRequestManager;

    private NestedScrollView newhometabScrollview;

    private TabCategoryController tabCategoryController;
    private SectionController sectionController;
    private LoveController loveController;
    private PremiumController premiumController;
    private ContentController contentController;
    private CompanyController companyController;
    private HonorController honorController;
    private SubscriptionController subscriptionController;
    private DDayController dDayController;

    private Toolbar mToolbar;
    private SwipeRefreshLayout newhomeRefresh;

    private TextView myDday;
    private TextView myCbudgetLabel;
    private TextView myCbudget;
    private TextView budgetUnit;

    private ViewPager premiumAdViewPager;
    private CircleIndicator premiumAdIndicator;
    private LinearLayout bannerWrapper;
    private List<PremiumAd> premiumAds;

    private TextView bestRealreviewTitle;
    private TextView bestRealreviewDesc;
    private TextView eventTitle;
    private TextView eventDesc;
    private TextView honorTitle;
    private TextView honorDesc;

    private GridLayout categoryWrapper;
    private GridLayout realreviewWrapper;
    private LinearLayout eventWrapper;
    private LinearLayout honorUserWrapper;

    private HorizontalScrollView horizontalScrollView;

    private HashMap<String, ArrayList<TabCategory>> allTabCategories;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGlideRequestManager = Glide.with( this );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_newhome, container, false);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics( displayMetrics );
        displayWidth = displayMetrics.widthPixels;

//        mToolbar = (Toolbar) view.findViewById( R.id.newhome_toolbar );
//        ((AppCompatActivity)getActivity()).setSupportActionBar( mToolbar );

        prefs = new Prefs( getActivity() );


        newhometabScrollview = (NestedScrollView) view.findViewById( R.id.newhometab_scrollview );

        dDayController = new DDayController();
        myDday = (TextView)view.findViewById( R.id.my_dday );
        myCbudgetLabel = (TextView)view.findViewById( R.id.my_cbudget_label );
        myCbudget = (TextView)view.findViewById( R.id.my_cbudget );
        budgetUnit = view.findViewById( R.id.budget_unit );
        setMyInfo();

        view.findViewById( R.id.show_all_banner ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
                    Intent intent = new Intent( getActivity(), PremiumAdsActivity.class );
                    startActivity( intent );
                } else {
                    Intent intent = new Intent( getActivity(), AccountActivity.class );
                    intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
                    intent.putExtra( Constants.LOGIN_FROM, "home_all_banner" );
                    getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
                }
            }
        });

        view.findViewById( R.id.dday_section ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
                    ((MainActivity)getActivity()).changeViewPagerTo( 2 );
                } else {
                    Intent intent = new Intent( getActivity(), AccountActivity.class );
                    intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
                    intent.putExtra( Constants.LOGIN_FROM, "dday_section" );
                    getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
                }
            }
        });

        sectionController = new SectionController();
        loveController = new LoveController();
        bestRealreviewTitle = (TextView) view.findViewById( R.id.best_realreview_title );
        bestRealreviewDesc = (TextView) view.findViewById( R.id.best_realreview_desc );
        eventTitle = (TextView) view.findViewById( R.id.event_title );
        eventDesc = (TextView) view.findViewById( R.id.event_desc );
        honorTitle = (TextView) view.findViewById( R.id.honor_title );
        honorDesc = (TextView) view.findViewById( R.id.honor_desc );
        getHomeSectionDataFromServer();

//        premiumAdViewPager = (ViewPager) view.findViewById( R.id.premiumad_viewpager );
//        premiumAdIndicator = (CircleIndicator) view.findViewById( R.id.tutorial_indicator );
        premiumController = new PremiumController();
        bannerWrapper = (LinearLayout)view.findViewById( R.id.banner_wrapper );
        getPremiumAdFromServer();

        view.findViewById( R.id.go_search ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(), SearchActivity.class );
                startActivity( intent );
                getActivity().overridePendingTransition( R.anim.slide_up, R.anim.no_change );
            }
        });

        tabCategoryController = new TabCategoryController();
        categoryWrapper = (GridLayout) view.findViewById( R.id.category_wrapper );
        allTabCategories = prefs.getCurrentTabCategories();
        setCategoryIcons();

        contentController = new ContentController();
        realreviewWrapper = (GridLayout) view.findViewById( R.id.realreview_wrapper );
        getBestRealReviewFromServer();
        view.findViewById( R.id.best_realreview_more ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(), RealWeddingActivity.class );
                intent.putExtra( "categoryName", "" );
                getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_REALWEDDING_ACTIVITY );
            }
        });

        companyController = new CompanyController();
        eventWrapper = (LinearLayout) view.findViewById( R.id.event_wrapper );
//        horizontalScrollView = view.findViewById( R.id.horizontal_scrollview );
//        horizontalScrollView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//
//                if( motionEvent.getAction() == MotionEvent.ACTION_UP ) {
//                    float currentPosition = horizontalScrollView.getScrollX();
//                    float pagesCount = eventWrapper.getChildCount();
//                    float pageLengthInPx = eventWrapper.getMeasuredWidth() / pagesCount;
//                    float currentPage = currentPosition / pageLengthInPx;
//
//                    Boolean isBehindHalfScreen = currentPage - (int)currentPage > 0.5;
//
//                    float edgePosition = 0;
//                    if( isBehindHalfScreen ) {
//                        edgePosition = (int) ( currentPage + 1 ) * pageLengthInPx;
//                    } else {
//                        edgePosition = (int) currentPage * pageLengthInPx;
//                    }
//
//                    horizontalScrollView.scrollTo( (int)edgePosition, 0 );
//                }
//
//                return false;
//            }
//        });
        getEventFromServer();

        honorController = new HonorController();
        subscriptionController = new SubscriptionController();
        honorUserWrapper = (LinearLayout) view.findViewById( R.id.honor_user_wrapper );
        getHonorUserFromServer();

        newhomeRefresh = (SwipeRefreshLayout)view.findViewById( R.id.newhome_refresh );
        newhomeRefresh.setColorSchemeResources(
                R.color.colorPrimaryDark, R.color.colorAccent,
                R.color.colorAccent, R.color.colorAccent);
        newhomeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
                newhomeRefresh.setRefreshing( false );
            }
        });

        return view;
    }

    public void setMyInfo() {
        if( !MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            myDday.setText( "로그인이 필요해요 👰" );
            myCbudgetLabel.setVisibility( GONE );
            myCbudget.setVisibility( GONE );
            budgetUnit.setVisibility( GONE );
        } else {
            myCbudgetLabel.setVisibility( View.VISIBLE );
            myCbudget.setVisibility( View.VISIBLE );
            budgetUnit.setVisibility( View.VISIBLE );

            Call<DDayV2Response> ddayCall = dDayController.getDDayV2();
            ddayCall.enqueue(new Callback<DDayV2Response>() {
                @Override
                public void onResponse(Call<DDayV2Response> call, Response<DDayV2Response> response) {
                    if( !ObjectUtils.isEmpty( response.body() ) ) {
                        DDayV2Response dDayV2Response = response.body();
                        DDayDto dDayDto = dDayV2Response.getDdayData();
                        if( !ObjectUtils.isEmpty( dDayDto ) ) {
                            myDday.setText( dDayDto.getDdayToString() );
                            myCbudget.setText( DecimalUtils.convertDecimalFormat( (int)dDayDto.getcBudget() ) );
                        } else {
                            setMyInfoFromPrefs();
                        }
                    } else {
                        setMyInfoFromPrefs();
                    }
                }

                @Override
                public void onFailure(Call<DDayV2Response> call, Throwable t) {
                    setMyInfoFromPrefs();
                }
            });
        }
    }

    public void setMyInfoFromPrefs() {
        if( !ObjectUtils.isEmpty( prefs.getDdayDate() ) ) {
            myDday.setText( CalendarUtil.getdDayString( prefs.getDdayDate() ) );
        } else {
            myDday.setText( "D-?" );
        }
        if( prefs.getCBudget() > 0 ) {
            myCbudget.setText(DecimalUtils.convertDecimalFormat( prefs.getCBudget() ) );
        }
    }

    public void refreshData() {
        setMyInfo();
        getHomeSectionDataFromServer();
        setCategoryIcons();
        getBestRealReviewFromServer();
        getEventFromServer();
        getHonorUserFromServer();
    }

    private void getHomeSectionDataFromServer() {
        Call<SectionResponse> sectionCall = sectionController.getSections( "home" );
        sectionCall.enqueue(new Callback<SectionResponse>() {
            @Override
            public void onResponse(Call<SectionResponse> call, Response<SectionResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    SectionResponse sectionResponse = response.body();

                    List<Section> sections = sectionResponse.getSection();
                    if( !ObjectUtils.isEmpty( sections ) ) {
                        for( Section section : sections ) {
                            if( section.getPosition() == 0 ) {
                                bestRealreviewTitle.setText( section.getName() );
                                bestRealreviewDesc.setText( section.getDescription() );
                            } else if( section.getPosition() == 1 ) {
                                eventTitle.setText( section.getName() );
                                eventDesc.setText( section.getDescription() );
                            } else if( section.getPosition() == 2 ) {
                                honorTitle.setText( section.getName() );
                                honorDesc.setText( section.getDescription() );
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SectionResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void getPremiumAdFromServer() {

        Call<List<PremiumAd>> premiumAdCall = premiumController.getPremiumAds();
        premiumAdCall.enqueue(new Callback<List<PremiumAd>>() {
            @Override
            public void onResponse(Call<List<PremiumAd>> call, Response<List<PremiumAd>> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    List<PremiumAd> premiumAds = response.body();

                    if( premiumAds.size() > 0 ) {
                        int premiumAdIdx = prefs.getPremiumAdIdx() + 1;
                        if( premiumAdIdx > premiumAds.size() - 1 ) {
                            premiumAdIdx = 0;
                        }
                        prefs.setPremiumAdIdx( premiumAdIdx );


                        bannerWrapper.removeAllViews();

                        PremiumAd premiumAd = premiumAds.get( premiumAdIdx );

                        if( !ObjectUtils.isEmpty( getActivity() ) ) {
                            CustomPremiumAdModel customPremiumAdModel = new CustomPremiumAdModel( getActivity(), mGlideRequestManager );
                            customPremiumAdModel.setPremiumAdId( premiumAd.getId() );
                            postPremiumAdLogToServer( "expose", premiumAd.getId() );
                            customPremiumAdModel.setAdImg( premiumAd.getImage() );
                            customPremiumAdModel.setAdTitle( premiumAd.getTitle() );
                            customPremiumAdModel.setAdAux( premiumAd.getAuxName() );
                            customPremiumAdModel.setRelativeLayoutPadding( 0 );
                            customPremiumAdModel.setTag( premiumAd.getLinkUrl() );
                            customPremiumAdModel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    postPremiumAdLogToServer( "click", ((CustomPremiumAdModel)view).getPremiumAdId() );
                                    startFullscreenActivity( view.getTag() + "" );
                                }
                            });
                            bannerWrapper.addView( customPremiumAdModel );
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PremiumAd>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void setCategoryIcons() {

        Call<TabCategoryResponse> tabCategoryResponseCall = tabCategoryController.getTabCategories();
        tabCategoryResponseCall.enqueue(new Callback<TabCategoryResponse>() {
            @Override
            public void onResponse(Call<TabCategoryResponse> call, Response<TabCategoryResponse> response) {

                categoryWrapper.removeAllViews();

                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    TabCategoryResponse tabCategoryResponse = response.body();
                    List<com.jjlee.wedqueen.rest.tabCategory.model.TabCategory> tabCategories = tabCategoryResponse.getTabCategories();
                    ArrayList<TabCategory> prefTabCategories = new ArrayList<TabCategory>();

                    if( !ObjectUtils.isEmpty( tabCategories ) ) {
                        for( com.jjlee.wedqueen.rest.tabCategory.model.TabCategory tabCategory : tabCategories ) {

                            prefTabCategories.add( new TabCategory( tabCategory ) );

                            if( ObjectUtils.isEmpty( tabCategory.getCategoryName() ) ) {
                                continue;
                            }

                            if( !ObjectUtils.isEmpty( getActivity() ) ) {
                                CustomCategoryIcon customCategoryIcon = new CustomCategoryIcon( getActivity(), mGlideRequestManager );
                                customCategoryIcon.setCategoryImgSize( ( displayWidth - SizeConverter.dpToPx( 3 ) ) / 4 );
                                customCategoryIcon.setCategoryImg( tabCategory.getIconUrl() );
                                customCategoryIcon.setCategoryText( tabCategory.getCategoryNameKor() );
                                customCategoryIcon.setTag( tabCategory.getCategoryName() );
                                customCategoryIcon.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent( getActivity(), CategoryHomeActivity.class );
                                        intent.putExtra( "categoryName", v.getTag() + "" );
                                        startActivity( intent );
                                    }
                                });
                                categoryWrapper.addView( customCategoryIcon );
                            }
                        }

                        allTabCategories.put( "contentfeed", prefTabCategories );
                        prefs.setCurrentTabCategories( allTabCategories );

                    }
                }
            }

            @Override
            public void onFailure(Call<TabCategoryResponse> call, Throwable t) {
                categoryWrapper.removeAllViews();
                t.printStackTrace();
            }
        });
    }

    private void getBestRealReviewFromServer() {

        Call<ContentResponse> bestContentCall = contentController.getBestContent();
        bestContentCall.enqueue(new Callback<ContentResponse>() {
            @Override
            public void onResponse(Call<ContentResponse> call, Response<ContentResponse> response) {

                realreviewWrapper.removeAllViews();

                if( !ObjectUtils.isEmpty( response.body() ) && !ObjectUtils.isEmpty( getActivity() ) ) {
                    List<Content> contents = response.body().getContents();

                    if( !ObjectUtils.isEmpty( contents ) ) {
                        for( Content content : contents ) {
                            CustomRealReviewModel customRealReviewModel = new CustomRealReviewModel( getActivity(), mGlideRequestManager );
                            customRealReviewModel.setRealreviewSize( ( displayWidth - SizeConverter.dpToPx( 56 ) ) / 2 );
                            customRealReviewModel.setRealreviewTitle( content.getTitle() );
                            customRealReviewModel.setRealreviewImg( content.getImageUrl() );
                            customRealReviewModel.setRealreviewEditor( content.getExposedName() );
                            customRealReviewModel.setRealreviewViewcount( content.getViewCountToString() );
                            customRealReviewModel.setTag( content.getUrl() );
                            customRealReviewModel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startFullscreenActivity( v.getTag() + "" );
                                }
                            });

                            customRealReviewModel.setContentId( content.getId() );
                            customRealReviewModel.setRealreviewLoved( content.isLoved() );
                            customRealReviewModel.getRealreviewScrap().setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {

                                        CustomRealReviewModel targetModel = (CustomRealReviewModel)v.getParent().getParent();
                                        boolean isLoved = targetModel.isLoved();

                                        setLoveContent( isLoved, targetModel.getContentId() );
                                        targetModel.setRealreviewLoved( !isLoved );
                                    } else {
                                        Intent intent = new Intent( getActivity(), AccountActivity.class );
                                        intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
                                        intent.putExtra( Constants.LOGIN_FROM, "home_btn_love" );
                                        getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
                                    }
                                }
                            });
                            realreviewWrapper.addView( customRealReviewModel );
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ContentResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void getEventFromServer() {
        eventWrapper.removeAllViews();

        Call<List<RealWeddingContentDto>> eventCall = contentController.getPremiums();
        eventCall.enqueue(new Callback<List<RealWeddingContentDto>>() {
            @Override
            public void onResponse(Call<List<RealWeddingContentDto>> call, Response<List<RealWeddingContentDto>> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    List<RealWeddingContentDto> realWeddingContentDtos = response.body();

                    for( RealWeddingContentDto realWeddingContentDto : realWeddingContentDtos ) {

                        if( !ObjectUtils.isEmpty( realWeddingContentDto.getType() )
                                && realWeddingContentDto.getType().equals( "premiumCompany" )
                                && !ObjectUtils.isEmpty( getActivity() ) ) {
                            CustomEventModel customEventModel = new CustomEventModel( getActivity(), mGlideRequestManager );
                            customEventModel.setEventImgSize( displayWidth - SizeConverter.dpToPx( 40 ) );
                            customEventModel.setEventTitle( realWeddingContentDto.getTitle() );
                            customEventModel.setEventTag( realWeddingContentDto.getStrEvent() );
                            customEventModel.setEventImg( realWeddingContentDto.getImageUrl() );
                            customEventModel.setEventEditor( realWeddingContentDto.getWriter() );
                            customEventModel.setEventViewcount( realWeddingContentDto.getViewCountToString() );
                            if( ObjectUtils.isEmpty( realWeddingContentDto.getWriter() ) && ObjectUtils.isEmpty( realWeddingContentDto.getViewCountToString() ) ) {
                                customEventModel.getEventDesc().setVisibility( GONE );
                            }
                            customEventModel.setShoppable( realWeddingContentDto.isPaymentRequired() );
                            customEventModel.setBookable( realWeddingContentDto.isSurveyRequired() );
                            customEventModel.setInquirable( realWeddingContentDto.isInquiryRequired() );
                            customEventModel.setTag( realWeddingContentDto.getUrl() );
                            customEventModel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startFullscreenActivity( v.getTag() + "" );
                                }
                            });

                            if( realWeddingContentDto.getCompanyLatitude() == 0 && realWeddingContentDto.getCompanyLongitude() == 0 ) {
                                customEventModel.getCompanyMap().setVisibility( GONE );
                            } else {
                                customEventModel.setCompanyName( realWeddingContentDto.getWriter() );
                                customEventModel.setLatitude( realWeddingContentDto.getCompanyLatitude() );
                                customEventModel.setLongitude( realWeddingContentDto.getCompanyLongitude() );
                                customEventModel.getCompanyMap().setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {

                                        CustomEventModel targetModel = (CustomEventModel)view.getParent().getParent().getParent();

                                        Intent intent = new Intent( getActivity(), CompanyMapActivity.class );
                                        intent.putExtra( "companyName", targetModel.getCompanyName() );
                                        intent.putExtra( "latitude", targetModel.getLatitude() );
                                        intent.putExtra( "longitude", targetModel.getLongitude() );
                                        startActivity( intent );
                                    }
                                });
                            }

                            customEventModel.setContentId( realWeddingContentDto.getId() );
                            if( realWeddingContentDto.getCompanyId() > 0 ) {
                                customEventModel.getRealreviewScrap().setVisibility( View.GONE );
                            } else {
                                customEventModel.setRealreviewLoved( realWeddingContentDto.isLoved() );
                                customEventModel.getRealreviewScrap().setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {

                                            CustomEventModel targetModel = (CustomEventModel)view.getParent().getParent();
                                            boolean isLoved = targetModel.isLoved();

                                            setLoveContent( isLoved, targetModel.getContentId() );
                                            targetModel.setRealreviewLoved( !isLoved );
                                        } else {
                                            Intent intent = new Intent( getActivity(), AccountActivity.class );
                                            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
                                            intent.putExtra( Constants.LOGIN_FROM, "home_btn_love" );
                                            getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
                                        }
                                    }
                                });
                            }
                            eventWrapper.addView( customEventModel );
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<RealWeddingContentDto>> call, Throwable t) {
                realreviewWrapper.removeAllViews();
                t.printStackTrace();
            }
        });
    }

    private void getHonorUserFromServer() {
        Call<HonorUserResponse> honorUserCall = honorController.getHonorUser();
        honorUserCall.enqueue(new Callback<HonorUserResponse>() {
            @Override
            public void onResponse(Call<HonorUserResponse> call, Response<HonorUserResponse> response) {
                honorUserWrapper.removeAllViews();

                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    HonorUserResponse honorUserResponse = response.body();

                    if( !ObjectUtils.isEmpty( honorUserResponse.getUsers() ) && !ObjectUtils.isEmpty( getActivity() ) ) {
                        for( UserRestDto userRestDto : honorUserResponse.getUsers() ) {
                            CustomHonorUserModel customHonorUserModel = new CustomHonorUserModel( getActivity(), mGlideRequestManager );
                            customHonorUserModel.setHonorUserImg( userRestDto.getImageUrl() );
                            customHonorUserModel.setHonorUserName( userRestDto.getNickname() );
                            customHonorUserModel.setUserId( userRestDto.getId() );
                            customHonorUserModel.setHonorUserReviewCount( userRestDto.getRealweddingCountToString() );

                            GridLayout honorUserReviewWrapper = customHonorUserModel.getHonorUserReviewWrapper();
                            honorUserReviewWrapper.removeAllViews();
                            List<Content> contents = userRestDto.getContents();
                            if( !ObjectUtils.isEmpty( contents ) ) {
                                int contentMax = 2;
                                for( Content content : contents ) {
                                    CustomRealReviewModel customRealReviewModel = new CustomRealReviewModel( getActivity(), mGlideRequestManager );
                                    customRealReviewModel.setRealreviewSize( ( displayWidth - SizeConverter.dpToPx( 56 ) ) / 2 );
                                    customRealReviewModel.setRealreviewTitle( content.getTitle() );
                                    customRealReviewModel.setRealreviewImg( content.getImageUrl() );
                                    customRealReviewModel.setRealreviewEditor( content.getExposedName() );
                                    customRealReviewModel.setRealreviewViewcount( content.getViewCountToString() );
                                    customRealReviewModel.setTag( content.getUrl() );
                                    customRealReviewModel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            startFullscreenActivity( v.getTag() + "" );
                                        }
                                    });

                                    customRealReviewModel.setContentId( content.getId() );
                                    customRealReviewModel.setRealreviewLoved( content.isLoved() );
                                    customRealReviewModel.getRealreviewScrap().setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {

                                                CustomRealReviewModel targetModel = (CustomRealReviewModel)v.getParent().getParent();
                                                boolean isLoved = targetModel.isLoved();

                                                setLoveContent( isLoved, targetModel.getContentId() );
                                                targetModel.setRealreviewLoved( !isLoved );
                                            } else {
                                                Intent intent = new Intent( getActivity(), AccountActivity.class );
                                                intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
                                                intent.putExtra( Constants.LOGIN_FROM, "home_btn_love" );
                                                getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
                                            }
                                        }
                                    });
                                    honorUserReviewWrapper.addView( customRealReviewModel );

                                    if( --contentMax < 1 ) {
                                        break;
                                    }
                                }
                            }

                            customHonorUserModel.setFollow( userRestDto.isHasSubscription() );
                            customHonorUserModel.getFollowBtn().setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
                                        CustomHonorUserModel targetModel = (CustomHonorUserModel)v.getParent().getParent().getParent();
                                        boolean isFollow = targetModel.isFollow();

                                        setFollowUser( isFollow, (int) targetModel.getUserId() );
                                        targetModel.setFollow( !isFollow );
                                    } else {
                                        Intent intent = new Intent( getActivity(), AccountActivity.class );
                                        intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
                                        intent.putExtra( Constants.LOGIN_FROM, "home_btn_follow" );
                                        getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
                                    }
                                }
                            });
                            customHonorUserModel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    long userId = ((CustomHonorUserModel)view).getUserId();
                                    Intent intent = new Intent( getActivity(), OtherUserDiaryActivity.class);
                                    intent.putExtra( "userId", (int)userId );
                                    getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_OTHERUSERDIARY_ACTIVITY );
                                }
                            });

                            honorUserWrapper.addView( customHonorUserModel );
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<HonorUserResponse> call, Throwable t) {
                honorUserWrapper.removeAllViews();
                t.printStackTrace();
            }
        });
    }


    public void setNewHomeScrollToTop() {
        if( !ObjectUtils.isEmpty( newhometabScrollview ) ) {
            newhometabScrollview.smoothScrollTo( 0, 0 );
        }
    }



    private void setFollowUser( boolean isFollow, int targetUserId ) {
        if( isFollow ) {
            Call<SubscriptionResponse> subscriptionCall = subscriptionController.delSubscription( "user", targetUserId );
            subscriptionCall.enqueue(new Callback<SubscriptionResponse>() {
                @Override
                public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                }

                @Override
                public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                }
            });
        } else {
            Call<SubscriptionResponse> subscriptionCall = subscriptionController.postSubscription( "user", targetUserId );
            subscriptionCall.enqueue(new Callback<SubscriptionResponse>() {
                @Override
                public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                }

                @Override
                public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                }
            });
        }
    }

    private void setLoveContent( boolean isLove, long targetContentId ) {

        Call<CommonResponse> call;
        if( isLove ) {
            call = loveController.loveOff( targetContentId, "content" );
        } else {
            call = loveController.loveOn( targetContentId, "content" );
        }

        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> commonResponse) {
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
            }
        });
    }


    private void postPremiumAdLogToServer( String logType, long premiumAdId ) {
        String userId = MyApplication.getGlobalApplicationContext().getUserId();
        if( ObjectUtils.isEmpty( userId ) ) {
            userId = "-1";
        }

        PremiumAdLog premiumAdLog = new PremiumAdLog( premiumAdId, logType, Long.parseLong( userId ) );
        Call<PremiumAdLogResponse> premiumAdLogCall = premiumController.postPremiumAdLog( premiumAdLog );
        premiumAdLogCall.enqueue(new Callback<PremiumAdLogResponse>() {
            @Override
            public void onResponse(Call<PremiumAdLogResponse> call, Response<PremiumAdLogResponse> response) {
            }

            @Override
            public void onFailure(Call<PremiumAdLogResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private void startFullscreenActivity(String url) {

        if( MyApplication.getGlobalApplicationContext().canLoadDetailContent() ) {
            Intent intent = new Intent(getActivity(), FullscreenActivity.class);
            intent.putExtra("URL", url);
            getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY );
        } else {
            Intent intent = new Intent( getActivity(), AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            intent.putExtra( Constants.LOGIN_FROM, "home_other_content" );
            getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
        }
    }

//    public class PremiumAdAdapter extends FragmentStatePagerAdapter {
//
//        public PremiumAdAdapter(FragmentManager fm) {
//            super(fm);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            return PremiumAdFragment.newInstance( premiumAds.get( position ) );
//        }
//
//        @Override
//        public int getCount() {
//            return premiumAds.size();
//        }
//    }
}
