package com.jjlee.wedqueen.rest.verification.controller;

import com.jjlee.wedqueen.rest.verification.model.VerificationResponse;
import com.jjlee.wedqueen.rest.verification.service.VerificationService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

import static com.jjlee.wedqueen.support.RetrofitController.RETROFIT;


public class VerificationController extends RetrofitController {
    private VerificationService verificationService;

    public VerificationController() {
        super();
        verificationService = RETROFIT.create(VerificationService.class);
    }

    public Call<VerificationResponse> getVerification( String type ) {
        return verificationService.getVerification( type );
    }
}
