package com.jjlee.wedqueen.rest.sector.model;

/**
 * Created by esmac on 2017. 12. 18..
 */

public class SectorItemAux {
    private String auxKey;
    private String auxValue;

    public String getAuxKey() {
        return auxKey;
    }

    public void setAuxKey(String auxKey) {
        this.auxKey = auxKey;
    }

    public String getAuxValue() {
        return auxValue;
    }

    public void setAuxValue(String auxValue) {
        this.auxValue = auxValue;
    }
}
