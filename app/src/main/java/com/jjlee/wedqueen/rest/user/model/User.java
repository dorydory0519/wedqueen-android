package com.jjlee.wedqueen.rest.user.model;

import com.jjlee.wedqueen.rest.content.model.Content;

import java.util.List;

public class User {

    private Long id;
    private String nickname;
    private String imageUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
