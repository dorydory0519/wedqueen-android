package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.jjlee.wedqueen.R;

public class CustomBannerModel extends LinearLayout {

    private long sectorItemId;

    public CustomBannerModel(Context context) {
        super(context);
        initView();
    }

    public CustomBannerModel(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_bannerimage, this, false);
        addView(v);

    }

    public long getSectorItemId() {
        return sectorItemId;
    }

    public void setSectorItemId(long sectorItemId) {
        this.sectorItemId = sectorItemId;
    }
}
