package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.jjlee.wedqueen.R;


public class CustomColorPickerModel extends FrameLayout {

    Button colorInner;
    Button colorOuter;
    boolean isSelected = false;
    String colorCode;

    public CustomColorPickerModel(Context context) {
        super(context);
        initView();

    }

    public CustomColorPickerModel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomColorPickerModel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_colorpicker, this, false);
        addView(v);

        colorInner = (Button)findViewById(R.id.color_inner);
        colorInner.setClickable( false );
        colorOuter = (Button)findViewById(R.id.color_outer);

    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ColorPickerModel);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ColorPickerModel, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {

        String colorCode = typedArray.getString(R.styleable.ColorPickerModel_color_code);
        setColorCode( colorCode );

        boolean isSelected = typedArray.getBoolean(R.styleable.ColorPickerModel_color_isselected, false);
        setSelected( isSelected );

        typedArray.recycle();
    }

    public void setSelected( boolean isSelected ) {
        this.isSelected = isSelected;

        if( isSelected ) {
            colorOuter.setVisibility(View.VISIBLE);
        } else {
            colorOuter.setVisibility(View.GONE);
        }
    }

    public void setColorCode( String colorCode ) {

        this.colorCode = colorCode;

        if( colorCode.equals( "red" ) ) {
            colorInner.setBackgroundResource( R.drawable.red_inner );
            colorOuter.setBackgroundResource( R.drawable.red_ring );
        } else if( colorCode.equals( "orange" ) ) {
            colorInner.setBackgroundResource( R.drawable.orange_inner );
            colorOuter.setBackgroundResource( R.drawable.orange_ring );
        } else if( colorCode.equals( "yellow" ) ) {
            colorInner.setBackgroundResource( R.drawable.yellow_inner );
            colorOuter.setBackgroundResource( R.drawable.yellow_ring );
        } else if( colorCode.equals( "green" ) ) {
            colorInner.setBackgroundResource( R.drawable.green_inner );
            colorOuter.setBackgroundResource( R.drawable.green_ring );
        } else if( colorCode.equals( "blue" ) ) {
            colorInner.setBackgroundResource( R.drawable.blue_inner );
            colorOuter.setBackgroundResource( R.drawable.blue_ring );
        } else if( colorCode.equals( "purple" ) ) {
            colorInner.setBackgroundResource( R.drawable.purple_inner );
            colorOuter.setBackgroundResource( R.drawable.purple_ring );
        } else {
            colorInner.setBackgroundResource( R.drawable.black_inner );
            colorOuter.setBackgroundResource( R.drawable.black_ring );
        }
    }

    public String getColorCode() {
        return colorCode;
    }

}
