package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;

public class CustomEventModel extends RelativeLayout {

    private RequestManager mRequestManager;

    private ImageView eventImg;
    private ImageView colorFilter;
    private TextView eventTitle;
    private TextView eventTag;
    private LinearLayout eventDesc;
    private TextView eventEditor;
    private TextView eventViewcount;
    private ImageView realreviewScrap;

    private LinearLayout companyMap;

    private TextView shoppableSign;
    private TextView bookableSign;
    private TextView inquirableSign;

    private long contentId;
    private boolean isLoved;
    private String companyName;
    private double latitude;
    private double longitude;

    public CustomEventModel(Context context) {
        super(context);
        initView();

    }

    public CustomEventModel(Context context, RequestManager requestManager) {
        super(context);
        mRequestManager = requestManager;
        initView();
    }

    public CustomEventModel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_event, this, false);
        addView(v);

        eventImg = (ImageView) findViewById( R.id.event_img );
        colorFilter = (ImageView) findViewById( R.id.color_filter );
        eventTitle = (TextView) findViewById( R.id.event_title );
        eventTag = (TextView) findViewById( R.id.event_tag );
        eventDesc = (LinearLayout) findViewById( R.id.event_desc );
        eventEditor = (TextView) findViewById( R.id.event_editor );
        eventViewcount = (TextView) findViewById( R.id.event_viewcount );
        realreviewScrap = (ImageView) findViewById( R.id.realreview_scrap );

        companyMap = (LinearLayout) findViewById( R.id.company_map );

        shoppableSign = (TextView) findViewById( R.id.shoppable_sign );
        bookableSign = (TextView) findViewById( R.id.bookable_sign );
        inquirableSign = (TextView) findViewById( R.id.inquirable_sign );

        isLoved = false;
    }

    public long getContentId() {
        return contentId;
    }
    public void setContentId(long contentId) {
        this.contentId = contentId;
    }
    public void setEventImg( String resUrl ) {
        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load( resUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( eventImg );
        } else {
            Glide.with( getContext() ).load( resUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( eventImg );
        }
    }
    public void setEventTitle( String resString ) {
        eventTitle.setText( resString );
    }
    public void setEventTag( String resString ) {

        if( !ObjectUtils.isEmpty( resString ) ) {
            eventTag.setVisibility( VISIBLE );
            eventTag.setText( resString );
            colorFilter.setVisibility( VISIBLE );
        } else {
            eventTag.setVisibility( GONE );
            colorFilter.setVisibility( GONE );
        }
    }
    public void setEventEditor( String resString ) {
        if( resString.length() > 14 ) {
            resString = resString.substring( 0, 12 ) + "...";
        }
        eventEditor.setText( resString );
    }
    public void setEventViewcount( String resString ) {
        eventViewcount.setText( resString );
    }
    public void setEventImgSize( int size ) {
        eventImg.getLayoutParams().width = size;
        eventImg.getLayoutParams().height = size / 2;
        eventTitle.getLayoutParams().width = size;
        eventDesc.getLayoutParams().width = size;
    }
    public void setShoppable( boolean shoppable ) {
        if( shoppable ) {
            shoppableSign.setVisibility( VISIBLE );
        } else {
            shoppableSign.setVisibility( GONE );
        }
    }
    public void setBookable( boolean bookable ) {
        if( bookable ) {
            bookableSign.setVisibility( View.VISIBLE );
        } else {
            bookableSign.setVisibility( View.GONE );
        }
    }
    public void setInquirable( boolean inquirable ) {
        if( inquirable ) {
            inquirableSign.setVisibility( View.VISIBLE );
        } else {
            inquirableSign.setVisibility( View.GONE );
        }
    }

    public ImageView getRealreviewScrap() {
        return realreviewScrap;
    }
    public boolean isLoved() {
        return isLoved;
    }
    public void setRealreviewLoved( boolean isLoved ) {
        this.isLoved = isLoved;
        if( isLoved ) {
            realreviewScrap.setImageResource( R.drawable.scrap_on );
        } else {
            realreviewScrap.setImageResource( R.drawable.scrap_off );
        }
    }

    public LinearLayout getCompanyMap() {
        return companyMap;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public LinearLayout getEventDesc() {
        return eventDesc;
    }
}
