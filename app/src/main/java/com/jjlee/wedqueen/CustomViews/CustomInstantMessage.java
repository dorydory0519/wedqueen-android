package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjlee.wedqueen.R;

public class CustomInstantMessage extends LinearLayout {

    TextView instantMessage;

    public CustomInstantMessage(Context context) {
        super(context);
        initView();

    }

    public CustomInstantMessage(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomInstantMessage(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_instant_message, this, false);
        addView(v);

        instantMessage = (TextView)findViewById(R.id.instant_message);

    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.InstantMessage);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.InstantMessage, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {

        String instantMessage_resID = typedArray.getString(R.styleable.InstantMessage_instant_message);
        setInstantMessage( instantMessage_resID );

        typedArray.recycle();
    }


    public void setInstantMessage(String resString) {
        instantMessage.setText( resString );
    }

}
