package com.jjlee.wedqueen.rest.user.model;

/**
 * Created by leejungyi on 2017. 10. 22..
 */

public class RegionDto {

    private int index;
    private String korRegion;
    private String engRegion;
    private String korPrecinct;
    private String engPrecinct;
    private boolean selected;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getKorRegion() {
        return korRegion;
    }

    public void setKorRegion(String korRegion) {
        this.korRegion = korRegion;
    }

    public String getEngRegion() {
        return engRegion;
    }

    public void setEngRegion(String engRegion) {
        this.engRegion = engRegion;
    }

    public String getKorPrecinct() {
        return korPrecinct;
    }

    public void setKorPrecinct(String korPrecinct) {
        this.korPrecinct = korPrecinct;
    }

    public String getEngPrecinct() {
        return engPrecinct;
    }

    public void setEngPrecinct(String engPrecinct) {
        this.engPrecinct = engPrecinct;
    }

    public boolean isSelected() {
        return selected;
    }
    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
