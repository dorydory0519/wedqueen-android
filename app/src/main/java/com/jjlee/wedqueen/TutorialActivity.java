package com.jjlee.wedqueen;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.fragments.TutorialImageFragment;
import com.jjlee.wedqueen.support.Prefs;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.List;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by esmac on 2017. 11. 15..
 */

public class TutorialActivity extends AppCompatActivity {

    private List<String> tutorialImgUrls;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentType( "Activity" )
                .putContentName( "tutorial" ) );

        ViewPager tutorialViewPager = (ViewPager) findViewById(R.id.tutorial_viewpager);

        Prefs prefs = new Prefs( this );
        tutorialImgUrls = prefs.getTutorialImgUrl();

        TutorialAdapter tutorialAdapter = new TutorialAdapter( getSupportFragmentManager() );
        tutorialViewPager.setAdapter( tutorialAdapter );
        CircleIndicator tutorialIndicator = (CircleIndicator)findViewById(R.id.tutorial_indicator);
        tutorialIndicator.setViewPager( tutorialViewPager );
        tutorialViewPager.setOffscreenPageLimit( tutorialImgUrls.size() - 1 );
//        tutorialViewPager.setPageTransformer( true, new DefaultTransformer() );
//        tutorialViewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
//            @Override
//            public void transformPage(View page, float position) {
//
//            }
//        });

        findViewById(R.id.skip_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public class TutorialAdapter extends FragmentStatePagerAdapter {

        public TutorialAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return TutorialImageFragment.newInstance( tutorialImgUrls.get( position ) );
        }

        @Override
        public int getCount() {
            return tutorialImgUrls.size();
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
