package com.jjlee.wedqueen.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.AccountActivity;
import com.jjlee.wedqueen.DDaySettingActivity;
import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.OtherUserDiaryActivity;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.SearchActivity;
import com.jjlee.wedqueen.rest.app.model.TabCategory;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.utils.CalendarUtil;
import com.jjlee.wedqueen.utils.CustomScrollView;
import com.jjlee.wedqueen.utils.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeFragment extends Fragment {
    public static final String TABTYPE = "tab_type";

    private Prefs prefs;

    private TabLayout tabLayout;
    private HomeAdapter homeAdapter;
    private ViewPager homeViewPager;
    private HashMap<String, ArrayList<TabCategory>> allTabCategories;
    private HashMap<String, Boolean> isTipOpenList;
    protected ArrayList<TabCategory> tabCategories;
    protected String tabType;

    protected HorizontalScrollView categoryScrollView;

    private CustomScrollView hometabScrollview;

    private SwipeRefreshLayout swipeRefreshLayout;

    public HomeFragment() {} // Required empty public constructor

    public static void putFilterRelatedArgument( Bundle args, String tabType ) {
        args.putString(TABTYPE, tabType);
    }

    public static HomeFragment newInstance(String tabType) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        putFilterRelatedArgument( args, tabType );
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString( TABTYPE, tabType );
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();

        if(savedInstanceState == null) {
            tabType = args.getString(TABTYPE);
        } else {
            tabType = savedInstanceState.getString(TABTYPE, null);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        // Load TabCategories from Prefs
        prefs = new Prefs( getActivity() );
        allTabCategories = prefs.getCurrentTabCategories();
        tabCategories = allTabCategories.get(tabType);
        isTipOpenList = new HashMap<String, Boolean>();

        categoryScrollView = (HorizontalScrollView) view.findViewById(R.id.filter_scoller);
        hometabScrollview = (CustomScrollView) view.findViewById(R.id.hometab_scrollview);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimaryDark, R.color.colorPrimaryDark,
                R.color.colorPrimaryDark, R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });


        tabLayout = (TabLayout) view.findViewById(R.id.home_tablayout);
        homeViewPager = (ViewPager) view.findViewById(R.id.home_viewpager);

        homeAdapter = new HomeAdapter( getChildFragmentManager() );
        homeViewPager.setAdapter( homeAdapter );
        homeViewPager.setOffscreenPageLimit( 1 );
        tabLayout.setupWithViewPager( homeViewPager );

        for( TabCategory tabCategory : tabCategories ) {
            Log.e( "WQTEST", "tabCategory.getCategoryNameKor() : " + tabCategory.getCategoryNameKor() );
            tabLayout.newTab().setText( tabCategory.getCategoryNameKor() );
            isTipOpenList.put( tabCategory.getCategoryName(), tabCategory.isTipOpen() );
        }

        for( int i = 0; i < tabLayout.getTabCount(); i++ ) {
            TabLayout.Tab tab = tabLayout.getTabAt( i );

            if( !ObjectUtils.isEmpty( tab ) ) {
                TextView tabTextView = new TextView( getContext() );
                tab.setCustomView( tabTextView );
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT );
                layoutParams.weight = 0f;

                tabTextView.setLayoutParams( layoutParams );
                tabTextView.setText( tabCategories.get( i ).getCategoryNameKor() );
                tabTextView.setTextSize( TypedValue.COMPLEX_UNIT_DIP, 14 );
                if( i == 0 ) {
                    tabTextView.setTextColor( ContextCompat.getColor( getContext(), R.color.tab_text_selected ) );
                } else {
                    tabTextView.setTextColor( ContextCompat.getColor( getContext(), R.color.tab_text ) );
                }

                tabTextView.setTypeface( Typeface.createFromAsset( getActivity().getAssets(), "NanumSquareOTFExtraBold.otf" ), Typeface.BOLD );

            }

            tabLayout.getTabAt( i ).setText( tabCategories.get( i ).getCategoryNameKor() );
        }
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TextView tabTextView = (TextView) tab.getCustomView();

                if( !ObjectUtils.isEmpty( tabTextView ) ) {
                    tabTextView.setTextColor( ContextCompat.getColor( getContext(), R.color.tab_text_selected ) );
                }
                homeViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView tabTextView = (TextView) tab.getCustomView();

                if( !ObjectUtils.isEmpty( tabTextView ) ) {
                    tabTextView.setTextColor( ContextCompat.getColor( getContext(), R.color.tab_text ) );
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


        if( !ObjectUtils.isEmpty( prefs.getDdayDate() ) ) {
            ((TextView)view.findViewById(R.id.my_dday)).setText( CalendarUtil.getdDayString( prefs.getDdayDate() ) );
        }


        view.findViewById( R.id.search_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(), SearchActivity.class );
                startActivity( intent );
                getActivity().overridePendingTransition( R.anim.slide_up, R.anim.no_change );
            }
        });
        view.findViewById(R.id.dday_section).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDDaySettingActivity();
            }
        });

//        IgawAdbrix.Commerce.viewHome( getActivity() );

        return view;
    }

    private void startDDaySettingActivity() {

        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            Intent intent = new Intent( getActivity(), DDaySettingActivity.class );
            getActivity().startActivityForResult(intent, Constants.REQ_CODE_OPEN_DDAYSETTING_ACTIVITY);
        } else {
            Intent intent = new Intent( getActivity(), AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            intent.putExtra( Constants.LOGIN_FROM, "home_btn_profile" );
            getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
        }
    }

    public CustomScrollView getHometabScrollview() {
        return hometabScrollview;
    }

    public class HomeAdapter extends FragmentStatePagerAdapter {

        public HomeAdapter(FragmentManager fragmentManager) {
            super( fragmentManager );
        }

        @Override
        public Fragment getItem(int position) {

            return CategoryFragment.newInstance( tabCategories.get( position ).getCategoryName(), tabCategories.get( position ).getCategoryNameKor(), position, tabCategories.get( position ).getId(), isTipOpenList.get( tabCategories.get( position ).getCategoryName() ) );
        }

        @Override
        public int getCount() {
            return tabCategories.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            ((CategoryFragment)fragment).setTipOpen( isTipOpenList.get( tabCategories.get( position ).getCategoryName() ) );
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

            boolean isTipOpen = ((CategoryFragment)object).getWeddingTipIsOpen();
            isTipOpenList.put( tabCategories.get( position ).getCategoryName(), isTipOpen );
            super.destroyItem(container, position, object);
        }

        @Override
        public int getItemPosition(Object object) {

            return POSITION_NONE;

        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
//            super.restoreState(state, loader);
        }
    }


    public void refresh() {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.detach( this ).attach( this ).commit();
    }

    @Override
    public void onDestroyView() {

        // 현재 활성화되어있는 Fragment들의 isTipOn 값들도 받아와서 set
        if( !ObjectUtils.isEmpty( getChildFragmentManager().getFragments() ) ) {
            for( int i = 0; i < getChildFragmentManager().getFragments().size(); i++ ) {
                Fragment fragment = getChildFragmentManager().getFragments().get( i );
                if( fragment instanceof CategoryFragment ) {
                    isTipOpenList.put( ((CategoryFragment) fragment).getCategoryName(), ((CategoryFragment) fragment).getWeddingTipIsOpen() );
                }
            }

            prefs.setTabCategoriesOpenList( isTipOpenList );
        }

        super.onDestroyView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if( isVisibleToUser ) {

            Answers.getInstance().logContentView(new ContentViewEvent()
                    .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                    .putContentType( "Fragment" )
                    .putContentName( "home_tab" ) );

        }
    }
}
