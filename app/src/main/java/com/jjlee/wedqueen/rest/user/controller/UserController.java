package com.jjlee.wedqueen.rest.user.controller;

import com.jjlee.wedqueen.rest.common.model.AppResponse;
import com.jjlee.wedqueen.rest.content.model.PostContentResponse;
import com.jjlee.wedqueen.rest.invitation.model.InvitationForm;
import com.jjlee.wedqueen.rest.user.model.GetUserResponse;
import com.jjlee.wedqueen.rest.user.model.InvitationResponse;
import com.jjlee.wedqueen.rest.user.model.PostUserInfoResponse;
import com.jjlee.wedqueen.rest.user.model.UserContentResponse;
import com.jjlee.wedqueen.rest.user.model.UserInfoForm;
import com.jjlee.wedqueen.rest.user.model.UserInfoResponse;
import com.jjlee.wedqueen.rest.user.model.UserResponse;
import com.jjlee.wedqueen.rest.user.model.UserRestInfo;
import com.jjlee.wedqueen.rest.user.service.UserService;
import com.jjlee.wedqueen.support.RetrofitController;
import com.jjlee.wedqueen.utils.MapUtil;

import java.util.Calendar;
import java.util.Map;

import retrofit2.Call;

public class UserController extends RetrofitController {
    private UserService userService;

    public UserController() {
        super();
        userService = RETROFIT.create(UserService.class);
    }

    public Call<UserContentResponse> getContents() {
        return userService.getContents();
    }

    public Call<UserResponse> getUser(long id) {
        return userService.getUser(id);
    }

    public Call<GetUserResponse> getUser() {
        return userService.getUser( Calendar.getInstance().getTimeInMillis() + "" );
    }

    public Call<UserInfoResponse> getUserInfo() {
        return userService.getUserInfo();
    }

    public Call<PostUserInfoResponse> postUserInfo(UserInfoForm userInfoForm) {
        return userService.postUserInfo(userInfoForm);
    }

    public Call<PostUserInfoResponse> putUserInfo(UserInfoForm userInfoForm) {
        return userService.putUserInfo(userInfoForm);
    }

    public Call<InvitationResponse> getCoupleInvitationCode() {
        return userService.getCoupleInvitationCode();
    }

    public Call<AppResponse> matchCoupleInvitation(InvitationForm form ) {
        Map<String, String> map = MapUtil.getMapFromObject( form );
        return userService.matchCoupleInvitation( map );
    }

    public Call<AppResponse> disconnectCouple() {
        return userService.disconnectCouple();
    }
}
