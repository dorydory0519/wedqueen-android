package com.jjlee.wedqueen.rest.weddingtalk.service;

import com.jjlee.wedqueen.rest.common.model.CommonResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by JJLEE on 2016. 5. 7..
 */
public interface WeddingtalkService {
    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("weddingtalk/{weddingTalkId}")
    Call<CommonResponse> deleteWeddingtalk(@Path("weddingTalkId")Long weddingTalkId, @Field("_method") String method);

}
