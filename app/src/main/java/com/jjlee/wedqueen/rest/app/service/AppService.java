package com.jjlee.wedqueen.rest.app.service;
import com.jjlee.wedqueen.rest.app.model.TabCategory;
import com.jjlee.wedqueen.rest.app.model.TabCategoryResponse;
import com.jjlee.wedqueen.rest.app.model.VersionResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by JJLEE on 2016. 4. 4..
 */

public interface AppService {
    @Headers("Accept: application/json")
    @GET("version")
    Call<VersionResponse> checkVersion(@Query("version") String version);

    @Headers("Accept: application/json")
    @GET("app/tabcategories")
    Call<TabCategoryResponse> getTabCategories();

}
