package com.jjlee.wedqueen.rest.common.model;

/**
 * Created by JJLEE on 2016. 5. 7..
 */
public class CommonResponse {
    public class Error {
        public static final int INVALID_PARAMETER = -1;
        public static final int NOT_AUTHENTICATED = 10;
        public static final int NOT_AUTHORIZED = 11;
        public static final int NOT_FOUND = 20;
        public static final int ALREADY_DONE = 30;
        public static final int ERROR = 100;
        public static final int BLOCKED_USER = 200;
    }

    protected int resultCode;
    protected String msg;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
