package com.jjlee.wedqueen.rest.app.model;

import java.util.List;

/**
 * Created by JJLEE on 2016. 4. 23..
 */
public class TabCategoryResponse {
    private List<TabCategory> tabCategories;

    public List<TabCategory> getTabCategories() {
        return tabCategories;
    }

    public void setTabCategories(List<TabCategory> tabCategories) {
        this.tabCategories = tabCategories;
    }
}
