package com.jjlee.wedqueen.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;

/**
 * Created by esmac on 2017. 11. 15..
 */

public class TutorialImageFragment extends Fragment {

    private String imageUrl;

    public static TutorialImageFragment newInstance( String imageUrl ) {
        TutorialImageFragment fragment = new TutorialImageFragment();
        Bundle args = new Bundle();
        args.putString( "imageUrl", imageUrl );
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        imageUrl = args.getString( "imageUrl" );
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tutorialimage, container, false);

        ImageView tutorialImg = (ImageView)view.findViewById(R.id.tutorial_img);

        if( !ObjectUtils.isEmpty( imageUrl ) ) {
            Glide.with( this ).load( imageUrl ).centerCrop().into( tutorialImg );
        }

        return view;
    }
}
