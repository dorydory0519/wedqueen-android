package com.jjlee.wedqueen.rest.honor.controller;

import com.jjlee.wedqueen.rest.honor.model.HonorUserResponse;
import com.jjlee.wedqueen.rest.honor.service.HonorService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class HonorController extends RetrofitController {

    private HonorService honorService;

    public HonorController() {
        super();
        honorService = RETROFIT.create( HonorService.class );
    }

    public Call<HonorUserResponse> getHonorUser() {
        return honorService.getHonorUsers();
    }
}
