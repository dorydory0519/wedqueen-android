package com.jjlee.wedqueen.rest.dday.model;

import java.util.List;

/**
\ * Created by esmac on 2017. 5. 29..
 */

public class CategoryDto {

    private Long id;

    private String title;

    private String type;

    private long cBudget;

    private long expense;

    private int dDayStart;

    private int dDayEnd;

    private String dDayStartDate;

    private String dDayEndDate;

    private int talkCount;

    private String recentTalk;

    private boolean newTalk;

    private int progress;

    private boolean complete;

    private int totalCount;

    private int checkedCount;

//    private List<ItemDto> items;

    private String companyName;

    private String memo;

    private Long position;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getcBudget() {
        return cBudget;
    }

    public void setcBudget(long cBudget) {
        this.cBudget = cBudget;
    }

    public long getExpense() {
        return expense;
    }

    public void setExpense(long expense) {
        this.expense = expense;
    }

    public int getdDayStart() {
        return dDayStart;
    }

    public void setdDayStart(int dDayStart) {
        this.dDayStart = dDayStart;
    }

    public int getdDayEnd() {
        return dDayEnd;
    }

    public void setdDayEnd(int dDayEnd) {
        this.dDayEnd = dDayEnd;
    }

    public String getdDayStartDate() {
        return dDayStartDate;
    }

    public void setdDayStartDate(String dDayStartDate) {
        this.dDayStartDate = dDayStartDate;
    }

    public String getdDayEndDate() {
        return dDayEndDate;
    }

    public void setdDayEndDate(String dDayEndDate) {
        this.dDayEndDate = dDayEndDate;
    }

    public int getTalkCount() {
        return talkCount;
    }

    public void setTalkCount(int talkCount) {
        this.talkCount = talkCount;
    }

    public String getRecentTalk() {
        return recentTalk;
    }

    public void setRecentTalk(String recentTalk) {
        this.recentTalk = recentTalk;
    }

    public boolean isNewTalk() {
        return newTalk;
    }

    public void setNewTalk(boolean newTalk) {
        this.newTalk = newTalk;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

//    public List<ItemDto> getItems() {
//        return items;
//    }
//
//    public void setItems(List<ItemDto> items) {
//        this.items = items;
//    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getCheckedCount() {
        return checkedCount;
    }

    public void setCheckedCount(int checkedCount) {
        this.checkedCount = checkedCount;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

}