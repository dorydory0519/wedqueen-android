package com.jjlee.wedqueen.view;

import android.util.Log;

import com.jjlee.wedqueen.rest.dday.model.CategoryDto;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class CategoryDataProvider {
    private List<CategoryData> mCategoryData;
    private CategoryData mLastRemovedData;
    private int mLastRemovedPosition = -1;

    public CategoryDataProvider( List<CategoryDto> categoryDtos, int dDay ) {

        mCategoryData = new LinkedList<>();

        for( int i = 0; i < categoryDtos.size(); i++ ) {
            mCategoryData.add( new CategoryData( categoryDtos.get( i ), dDay ) );
        }
    }


    public int getCount() {
        return mCategoryData.size();
    }


    public CategoryData getItem(int index) {
        if (index < 0 || index >= getCount()) {
            throw new IndexOutOfBoundsException("index = " + index);
        }

        return mCategoryData.get( index );
    }


    public int undoLastRemoval() {
        if( mLastRemovedData != null ) {
            int insertedPosition;
            if( mLastRemovedPosition >= 0 && mLastRemovedPosition < mCategoryData.size() ) {
                insertedPosition = mLastRemovedPosition;
            } else {
                insertedPosition = mCategoryData.size();
            }

            mCategoryData.add( insertedPosition, mLastRemovedData );

            mLastRemovedData = null;
            mLastRemovedPosition = -1;

            return insertedPosition;
        } else {
            return -1;
        }
    }

    public void addItem( CategoryDto categoryDto ) {
        mCategoryData.add( 0, new CategoryData( categoryDto ) );
    }


    public void moveItem( int fromPosition, int toPosition ) {
        if( fromPosition == toPosition ) {
            return;
        }

        CategoryData item = mCategoryData.remove( fromPosition );

        mCategoryData.add( toPosition, item );
        mLastRemovedPosition = -1;
    }


    public void swapItem( int fromPosition, int toPosition ) {
        if( fromPosition == toPosition ) {
            return;
        }

        Collections.swap( mCategoryData, toPosition, fromPosition );
        mLastRemovedPosition = -1;
    }


    public void removeItem(int position) {
        //noinspection UnnecessaryLocalVariable
        CategoryData removedItem = mCategoryData.remove( position );

        mLastRemovedData = removedItem;
        mLastRemovedPosition = position;
    }

    public class CategoryData {

        private final long mId;
        private final int mViewType;
        private boolean mPinned;
        private boolean mCompleteChecked;
        private String mDDayStatus;

        private long mCategoryId;
        private String mTitle;
        private long mBudget;
        private long mExpense;

        CategoryData( CategoryDto categoryDto, int dDay ) {
            mId = categoryDto.getId();
            mViewType = 0;
            mPinned = true;
            mCompleteChecked = categoryDto.isComplete();
            mDDayStatus = calculateDdayStatus( dDay, categoryDto.getdDayStart(), categoryDto.getdDayEnd() );
            mCategoryId = categoryDto.getId();
            mTitle = categoryDto.getTitle();
            mBudget = categoryDto.getcBudget();
            mExpense = categoryDto.getExpense();
        }

        CategoryData( CategoryDto categoryDto ) {
            mId = categoryDto.getId();
            mViewType = 0;
            mPinned = true;
            mCompleteChecked = categoryDto.isComplete();
            mDDayStatus = "green";
            mCategoryId = categoryDto.getId();
            mTitle = categoryDto.getTitle();
            mBudget = categoryDto.getcBudget();
            mExpense = categoryDto.getExpense();
        }

        private String calculateDdayStatus( int dday, int dDayStart, int dDayEnd ) {

            String retStr = "green";

            if( dday <= dDayEnd ) {
                retStr = "red";
            } else if( dDayStart >= dday && dday > dDayEnd ) {
                retStr = "orange";
            } else if( dday > 7 && dDayStart  >= dday - 7 ) {
                retStr = "yellow";
            }

            return retStr;
        }

        public boolean isSectionHeader() {
            return false;
        }

        public int getViewType() {
            return mViewType;
        }

        public long getId() {
            return mId;
        }

        public long getCategoryId() {
            return mCategoryId;
        }

        public void setCategoryId(long categoryId) {
            this.mCategoryId = categoryId;
        }

        public boolean isPinned() {
            return mPinned;
        }

        public void setPinned(boolean pinned) {
            mPinned = pinned;
        }

        public boolean isCompleteChecked() {
            return mCompleteChecked;
        }

        public void setCompleteChecked(boolean completeChecked) {
            this.mCompleteChecked = completeChecked;
        }

        public String getDDayStatus() {
            return mDDayStatus;
        }

        public String getTitle() {
            return mTitle;
        }

        public long getBudget() {
            return mBudget;
        }

        public long getExpense() {
            return mExpense;
        }
    }

}
