package com.jjlee.wedqueen;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.fragments.EditDetailFragment;
import com.jjlee.wedqueen.fragments.EditMainFragment;
import com.jjlee.wedqueen.rest.content.controller.ContentController;
import com.jjlee.wedqueen.rest.content.model.Content;
import com.jjlee.wedqueen.rest.content.model.GetContentResponse;
import com.jjlee.wedqueen.rest.content.model.PostContent;
import com.jjlee.wedqueen.rest.content.model.PostContentResponse;
import com.jjlee.wedqueen.rest.image.controller.ImageController;
import com.jjlee.wedqueen.rest.image.model.ImageResponse;
import com.jjlee.wedqueen.rest.verification.controller.VerificationController;
import com.jjlee.wedqueen.rest.verification.model.Verification;
import com.jjlee.wedqueen.rest.verification.model.VerificationResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.FileUtils;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

//import io.userhabit.service.Userhabit;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by esmac on 2017. 9. 29..
 */

public class ContentEditActivity extends AppCompatActivity {

    private ViewPager contenteditViewPager;
    private ContentEditAdapter contentEditAdapter;
    private ImageController imageController;
    private ContentController contentController;
    private Uri imgUri;
    private List<File> selectedImgFiles;
    private String filePath;

    private String surveyReviewParam;
    private String paymentReviewParam;

    // v2 POST
    private Long paymentId;
    private Long surveyAnswerId;

    private boolean isSaved = false;
    private Dialog closeDlg;

    private Long contentId = (long)-1;
    private String contentState = "";
    private VerificationController verificationController;
    private Verification verification;

    private AppCompatDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_contentedit);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "edit_realwedding" ) );

        verificationController = new VerificationController();
        Call<VerificationResponse> verificationCall = verificationController.getVerification( "content_applied" );
        verificationCall.enqueue(new Callback<VerificationResponse>() {
            @Override
            public void onResponse(Call<VerificationResponse> call, Response<VerificationResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    VerificationResponse verificationResponse = response.body();
                    verification = verificationResponse.getVerification();
                }
            }

            @Override
            public void onFailure(Call<VerificationResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.contentedit_tablayout);
        contenteditViewPager = (ViewPager) findViewById(R.id.contentedit_viewpager);

        contentEditAdapter = new ContentEditAdapter( getSupportFragmentManager() );
        contenteditViewPager.setAdapter( contentEditAdapter );
        tabLayout.setupWithViewPager( contenteditViewPager );

        initialCloseDlg();

        imageController = new ImageController();
        contentController = new ContentController();

        surveyReviewParam = getIntent().getStringExtra( "surveyReviewParam" );
        paymentReviewParam = getIntent().getStringExtra( "paymentReviewParam" );

        // param으로 넘어온 reviewParam 우선 체크
        if( !ObjectUtils.isEmpty( surveyReviewParam ) ) {
            String[] arrSurveyReviewParam = surveyReviewParam.split( ";" );

            if( !ObjectUtils.isEmpty( arrSurveyReviewParam ) ) {
                surveyAnswerId = Long.parseLong( arrSurveyReviewParam[0] );
            }
        } else if( !ObjectUtils.isEmpty( paymentReviewParam ) ) {
            paymentId = Long.parseLong( paymentReviewParam );
        }

        contentId = getIntent().getLongExtra( "contentId", -1 );
        if( contentId != -1 ) {

            Log.e( "WQTEST", "contentId : " + contentId );

            progressON( ContentEditActivity.this, "Load..." );

            // 임시저장하거나 발행된 Content를 수정하기 위한 Content 정보를 서버로부터 불러와서 각 Fragment들에 Setting해주어야 함
            Call<GetContentResponse> contentCall = contentController.getContent( contentId );
            contentCall.enqueue(new Callback<GetContentResponse>() {
                @Override
                public void onResponse(Call<GetContentResponse> call, Response<GetContentResponse> response) {

                    progressOFF();

                    if( !ObjectUtils.isEmpty( response.body() ) ) {
                        GetContentResponse contentResponse = response.body();

                        if( !ObjectUtils.isEmpty( contentResponse ) ) {
                            contentState = contentResponse.getContent().getState();

                            // 저장된 reviewableId가 있는 경우 set
                            if( !ObjectUtils.isEmpty( contentResponse.getContent().getPaymentId() ) ) {
                                paymentId = contentResponse.getContent().getPaymentId();
                            }
                            if( !ObjectUtils.isEmpty( contentResponse.getContent().getSurveyAnswerId() ) ) {
                                surveyAnswerId = contentResponse.getContent().getSurveyAnswerId();
                            }

                            if( !contentState.equals( "temp" ) ) {
                                TextView cancelBtn = (TextView) closeDlg.findViewById(R.id.cancel_btn);
                                cancelBtn.setText( "수정" );
                                cancelBtn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        closeDlg.dismiss();
                                        putModifyContent();
                                    }
                                });
                            }

                            ((EditMainFragment)contentEditAdapter.getRegisteredFragment( 0 )).setGetContentData( contentResponse.getContent() );
                            ((EditDetailFragment)contentEditAdapter.getRegisteredFragment( 1 )).setGetContentData( contentResponse.getContent() );
                            isSaved = true;
                        }
                    }
                }

                @Override
                public void onFailure(Call<GetContentResponse> call, Throwable t) {
                    progressOFF();
                    t.printStackTrace();
                }
            });
        }
    }

    public PostContent setPostContentData() {
        PostContent postContent = new PostContent();
        ((EditMainFragment)contentEditAdapter.getRegisteredFragment(0)).setPostContentData( postContent );
        ((EditDetailFragment)contentEditAdapter.getRegisteredFragment(1)).setPostContentData( postContent );
        postContent.setAutoSave( false );

        return postContent;
    }


    // 수정 PUT ACTION
    public void putModifyContent() {
        // 필수값 입력 체크
        if( !((EditMainFragment)contentEditAdapter.getRegisteredFragment( 0 )).checkMainDataSet()
                || !((EditDetailFragment)contentEditAdapter.getRegisteredFragment( 1 )).checkDetailDataSet() ) {
            return;
        }

        progressON( ContentEditActivity.this, "Save..." );

        PostContent postContent = setPostContentData();
        postContent.setState( contentState );

        // v2 POST
        if( !ObjectUtils.isEmpty( surveyAnswerId ) ) {
            postContent.setSurveyAnswerId( surveyAnswerId );
        }
        if( !ObjectUtils.isEmpty( paymentId ) ) {
            postContent.setPaymentId( paymentId );
        }

        Call<PostContentResponse> call = contentController.putContentV2( postContent, contentId );
        call.enqueue(new Callback<PostContentResponse>() {
            @Override
            public void onResponse(Call<PostContentResponse> call, Response<PostContentResponse> response) {
                progressOFF();
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    PostContentResponse postContentResponse = response.body();

                    if( postContentResponse.getCode() == 200 ) {
                        isSaved = true;
                        Toast.makeText( ContentEditActivity.this, "수정에 성공하였습니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText( ContentEditActivity.this, postContentResponse.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e( "WQTEST", "response's body is null" );
                    Toast.makeText( ContentEditActivity.this, "수정에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PostContentResponse> call, Throwable t) {
                progressOFF();
                t.printStackTrace();
                Toast.makeText( ContentEditActivity.this, "수정에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }


    // 발행 POST ACTION
    public void postPublishContent() {

        // 필수값 입력 체크
        if( !((EditMainFragment)contentEditAdapter.getRegisteredFragment( 0 )).checkMainDataSet()
                || !((EditDetailFragment)contentEditAdapter.getRegisteredFragment( 1 )).checkDetailDataSet() ) {
            return;
        }

        if( contentId != -1 ) {
            putPublishContent();
            return;
        }

        progressON( ContentEditActivity.this, "Save..." );

        PostContent postContent = setPostContentData();
        postContent.setState( "applied" );

        // v2 POST
        if( !ObjectUtils.isEmpty( surveyAnswerId ) ) {
            postContent.setSurveyAnswerId( surveyAnswerId );
        }
        if( !ObjectUtils.isEmpty( paymentId ) ) {
            postContent.setPaymentId( paymentId );
        }

        Call<PostContentResponse> call = contentController.postContentV2( postContent );
        call.enqueue(new Callback<PostContentResponse>() {
            @Override
            public void onResponse(Call<PostContentResponse> call, Response<PostContentResponse> response) {
                progressOFF();
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    PostContentResponse postContentResponse = response.body();

                    if( postContentResponse.getCode() == 200 ) {
                        Toast.makeText( ContentEditActivity.this, "발행에 성공하였습니다.", Toast.LENGTH_SHORT).show();
                        finishThisActivity();
                    } else {
                        Toast.makeText( ContentEditActivity.this, postContentResponse.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e( "WQTEST", "response's body is null" );
                    Toast.makeText( ContentEditActivity.this, "발행에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PostContentResponse> call, Throwable t) {
                progressOFF();
                t.printStackTrace();
                Toast.makeText( ContentEditActivity.this, "발행에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // 임시저장 POST ACTION
    public void postTempContent() {

        // 필수값 입력 체크
        if( !((EditMainFragment)contentEditAdapter.getRegisteredFragment( 0 )).checkMainDataSet()
                || !((EditDetailFragment)contentEditAdapter.getRegisteredFragment( 1 )).checkDetailDataSet() ) {
            return;
        }

        if( contentId != -1 ) {
            putTempContent();
            return;
        }

        progressON( ContentEditActivity.this, "Save..." );

        PostContent postContent = setPostContentData();
        postContent.setState( "temp" );

        // v2 POST
        if( !ObjectUtils.isEmpty( surveyAnswerId ) ) {
            postContent.setSurveyAnswerId( surveyAnswerId );
        }
        if( !ObjectUtils.isEmpty( paymentId ) ) {
            postContent.setPaymentId( paymentId );
        }

        Call<PostContentResponse> call = contentController.postContentV2( postContent );
        call.enqueue(new Callback<PostContentResponse>() {
            @Override
            public void onResponse(Call<PostContentResponse> call, Response<PostContentResponse> response) {
                progressOFF();
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    PostContentResponse postContentResponse = response.body();

                    if( postContentResponse.getCode() == 200 ) {
                        contentId = (long)postContentResponse.getResult().getContentId();
                        isSaved = true;
                        Toast.makeText( ContentEditActivity.this, "작성중이던 내용이 임시저장 되었습니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText( ContentEditActivity.this, postContentResponse.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e( "WQTEST", "response's body is null" );
                    Toast.makeText( ContentEditActivity.this, "임시저장에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PostContentResponse> call, Throwable t) {
                progressOFF();
                t.printStackTrace();
                Toast.makeText( ContentEditActivity.this, "임시저장에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // 수정 PUT ACTION
    public void putPublishContent() {

        progressON( ContentEditActivity.this, "Save..." );

        PostContent postContent = setPostContentData();
        postContent.setState( "applied" );

        // v2 POST
        if( !ObjectUtils.isEmpty( surveyAnswerId ) ) {
            postContent.setSurveyAnswerId( surveyAnswerId );
        }
        if( !ObjectUtils.isEmpty( paymentId ) ) {
            postContent.setPaymentId( paymentId );
        }

        Call<PostContentResponse> call = contentController.putContentV2( postContent, contentId );
        call.enqueue(new Callback<PostContentResponse>() {
            @Override
            public void onResponse(Call<PostContentResponse> call, Response<PostContentResponse> response) {
                progressOFF();
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    PostContentResponse postContentResponse = response.body();

                    if( postContentResponse.getCode() == 200 ) {
                        Toast.makeText( ContentEditActivity.this, "발행에 성공하였습니다.", Toast.LENGTH_SHORT).show();
                        finishThisActivity();
                    } else {
                        Toast.makeText( ContentEditActivity.this, postContentResponse.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e( "WQTEST", "response's body is null" );
                    Toast.makeText( ContentEditActivity.this, "발행에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PostContentResponse> call, Throwable t) {
                progressOFF();
                t.printStackTrace();
                Toast.makeText( ContentEditActivity.this, "발행에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    // 임시저장 PUT ACTION ( 이미 임시저장된 ContentId가 존재 )
    public void putTempContent() {

        progressON( ContentEditActivity.this, "Save..." );

        PostContent postContent = setPostContentData();
        postContent.setState( "temp" );

        // v2 POST
        if( !ObjectUtils.isEmpty( surveyAnswerId ) ) {
            postContent.setSurveyAnswerId( surveyAnswerId );
        }
        if( !ObjectUtils.isEmpty( paymentId ) ) {
            postContent.setPaymentId( paymentId );
        }

        Call<PostContentResponse> call = contentController.putContentV2( postContent, contentId );
        call.enqueue(new Callback<PostContentResponse>() {
            @Override
            public void onResponse(Call<PostContentResponse> call, Response<PostContentResponse> response) {
                progressOFF();
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    PostContentResponse postContentResponse = response.body();
                    Log.e( "WQTEST", "contentId : " + postContentResponse.getResult().getContentId() );

                    if( postContentResponse.getCode() == 200 ) {
                        contentId = (long)postContentResponse.getResult().getContentId();
                        isSaved = true;
                        Toast.makeText( ContentEditActivity.this, "작성중이던 내용이 임시저장 되었습니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText( ContentEditActivity.this, postContentResponse.getMsg(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e( "WQTEST", "response's body is null" );
                    Toast.makeText( ContentEditActivity.this, "임시저장에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PostContentResponse> call, Throwable t) {
                progressOFF();
                t.printStackTrace();
                Toast.makeText( ContentEditActivity.this, "임시저장에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( resultCode == Activity.RESULT_CANCELED ) {
            return;
        }

        File file;
        selectedImgFiles = new ArrayList<>();
        int dotIdx = -1;

        if( ObjectUtils.isEmpty( data ) || resultCode != Activity.RESULT_OK ) {
            return;
        }

        switch(requestCode) {

            // 글쓰기 대표이미지 추가
            case Constants.REQ_CODE_OPEN_THUMBNAIL_IMG :

                imgUri = data == null || resultCode != Activity.RESULT_OK ? null : data.getData();

                file = FileUtils.getFile( this, imgUri );

                if( file != null && file.getName() != null ) {
                    dotIdx = file.getName().lastIndexOf('.');
                }
                filePath = file.getAbsolutePath();

                if( dotIdx > 0 && (
                        "jpg".equals(file.getName().substring( dotIdx + 1 ).toLowerCase())
                                || "jpeg".equals(file.getName().substring( dotIdx + 1 ).toLowerCase())
                                || "png".equals(file.getName().substring( dotIdx + 1 ).toLowerCase())
                )) {

                    progressON( ContentEditActivity.this, "Upload..." );

                    Call<ImageResponse> call = imageController.postImageUploadToServer( file, "general" );

                    call.enqueue(new Callback<ImageResponse>() {
                        @Override
                        public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                            ImageResponse imageResponse = response.body();

                            isSaved = false;
                            Fragment fragment = contentEditAdapter.getRegisteredFragment( 0 );
                            if( !ObjectUtils.isEmpty( imageResponse.getFileNames() ) && !ObjectUtils.isEmpty( imageResponse.getFileNames().get( 0 ) ) ) {
                                ((EditMainFragment) fragment).setThumbnailImg( imgUri, imageResponse.getFileNames().get( 0 ) );
                            } else {
                                Toast.makeText(ContentEditActivity.this, "이미지 업로드에 실패하였습니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_LONG).show();
                            }
                            progressOFF();
                        }

                        @Override
                        public void onFailure(Call<ImageResponse> call, Throwable t) {
                            progressOFF();
                            t.printStackTrace();
                            Log.e("WQTEST", t.getMessage());
                            Toast.makeText(ContentEditActivity.this, "이미지 업로드에 실패하였습니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    Toast.makeText( ContentEditActivity.this, "이미지 파일만 업로드 가능합니다.", Toast.LENGTH_LONG ).show();
                }

                break;

            // 글쓰기 본문 이미지 추가
            case Constants.REQ_CODE_OPEN_CONTENT_IMG :

                imgUri = data.getData();

                if( ObjectUtils.isEmpty( imgUri ) ) {
                    ClipData clipData = data.getClipData();

                    for( int i = 0; i < clipData.getItemCount(); i++ ) {
                        Uri uri = clipData.getItemAt( i ).getUri();
                        File imgFile = FileUtils.getFile( this, uri );

                        // 이미지 파일이고 정상적인 파일명을 가지고 있지 않으면 break
                        if( !checkImgFileVerification( imgFile ) ) {
                            Toast.makeText( ContentEditActivity.this, "이미지 파일만 업로드 가능합니다.", Toast.LENGTH_LONG ).show();
                            return;
                        } else {
                            selectedImgFiles.add( imgFile );
                        }
                    }

                } else {
                    File imgFile = FileUtils.getFile( this, imgUri );

                    // 이미지 파일이고 정상적인 파일명을 가지고 있지 않으면 break;
                    if( !checkImgFileVerification( imgFile ) ) {
                        Toast.makeText( ContentEditActivity.this, "이미지 파일만 업로드 가능합니다.", Toast.LENGTH_LONG ).show();
                        return;
                    } else {
                        selectedImgFiles.add( imgFile );
                    }
                }

                if( selectedImgFiles.size() > 0 ) {
                    progressON( ContentEditActivity.this, "Upload..." );

                    Call<ImageResponse> call = imageController.postImagesUploadToServer( selectedImgFiles, "general" );

                    call.enqueue(new Callback<ImageResponse>() {
                        @Override
                        public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                            ImageResponse imageResponse = response.body();

                            isSaved = false;

                            Fragment fragment = contentEditAdapter.getRegisteredFragment( 0 );

                            List<String> imgFileNames = imageResponse.getFileNames();

                            if( !ObjectUtils.isEmpty( imgFileNames ) && imgFileNames.size() > 0 ) {
                                for( int i = 0; i < imgFileNames.size(); i++ ) {
                                    ((EditMainFragment) fragment).addContentImg( selectedImgFiles.get( i ).getAbsolutePath(), imageResponse.getFileNames().get( i ) );
                                }
                            }

                            progressOFF();
                        }

                        @Override
                        public void onFailure(Call<ImageResponse> call, Throwable t) {
                            progressOFF();
                            t.printStackTrace();
                            Log.e("WQTEST", t.getMessage());
                            Toast.makeText(ContentEditActivity.this, "이미지 업로드에 실패하였습니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_LONG).show();
                        }
                    });
                }

                break;
        }
    }


    // 정상적인 파일명을 가진 이미지파일인지 Check
    private boolean checkImgFileVerification( File imgFile ) {

        boolean retVal = false;
        int dotIdx = -1;

        if( imgFile != null && imgFile.getName() != null ) {
            dotIdx = imgFile.getName().lastIndexOf( '.' );
        }

        if( dotIdx > 0 && (
                "jpg".equals(imgFile.getName().substring( dotIdx + 1 ).toLowerCase())
                        || "jpeg".equals(imgFile.getName().substring( dotIdx + 1 ).toLowerCase())
                        || "png".equals(imgFile.getName().substring( dotIdx + 1 ).toLowerCase())
        )) {
            retVal = true;
        }

        return retVal;
    }


    public class ContentEditAdapter extends FragmentPagerAdapter {

        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        public ContentEditAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch ( position ) {
                case 0 :
                    return new EditMainFragment();
                case 1 :
                    return EditDetailFragment.newInstance( surveyReviewParam );

            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
    }

    public void moveFragment(int position) {
        if( contenteditViewPager != null ) {
            contenteditViewPager.setCurrentItem( position );
        }
    }


    // 이미지 로딩 Dialog
    public void progressON(Activity activity, String message) {

        if (activity == null || activity.isFinishing()) {
            return;
        }


        if (progressDialog != null && progressDialog.isShowing()) {
            progressSET(message);
        } else {

            progressDialog = new AppCompatDialog(activity);
            progressDialog.setCancelable(false);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.layout_frame_loading);
            progressDialog.show();
//            Userhabit.setScreen( progressDialog, "이미지로딩 Dialog" );

        }


        final ImageView img_loading_frame = (ImageView) progressDialog.findViewById(R.id.iv_frame_loading);
        final AnimationDrawable frameAnimation = (AnimationDrawable) img_loading_frame.getBackground();
        img_loading_frame.post(new Runnable() {
            @Override
            public void run() {
                frameAnimation.start();
            }
        });

        TextView tv_progress_message = (TextView) progressDialog.findViewById(R.id.tv_progress_message);
        if (!TextUtils.isEmpty(message)) {
            tv_progress_message.setText(message);
        }
    }

    public void progressSET(String message) {

        if (progressDialog == null || !progressDialog.isShowing()) {
            return;
        }

        TextView tv_progress_message = (TextView) progressDialog.findViewById(R.id.tv_progress_message);
        if (!TextUtils.isEmpty(message)) {
            tv_progress_message.setText(message);
        }

    }

    public void progressOFF() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }


    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean saved) {
        isSaved = saved;
    }

    public long getContentId() {
        return this.contentId;
    }

    public void initialCloseDlg() {
        closeDlg = new Dialog( this );
        closeDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        closeDlg.setContentView(R.layout.dialog_save);

        ((TextView)closeDlg.findViewById(R.id.text_to_show)).setText( "저장되지 않았습니다.\n나가시겠습니까?" );

        TextView okBtn = (TextView) closeDlg.findViewById(R.id.ok_btn);
        okBtn.setText( "나가기" );
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDlg.dismiss();
                // 해당 Activity를 종료하고 MainActivity로 이동
                finishThisActivity();
            }
        });

        TextView cancelBtn = (TextView) closeDlg.findViewById(R.id.cancel_btn);
        cancelBtn.setText( "임시저장" );
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDlg.dismiss();
                // 작성한 내용들 임시저장 Action
                postTempContent();
            }
        });
    }


    // 글쓰기 Activity를 종료하기전에 result Code를 OK 상태로 변경해놓기 위한 작업
    public void finishThisActivity() {
        Intent intent = new Intent();
        setResult( RESULT_OK, intent );
        finish();
    }


    public void showCloseDlg() {
        if( !isSaved ) {
            closeDlg.show();
        } else {
            finishThisActivity();
        }
    }

    public Verification getVerification() {
        return verification;
    }

    @Override
    public void onBackPressed() {

        showCloseDlg();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
