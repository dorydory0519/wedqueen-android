package com.jjlee.wedqueen;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.jjlee.wedqueen.rest.app.controller.AppController;
import com.jjlee.wedqueen.rest.app.model.VersionResponse;
import com.jjlee.wedqueen.services.RegistrationIntentService;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.support.UpdateDialog;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by JJLEE on 2016. 3. 30..
 *
 * Splash Activity에서는
 * 1. deviceToken을 구하고,
 * 2. 로그인 시도를 하여
 * 3. 로그인 성공시 -> 로그인 후 MainActivity를 띄운다
 * 4. 로그인 실패시 -> 그냥 MainActivity를 띄운다
 */

public class SplashActivity extends AppCompatActivity {

    private static final int ERR_CODE_MANDATORY_UPDATE = 301;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final int AUTO_LOGIN_REQUEST = 1313;
    private static final String TAG = "SPLASH_ACTIVITY";

    private ImageView mLaunchImage;
    private ImageView mLaunchText;
    private boolean isLaunchImageAnimated = false;

    private Prefs prefs;
    public static CountDownLatch countDownLatch;

    private int errCode = -1; // -1 NO_ERR

    private PowerManager.WakeLock wl;

    private AppController appController;

    @SuppressWarnings("AccessStaticViaInstance")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentType( "Activity" )
                .putContentName( "splash" ) );

        MyApplication.getGlobalApplicationContext().setCurrentActivity(this);
        appController = new AppController();
        prefs = new Prefs(this);
        PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock( PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, TAG);
        wl.acquire();

        if (!isTaskRoot()
                && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                && getIntent().getAction() != null
                && getIntent().getAction().equals(Intent.ACTION_MAIN)) {

            finish();
            return;
        } else {
            setContentView(R.layout.activity_splash);
            processValidation();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (errCode == ERR_CODE_MANDATORY_UPDATE) {
            AlertDialog updateDialog = UpdateDialog.createDialog(SplashActivity.this);
            updateDialog.show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @SuppressWarnings("AccessStaticViaInstance")
    @Override
    protected void onDestroy() {
        MyApplication.getGlobalApplicationContext().setCurrentActivity(null);
        Runtime.getRuntime().gc();
        wl.release();
        super.onDestroy();
    }


    private void processValidation()  {

//        Prefs prefs = new Prefs(SplashActivity.this);

        checkLatestVersionFromServer();

        /////////////////////////////////////////////////////
        countDownLatch = new CountDownLatch(2);
        startAccountActivity();
        startLaunchImageAnimation();
        /////////////////////////////////////////////////////


        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    countDownLatch.await(20, TimeUnit.SECONDS);
                    // 20초가 지났는데도 위 작업이 다 안끝났으면 그냥 진행
                    //Constants.TAB_CATEGORIES_NAME.clear();
                    //Constants.TAB_CATEGORIES_NAME.putAll(prefs.getCurrentTabCategories());
                    if (countDownLatch.getCount() != 0) {
                        forceSetMandatoryThings();
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (errCode == -1) {
                                getInstanceIdToken();
                                startMainActivity();// DeviceToken을 구해서 저장함
                            } else {
                                if (errCode == ERR_CODE_MANDATORY_UPDATE) {
                                    AlertDialog updateDialog = UpdateDialog.createDialog(SplashActivity.this);
                                    updateDialog.show();
                                }
                            }
                        }
                    });

                } catch (InterruptedException e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            startMainActivity();
                        }
                    });
                }
            }
        });


    }

    private void pickRandomLaunchImageAndText() {
        mLaunchImage = (ImageView) findViewById(R.id.launch_image);
        mLaunchText = (ImageView) findViewById(R.id.launch_text);
        Random rand = new Random();
        int imageIndex = rand.nextInt(21) + 1;
        int textIndex = rand.nextInt(15) + 1;
        int imageId = this.getResources().getIdentifier("launch_image_" + imageIndex, "drawable", this.getPackageName());
        int textId = this.getResources().getIdentifier("launch_text_" + textIndex, "drawable", this.getPackageName());
        mLaunchImage.setImageResource(imageId);
        mLaunchText.setImageResource(textId);
    }

    private void startLaunchImageAnimation() {
        pickRandomLaunchImageAndText();
        if (!isLaunchImageAnimated) {
            mLaunchText.setAlpha(0.0f);
            mLaunchImage.setAlpha(0.0f);

            final Animator alphaAnimatorImage = ObjectAnimator.ofFloat(mLaunchImage, View.ALPHA, 0.0f, 1.0f);
//            alphaAnimatorImage.setDuration(1000);
//            alphaAnimatorImage.setStartDelay(500);
            alphaAnimatorImage.setDuration(500);
            alphaAnimatorImage.setStartDelay(250);

            final Animator alphaAnimatorText = ObjectAnimator.ofFloat(mLaunchText, View.ALPHA, 0.0f, 1.0f);
//            alphaAnimatorText.setDuration(1000);
//            alphaAnimatorText.setStartDelay(500);
            alphaAnimatorText.setDuration(500);
            alphaAnimatorText.setStartDelay(250);

            final Animator delayAnimator = ObjectAnimator.ofFloat(mLaunchText, View.ALPHA, 1.0f, 1.0f);
//            delayAnimator.setDuration(10);
//            delayAnimator.setStartDelay(1000); // 런칭 이미지 다 뜬 후 1.01초 후에 로그인 시도
            delayAnimator.setDuration(5);
            delayAnimator.setStartDelay(500);

            final AnimatorSet animatorSetImage = new AnimatorSet();
            final AnimatorSet animatorSetText = new AnimatorSet();

            animatorSetText.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    countDownLatch.countDown();
                }
            });

            animatorSetImage.play(alphaAnimatorImage);
            animatorSetText.play(delayAnimator).after(alphaAnimatorText);
            animatorSetImage.start();
            animatorSetText.start();

            isLaunchImageAnimated = true;
        }
    }

    /**
     * Instance ID를 이용하여 디바이스 토큰을 가져오는 RegistrationIntentService를 실행한다.
     */
    public void getInstanceIdToken() {
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            MyApplication.getGlobalApplicationContext().startService(intent);
        }
    }

    /**
     * Google Play Service를 사용할 수 있는 환경인지를 체크한다. ( 사용 못하는 환경에서도 쓸수 있음 )
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability googleApi = GoogleApiAvailability.getInstance();
        int resultCode = googleApi.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApi.isUserResolvableError(resultCode)) {
                googleApi.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);

        intent.putExtra("URL", getIntent().getStringExtra("URL")); // 노티피케이션 URL extra 전달
        intent.putExtra("NOTI_ID", getIntent().getStringExtra("NOTI_ID")); // 노티피케이션 NOTI_ID extra 전달

        // 뷰페이져 기본 페이지 인덱스 전달
        intent.putExtra("SHOULD_CHANGE_PAGE_TO", getIntent().getIntExtra("SHOULD_CHANGE_PAGE_TO", 0));
        startActivity(intent);
        finish();
    }

    private void startAccountActivity() { // 자동 로그인 시도
        //Toast.makeText(this,"디버깅 메세지: 자동 로그인 시도",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, AccountActivity.class);
        intent.putExtra(Constants.ACTION_TYPE, Constants.ACTION_TYPE_AUTO_LOGIN);
        startActivityForResult(intent, AUTO_LOGIN_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) { // 자동 로그인 결과
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == AUTO_LOGIN_REQUEST) {
            if(resultCode == Activity.RESULT_OK) {
                //Toast.makeText(this,"디버깅 메세지: AccountActivity Result 성공 (자동로그인 or 익명로그인)",Toast.LENGTH_SHORT).show();
            } else {
                switch(resultCode) {
                    case 426:
                        errCode = ERR_CODE_MANDATORY_UPDATE;
                        //Toast.makeText(this,"디버깅 메세지: 로그인 실패. 업데이트 필요.",Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        //Toast.makeText(this,"디버깅 메세지: 로그인 실패. StatusCode: " + resultCode + " | 익명으로 로그인 진행" ,Toast.LENGTH_SHORT).show();
                        break;
                }
            }
            countDownLatch.countDown(); // 자동 로그인 결과에 상관없이 진행
        }
    }

    /**
     * version?version=1.4.0 GET 통해 버전 체크.
     * 강제 업데이트랑은 상관없고, MyApplication.isLatest 만 세팅한다.
     *
     */
    private void checkLatestVersionFromServer() {
        Call<VersionResponse> call = appController.checkVersion(MyApplication.getGlobalApplicationContext().getVersionName());
        call.enqueue(new Callback<VersionResponse>() {
            @Override
            public void onResponse(Call<VersionResponse> call, Response<VersionResponse> response) {
                try {
                    MyApplication.getGlobalApplicationContext().setIsLatestVersion(response.body().isLatest());
                } catch(Exception e) {
                    MyApplication.getGlobalApplicationContext().setIsLatestVersion(true);
                }
            }
            @Override
            public void onFailure(Call<VersionResponse> call, Throwable t) {
                // 최신 버전인지 아닌지 모르지만 문제가 발생할 수 있으므로
                // 최신 버전이라 가정하고 진행.
                MyApplication.getGlobalApplicationContext().setIsLatestVersion(true);
            }
        });
    }


    /**
     *
     * countDownLatch 의 카운트가 0이 아닌데 진행이 되었다면
     *
     *  startLaunchImageAnimation();
     *  startAccountActivity();
     *
     *  두개 중 진행이 안된 것이 존재함. 따라서 앱 진행에 있어 꼭 필요한 것을
     *  여기서 세팅해준다.
     */
    public void forceSetMandatoryThings() {
        MyApplication.getGlobalApplicationContext().setIsLatestVersion(true);
    }

    @Override
    public void onBackPressed() {
        // should do nothing
    }
}


