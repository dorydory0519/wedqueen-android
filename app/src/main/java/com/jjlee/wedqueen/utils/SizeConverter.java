package com.jjlee.wedqueen.utils;

import android.content.res.Resources;

/**
 * Created by JJLEE on 2016. 4. 14..
 */
public class SizeConverter {
    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static float dpToPx(float dp)
    {
        return dp * Resources.getSystem().getDisplayMetrics().density;
    }


    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
}
