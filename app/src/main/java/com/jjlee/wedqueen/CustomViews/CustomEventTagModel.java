package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjlee.wedqueen.R;

public class CustomEventTagModel extends LinearLayout {

    private TextView eventTag;

    public CustomEventTagModel(Context context) {
        super(context);
        initView();
    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_eventtag, this, false);
        addView(v);

        eventTag = (TextView) findViewById( R.id.event_tag );

    }

    public void setEventTag( String resString ) {

        eventTag.setText( resString );
    }
}
