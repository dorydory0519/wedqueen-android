package com.jjlee.wedqueen.support;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.rest.app.model.TabCategory;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Cookie;

/**
 * Created by JJLEE on 2016. 4. 4..
 */
public class Prefs {

    private SharedPreferences prefs ;
    private SharedPreferences.Editor editor ;

    public Prefs( Context context ) {
        prefs = context.getSharedPreferences( PrefParams.PREF_NAME , Context.MODE_PRIVATE ) ;
    }

    // 로그인 무엇으로 헀는지 기억해놓기
    public void setLogin ( int type ) {
        editor = prefs.edit();
        editor.putInt(PrefParams.LOGIN, type) ;
        editor.commit() ;
    }

    public int getLogin ( ) {
        return prefs.getInt(PrefParams.LOGIN, -1) ;
    }

    public void setLastLogin ( int type ) {
        editor = prefs.edit();
        editor.putInt( PrefParams.LAST_LOGIN, type ) ;
        editor.commit() ;
    }

    public int getLastLogin ( ) {
            return prefs.getInt( PrefParams.LAST_LOGIN, -1 ) ;
        }


    public boolean isLoginContentDetail() {
        return prefs.getBoolean(PrefParams.ISLOGIN_CONTENTDETAIL, true);
    }

    public void setLoginContentDetail( boolean isLoginContentDetail ) {
        editor = prefs.edit();
        editor.putBoolean(PrefParams.ISLOGIN_CONTENTDETAIL, isLoginContentDetail);
        editor.commit();
    }


    public void setLastDeviceToken(String deviceToken) {
        editor = prefs.edit();
        editor.putString(PrefParams.LAST_DEVICE_TOKEN, deviceToken);
        editor.commit();
    }
    public String getLastDeviceToken() {
        return prefs.getString(PrefParams.LAST_DEVICE_TOKEN, null);
    }

    public void setLaunchCount(int count) {
        editor = prefs.edit();
        editor.putInt(PrefParams.LAUNCH_COUNT, count);
        editor.commit();
    }
    public int getLaunchCount() {
        return prefs.getInt(PrefParams.LAUNCH_COUNT, 0);
    }

    public String getUuid() {
        return prefs.getString(PrefParams.UUID, null);
    }

    public void setUuid( String uuid ) {
        editor = prefs.edit();
        editor.putString(PrefParams.UUID, uuid);
        editor.commit();
    }

    public String getAdid() {
        return prefs.getString(PrefParams.ADID, null);
    }

    public void setAdid( String uuid ) {
        editor = prefs.edit();
        editor.putString(PrefParams.ADID, uuid);
        editor.commit();
    }

    public Calendar getDdayDate() {

        Calendar targetDate = Calendar.getInstance();

        try {
            String[] arrDday = prefs.getString(PrefParams.DDAY_DATE, "").split("-");
            targetDate.set(Calendar.YEAR, Integer.parseInt(arrDday[0]));
            targetDate.set(Calendar.MONTH, Integer.parseInt(arrDday[1]) - 1);
            targetDate.set(Calendar.DATE, Integer.parseInt(arrDday[2]));
            targetDate.set(Calendar.HOUR, 0);
            targetDate.set(Calendar.MINUTE, 0);
            targetDate.set(Calendar.SECOND, 0);
            targetDate.set(Calendar.MILLISECOND, 0);
        } catch ( Exception e ) {
            targetDate = null;
        }

        return targetDate;
    }

    public void setDdayDate( String ddayDate ) {
        editor = prefs.edit();
        editor.putString(PrefParams.DDAY_DATE, ddayDate);
        editor.commit();
    }

    public int getCBudget() {
        return prefs.getInt( "MY_CBUDGET", 0 );
    }

    public void setCBudget( int cBudget ) {
        editor = prefs.edit();
        editor.putInt( "MY_CBUDGET", cBudget );
        editor.commit();
    }

    public boolean getShowTutorial() {
        return prefs.getBoolean(PrefParams.SHOW_TUTORIAL, true);
    }

    public void setShowTutorial( boolean showTutorial ) {
        editor = prefs.edit();
        editor.putBoolean(PrefParams.SHOW_TUTORIAL, showTutorial);
        editor.commit();
    }

    public boolean getShowPopup() {
        return prefs.getBoolean(PrefParams.SHOW_POPUP, true);
    }

    public void setShowPopup( boolean showPopup ) {
        editor = prefs.edit();
        editor.putBoolean(PrefParams.SHOW_POPUP, showPopup);
        editor.commit();
    }

    public int getShowInstantCount() {
        return prefs.getInt( PrefParams.SHOW_INSTANT_COUNT, 0 );
    }

    public void setShowInstantCount( int showInstantCount ) {
        editor = prefs.edit();
        editor.putInt( PrefParams.SHOW_INSTANT_COUNT, showInstantCount );
        editor.commit();
    }

    public int getShowHomeGuideCount() {
        return prefs.getInt( PrefParams.SHOW_HOME_GUIDE_COUNT, 0 );
    }

    public void setShowHomeGuideCount( int showHomeGuideCount ) {
        editor = prefs.edit();
        editor.putInt( PrefParams.SHOW_HOME_GUIDE_COUNT, showHomeGuideCount );
        editor.commit();
    }

    public int getShowWeddingtalkGuideCount() {
        return prefs.getInt( PrefParams.SHOW_WEDDINGTALK_GUIDE_COUNT, 0 );
    }

    public void setShowWeddingtalkGuideCount( int showWeddingtalkGuideCount ) {
        editor = prefs.edit();
        editor.putInt( PrefParams.SHOW_WEDDINGTALK_GUIDE_COUNT, showWeddingtalkGuideCount );
        editor.commit();
    }

    public int getShowChecklistGuideCount() {
        return prefs.getInt( PrefParams.SHOW_CHECKLIST_GUIDE_COUNT, 0 );
    }

    public void setShowChecklistGuideCount( int showChecklistGuideCount ) {
        editor = prefs.edit();
        editor.putInt( PrefParams.SHOW_CHECKLIST_GUIDE_COUNT, showChecklistGuideCount );
        editor.commit();
    }

    public int getShowPointshopGuideCount() {
        return prefs.getInt( PrefParams.SHOW_POINTSHOP_GUIDE_COUNT, 0 );
    }

    public void setShowPointshopGuideCount( int showPointshopGuideCount ) {
        editor = prefs.edit();
        editor.putInt( PrefParams.SHOW_POINTSHOP_GUIDE_COUNT, showPointshopGuideCount );
        editor.commit();
    }

    public int getShowMypageGuideCount() {
        return prefs.getInt( PrefParams.SHOW_MYPAGE_GUIDE_COUNT, 0 );
    }

    public void setShowMypageGuideCount( int showMypageGuideCount ) {
        editor = prefs.edit();
        editor.putInt( PrefParams.SHOW_MYPAGE_GUIDE_COUNT, showMypageGuideCount );
        editor.commit();
    }

    public int getUserPoint() {
        return prefs.getInt( PrefParams.USER_POINT, 0 );
    }

    public void setUserPoint( int point ) {
        editor = prefs.edit();
        editor.putInt( PrefParams.USER_POINT, point );
        editor.commit();
    }

    public String getUserId() {
        return prefs.getString( PrefParams.USER_ID, "" );
    }

    public void setUserId( String userId ) {
        editor = prefs.edit();
        editor.putString( PrefParams.USER_ID, userId );
        editor.commit();
    }

    public String getUserNickname() {
        return prefs.getString( PrefParams.USER_NICKNAME, "" );
    }

    public void setUserNickname( String nickname ) {
        editor = prefs.edit();
        editor.putString( PrefParams.USER_NICKNAME, nickname );
        editor.commit();
    }

    public String getUserRegion() {
        return prefs.getString( PrefParams.USER_REGION, "서울" );
    }

    public void setUserRegion( String region ) {
        editor = prefs.edit();
        editor.putString( PrefParams.USER_REGION, region );
        editor.commit();
    }

    public void setCookieData( List<Cookie> cookieData ) {
        editor = prefs.edit();
        String response = new Gson().toJson( cookieData );
        editor.putString( PrefParams.COOKIE_DATA, response );
        editor.commit();
    }

    public List<Cookie> getCookieData() {
        String strCookieData = prefs.getString( PrefParams.COOKIE_DATA, null );
        Type type = new TypeToken<List<Cookie>>(){}.getType();
        return new Gson().fromJson( strCookieData, type );
    }

    public void setRestrictedPopupList( List<Integer> popupList ) {
        editor = prefs.edit();
        String response = new Gson().toJson( popupList );
        editor.putString( PrefParams.RESTRICTED_POPUP_LIST, response );
        editor.commit();
    }

    public List<Integer> getRestrictedPopupList() {
        String strRestrictedPopupList = prefs.getString( PrefParams.RESTRICTED_POPUP_LIST, null );
        Type type = new TypeToken<List<Integer>>(){}.getType();
        return new Gson().fromJson( strRestrictedPopupList, type );
    }

    public void setLastPopupId( int lastPopupId ) {
        editor = prefs.edit();
        editor.putInt( "LAST_POPUP_ID", lastPopupId );
        editor.commit();
    }

    public int getLastPopupId() {
        return prefs.getInt( "LAST_POPUP_ID", -1 );
    }

    public void setRestrictedPopupDateMilli( long restrictedPopupDateMilli ) {
        editor = prefs.edit();
        editor.putLong( "RESTRICTED_POPUP_DATE_MILLI", restrictedPopupDateMilli );
        editor.commit();
    }

    public long getRestrictedPopupDateMilli() {
        return prefs.getLong( "RESTRICTED_POPUP_DATE_MILLI", 0 );
    }

    public void setTutorialImgUrl( List<String> imgUrls ) {
        editor = prefs.edit();
        String response = new Gson().toJson( imgUrls );
        editor.putString( PrefParams.TUTORIAL_IMGURL, response );
        editor.commit();
    }

    public List<String> getTutorialImgUrl() {
        String strTutorialImgUrlList = prefs.getString( PrefParams.TUTORIAL_IMGURL, null );
        Type type = new TypeToken<List<String>>(){}.getType();
        return new Gson().fromJson( strTutorialImgUrlList, type );
    }

    public void setPremiumAdIdx ( int idx ) {
        editor = prefs.edit();
        editor.putInt( "PREMIUM_AD_IDX", idx ) ;
        editor.commit() ;
    }

    public int getPremiumAdIdx ( ) {
        return prefs.getInt( "PREMIUM_AD_IDX", 0 ) ;
    }

    public void setTabCategoriesOpenList( HashMap<String, Boolean> tabCategoriesOpenList ) {
        editor = prefs.edit();
        String response = new Gson().toJson( tabCategoriesOpenList );
        editor.putString( PrefParams.TAB_CATEGORIES_OPENLIST, response );
        editor.commit();
    }

    public HashMap<String, Boolean> getTabCategoriesOpenList() {
        String strTabCategoriesOpenList = prefs.getString( PrefParams.TAB_CATEGORIES_OPENLIST, null );
        Type type = new TypeToken<HashMap<String, Boolean>>(){}.getType();
        return new Gson().fromJson( strTabCategoriesOpenList, type );
    }

    public void setCurrentTabCategories(HashMap<String, ArrayList<TabCategory>> tabCategories) {
        editor = prefs.edit();
        String response = new Gson().toJson(tabCategories);
        editor.putString(PrefParams.CURRENT_TAB_CATEGORIES, response);
        editor.commit();
    }

    public HashMap<String, ArrayList<TabCategory>> getCurrentTabCategories() {
        String currentTabCategoriesJson = prefs.getString(PrefParams.CURRENT_TAB_CATEGORIES, null);

        if (currentTabCategoriesJson == null || currentTabCategoriesJson.isEmpty()) {
            setCurrentTabCategories(getStaticTabCategories());
            return getStaticTabCategories();
        } else {
            Type type = new TypeToken<HashMap<String, ArrayList<TabCategory>>>(){}.getType();
            return new Gson().fromJson(currentTabCategoriesJson, type);
        }
    }

    /**
     * 각 탭들의 카테고리 설정 ( Static )
     */
    private HashMap<String, ArrayList<TabCategory>> getStaticTabCategories() {

        HashMap<String, ArrayList<TabCategory>> staticTabCategories = new HashMap<>();

        // 실패했다면 앱에서 직접 추가해줘.. (실시간이 아닌 셈)
        ArrayList<TabCategory> weddingTalkCategories = new ArrayList<>();
        ArrayList<TabCategory> contentFeedCategories = new ArrayList<>();

        contentFeedCategories.add(new TabCategory(9l, "전체", "", "contentfeed", 1l));
        contentFeedCategories.add(new TabCategory(10l, "상견례/시작", "start", "contentfeed", 10l));
        contentFeedCategories.add(new TabCategory(11l, "웨딩홀", "hall", "contentfeed", 30l));
        contentFeedCategories.add(new TabCategory(12l, "신혼여행", "honeymoon", "contentfeed", 70l));
        contentFeedCategories.add(new TabCategory(13l, "촬영", "studio", "contentfeed", 60l));
        contentFeedCategories.add(new TabCategory(14l, "드레스", "dress", "contentfeed", 40l));
        contentFeedCategories.add(new TabCategory(15l, "메이크업", "makeup", "contentfeed", 50l));
        contentFeedCategories.add(new TabCategory(18l, "청첩장", "invitation", "contentfeed", 120l));
        contentFeedCategories.add(new TabCategory(29l, "신혼집", "house", "contentfeed", 110l));
        contentFeedCategories.add(new TabCategory(50l, "예물", "jewelry", "contentfeed", 80l));
        contentFeedCategories.add(new TabCategory(51l, "예복", "suit", "contentfeed", 90l));
        contentFeedCategories.add(new TabCategory(52l, "웨딩케어", "care", "contentfeed", 140l));
        contentFeedCategories.add(new TabCategory(57l, "한복/예단", "giftclothes", "contentfeed", 100l));
        contentFeedCategories.add(new TabCategory(58l, "축가/DVD", "singvideo", "contentfeed", 130l));
        contentFeedCategories.add(new TabCategory(72l, "마무리", "end2", "contentfeed", 150l));
        contentFeedCategories.add(new TabCategory(73l, "보정/액자", "frame", "contentfeed", 65l));

        weddingTalkCategories.add(new TabCategory(20l, "웨딩톡", "publicTalk", "weddingtalk", 1l));

        staticTabCategories.put("contentfeed", contentFeedCategories);
        staticTabCategories.put("weddingtalk", weddingTalkCategories);

        return staticTabCategories;
    }


}
