package com.jjlee.wedqueen.rest.payment.service;

import com.jjlee.wedqueen.rest.payment.model.PaymentResponse;
import com.jjlee.wedqueen.rest.survey.model.SurveyResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface PaymentService {
    @Headers("Accept: application/json")
    @GET("api/v1/user/payment/{filter}")
    Call<PaymentResponse> getPayment(@Path("filter") String filter);

    @Headers("Accept: application/json")
    @GET("api/v1/user/payment")
    Call<PaymentResponse> getPayments();

}
