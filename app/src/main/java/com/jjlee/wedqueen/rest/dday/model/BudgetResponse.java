package com.jjlee.wedqueen.rest.dday.model;

public class BudgetResponse {
    private String budgetAvg;

    public String getBudgetAvg() {
        return budgetAvg;
    }

    public void setBudgetAvg(String budgetAvg) {
        this.budgetAvg = budgetAvg;
    }
}
