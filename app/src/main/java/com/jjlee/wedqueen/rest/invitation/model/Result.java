package com.jjlee.wedqueen.rest.invitation.model;

/**
 * Created by esmac on 2018. 3. 23..
 */

public class Result {
    private String msg;
    private boolean status;

    public Result() {
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
