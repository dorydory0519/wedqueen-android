package com.jjlee.wedqueen;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.gson.JsonObject;
import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.rest.content.controller.ContentController;
import com.jjlee.wedqueen.rest.content.model.PostContentResponse;
import com.jjlee.wedqueen.rest.file.controller.FileController;
import com.jjlee.wedqueen.rest.love.controller.LoveController;
import com.jjlee.wedqueen.rest.weddingtalk.controller.WeddingtalkController;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.CustomViews.CustomWebChromeClientForFullscreenActivity;
import com.jjlee.wedqueen.CustomViews.CustomWebviewClientForFullscreenActivity;
import com.jjlee.wedqueen.support.ProgressRequestBody;
import com.jjlee.wedqueen.support.WebviewJavascriptInterfaceForFullscreenActivity;
import com.jjlee.wedqueen.utils.FileUtils;
import com.jjlee.wedqueen.utils.KeyboardUtil;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FullscreenActivity extends WebviewContainingActivity implements ProgressRequestBody.UploadCallbacks {

    private AppEventsLogger logger;

    private LoveController loveController;
    private WeddingtalkController weddingtalkController;
    private FileController fileController;
    private ContentController contentController;

    private WebView mWebview;
    private CustomWebviewClientForFullscreenActivity customWVClient;
    private CustomWebChromeClientForFullscreenActivity customWCClient;

    private Toolbar toolbar;
    private ContentLoadingProgressBar mProgressBar;
    private View mNetworkErrorView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private boolean shouldNeverRefresh = false;
    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener;

    private String requestedUrl;
    private Map<String, String> customHeader;

    private KeyboardUtil keyboardUtil;

    private long contentId;

    private Date uploadStartDate;
    private Timer fakeFileUploadTimer;
    private int fakePercentage;

//    private String fabButtonUrl;

    public void initToolbarButtons() {

        shareButton = (ImageButton)findViewById(R.id.share_button);
        lovedOnButton = (ImageButton)findViewById(R.id.love_on_button);
        lovedOffButton = (ImageButton)findViewById(R.id.love_off_button);
        editButton = (ImageButton)findViewById(R.id.edit_button);
        deleteButton = (ImageButton)findViewById(R.id.delete_button);
        editContentButton = (ImageButton)findViewById(R.id.edit_content_button);
        deleteContentButton = (ImageButton)findViewById(R.id.delete_content_button);
        cancelContentButton = (ImageButton)findViewById(R.id.cancel_content_button);

        phoneButton = (ImageButton)findViewById(R.id.phone_button);
        talkButton = (ImageButton)findViewById(R.id.inquiry_button);
        locationButton =  (ImageButton)findViewById(R.id.location_button);
        homepageButton = (ImageButton)findViewById(R.id.homepage_button);
        homeButton = (ImageButton)findViewById(R.id.home_button);
        backButton = (ImageButton)findViewById(R.id.back_button);

        if(getIntent().getBooleanExtra("NO_TOOL_BAR", false)) {
            toolbar.setVisibility(View.GONE);
        }

        if( contentId > -1 ) {
            editContentButton.setVisibility( View.VISIBLE );
            deleteContentButton.setVisibility( View.VISIBLE );

            String state = getIntent().getStringExtra( "state" );
            if( !ObjectUtils.isEmpty( state ) &&  state.equals( "open" ) ) {
                cancelContentButton.setVisibility( View.VISIBLE );
            }
        }
    }

    @SuppressWarnings("ConstantConditions")
    public void initBrowsingSupportFunctionality() {
        mNetworkErrorView = findViewById(R.id.network_error);
        mNetworkErrorView.bringToFront();
        findViewById(R.id.retry_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideNetworkError();
                mWebview.loadUrl(requestedUrl, customHeader);
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimaryDark, R.color.colorAccent,
                R.color.colorAccent, R.color.colorAccent);
        mSwipeRefreshLayout.setProgressViewOffset(false, 0, SizeConverter.dpToPx(68));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {


            @Override
            public void onRefresh() {

                // 웨딩톡 글작성중일 때에는 refresh가 되지 않도록 제한
                if( requestedUrl.contains("/weddingtalk/new") ) {
                    mSwipeRefreshLayout.setRefreshing(false);
                } else {
                    hideNetworkError();
                    mWebview.loadUrl(requestedUrl, customHeader);
                }
            }
        });

        mProgressBar = (ContentLoadingProgressBar) findViewById(R.id.progress_bar);
        mProgressBar.getIndeterminateDrawable().setColorFilter(0xFFAF9A5E, android.graphics.PorterDuff.Mode.MULTIPLY);
        mProgressBar.bringToFront();
    }

    public void setCustomHeader() {
        int actionBarHeight = 48;
        customHeader = new HashMap<>();
        customHeader.put("deviceType", "android");
        if(!getIntent().getBooleanExtra("NO_TOOL_BAR", false)) {
            customHeader.put("Additional-Margin", actionBarHeight + "");
        }
    }

    public void initControllers() {
        loveController = new LoveController();
        weddingtalkController = new WeddingtalkController();
        fileController = new FileController();
        contentController = new ContentController();
    }

//    public void initFabButton() {
//        fabButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(MyApplication.getGlobalApplicationContext().isLoggedIn()) {
//                    // 지금은 그룹톡 쓰기만 있어서 당연히 로그인 되어 있어야 하는데, 그냥 노파심에 체크 함.
//                    mWebview.loadUrl("javascript:androidPauseAllVideo()");
//                    Intent intent = new Intent(FullscreenActivity.this, FullscreenActivity.class);
//                    intent.putExtra("URL", fabButtonUrl);
//                    startActivityForResult(intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY);
//                } else {
//                    Intent intent = new Intent(FullscreenActivity.this, AccountActivity.class);
//                    intent.putExtra(Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN);
//                    startActivityForResult(intent, Constants.REQ_CODE_LOGIN);
//                }
//            }
//        });
//
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);

        keyboardUtil = new KeyboardUtil(this, findViewById(android.R.id.content));
        keyboardUtil.enable();

        logger = AppEventsLogger.newLogger(this);
        ((MyApplication)MyApplication.getGlobalApplicationContext()).setCurrentActivity(this);

//        setFullscreenWindow();
        setCustomHeader();

        initControllers();

        toolbar = (Toolbar)findViewById(R.id.toolbar);

        mWebview = new WebView( this );

        requestedUrl = getIntent().getStringExtra("URL");
        contentId = getIntent().getLongExtra( "contentId", -1 );
        logWithUrl(requestedUrl);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "inner_webview" )
                .putCustomAttribute( "requestedUrl", requestedUrl ) );

        // notification 목록 확인하는 순간 Badge Count를 0으로 초기화
        if( requestedUrl.contains( "noti" ) ) {
            MyApplication.getGlobalApplicationContext().setBadgeCount(0);
        }

        customWVClient = new CustomWebviewClientForFullscreenActivity(this);
        customWCClient = new CustomWebChromeClientForFullscreenActivity(this);

        mWebview.setWebViewClient(customWVClient);
        mWebview.setWebChromeClient(customWCClient);
        mWebview.getSettings().setJavaScriptEnabled(true);

        mWebview.getSettings().setAppCachePath(this.getCacheDir().getPath());
        mWebview.getSettings().setAppCacheEnabled(true);
        mWebview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
//        mWebview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
//        mWebview.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

        if (Build.VERSION.SDK_INT >= 19) {
            mWebview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        else {
            mWebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);
            cookieManager.setAcceptThirdPartyCookies(mWebview, true);
        }

        if( Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT ) {
            mWebview.setWebContentsDebuggingEnabled(true);
        }

        mWebview.addJavascriptInterface(new WebviewJavascriptInterfaceForFullscreenActivity(this), "androidJSI");

        initBrowsingSupportFunctionality();
        initToolbarButtons();

        mSwipeRefreshLayout.addView(mWebview);

        addToolbarButtonListeners();

//        fabButton = (FloatingActionButton)findViewById(R.id.fab_button);
//        fabButtonUrl = "";
//        initFabButton();

        mWebview.loadUrl(requestedUrl, customHeader);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(this);

        mSwipeRefreshLayout.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener =
            new ViewTreeObserver.OnScrollChangedListener() {
                int prevY = 0;
                int toolbarOffset = 0;

                @Override
                public void onScrollChanged() {
                    int changed = Math.abs(prevY - mWebview.getScrollY());
                    if (prevY < mWebview.getScrollY()) { // scrolling Down
                        toolbarOffset -= changed * 0.4;
                        if (toolbarOffset < SizeConverter.dpToPx(-48)) {
                            toolbarOffset = SizeConverter.dpToPx(-48);
                        }
                    } else {
                        toolbarOffset += changed * 0.4;
                        if (toolbarOffset > 0) {
                            toolbarOffset = 0;
                        }
                    }
                    prevY = mWebview.getScrollY();
                    toolbar.setY(toolbarOffset);

                    if (mWebview.getScrollY() == 0 && !shouldNeverRefresh)
                        mSwipeRefreshLayout.setEnabled(true);
                    else
                        mSwipeRefreshLayout.setEnabled(false);

                }
            }
        );
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onPause() {
        super.onPause();
        mSwipeRefreshLayout.getViewTreeObserver().removeOnScrollChangedListener(mOnScrollChangedListener);
        if (mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setRefreshing(false);
            mSwipeRefreshLayout.destroyDrawingCache();
            mSwipeRefreshLayout.clearAnimation();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        keyboardUtil.disable();
        removeToolbarButtonListeners();
        customWVClient.nullStrongActivity();
        customWCClient.nullStrongActivity();
        if(mWebview != null) {
            mWebview.clearHistory();
//            mWebview.clearCache(true);
            mWebview.loadUrl("about:blank");
            mWebview.freeMemory();
            mWebview.destroy();
            mWebview = null;
        }
        super.onDestroy();
        ((MyApplication)MyApplication.getGlobalApplicationContext()).setCurrentActivity(null);
        Runtime.getRuntime().gc();
    }


    public void showInviteFriendPopup() {
        if( !MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            Intent intent = new Intent( FullscreenActivity.this, AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            FullscreenActivity.this.startActivityForResult( intent, Constants.REQ_CODE_LOGIN );
        } else {
            Intent intent = new Intent( this, FriendInviteActivity.class );
            startActivity( intent );
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_CANCELED) {
            return;
        }

        switch(requestCode) {
            case Constants.REQ_CODE_LOGIN:
            case Constants.REQ_CODE_LOGOUT: {
                refresh();
                Intent intent = new Intent();
                intent.putExtra("shouldRefresh", true);
                setResult(RESULT_OK, intent);
                break;
            }
            case Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY: {
                if (data.getBooleanExtra("shouldRefresh", false)) {
                    refresh();
                    Intent intent = new Intent();
                    intent.putExtra("shouldRefresh", true);
                    setResult(RESULT_OK, intent);
                }
                break;
            }
            case Constants.FILE_CHOOSE: {
                Uri result = data == null || resultCode != Activity.RESULT_OK ? null : data.getData();


                File file = FileUtils.getFile(this, result);

                int i = -1;
                if( file != null && file.getName() != null ) {
                    i = file.getName().lastIndexOf('.');
                }

                if( ObjectUtils.isEmpty( file ) ) {
                    Toast.makeText(FullscreenActivity.this, "파일이 정상적이지 않습니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_LONG).show();
                    break;
                }

                if( !ObjectUtils.isEmpty( mRequestPageType ) ) {
                    if( mRequestPageType.equals("invitationVideoUpload") ) {
                        if( file.length() / 1024 / 1024 > 200 ) {
                            Toast.makeText(FullscreenActivity.this, "최대 업로드 가능 크기는 200MB 입니다.", Toast.LENGTH_LONG).show();
                            break;
                        }
                        uploadStartDate = new Date();
                        mWebview.loadUrl("javascript:window.$progressBar = showOnProgress('동영상을 올리는 중입니다...', 0.75, true, false, 5);");
                        Call<JsonObject> call = fileController.postInvitationVideoFileToServer(file, mMimeType, this);
                        call.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                mWebview.loadUrl("javascript: " + mCallbackFunctionName + "('" + response.body().toString() + "');");
                                mWebview.loadUrl("javascript:hideOnProgress();");
                                fakeFileUploadTimer.cancel();
                                fakeFileUploadTimer.purge();
                                mCallbackFunctionName = null;
                                mRequestPageType = null;
                                mMimeType = null;

                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                Toast.makeText(FullscreenActivity.this, "파일 업로드를 실패하였습니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_LONG).show();
                                mWebview.loadUrl("javascript:hideOnProgress();");
                                mCallbackFunctionName = null;
                                mRequestPageType = null;
                                mMimeType = null;
                                fakeFileUploadTimer.cancel();
                                fakeFileUploadTimer.purge();
                                t.printStackTrace();
                            }
                        });

                    } else if( mRequestPageType.equals("sendbirdFileUpload") ) {
                        if( file.length() / 1024 / 1024 > 25 ) {
                            Toast.makeText(FullscreenActivity.this, "최대 업로드 가능 크기는 25MB 입니다.", Toast.LENGTH_LONG).show();
                            break;
                        }
                        mWebview.loadUrl("javascript:showOnProgress('파일을 업로드 중입니다.<br>잠시만 기다려주세요!', 0.7, false, true, 5000);");
                        Call<JsonObject> call = fileController.postLiveChatFileToServer(file, mMimeType);
                        call.enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                mWebview.loadUrl("javascript: " + mCallbackFunctionName + "('" + response.body().toString() + "');");
                                mWebview.loadUrl("javascript:hideOnProgress();");
                                mCallbackFunctionName = null;
                                mRequestPageType = null;
                                mMimeType = null;
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                Toast.makeText(FullscreenActivity.this, "파일 업로드를 실패하였습니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_LONG).show();
                                mWebview.loadUrl("javascript:hideOnProgress();");
                                mCallbackFunctionName = null;
                                mRequestPageType = null;
                                mMimeType = null;
                                t.printStackTrace();
                            }
                        });

                    } else {
                        if( file.length() / 1024 / 1024 > 25 ) {
                            Toast.makeText(FullscreenActivity.this, "최대 업로드 가능 크기는 25MB 입니다.", Toast.LENGTH_LONG).show();
                            break;
                        }
                        if (i > 0 && (
                                "jpg".equals(file.getName().substring(i + 1).toLowerCase())
                                        || "jpeg".equals(file.getName().substring(i + 1).toLowerCase())
                                        || "png".equals(file.getName().substring(i + 1).toLowerCase()))) {
                            // Main Activity 에서 이미지 요청을 하는 녀석은 마이페이지밖에 없음
                            mWebview.loadUrl("javascript:showOnProgress('파일을 업로드 중입니다.<br>잠시만 기다려주세요!', 0.7, false, true, 5000);");
                            Call<String> call = imageController.postImageToServer(file, mRequestPageType);

                            call.enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {

                                    if( !ObjectUtils.isEmpty( mWebview ) ) {
                                        if( !ObjectUtils.isEmpty( response.body() ) ) {
                                            mWebview.loadUrl("javascript: " + mCallbackFunctionName + "('" + response.body() + "'); ");
                                        }
                                        mWebview.loadUrl("javascript:hideOnProgress();");
                                    }

                                    mCallbackFunctionName = null;
                                    mRequestPageType = null;
                                    mMimeType = null;

                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {
                                    if( !ObjectUtils.isEmpty( mWebview ) ) {
                                        mWebview.loadUrl("javascript:hideOnProgress();");
                                    }

                                    Toast.makeText(FullscreenActivity.this, "업로드를 실패하였습니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_LONG).show();
                                    mCallbackFunctionName = null;
                                    mRequestPageType = null;
                                    mMimeType = null;

                                }
                            });

                        } else {
                            mWebview.loadUrl("javascript:showErrMsg('지원하지 않는 파일 형식입니다.'); ");
                            mCallbackFunctionName = null;
                            mRequestPageType = null;
                            mMimeType = null;
                        }
                    }
                }

                break;
            }
        }
    }

    ////// Multipart Request Callbacks (start)
    @Override
    public void onProgressUpdate(int percentage) {
        if( !ObjectUtils.isEmpty( mWebview ) ) {
            mWebview.loadUrl( "javascript:window.$progressBar.showProgress(" + (percentage / 10) + ", 100);" );
            mWebview.loadUrl( "javascript:window.$progressBar.changeProgressPercentage("+(percentage/10)+", 100);" );
        }
    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {
        final long elapsedTimeInMilli = new Date().getTime() - uploadStartDate.getTime();

        fakePercentage = 10;

        fakeFileUploadTimer = new Timer();
        fakeFileUploadTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(fakePercentage < 98) {
                    fakePercentage++;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if( !ObjectUtils.isEmpty( mWebview ) ) {
                                mWebview.loadUrl("javascript:window.$progressBar.showProgress(" + fakePercentage + ", 100);");
                                mWebview.loadUrl("javascript:window.$progressBar.changeProgressPercentage("+ fakePercentage +", 100);");
                            }
                        }
                    });
                }
            }
        }, 0, elapsedTimeInMilli / 10);

    }
    ////// Multipart Request Callbacks (finished)

    public void showNetworkError() {
        mNetworkErrorView.setVisibility(View.VISIBLE);
    }
    public void hideNetworkError() {
        mNetworkErrorView.setVisibility(View.GONE);
    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }
    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void refresh() {
        mWebview.loadUrl(requestedUrl, customHeader);
    }

    public void addToolbarButtonListeners() {
        ToolbarButtonsClickListener tcl = new ToolbarButtonsClickListener();
        backButton.setOnClickListener(tcl);
        homeButton.setOnClickListener(tcl);
        shareButton.setOnClickListener(tcl);

        lovedOnButton.setOnClickListener(tcl);
        lovedOffButton.setOnClickListener(tcl);
        editButton.setOnClickListener(tcl);
        deleteButton.setOnClickListener(tcl);
        editContentButton.setOnClickListener(tcl);
        deleteContentButton.setOnClickListener(tcl);
        cancelContentButton.setOnClickListener(tcl);

        phoneButton.setOnClickListener(tcl);
        talkButton.setOnClickListener(tcl);
        homepageButton.setOnClickListener(tcl);
        locationButton.setOnClickListener(tcl);
    }

    public void removeToolbarButtonListeners() {
        backButton.setOnClickListener(null);
        homeButton.setOnClickListener(null);
        shareButton.setOnClickListener(null);

        lovedOnButton.setOnClickListener(null);
        lovedOffButton.setOnClickListener(null);
        editButton.setOnClickListener(null);
        deleteButton.setOnClickListener(null);
        editContentButton.setOnClickListener(null);
        deleteContentButton.setOnClickListener(null);
        cancelContentButton.setOnClickListener(null);

        phoneButton.setOnClickListener(null);
        talkButton.setOnClickListener(null);
        homepageButton.setOnClickListener(null);
        locationButton.setOnClickListener(null);
    }

    public void toastErrMsg() {
        Toast.makeText(FullscreenActivity.this, "서버 오류: 잠시 후 다시 시도해주세요", Toast.LENGTH_SHORT).show();
    }

    public class ToolbarButtonsClickListener implements View.OnClickListener {

        public JSONObject getData(String itemName) {
            for (int i = 0; i < toolbarItemsJson.length(); i++) {
                final JSONObject obj;
                try {
                    obj = toolbarItemsJson.getJSONObject(i);
                    final String item = obj.getString("item");
                    if (item.equals(itemName)) {
                        return obj;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    return null;
                }
            }
            return null;
        }

        public void shareAction() {
            JSONObject shareObject = getData("share");
            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/plain");
            share.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);

            String title = "";
            String url = "http://www.wedqueen.com/";
            String imageUrl = "";
            try {
                title = URLDecoder.decode(shareObject.getString("title"), "utf-8").replaceAll("\\\\", "");
                url = URLDecoder.decode(shareObject.getString("url"), "utf-8");
                imageUrl = URLDecoder.decode(imageUrl, "utf-8");
            } catch(JSONException e) {
                e.printStackTrace();
                return;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            // Add data to the intent, the receiving app will decide
            // what to do with it.

            share.putExtra(Intent.EXTRA_SUBJECT, "[웨딩의 여신]\n\n" + title + "\n\n아래 링크를 클릭해서 보러가기\n");
            share.putExtra(Intent.EXTRA_TEXT, url);
            //share.putExtra(Intent.EXTRA_STREAM, imageUrl);
            //TODO: imageURL 을 어떻게 공유할지 잘 생각해보기

            startActivity(Intent.createChooser(share, "공유하기"));
        }

        public void loveAction(boolean willLove) {
            JSONObject loveObject = getData("love");
            Long lovableId;
            String type;

            if (loveObject == null) return;

            try {
                lovableId = loveObject.getLong("lovableId");
                type = loveObject.getString("type");
            } catch(JSONException e) {
                e.printStackTrace();
                return;
            }

            if (willLove) { // 러브 하려고 누름

                Call<CommonResponse> call = loveController.loveOn(lovableId, type);
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> commonResponse) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                lovedOffButton.setVisibility(View.GONE);
                                lovedOnButton.setVisibility(View.VISIBLE);
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        toastErrMsg();
                    }
                });

            } else {

                Call<CommonResponse> call = loveController.loveOff(lovableId, type);
                call.enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> commonResponse) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                lovedOffButton.setVisibility(View.VISIBLE);
                                lovedOnButton.setVisibility(View.GONE);
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        toastErrMsg();
                    }
                });
            }
        }


        public void editAction() {
            JSONObject editObject = getData("edit");
            try {
                String url = editObject.getString("url");
                String fullUrl = Constants.BASE_URL + url;
                startAnotherFullscreenActivityWithUrl(fullUrl);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        public void deleteAction() {
            new AlertDialog.Builder(FullscreenActivity.this)
                    .setTitle("웨딩톡 삭제")
                    .setMessage("정말 삭제하시겠습니까?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            JSONObject deleteObject = getData("delete");
                            try {
                                Long weddingTalkId = deleteObject.getLong("modelId");
                                Call<CommonResponse> call = weddingtalkController.deleteWeddingtalk(weddingTalkId);
                                call.enqueue(new Callback<CommonResponse>() {
                                    @Override
                                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                                        if(response.body().getResultCode() == 0) {
                                            Intent intent = new Intent();
                                            intent.putExtra("shouldRefresh", true);
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        } else {
                                            toastErrMsg();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                                        toastErrMsg();
                                    }
                                });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();


        }

        public void editContentAction() {
            // Move To ContentEditActivity with ContentId
            Intent intent = new Intent();
            intent.putExtra( "contentId", contentId );
            setResult(Activity.RESULT_OK, intent);
            finish();
        }

        public void deleteContentAction() {
            new AlertDialog.Builder(FullscreenActivity.this)
                    .setTitle("리얼웨딩 삭제를 원하시나요?")
                    .setMessage("발행된 리얼웨딩을 삭제시 적립된 포인트가 차감됩니다. 정말 삭제하시겠습니까?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            if( contentId > -1 ) {
                                Call<PostContentResponse> deleteCall = contentController.deleteContent( contentId );
                                deleteCall.enqueue(new Callback<PostContentResponse>() {
                                    @Override
                                    public void onResponse(Call<PostContentResponse> call, Response<PostContentResponse> response) {
                                        if( response.body().getCode() == 200 ) {
                                            Intent intent = new Intent();
                                            intent.putExtra("shouldRefresh", true);
                                            setResult(RESULT_OK, intent);
                                            finish();
                                        } else {

                                            if( !ObjectUtils.isEmpty( response.body().getMsg() ) ) {
                                                Toast.makeText(FullscreenActivity.this, response.body().getMsg(), Toast.LENGTH_LONG).show();
                                            } else {
                                                toastErrMsg();
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<PostContentResponse> call, Throwable t) {
                                        t.printStackTrace();
                                        toastErrMsg();
                                    }
                                });
                            }
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        public void cancelContentAction() {

            new AlertDialog.Builder(FullscreenActivity.this)
                    .setTitle("발행취소")
                    .setMessage("발행되어있던 컨텐츠를 노출이 되지 않도록 발행취소 하시겠습니까?")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            Call<PostContentResponse> cancelCall = contentController.putContentState( contentId, "closed" );
                            cancelCall.enqueue(new Callback<PostContentResponse>() {
                                @Override
                                public void onResponse(Call<PostContentResponse> call, Response<PostContentResponse> response) {
                                    if( response.body().getCode() == 200 ) {
                                        Toast.makeText( FullscreenActivity.this, "발행취소가 완료되었습니다.", Toast.LENGTH_SHORT ).show();
                                        Intent intent = new Intent();
                                        intent.putExtra("shouldRefresh", true);
                                        setResult(RESULT_OK, intent);
                                        finish();
                                    } else {
                                        toastErrMsg();
                                    }
                                }

                                @Override
                                public void onFailure(Call<PostContentResponse> call, Throwable t) {
                                    t.printStackTrace();
                                    toastErrMsg();
                                }
                            });
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        }

        public void inquiryAction() {
            JSONObject talkObject = getData("talk");
            try {
                String url = talkObject.getString("url");
                startAnotherFullscreenActivityWithUrl(url);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public void callAction() {
            JSONObject phoneObject = getData("phone");
            try {
                String url = phoneObject.getString("url");
                String telUri = "tel:" + url;
                Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.parse(telUri));
                FullscreenActivity.this.startActivity(callIntent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public void openHomepageAction() {

            JSONObject homepageObject = getData("homepage");
            try {
                String url = homepageObject.getString("url");
                Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setPackage("com.android.chrome");
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    // Chrome browser presumably not installed so allow user to choose instead
                    intent.setPackage(null);
                    startActivity(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        public void openLocationAction() {
            JSONObject homepageObject = getData("location");
            try {
                String address = homepageObject.getString("url").replace(" ", "+");
                String addressUri = "geo:0,0?q=" + address;
                Intent intent=new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(addressUri));
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Snackbar.make(FullscreenActivity.this.mWebview, "위치를 보여줄 지도 어플리케이션이 없습니다", Snackbar.LENGTH_SHORT).show();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.share_button:
                    shareAction();
                    break;
                case R.id.edit_button:
                    editAction();
                    break;
                case R.id.delete_button:
                    deleteAction();
                    break;
                case R.id.edit_content_button:
                    editContentAction();
                    break;
                case R.id.delete_content_button:
                    deleteContentAction();
                    break;
                case R.id.cancel_content_button:
                    cancelContentAction();
                    break;
                case R.id.love_off_button:
                    if( !MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
                        Intent intent = new Intent(FullscreenActivity.this, AccountActivity.class);
                        intent.putExtra(Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN);
                        FullscreenActivity.this.startActivityForResult(intent, Constants.REQ_CODE_LOGIN);
                    } else {
                        loveAction(true);
                    }
                    break;
                case R.id.love_on_button:
                    loveAction(false);
                    break;
                case R.id.phone_button:
                    callAction();
                    break;
                case R.id.inquiry_button:
                    if( !MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
                        Intent intent = new Intent(FullscreenActivity.this, AccountActivity.class);
                        intent.putExtra(Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN);
                        FullscreenActivity.this.startActivityForResult(intent, Constants.REQ_CODE_LOGIN);
                    } else {
                        inquiryAction();
                    }
                    break;
                case R.id.homepage_button:
                    openHomepageAction();
                    break;
                case R.id.location_button:
                    openLocationAction();
                    break;
                case R.id.home_button:
                    setRefreshMain();
                    NavUtils.navigateUpFromSameTask(FullscreenActivity.this);
                    break;
                case R.id.back_button:
                    onBackPressed();
                    break;
            }
        }

    }

    public void startAnotherFullscreenActivityWithUrl(String url) {
        Intent intent = new Intent(FullscreenActivity.this, FullscreenActivity.class);
        intent.putExtra("URL", url);
        startActivityForResult(intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY);
    }



    private String getDetailId(String url) {
        Pattern pat = Pattern.compile("/([0-9])+(/|\\?|.)");
        Matcher matcher = pat.matcher(url);
        if (matcher.find()) {
            return matcher.group(0).replaceAll("/", "");
        }
        return null;
    }

    private void logWithUrl(String url) {
        String type;
        String typeInEng;
        String contentId;
        if(url == null) return;

        if(url.contains("/weddingtalk/")) {
            type = "웨딩톡세부";
            typeInEng = "weddingtalk_detail";
            contentId = getDetailId(url);
        } else if(url.contains("/content/")) {
            type = "컨텐트세부";
            typeInEng = "content_detail";
            contentId = getDetailId(url);
        } else if(url.contains("/search")) {
            type = "검색페이지";
            typeInEng = "search";
            contentId = null;
        } else if(url.contains("/user/dday/")) {
            type = "디데이세부";
            typeInEng = "dday_detail";
            contentId = null;
        } else {
            type = "기타";
            typeInEng = "etc";
            contentId = null;
        }

        Bundle parameters = new Bundle();
        parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_TYPE, type);
        if(contentId != null) {
            parameters.putString(AppEventsConstants.EVENT_PARAM_CONTENT_ID, contentId);
        }
        logger.logEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT, parameters);
    }


//    public void setFabButtonUrlAndMakeVisible(String uri) {
//        fabButtonUrl = Constants.BASE_URL + uri;
//        showFab();
//    }
//
//    public void showFab() {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                fabButton.setVisibility(View.VISIBLE);
//            }
//        });
//    }
//
//    public void hideFab() {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                fabButton.setVisibility(View.GONE);
//            }
//        });
//    }

    public void hideToolbar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                toolbar.setVisibility(View.GONE);
            }
        });
    }

    public void showToolbar() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                toolbar.setVisibility(View.VISIBLE);
            }
        });

    }

    public void disableRefresh() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setEnabled(false);
                shouldNeverRefresh = true;
            }
        });

    }

    public void enableRefresh() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setEnabled(true);
                shouldNeverRefresh = false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if(mWebview.canGoBack()) {
            mWebview.goBack();
        } else {
            setRefreshMain();
            super.onBackPressed();
        }
    }

    private void setRefreshMain() {
        if( !ObjectUtils.isEmpty( requestedUrl ) && requestedUrl.contains( Constants.CHECKLIST_CATEGORY_URL ) ) {
            Intent intent = new Intent();
            intent.putExtra("refreshMain", true);
            setResult(RESULT_OK, intent);
        }
    }
}


