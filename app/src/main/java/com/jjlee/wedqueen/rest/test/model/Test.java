package com.jjlee.wedqueen.rest.test.model;

import java.io.Serializable;

/**
 * Created by LeeJungYi on 2016-04-01.
 */
public class Test implements Serializable {

    private int id;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Test() {

    }

    public Test(String name) {
        this.name = name;
    }
}
