package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.widget.AutoCompleteTextView;
import android.widget.SearchView;

/**
 * Created by esmac on 2017. 10. 15..
 */

public class CustomSearchView extends SearchView {

    private AutoCompleteTextView searchTextView;

    public CustomSearchView(Context context) {
        super(context);
    }


    public AutoCompleteTextView getSearchTextView() {

        return searchTextView;
    }

    public void setSearchTextView(AutoCompleteTextView searchTextView) {
        this.searchTextView = searchTextView;
    }
}
