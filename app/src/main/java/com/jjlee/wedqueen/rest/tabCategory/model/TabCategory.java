package com.jjlee.wedqueen.rest.tabCategory.model;

import java.io.Serializable;

public class TabCategory implements Serializable {

    private int id;
    private String categoryNameKor;
    private String categoryName;
    private String tabType;
    private String position;
    private boolean appVisible;
    private String iconUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryNameKor() {
        return categoryNameKor;
    }

    public void setCategoryNameKor(String categoryNameKor) {
        this.categoryNameKor = categoryNameKor;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTabType() {
        return tabType;
    }

    public void setTabType(String tabType) {
        this.tabType = tabType;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public boolean isAppVisible() {
        return appVisible;
    }

    public void setAppVisible(boolean appVisible) {
        this.appVisible = appVisible;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
