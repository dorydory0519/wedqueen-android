package com.jjlee.wedqueen;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.support.Constants;
import com.kakao.kakaolink.AppActionBuilder;
import com.kakao.kakaolink.AppActionInfoBuilder;
import com.kakao.kakaolink.KakaoLink;
import com.kakao.kakaolink.KakaoTalkLinkMessageBuilder;
import com.kakao.util.KakaoParameterException;

import java.lang.ref.WeakReference;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

public class ExternalWebviewActivity extends WebviewContainingActivity {

    private WebView mWebview;
    private String urlToLoad;
    private ImageButton closeBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_external_webview);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentType( "Activity" )
                .putContentName( "external_webview" ) );

        urlToLoad = getIntent().getStringExtra("url");
        mWebview = new WebView(this);
        mWebview.setWebViewClient(new ExternalWebViewClient(this));
        mWebview.setWebChromeClient(new WebChromeClient());
        mWebview.getSettings().setJavaScriptEnabled(true);
        ((FrameLayout)findViewById(R.id.webview_wrapper)).addView(mWebview);
        closeBtn = (ImageButton)findViewById(R.id.finish_exwv_activity_button);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.no_change, R.anim.slide_down);
            }
        });

        Map<String, String> customHeader = new HashMap<>();
        if(urlToLoad.contains(Constants.BASE_URL)) {
            customHeader.put("deviceType", "android");
        }
        mWebview.loadUrl(urlToLoad, customHeader);
    }

    @Override
    public void onBackPressed() {
        if(mWebview.canGoBack()) {
            mWebview.goBack();
        } else {
            finish();
            overridePendingTransition(R.anim.no_change, R.anim.slide_down);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        destroyWebview();
        super.onDestroy();
    }

    private void destroyWebview() {
        ((FrameLayout)findViewById(R.id.webview_wrapper)).removeAllViews();
        if(mWebview != null) {
            mWebview.clearHistory();
//            mWebview.clearCache(true);
            mWebview.loadUrl("about:blank");
            mWebview.freeMemory();
            mWebview.destroy();
            mWebview = null;
            Runtime.getRuntime().gc();
        }
    }

    private static class ExternalWebViewClient extends WebViewClient {
        private static WeakReference<Activity> weakReference = null;
        private Activity activity;


        public ExternalWebViewClient(Activity _activity) {
            WeakReference<Activity> wr = new WeakReference<>(_activity);
            weakReference = wr;
            activity = _activity;
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            Log.i("urlToLoad4", url);

            if( url.contains("kakaolink://download")) {
                try {
                    final KakaoLink kakaoLink = KakaoLink.getKakaoLink(MyApplication.getGlobalApplicationContext());
                    final KakaoTalkLinkMessageBuilder kakaoTalkLinkMessageBuilder = kakaoLink.createKakaoTalkLinkMessageBuilder();
                    kakaoTalkLinkMessageBuilder.addText("국내 최초 웨딩 SNS,\n웨딩의 여신에 당신을 초대합니다!\n모두 함께 스마트한 결혼준비 해봐요!")
                            .addImage("http://jcleeco.godohosting.com/content/4351/thumb.jpg", 400, 400)
                            .addAppButton("다운받으러 가기", new AppActionBuilder()
                                    .addActionInfo(AppActionInfoBuilder
                                            .createAndroidActionInfoBuilder()
                                            .setMarketParam("referrer=about_invite")
                                            .build())
                                    .setUrl("http://www.wedqueen.com/")
                                    .build());
                    kakaoLink.sendMessage(kakaoTalkLinkMessageBuilder, weakReference.get());

                } catch (KakaoParameterException e) {
                    e.printStackTrace();
                }
                return true;
            }
            else if(url.startsWith("market://") && url.contains("kr.co.citibank.citimobile")) {
//                view.loadUrl("market://search?q=pname:kr.co.citibank.citimobile");
//                Intent intent = new Intent(Intent.ACTION_MAIN);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=kr.co.citibank.citimobile"));
                activity.startActivity(intent);
                return true;
            }
            else if(url.startsWith("intent://") && url.contains("citimobileapp")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    Uri uri = Uri.parse(intent.getDataString());
                    intent = new Intent(Intent.ACTION_VIEW, uri);
                    activity.startActivity(intent);
                    return true;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                return true;
            }
            else if(url.startsWith("intent://") && url.contains("kakaotalkqrcode")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    Uri uri = Uri.parse(intent.getDataString());
                    intent = new Intent(Intent.ACTION_VIEW, uri);
                    activity.startActivity(intent);
                    return true;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                return true;
            }
            else if(url.startsWith("intent://") && url.contains("plusfriend")) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    Uri uri = Uri.parse(intent.getDataString());
                    intent = new Intent(Intent.ACTION_VIEW, uri);
                    activity.startActivity(intent);
                    return true;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                return true;
            }
            else if( url.contains("docs.google.com") || url.contains("drive.google.com") ) {
                try {
                    Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    Uri uri = Uri.parse(intent.getDataString());
                    intent = new Intent(Intent.ACTION_VIEW, uri);
                    activity.startActivity(intent);
                    return true;
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                return true;
            }
            else {
                return false;
            }
        }
    }
}