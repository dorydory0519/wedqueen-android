package com.jjlee.wedqueen.rest.survey.controller;

import com.jjlee.wedqueen.rest.subscription.model.SubscriptionResponse;
import com.jjlee.wedqueen.rest.subscription.service.SubscriptionService;
import com.jjlee.wedqueen.rest.survey.model.SurveyResponse;
import com.jjlee.wedqueen.rest.survey.service.SurveyService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class SurveyController extends RetrofitController {
    private SurveyService surveyService;

    public SurveyController() {
        super();
        surveyService = RETROFIT.create(SurveyService.class);
    }

    public Call<SurveyResponse> getSurveyAnswer() {
        return surveyService.getSurveyAnswer();
    }

    public Call<SurveyResponse> getSurveyAnswers() {
        return surveyService.getSurveyAnswers();
    }

}
