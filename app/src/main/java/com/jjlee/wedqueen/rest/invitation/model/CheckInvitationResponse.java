package com.jjlee.wedqueen.rest.invitation.model;

public class CheckInvitationResponse {

    private boolean invited;

    public boolean isInvited() {
        return invited;
    }

    public void setInvited(boolean invited) {
        this.invited = invited;
    }
}
