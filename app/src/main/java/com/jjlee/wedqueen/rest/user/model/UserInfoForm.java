package com.jjlee.wedqueen.rest.user.model;

import java.util.List;

/**
 * Created by leejungyi on 2017. 10. 26..
 */

public class UserInfoForm {

    private String nickname;
    private String filename;
    private String region;
    private String marriageDate;
    private Long weddingStyleId;
    private List<Long> tabCategoryIds;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getMarriageDate() {
        return marriageDate;
    }

    public void setMarriageDate(String marriageDate) {
        this.marriageDate = marriageDate;
    }

    public Long getWeddingStyleId() {
        return weddingStyleId;
    }

    public void setWeddingStyleId(Long weddingStyleId) {
        this.weddingStyleId = weddingStyleId;
    }

    public List<Long> getTabCategoryIds() {
        return tabCategoryIds;
    }

    public void setTabCategoryIds(List<Long> tabCategoryIds) {
        this.tabCategoryIds = tabCategoryIds;
    }
}
