package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;

public class CustomCheckCategory extends LinearLayout {

    private LinearLayout categoryItem;
    private CheckBox completeCheck;
    private TextView categoryTitle;
    private TextView categoryStatus;
    private TextView budget;
    private TextView expense;

    private long categoryId;

    public CustomCheckCategory(Context context) {
        super(context);
        initView();
    }

    public CustomCheckCategory(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService( infService );
        View v = li.inflate( R.layout.layout_checkcategory, this, false );

        categoryItem = v.findViewById( R.id.category_item );
        completeCheck = v.findViewById( R.id.completeCheck );
        categoryTitle = v.findViewById( R.id.category_title );
        categoryStatus = v.findViewById( R.id.category_status );
        budget = v.findViewById( R.id.budget );
        expense = v.findViewById( R.id.expense );

//        completeCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                SpannableString spannableString = new SpannableString( categoryTitle.getText() );
//                if( isChecked ) {
//                    spannableString.setSpan( new StrikethroughSpan(), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
//                    categoryTitle.setText( spannableString );
//                    categoryTitle.setTextColor( ContextCompat.getColor( getContext(), R.color.color_gray ) );
//                    categoryStatus.setTextColor( ContextCompat.getColor( getContext(), R.color.color_gray ) );
//                    categoryItem.setBackgroundColor( ContextCompat.getColor( getContext(), R.color.checked_category ) );
//                } else {
//                    StrikethroughSpan[] strikethroughSpans = spannableString.getSpans( 0, spannableString.length(), StrikethroughSpan.class );
//                    for( StrikethroughSpan strikethroughSpan : strikethroughSpans ) {
//                        spannableString.removeSpan( strikethroughSpan );
//                    }
//                    categoryTitle.setText( spannableString );
//                    categoryTitle.setTextColor( Color.parseColor( "#000000" ) );
//                    categoryStatus.setTextColor( Color.parseColor( "#FFFFFF" ) );
//
//                    categoryItem.setBackgroundColor( ContextCompat.getColor( getContext(), R.color.colorWhite ) );
//                }
//            }
//        });

        addView( v );
    }

    public void setCategoryTitle( String resString ) {
        if( ObjectUtils.isEmpty( resString ) ) {
            categoryTitle.setText( "제목없음" );
        } else {
            categoryTitle.setText( resString );
        }
    }

    public void setBudget( String resString ) {
        budget.setText( resString );
    }

    public void setExpense( String resString ) {
        expense.setText( resString );
    }

//    public void setCompleteCheck( boolean isChecked ) {
//        completeCheck.setChecked( isChecked );
//    }

    public CheckBox getCompleteCheck() {
        return completeCheck;
    }

    public LinearLayout getCategoryItem() {
        return categoryItem;
    }

    public TextView getCategoryTitle() {
        return categoryTitle;
    }

    public TextView getCategoryStatus() {
        return categoryStatus;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public void setCategoryStatus( String status ) {
        if( !ObjectUtils.isEmpty( status ) ) {
            if( status.equals( "red" ) ) {
                categoryStatus.setVisibility( VISIBLE );
                categoryStatus.setText( "급해요!" );
                categoryStatus.setBackground( ContextCompat.getDrawable( getContext(), R.drawable.category_status_emergency ) );
            } else if( status.equals( "orange" ) ) {
                categoryStatus.setVisibility( VISIBLE );
                categoryStatus.setText( "지금 해야해요!" );
                categoryStatus.setBackground( ContextCompat.getDrawable( getContext(), R.drawable.category_status_now ) );
            } else if( status.equals( "yellow" ) ) {
                categoryStatus.setVisibility( VISIBLE );
                categoryStatus.setText( "여유있어요" );
                categoryStatus.setBackground( ContextCompat.getDrawable( getContext(), R.drawable.category_status_spare ) );
            } else {
                categoryStatus.setVisibility( GONE );
            }
        }
    }

}
