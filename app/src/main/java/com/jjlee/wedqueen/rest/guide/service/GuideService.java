package com.jjlee.wedqueen.rest.guide.service;

import com.jjlee.wedqueen.rest.guide.model.GuideResponse;
import com.jjlee.wedqueen.rest.recommend.model.RecommendResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface GuideService {
    @Headers("Accept: application/json")

    @GET("api/v1/search/guide")
    Call<GuideResponse> getKeywordGuide();

}
