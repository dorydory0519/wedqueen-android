package com.jjlee.wedqueen.rest.company.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by LeeJungYi on 2016-04-01.
 */
public class Company implements Serializable {

    private int id;
    private String name;
    private String exposedName;
    private String thumbnailUrl;
    private String description;
    private String auxName;
    private String url;
    private String address;
    private Double latitude;
    private Double longitude;
    private List<Content> contents;
    private int paymentCount;
    private int surveyCount;
    private int realWeddingCount;

    private boolean inquirable;
    private boolean bookable;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExposedName() {
        return exposedName;
    }

    public void setExposedName(String exposedName) {
        this.exposedName = exposedName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuxName() {
        return auxName;
    }

    public void setAuxName(String auxName) {
        this.auxName = auxName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    public int getPaymentCount() {
        return paymentCount;
    }

    public void setPaymentCount(int paymentCount) {
        this.paymentCount = paymentCount;
    }

    public int getSurveyCount() {
        return surveyCount;
    }

    public void setSurveyCount(int surveyCount) {
        this.surveyCount = surveyCount;
    }

    public int getRealWeddingCount() {
        return realWeddingCount;
    }

    public void setRealWeddingCount(int realWeddingCount) {
        this.realWeddingCount = realWeddingCount;
    }

    public boolean isInquirable() {
        return inquirable;
    }

    public void setInquirable(boolean inquirable) {
        this.inquirable = inquirable;
    }

    public boolean isBookable() {
        return bookable;
    }

    public void setBookable(boolean bookable) {
        this.bookable = bookable;
    }
}
