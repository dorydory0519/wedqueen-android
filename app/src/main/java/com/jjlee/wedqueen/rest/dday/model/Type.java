package com.jjlee.wedqueen.rest.dday.model;

public enum Type {
    plan, hall, consulting, sdm, honeymoon, home, presentFromBride, presentToExchange, health, dress,
    rehearsal, interior, furnishings, invitation, preparation, proceduce, pyebaek, finalCheck;
}
