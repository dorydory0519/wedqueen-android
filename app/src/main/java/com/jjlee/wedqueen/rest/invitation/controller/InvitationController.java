package com.jjlee.wedqueen.rest.invitation.controller;

import com.jjlee.wedqueen.rest.invitation.model.CheckInvitationResponse;
import com.jjlee.wedqueen.rest.invitation.model.InvitationForm;
import com.jjlee.wedqueen.rest.invitation.model.InvitationResponse;
import com.jjlee.wedqueen.rest.invitation.model.InvitationSaveResponse;
import com.jjlee.wedqueen.rest.invitation.service.InvitationService;
import com.jjlee.wedqueen.support.RetrofitController;

import java.util.List;

import retrofit2.Call;

public class InvitationController extends RetrofitController {
    private InvitationService invitationService;

    public InvitationController() {
        super();
        invitationService = RETROFIT.create(InvitationService.class);
    }

    public Call<InvitationResponse> getMyInvitationInfo() {
        return invitationService.getMyInvitationInfo();
    }
    public Call<CheckInvitationResponse> checkInvitation() {
        return invitationService.checkInvitation();
    }
    public Call<InvitationSaveResponse> saveInvitationCode( InvitationForm invitationForm ) {
        return invitationService.saveInvitationCode( invitationForm );
    }

}
