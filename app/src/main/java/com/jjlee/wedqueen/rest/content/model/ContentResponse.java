package com.jjlee.wedqueen.rest.content.model;

import java.util.List;

/**
 * Created by JJLEE on 2016. 4. 23..
 */
public class ContentResponse {
    private List<Content> contents;

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }
}
