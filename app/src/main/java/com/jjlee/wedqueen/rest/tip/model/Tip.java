package com.jjlee.wedqueen.rest.tip.model;

import java.util.List;

public class Tip {

    private List<Content> content;
    boolean lastPage;

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }

    public boolean isLastPage() {
        return lastPage;
    }

    public void setLastPage(boolean lastPage) {
        this.lastPage = lastPage;
    }
}
