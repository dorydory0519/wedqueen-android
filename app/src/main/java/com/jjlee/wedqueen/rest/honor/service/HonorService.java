package com.jjlee.wedqueen.rest.honor.service;

import com.jjlee.wedqueen.rest.honor.model.HonorUserResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface HonorService {
    @Headers("Accept: application/json")
    @GET("api/v1/honor/users")
    Call<HonorUserResponse> getHonorUsers();
}
