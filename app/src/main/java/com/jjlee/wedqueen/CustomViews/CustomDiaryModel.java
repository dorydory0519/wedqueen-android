package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;

/**
 * Created by esmac on 2017. 9. 9..
 */

public class CustomDiaryModel extends RelativeLayout {

    RequestManager mRequestManager;

    private Long contentId;
    private String url;
    private String role;
    private String state;

    private TextView diaryDday;
    private TextView diaryCategory;
    private ImageView diaryImg;
    private TextView diaryViewcount;
    private TextView diaryTitle;
    private TextView diaryState;

    public CustomDiaryModel(Context context) {
        super(context);
        initView();
    }

    public CustomDiaryModel(Context context, RequestManager requestManager) {
        super(context);
        this.mRequestManager = requestManager;
        initView();
    }

    public CustomDiaryModel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomDiaryModel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }


    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_diary, this, false);
        addView(v);

        diaryDday = (TextView)findViewById(R.id.diary_dday);
        diaryCategory = (TextView)findViewById(R.id.diary_category);
        diaryImg = (ImageView) findViewById(R.id.diary_img);
        diaryViewcount = (TextView)findViewById(R.id.diary_viewcount);
        diaryTitle = (TextView)findViewById(R.id.diary_title);
        diaryState = (TextView)findViewById(R.id.diary_state);

    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.DiaryModel);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.DiaryModel, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {

        String diaryDday_resID = typedArray.getString(R.styleable.DiaryModel_diary_dday);
        setDiaryDday( diaryDday_resID );

        String diaryCategory_resID = typedArray.getString(R.styleable.DiaryModel_diary_category);
        setDiaryCategory( diaryCategory_resID );

        String diaryImg_resID = typedArray.getString(R.styleable.DiaryModel_diary_img);
        setDiaryImg( diaryImg_resID );

        String diaryViewcount_resID = typedArray.getString(R.styleable.DiaryModel_diary_viewcount);
        setDiaryViewcount( diaryViewcount_resID );

        String diaryTitle_resID = typedArray.getString(R.styleable.DiaryModel_diary_title);
        setDiaryTitle( diaryTitle_resID );

        typedArray.recycle();
    }

    public void setDiaryDday(String resString) {
        diaryDday.setText( resString );
    }
    public void setDiaryCategory(String resString) {
        diaryCategory.setText( resString );
    }
    public void setDiaryViewcount(String resString) {
        diaryViewcount.setText( resString + " view" );
    }
    public void setDiaryTitle(String resString) {
        diaryTitle.setText( resString );
    }

    public void setDiaryImg(String resUrl) {
        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load(resUrl).centerCrop().placeholder( R.color.image_placeholder_color ).into( diaryImg );
        } else {
            Glide.with( getContext() ).load(resUrl).centerCrop().placeholder( R.color.image_placeholder_color ).into( diaryImg );
        }
    }

    public Long getContentId() {
        return contentId;
    }

    public void setContentId(Long contentId) {
        this.contentId = contentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setDiaryState(String resString) {
        if( ObjectUtils.isEmpty( resString ) ) {
            // 비어있으면
            diaryState.setVisibility( View.GONE );
        } else {
            if( resString.equals( "closed" ) ) {
                resString = "발행취소";
            } else if( resString.equals( "applied" ) ) {
                resString = "발행";
            } else if( resString.equals( "open" ) ) {
                resString = "발행";
            } else if( resString.equals( "edited" ) ) {
                resString = "발행";
            } else if( resString.equals( "temp" ) ) {
                resString = "임시저장";
            }
            diaryState.setText( resString );
        }

    }
}
