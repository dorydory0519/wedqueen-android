package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jjlee.wedqueen.R;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class CustomPaymentModel extends LinearLayout {

    private ImageView paymentCompanyThumb;
    private TextView paymentCompanyName;
    private TextView paymentAmount;
    private TextView paymentTime;
    private TextView paymentStatus;

    private LinearLayout contractSection;
    private LinearLayout reviewSection;

    private LinearLayout contractMark;
    private LinearLayout contractAuth;
    private LinearLayout reviewMark;
    private LinearLayout reviewAuth;

    private TextView contractPoint;
    private TextView reviewPoint;

    private String strContractPoint;
    private String strReviewPoint;

    private LinearLayout paymentOn;
    private LinearLayout paymentOff;


    public CustomPaymentModel(Context context) {
        super(context);
        initView();

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_payment, this, false);
        addView(v);

        contractSection = (LinearLayout)v.findViewById( R.id.contract_section );
        reviewSection = (LinearLayout)v.findViewById( R.id.review_section );

        paymentCompanyThumb = (ImageView)v.findViewById( R.id.payment_company_thumb );
        paymentCompanyName = (TextView)v.findViewById( R.id.payment_company_name );
        paymentAmount = (TextView)v.findViewById( R.id.payment_amount );
        paymentTime = (TextView)v.findViewById( R.id.payment_time );
        paymentStatus = (TextView)v.findViewById( R.id.payment_status );
        contractPoint = (TextView)v.findViewById( R.id.contract_point );
        reviewPoint = (TextView)v.findViewById( R.id.review_point );

        contractMark = v.findViewById( R.id.contract_mark );
        contractAuth = v.findViewById( R.id.contract_auth );
        reviewMark = v.findViewById( R.id.review_mark );
        reviewAuth = v.findViewById( R.id.review_auth );

        paymentOn = (LinearLayout)v.findViewById( R.id.payment_on );
        paymentOff = (LinearLayout)v.findViewById( R.id.payment_off );

    }

    public void setPaymentOnOff( boolean existPayment ) {
        if( existPayment ) {
            paymentOn.setVisibility( View.VISIBLE );
            paymentOff.setVisibility( View.GONE );
        } else {
            paymentOn.setVisibility( View.GONE );
            paymentOff.setVisibility( View.VISIBLE );
        }
    }


    public void setPaymentCompanyThumb( String resUrl ) {
        Glide.with( getContext() ).load( resUrl ).centerCrop().bitmapTransform( new CropCircleTransformation( getContext() ) ).placeholder( R.color.image_placeholder_color ).into( paymentCompanyThumb );
    }
    public void setPaymentCompanyName(String resString) {
        paymentCompanyName.setText( resString );
    }
    public void setPaymentAmount(String resString) {
        paymentAmount.setText( resString );
    }
    public void setPaymentTime(String resString) {
        paymentTime.setText( resString );
    }
    public void setPaymentStatus(String resString) {
        paymentStatus.setText( resString );
    }
    public void setPaymentStatusColorCode( String resString ) {
        try {
            paymentStatus.setTextColor( Color.parseColor( resString ) );
        } catch ( Exception e ) {
        }
    }
    public void setReviewParam( int paymentId ) {
        reviewSection.setTag( paymentId );
    }


    public void setContract( boolean isContract, String point ) {

        contractPoint.setText( point );

        if( isContract ) {
//            contractPoint.setText( "지급완료" );
//            contractPoint.setBackgroundColor( getResources().getColor( R.color.paid_point_back_color ) );
            contractMark.setVisibility( VISIBLE );
            contractAuth.setVisibility( GONE );
        } else {
//            contractPoint.setText( point );
//            contractPoint.setBackgroundColor( getResources().getColor( R.color.point_back_color ) );
            contractMark.setVisibility( GONE );
            contractAuth.setVisibility( VISIBLE );
        }
    }

    public void setReview( boolean isReview, String point ) {

        reviewPoint.setText( point );

        if( isReview ) {
//            reviewPoint.setText( "지급완료" );
//            reviewPoint.setBackgroundColor( getResources().getColor( R.color.paid_point_back_color ) );
            reviewMark.setVisibility( VISIBLE );
            reviewAuth.setVisibility( GONE );
        } else {
//            reviewPoint.setText( point );
//            reviewPoint.setBackgroundColor( getResources().getColor( R.color.point_back_color ) );
            reviewMark.setVisibility( GONE );
            reviewAuth.setVisibility( VISIBLE );
        }
    }


    public LinearLayout getContractSection() {
        return contractSection;
    }
    public LinearLayout getReviewSection() {
        return reviewSection;
    }

    public String getStrContractPoint() { return strContractPoint; }
    public String getStrReviewPoint() {
        return strReviewPoint;
    }
}
