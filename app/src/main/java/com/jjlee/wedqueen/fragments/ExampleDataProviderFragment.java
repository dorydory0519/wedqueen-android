package com.jjlee.wedqueen.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.jjlee.wedqueen.view.AbstractDataProvider;
import com.jjlee.wedqueen.view.ExampleDataProvider;

public class ExampleDataProviderFragment extends Fragment {
    private AbstractDataProvider mDataProvider;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);  // keep the mDataProvider instance
//        mDataProvider = new ExampleDataProvider( null );
    }

    public AbstractDataProvider getDataProvider() {
        return mDataProvider;
    }
}
