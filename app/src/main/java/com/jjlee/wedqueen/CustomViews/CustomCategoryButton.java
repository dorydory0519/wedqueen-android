package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.app.model.TabCategory;

/**
 * Created by esmac on 2017. 9. 9..
 */

public class CustomCategoryButton extends LinearLayout {

    // TabCategory 멤버변수들
    private long categoryId;
    private long position;
    private String categoryNameKor;
    private String categoryName;
    private String tabType;

    private TextView categoryButton;
    private ImageView checkedImg;
    private boolean isSelected = false;

    public CustomCategoryButton(Context context) {
        super(context);
        initView();

    }

    public CustomCategoryButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomCategoryButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_category_btn, this, false);
        addView(v);

        categoryButton = (TextView) findViewById(R.id.category_btn);
        categoryButton.setClickable( false );
        checkedImg = (ImageView)findViewById(R.id.checked_img);

    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CategoryBtn);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CategoryBtn, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {

        typedArray.recycle();
    }

    public TextView getCategoryButton() {
        return categoryButton;
    }

    public void setCategoryButton(String resString) {
        categoryButton.setText( resString );
    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    @Override
    public void setSelected(boolean selected) {

        if( selected ) {
            categoryButton.setBackgroundResource( R.drawable.white_round_whiteborder );
            categoryButton.setTextColor( Color.parseColor( "#676767" ) );
            checkedImg.setVisibility( View.VISIBLE );
        } else {
            categoryButton.setBackgroundResource( R.drawable.gray_round_whiteborder );
            categoryButton.setTextColor( Color.parseColor( "#FFFFFF" ) );
            checkedImg.setVisibility( View.GONE );
        }

        isSelected = selected;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }

    public String getCategoryNameKor() {
        return categoryNameKor;
    }

    public void setCategoryNameKor(String categoryNameKor) {
        this.categoryNameKor = categoryNameKor;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTabType() {
        return tabType;
    }

    public void setTabType(String tabType) {
        this.tabType = tabType;
    }

    public TabCategory getTabCategory() {
        return new TabCategory( categoryId, categoryNameKor, categoryName, tabType, position);
    }
}
