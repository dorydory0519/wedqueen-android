package com.jjlee.wedqueen.rest.tutorial.model;


public class TutorialDetail {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

