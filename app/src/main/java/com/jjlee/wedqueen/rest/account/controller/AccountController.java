package com.jjlee.wedqueen.rest.account.controller;


import android.content.res.Resources;

import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.rest.account.model.Anonymous;
import com.jjlee.wedqueen.rest.account.model.GoogleOauthResponse;
import com.jjlee.wedqueen.rest.account.model.Login;
import com.jjlee.wedqueen.rest.account.model.LoginCommonResponse;
import com.jjlee.wedqueen.rest.account.model.NotificationCountResponse;
import com.jjlee.wedqueen.rest.account.service.AccountService;
import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.support.RetrofitController;
import com.jjlee.wedqueen.utils.MapUtil;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;

public class AccountController extends RetrofitController {
    private AccountService accountService;

    public AccountController() {
        super();
        accountService = RETROFIT.create(AccountService.class);
    }

    public Call<LoginCommonResponse> kakaoLoggedIn(Login login) {
        Map<String, String> map = MapUtil.getMapFromObject(login);
        return accountService.kakaoLoggedIn(map);
    }

    public Call<LoginCommonResponse> googleLoggedIn(Login login) {
        Map<String, String> map = MapUtil.getMapFromObject(login);
        return accountService.googleLoggedIn(map);
    }

    public Call<LoginCommonResponse> facebookLoggedIn(Login login) {
        Map<String, String> map = MapUtil.getMapFromObject(login);
        return accountService.facebookLoggedIn(map);
    }

    public Call<LoginCommonResponse> twitterLoggedIn(Login login) {
        Map<String, String> map = MapUtil.getMapFromObject(login);
        return accountService.twitterLoggedIn(map);
    }

    public Call<LoginCommonResponse> anonymousLoggedIn(Login login) {
        Map<String, String> map = MapUtil.getMapFromObject(login);
        return accountService.anonymousLoggedIn(map);
    }

    public Call<Void> serverLogout(String deviceToken) {
        return accountService.serverLogout(deviceToken);
    }

    public Call<CommonResponse> sendDeviceTokenToServer(String token) {
        return accountService.sendDeviceTokenToServer(token);
    }

    public Call<CommonResponse> sendDeviceTokenToServer(String token, String advertisingId) {
        return accountService.sendDeviceTokenToServer(token, advertisingId);
    }

    public Call<CommonResponse> unjoinUser() {
        return accountService.unjoinUser();
    }

    public Call<GoogleOauthResponse> getGoogleToken(String grantType, String cliendId,
                                                    String clientSecret, String redirectUri,
                                                    String authCode ) {
        return accountService.getGoogleToken(grantType, cliendId, clientSecret, redirectUri, authCode);
    }

    public Call<NotificationCountResponse> countDownSingleNotification(String notiId) {
        return accountService.countDownSingleNotification(notiId);
    }

    public Call<Anonymous> postAnonymousDeviceToken(String deviceToken) {
        return accountService.postAnonymousDeviceToken(deviceToken);
    }

    public Call<CommonResponse> deleteAnonymousDeviceToken(String uuid) {
        return accountService.deleteAnonymousDeviceToken(uuid);
    }

    public Call<CommonResponse> updateAnonymousDeviceToken(String uuid, String deviceToken) {
        return accountService.updateAnonymousDeviceToken(uuid, deviceToken);
    }

}
