package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jjlee.wedqueen.R;


public class CustomTextStyleEditModel extends RelativeLayout {

    RelativeLayout layoutTextstyle;
    TextView editStyle;
    ImageView presentColor;
    boolean isSelected;
    String textStyleMode;
    int textSize;

    public CustomTextStyleEditModel(Context context) {
        super(context);
        initView();

    }

    public CustomTextStyleEditModel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomTextStyleEditModel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_textstyle_edit, this, false);
        addView(v);

        layoutTextstyle = (RelativeLayout)findViewById(R.id.layout_textstyle);
        editStyle = (TextView)findViewById(R.id.edit_style);
        presentColor = (ImageView)findViewById(R.id.present_color);

    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.TextStyleEditModel);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.TextStyleEditModel, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {

        textStyleMode = typedArray.getString(R.styleable.TextStyleEditModel_text_style_mode);
        if( textStyleMode != null ) {
            if( textStyleMode.equals( "color" ) ) {
                setTextColorStyle();
            } else if( textStyleMode.equals( "bold" ) ) {
                setTextBoldStyle();
            } else if( textStyleMode.equals( "underline" ) ) {
                setTextUnderLineStyle();
            } else if( textStyleMode.equals( "size" ) ) {
                setTextSizeStyle();
            }
        }

        int textSize = typedArray.getInteger(R.styleable.TextStyleEditModel_text_size, 14);
        setTextSize( textSize );

        boolean isSelected = typedArray.getBoolean(R.styleable.TextStyleEditModel_textstyle_isselected, false);
        setSelected( isSelected );

        typedArray.recycle();
    }

    public void setSelected( boolean isSelected ) {
        this.isSelected = isSelected;

        if( isSelected ) {
            layoutTextstyle.setBackgroundResource(R.drawable.white_round);
            editStyle.setTextColor( ContextCompat.getColor( getContext(), R.color.gray_overlay ) );

        } else {
            layoutTextstyle.setBackgroundResource(R.drawable.gray_round_whiteborder);
            editStyle.setTextColor( Color.parseColor( "#FFFFFF" ) );
        }
    }

    public boolean isSelected() {
        return isSelected;
    }

    public String getTextStyleMode() {
        return textStyleMode;
    }

    public void setTextSize(int textSize ) {
        this.textSize = textSize;
        editStyle.setTextSize( TypedValue.COMPLEX_UNIT_DIP, textSize );
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextColorStyle() {
        editStyle.setText( "A" );
        presentColor.setVisibility(View.VISIBLE);
    }
    public void setTextBoldStyle() {
        SpannableString spannableString = new SpannableString("A");
        spannableString.setSpan( new StyleSpan(Typeface.BOLD), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        editStyle.setText( spannableString );
        presentColor.setVisibility(View.GONE);
    }
    public void setTextUnderLineStyle() {
        SpannableString spannableString = new SpannableString("A");
        spannableString.setSpan( new UnderlineSpan(), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        editStyle.setText( spannableString );
        presentColor.setVisibility(View.GONE);
    }
    public void setTextSizeStyle() {
        editStyle.setText( "크기" );
        presentColor.setVisibility(View.GONE);
    }

    public void setPresentColor( String colorCode ) {

        if( colorCode.equals( "red" ) ) {
            presentColor.setImageResource(R.drawable.red_inner);
        } else if( colorCode.equals( "orange" ) ) {
            presentColor.setImageResource(R.drawable.orange_inner);
        } else if( colorCode.equals( "yellow" ) ) {
            presentColor.setImageResource(R.drawable.yellow_inner);
        } else if( colorCode.equals( "green" ) ) {
            presentColor.setImageResource(R.drawable.green_inner);
        } else if( colorCode.equals( "blue" ) ) {
            presentColor.setImageResource(R.drawable.blue_inner);
        } else if( colorCode.equals( "purple" ) ) {
            presentColor.setImageResource(R.drawable.purple_inner);
        } else {
            presentColor.setImageResource(R.drawable.black_inner);
        }

    }

}
