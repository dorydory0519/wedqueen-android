package com.jjlee.wedqueen.utils;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.util.Log;

import java.io.IOException;

public class ExifUtils {

    public static Bitmap rotateResizeBitmap( String src, Bitmap bitmap, int resizeWidth ) {
        try {

            int orientation = getExifOrientation(src);

            if( orientation == 1 ) {
                return resizeBitmap( bitmap, resizeWidth );
            }

            Matrix matrix = new Matrix();

            switch( orientation ) {
                case 2:
                    matrix.setScale(-1, 1);
                    break;
                case 3:
                    matrix.setRotate(180);
                    break;
                case 4:
                    matrix.setRotate(180);
                    matrix.postScale(-1, 1);
                    break;
                case 5:
                    matrix.setRotate(90);
                    matrix.postScale(-1, 1);
                    break;
                case 6:
                    matrix.setRotate(90);
                    break;
                case 7:
                    matrix.setRotate(-90);
                    matrix.postScale(-1, 1);
                    break;
                case 8:
                    matrix.setRotate(-90);
                    break;
                default:
                    return resizeBitmap( bitmap, resizeWidth );
            }

            // Rotate된 Bitmap 생성
            Bitmap oriented = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            if( oriented != bitmap ) {
                bitmap.recycle();
            }

            // resizeWidth에 맞게 Bitmap Resize
            return resizeBitmap( oriented, resizeWidth );

        } catch (IOException e) {
            e.printStackTrace();
        } catch( OutOfMemoryError e ) {
            e.printStackTrace();
        } catch( Exception e ) {
            e.printStackTrace();
        }

        return bitmap;
    }


    public static Bitmap resizeBitmap( Bitmap oriented, int resizeWidth ) {

        double aspectRatio = (double) oriented.getHeight() / (double) oriented.getWidth();
        int targetHeight = (int) ( resizeWidth * aspectRatio );
        Bitmap result = Bitmap.createScaledBitmap(oriented, resizeWidth, targetHeight, false );

        Log.e( "WQTEST3", " [resizeBitmap] targetWidth : " + resizeWidth );
        Log.e( "WQTEST3", " [resizeBitmap] targetHeight : " + targetHeight );

        if( result != oriented ) {
            oriented.recycle();
        }

        return result;
    }


    private static int getExifOrientation(String src) throws IOException {
        int orientation = 1;

        try {
            ExifInterface exif = new ExifInterface( src );
            String orientString = exif.getAttribute( ExifInterface.TAG_ORIENTATION );
            orientation = orientString != null ? Integer.parseInt(orientString) :  ExifInterface.ORIENTATION_NORMAL;

        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return orientation;
    }
}