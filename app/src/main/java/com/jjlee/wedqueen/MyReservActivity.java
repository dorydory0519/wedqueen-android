package com.jjlee.wedqueen;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.CustomViews.CustomReservModel;
import com.jjlee.wedqueen.rest.survey.controller.SurveyController;
import com.jjlee.wedqueen.rest.survey.model.SurveyAnswer;
import com.jjlee.wedqueen.rest.survey.model.SurveyResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyReservActivity extends AppCompatActivity {

    private LinearLayout reservWrapper;

    private Dialog pointHelpDlg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myreserv);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "my_reserv" ) );

        initPointHelpDlg();

        SurveyController surveyController = new SurveyController();
        reservWrapper = (LinearLayout)findViewById( R.id.reserv_wrapper );
        Call<SurveyResponse> surveyCall = surveyController.getSurveyAnswers();
        surveyCall.enqueue(new Callback<SurveyResponse>() {
            @Override
            public void onResponse(Call<SurveyResponse> call, Response<SurveyResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    SurveyResponse surveyResponse = response.body();

                    if( surveyResponse.getResultCode() == 0 && !ObjectUtils.isEmpty( surveyResponse.getSurveyAnswers() ) && surveyResponse.getSurveyAnswers().size() > 0 ) {
                        List<SurveyAnswer> surveyAnswers = surveyResponse.getSurveyAnswers();

                        for( SurveyAnswer surveyAnswer : surveyAnswers ) {

                            if( !ObjectUtils.isEmpty( MyReservActivity.this ) ) {
                                CustomReservModel customReservModel = new CustomReservModel( MyReservActivity.this );
                                customReservModel.setReservOnOff( true );
                                customReservModel.setReservCompanyThumb( surveyAnswer.getCompanyImageUrl() );

                                customReservModel.setReservCompanyName( surveyAnswer.getCompanyTitle() );
                                customReservModel.setReservTime( surveyAnswer.getReservationTime() );
                                customReservModel.setReviewParam( surveyAnswer.getId(), surveyAnswer.getCompanyTitle(), surveyAnswer.getReservationTime() );
                                customReservModel.setReservStatus( surveyAnswer.getStatus() );

                                customReservModel.setVisit( surveyAnswer.isVisit(), surveyAnswer.getVisitPoint() );
                                if( !surveyAnswer.isVisit() ) {
                                    customReservModel.getVisitSection().setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showPointHelpDlg( "visit"
                                                    , ""
                                                    , ""
                                                    , "" );
                                        }
                                    });
                                }

                                customReservModel.setContract( surveyAnswer.isContract(), surveyAnswer.getContractPoint() );
                                if( !surveyAnswer.isContract() ) {
                                    customReservModel.getContractSection().setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showPointHelpDlg( "contract"
                                                    , ""
                                                    , ((TextView)v.findViewById( R.id.contract_point )).getText().toString()
                                                    , "" );
                                        }
                                    });
                                }

                                customReservModel.setReview( surveyAnswer.isReview(), surveyAnswer.getReviewPoint() );
                                if( !surveyAnswer.isReview() ) {
                                    customReservModel.getReviewSection().setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            showPointHelpDlg( "review"
                                                    , v.getTag() + ""
                                                    , ""
                                                    , ((TextView)v.findViewById( R.id.review_point )).getText().toString() );
                                        }
                                    });
                                }

                                customReservModel.setTag( surveyAnswer.getLinkUrl() );
                                customReservModel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startFullscreenActivity( v.getTag() + "" );
                                    }
                                });

                                reservWrapper.addView( customReservModel );
                            }
                        }
                    } else {
                        CustomReservModel customReservModel = new CustomReservModel( MyReservActivity.this );
                        customReservModel.setReservOnOff( false );
                        reservWrapper.addView( customReservModel );
                    }
                } else {
                    CustomReservModel customReservModel = new CustomReservModel( MyReservActivity.this );
                    customReservModel.setReservOnOff( false );
                    reservWrapper.addView( customReservModel );
                }
            }

            @Override
            public void onFailure(Call<SurveyResponse> call, Throwable t) {
                CustomReservModel customReservModel = new CustomReservModel( MyReservActivity.this );
                customReservModel.setReservOnOff( false );
                reservWrapper.addView( customReservModel );
            }
        });
    }


    private void initPointHelpDlg() {
        pointHelpDlg = new Dialog( this );
        pointHelpDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pointHelpDlg.setContentView(R.layout.dialog_pointhelp);

        pointHelpDlg.findViewById( R.id.pointhelp_close ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pointHelpDlg.dismiss();
            }
        });
    }

    private void showPointHelpDlg( String type, String surveyReviewParam, String strContractPoint, String strReviewPoint ) {
        if( !ObjectUtils.isEmpty( type ) ) {

            TextView pointHelpLabel = (TextView)pointHelpDlg.findViewById( R.id.pointhelp_label );
            LinearLayout pointHelpTextWrapper = (LinearLayout)pointHelpDlg.findViewById( R.id.pointhelp_text_wrapper );
            pointHelpTextWrapper.removeAllViews();

            TextView pointStartBtn = (TextView)pointHelpDlg.findViewById( R.id.point_start_btn );

            TextView helpTextView1 = new TextView( MyReservActivity.this );
            helpTextView1.setPadding( 0, 10, 0, 10 );
            helpTextView1.setLineSpacing( 0, 1.2f );

            TextView helpTextView2 = new TextView( MyReservActivity.this );
            helpTextView2.setPadding( 0, 10, 0, 10 );
            helpTextView2.setLineSpacing( 0, 1.2f );

            if( type.equals( "visit" ) ) {
                pointHelpLabel.setText( "방문 포인트" );

                helpTextView1.setText( "∙ 예약한 업체에서 방문여부 체크 후 포인트가 적립됩니다." );
                pointHelpTextWrapper.addView( helpTextView1 );

                helpTextView2.setText( "∙ 방문한지 3일 이상이 지났는데도 계속 포인트가 적립되지 않는다면 아래의 버튼을 클릭하여 카카오톡 플러스 친구 '웨딩의 여신'으로 말씀해주세요!" );
                pointHelpTextWrapper.addView( helpTextView2 );

                pointStartBtn.setText( "포인트 문의하러 가기" );
                pointStartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startFullscreenActivity( "http://pf.kakao.com/_exjFpl" );
                    }
                });

            } else if( type.equals( "contract" ) ) {
                pointHelpLabel.setText( "계약 포인트" );

                helpTextView1.setText( "∙ 간편예약을 통해 방문 후 계약시 " + strContractPoint + "를 적립해드립니다." );
                pointHelpTextWrapper.addView( helpTextView1 );

                helpTextView2.setText( "∙ 계약 후 아래의 버튼을 클릭하여 카카오톡 플러스 친구 ‘웨딩의 여신’으로 계약서 사진을 보내주세요!" );
                pointHelpTextWrapper.addView( helpTextView2 );

                pointStartBtn.setText( "계약서 보내러 가기" );
                pointStartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startFullscreenActivity( "http://pf.kakao.com/_exjFpl" );
                    }
                });

            } else if( type.equals( "review" ) ) {
                pointHelpLabel.setText( "후기 포인트" );

                helpTextView1.setText( "∙ 지금 바로 아래의 버튼을 눌러 후기를 작성해주세요." );
                pointHelpTextWrapper.addView( helpTextView1 );

                helpTextView2.setText( "∙ " + strReviewPoint + "를 적립해드립니다!" );
                pointHelpTextWrapper.addView( helpTextView2 );

                pointStartBtn.setText( "후기쓰러 가기" );
                pointStartBtn.setTag( surveyReviewParam );
                pointStartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pointHelpDlg.dismiss();
                        Intent intent = new Intent( MyReservActivity.this, ContentEditActivity.class );
                        intent.putExtra( "surveyReviewParam", v.getTag() + "" );
                        startActivity(intent);
                    }
                });
            }

            pointHelpDlg.show();
        }
    }


    private void startFullscreenActivity(String url) {
        Intent intent = new Intent( this, FullscreenActivity.class );
        intent.putExtra( "URL", url );
        startActivityForResult( intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY );
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult( RESULT_OK, intent );
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
