package com.jjlee.wedqueen.rest.region.service;

import com.jjlee.wedqueen.rest.region.model.RegionResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface RegionService {
    @Headers("Accept: application/json")

    @GET("api/v1/region")
    Call<RegionResponse> getRegions();

}
