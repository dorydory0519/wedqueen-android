package com.jjlee.wedqueen.rest.premium.service;

import com.jjlee.wedqueen.rest.premium.model.PremiumAdLogResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface PremiumLogService {
    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("premium/ad/log")
    Call<PremiumAdLogResponse> postPremiumAdLog(@FieldMap Map<String, String> map);
}
