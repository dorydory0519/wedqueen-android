package com.jjlee.wedqueen.rest.content.model;

public class GetContentResponse {
    private ContentForFragment content;

    public ContentForFragment getContent() {
        return content;
    }

    public void setContent(ContentForFragment content) {
        this.content = content;
    }
}
