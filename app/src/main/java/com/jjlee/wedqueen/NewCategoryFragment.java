package com.jjlee.wedqueen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.jjlee.wedqueen.CustomViews.CustomEventModel;
import com.jjlee.wedqueen.CustomViews.CustomRealReviewModel;
import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.rest.content.controller.ContentController;
import com.jjlee.wedqueen.rest.content.model.RealWeddingContentDto;
import com.jjlee.wedqueen.rest.love.controller.LoveController;
import com.jjlee.wedqueen.rest.tabCategory.controller.TabCategoryController;
import com.jjlee.wedqueen.rest.tabCategory.model.TabCategoryLog;
import com.jjlee.wedqueen.rest.tabCategory.model.TabCategoryLogResponse;
import com.jjlee.wedqueen.rest.tip.controller.TipController;
import com.jjlee.wedqueen.rest.tip.model.Content;
import com.jjlee.wedqueen.rest.tip.model.Tip;
import com.jjlee.wedqueen.rest.tip.model.TipResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.CustomScrollView;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class NewCategoryFragment extends Fragment {

    private RequestManager mGlideRequestManager;
    private int displayWidth;

    private SwipeRefreshLayout swipeNewcategory;

    private String categoryName;
    private String categoryNameKor;
    private int position;
    private long categoryId;
    private boolean isTipOpen;

    private Fragment targetFragment;

    private TabCategoryController tabCategoryController;

    private TipController tipController;
    private FrameLayout tipWrapper;
    private ImageView tipBg;
    private TextView tipTitle;
    private TextView tipDesc;

    private LoveController loveController;
    private ContentController contentController;
    private GridLayout categoryContentWrapper;
    private int targetTime;
    private int contentPage;
    private boolean isLoadingContentData;

    public static NewCategoryFragment getInstance( String categoryName, long categoryId ) {
        NewCategoryFragment fragment = new NewCategoryFragment();
        Bundle args = new Bundle();
        args.putString( "categoryName", categoryName );
        args.putLong( "categoryId", categoryId );
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        targetFragment = this;
        mGlideRequestManager = Glide.with( this );

        tabCategoryController = new TabCategoryController();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics( displayMetrics );
        displayWidth = displayMetrics.widthPixels;

        Bundle args = getArguments();
        categoryName = args.getString( "categoryName" );
//        categoryNameKor = args.getString( "categoryNameKor" );
//        position = args.getInt( "position" );
        categoryId = args.getLong( "categoryId" );
//        isTipOpen = args.getBoolean( "isTipOpen" );
        targetTime = new Date().getSeconds();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_newcategory, container, false);

        contentPage = 1;
        isLoadingContentData = false;

        tipWrapper = view.findViewById( R.id.tip_wrapper );
        tipBg = view.findViewById( R.id.tip_bg );
        tipTitle = view.findViewById( R.id.tip_title );
        tipDesc = view.findViewById( R.id.tip_desc );
        tipController = new TipController();
        Call<TipResponse> tipCall = tipController.getTip( "category", 1, String.valueOf( categoryId ) );
        tipCall.enqueue(new Callback<TipResponse>() {
            @Override
            public void onResponse(Call<TipResponse> call, Response<TipResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    TipResponse tipResponse = response.body();
                    Tip tip = tipResponse.getTip();

                    if( !ObjectUtils.isEmpty( tip ) && !ObjectUtils.isEmpty( tip.getContent() ) ) {
                        Content content = tip.getContent().get( 0 );
                        tipTitle.setText( content.getTitle() );
                        tipDesc.setText( content.getDescription() );
                        mGlideRequestManager.load( content.getBgImage() ).centerCrop().placeholder( R.color.image_placeholder_color ).into( tipBg );
                    } else {
                        tipWrapper.setVisibility( View.GONE );
                    }
                }
            }

            @Override
            public void onFailure(Call<TipResponse> call, Throwable t) {
                t.printStackTrace();
                tipWrapper.setVisibility( View.GONE );
            }
        });


        loveController = new LoveController();
        contentController = new ContentController();
        categoryContentWrapper = view.findViewById( R.id.category_content_wrapper );
        getCategoryContentFromServer();


        ((CustomScrollView)view.findViewById( R.id.newcategory_scrollview )).setOnBottomReachedListener(new CustomScrollView.OnBottomReachedListener() {
            @Override
            public void onBottomReached() {
                if( !isLoadingContentData ) {
                    getCategoryContentFromServer();
                }
            }
        });

        tipWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
                    Intent intent = new Intent( getActivity(), ContentEditActivity.class );
                    getActivity().startActivity( intent );
                } else {
                    Intent intent = new Intent( getActivity(), AccountActivity.class );
                    intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
                    getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
                }
            }
        });

        swipeNewcategory = (SwipeRefreshLayout)view.findViewById( R.id.swipe_newcategory );
        swipeNewcategory.setColorSchemeResources(
                R.color.colorPrimaryDark, R.color.colorAccent,
                R.color.colorPrimaryDark, R.color.colorAccent);
        swipeNewcategory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                contentPage = 1;
                targetTime = new Date().getSeconds();
                getCategoryContentFromServer();
                swipeNewcategory.setRefreshing(false);
            }
        });

        return view;
    }


    private void getCategoryContentFromServer() {

        if( contentPage == 1 ) {
            categoryContentWrapper.removeAllViews();
        }

        isLoadingContentData = true;

        Call<List<RealWeddingContentDto>> contentCall = contentController.getV2Contents( categoryName, contentPage++, targetTime );
        contentCall.enqueue(new Callback<List<RealWeddingContentDto>>() {
            @Override
            public void onResponse(Call<List<RealWeddingContentDto>> call, Response<List<RealWeddingContentDto>> response) {

                isLoadingContentData = false;

                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    List<RealWeddingContentDto> realWeddingContentDtos = response.body();

                    for( RealWeddingContentDto realWeddingContentDto : realWeddingContentDtos ) {

                        if( !ObjectUtils.isEmpty( realWeddingContentDto.getType() ) && realWeddingContentDto.getType().equals( "premiumCompany" ) ) {

                            if( !ObjectUtils.isEmpty( getActivity() ) ) {
                                CustomEventModel customEventModel = new CustomEventModel( getActivity(), mGlideRequestManager );
                                customEventModel.setEventImgSize( displayWidth - SizeConverter.dpToPx( 30 ) );
                                customEventModel.setEventTitle( realWeddingContentDto.getTitle() );
                                customEventModel.setEventTag( realWeddingContentDto.getStrEvent() );
                                customEventModel.setEventImg( realWeddingContentDto.getImageUrl() );
                                customEventModel.setEventEditor( realWeddingContentDto.getWriter() );
                                customEventModel.setEventViewcount( realWeddingContentDto.getViewCountToString() );
                                if( ObjectUtils.isEmpty( realWeddingContentDto.getWriter() ) && ObjectUtils.isEmpty( realWeddingContentDto.getViewCountToString() ) ) {
                                    customEventModel.getEventDesc().setVisibility( GONE );
                                }
                                customEventModel.setShoppable( realWeddingContentDto.isPaymentRequired() );
                                customEventModel.setBookable( realWeddingContentDto.isSurveyRequired() );
                                customEventModel.setInquirable( realWeddingContentDto.isInquiryRequired() );
                                customEventModel.setTag( realWeddingContentDto.getUrl() );
                                customEventModel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startFullscreenActivity( v.getTag() + "" );
                                    }
                                });

                                if( realWeddingContentDto.getCompanyLatitude() == 0 && realWeddingContentDto.getCompanyLongitude() == 0 ) {
                                    customEventModel.getCompanyMap().setVisibility( GONE );
                                } else {
                                    customEventModel.setCompanyName( realWeddingContentDto.getWriter() );
                                    customEventModel.setLatitude( realWeddingContentDto.getCompanyLatitude() );
                                    customEventModel.setLongitude( realWeddingContentDto.getCompanyLongitude() );
                                    customEventModel.getCompanyMap().setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            CustomEventModel targetModel = (CustomEventModel)view.getParent().getParent().getParent();

                                            Intent intent = new Intent( getActivity(), CompanyMapActivity.class );
                                            intent.putExtra( "companyName", targetModel.getCompanyName() );
                                            intent.putExtra( "latitude", targetModel.getLatitude() );
                                            intent.putExtra( "longitude", targetModel.getLongitude() );
                                            startActivity( intent );
                                        }
                                    });
                                }

                                customEventModel.setContentId( realWeddingContentDto.getId() );
                                if( realWeddingContentDto.getCompanyId() > 0 ) {
                                    customEventModel.getRealreviewScrap().setVisibility( View.GONE );
                                } else {
                                    customEventModel.setRealreviewLoved( realWeddingContentDto.isLoved() );
                                    customEventModel.getRealreviewScrap().setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {

                                                CustomEventModel targetModel = (CustomEventModel)view.getParent().getParent();
                                                boolean isLoved = targetModel.isLoved();

                                                setLoveContent( isLoved, targetModel.getContentId() );
                                                targetModel.setRealreviewLoved( !isLoved );
                                            } else {
                                                Intent intent = new Intent( getActivity(), AccountActivity.class );
                                                intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
                                                intent.putExtra( Constants.LOGIN_FROM, "home_btn_love" );
                                                getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
                                            }
                                        }
                                    });
                                }
                                categoryContentWrapper.addView( customEventModel );

                                GridLayout.LayoutParams gridLayoutParams = ((GridLayout.LayoutParams)customEventModel.getLayoutParams());
                                gridLayoutParams.columnSpec = GridLayout.spec( 0, 2 );
                            }
                        } else if( !ObjectUtils.isEmpty( realWeddingContentDto.getType() ) && realWeddingContentDto.getType().equals( "content" ) ) {

                            if( !ObjectUtils.isEmpty( getActivity() ) ) {
                                CustomRealReviewModel customRealReviewModel = new CustomRealReviewModel( getActivity(), mGlideRequestManager );
                                customRealReviewModel.setRealreviewSize( ( displayWidth - SizeConverter.dpToPx( 56 ) ) / 2 );
                                customRealReviewModel.setRealreviewTitle( realWeddingContentDto.getTitle() );
                                customRealReviewModel.setRealreviewImg( realWeddingContentDto.getImageUrl() );
                                customRealReviewModel.setRealreviewEditor( realWeddingContentDto.getWriter() );
                                customRealReviewModel.setRealreviewViewcount( realWeddingContentDto.getViewCountToString() );
                                customRealReviewModel.setTag( realWeddingContentDto.getUrl() );
                                customRealReviewModel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        startFullscreenActivity( view.getTag() + "" );
                                    }
                                });

                                customRealReviewModel.setContentId( realWeddingContentDto.getId() );
                                customRealReviewModel.setRealreviewLoved( realWeddingContentDto.isLoved() );
                                customRealReviewModel.getRealreviewScrap().setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {

                                            CustomRealReviewModel targetModel = (CustomRealReviewModel)view.getParent().getParent();
                                            boolean isLoved = targetModel.isLoved();

                                            setLoveContent( isLoved, targetModel.getContentId() );
                                            targetModel.setRealreviewLoved( !isLoved );
                                        } else {
                                            Intent intent = new Intent( getActivity(), AccountActivity.class );
                                            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
                                            intent.putExtra( Constants.LOGIN_FROM, "home_btn_love" );
                                            getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
                                        }
                                    }
                                });
                                categoryContentWrapper.addView( customRealReviewModel );
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<RealWeddingContentDto>> call, Throwable t) {
                t.printStackTrace();
                isLoadingContentData = false;
            }
        });

    }


    private void setLoveContent( boolean isLove, long targetContentId ) {

        Call<CommonResponse> call;
        if( isLove ) {
            call = loveController.loveOff( targetContentId, "content" );
        } else {
            call = loveController.loveOn( targetContentId, "content" );
        }

        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> commonResponse) {
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
            }
        });
    }


    private void postTabCategoryLogToServer() {
        String userId = MyApplication.getGlobalApplicationContext().getUserId();
        if( ObjectUtils.isEmpty( userId ) ) {
            userId = "-1";
        }

        TabCategoryLog tabCategoryLog = new TabCategoryLog( categoryId, "android", Long.parseLong( userId ) );
        if( ObjectUtils.isEmpty( tabCategoryController ) ) {
            tabCategoryController = new TabCategoryController();
        }
        Call<TabCategoryLogResponse> tabCategoryLogCall = tabCategoryController.postTabCategoryLog( tabCategoryLog );
        tabCategoryLogCall.enqueue(new Callback<TabCategoryLogResponse>() {
            @Override
            public void onResponse(Call<TabCategoryLogResponse> call, Response<TabCategoryLogResponse> response) {
            }

            @Override
            public void onFailure(Call<TabCategoryLogResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if( isVisibleToUser ) {
            // user에게 보이는 화면에서만 tabCategoryLog를 전송
            postTabCategoryLogToServer();
        }
    }


    private void startFullscreenActivity(String url) {

        if( MyApplication.getGlobalApplicationContext().canLoadDetailContent() ) {
            Intent intent = new Intent( getActivity(), FullscreenActivity.class );
            intent.putExtra( "URL", url );
            getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY );
        } else {
            Intent intent = new Intent( getActivity(), AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            intent.putExtra( Constants.LOGIN_FROM, "home_other_content" );
            getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
        }
    }
}
