package com.jjlee.wedqueen.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.ContentEditActivity;
import com.jjlee.wedqueen.CustomViews.CustomDiaryModel;
import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MainActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.content.model.Content;
import com.jjlee.wedqueen.rest.user.controller.UserController;
import com.jjlee.wedqueen.rest.user.model.UserContentResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by esmac on 2017. 9. 6..
 */

public class RealWeddingFragment extends Fragment {

    private SwipeRefreshLayout swipeMyrealwedding;

    private UserController userController;
    private LinearLayout diaryWrapper;

    public static RealWeddingFragment newInstance(String text) {
        RealWeddingFragment fragment = new RealWeddingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_realwedding, container, false);

        // Diary Model Setting
        diaryWrapper = (LinearLayout) view.findViewById(R.id.diary_wrapper);
        userController = new UserController();
        getMyRealWeddingsFromServer();


        swipeMyrealwedding = (SwipeRefreshLayout)view.findViewById(R.id.swipe_myrealwedding);
        swipeMyrealwedding.setColorSchemeResources(
                R.color.colorPrimaryDark, R.color.colorAccent,
                R.color.colorPrimaryDark, R.color.colorAccent);
        swipeMyrealwedding.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getMyRealWeddingsFromServer();
                swipeMyrealwedding.setRefreshing(false);
            }
        });

        return view;
    }

    public void getMyRealWeddingsFromServer() {

        diaryWrapper.removeAllViews();

        Call<UserContentResponse> userCall = userController.getContents();
        userCall.enqueue(new Callback<UserContentResponse>() {
            @Override
            public void onResponse(Call<UserContentResponse> call, Response<UserContentResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {

                    diaryWrapper.removeAllViews();

                    UserContentResponse userContentResponse = response.body();

                    if( !ObjectUtils.isEmpty( userContentResponse ) && userContentResponse.getContents().size() > 0 ) {
                        List<Content> contents = userContentResponse.getContents();

                        for( Content content : contents ) {

                            if( !ObjectUtils.isEmpty( getActivity() ) ) {
                                CustomDiaryModel customDiaryModel = new CustomDiaryModel( getActivity() );
                                customDiaryModel.setContentId( content.getId() );
                                customDiaryModel.setDiaryDday( content.getDdayToString() );
                                customDiaryModel.setDiaryState( content.getState() );
                                customDiaryModel.setDiaryCategory( content.getTypeInKorean() );
                                customDiaryModel.setDiaryImg( content.getImageUrl() );
                                customDiaryModel.setDiaryViewcount( content.getViewCount().toString() );
                                customDiaryModel.setDiaryTitle( content.getTitle() );
                                customDiaryModel.setRole( content.getRole() );
                                customDiaryModel.setState( content.getState() );
                                customDiaryModel.setUrl( content.getUrl() );
                                customDiaryModel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        CustomDiaryModel targetDiaryModel = (CustomDiaryModel)v;

                                        // role이 write가 아닌 컨텐트들은 수정이 불가능하게 contentId를 -1로 넘김 ( toolbar에 수정/발행취소/삭제 버튼이 보이지 않음 )
                                        if( !ObjectUtils.isEmpty( targetDiaryModel.getRole() ) ) {
                                            if( targetDiaryModel.getRole().equals( "write" ) ) {
                                                startFullscreenActivity( targetDiaryModel.getUrl(), targetDiaryModel.getContentId(), targetDiaryModel.getState() );
                                            } else {
                                                startFullscreenActivity( targetDiaryModel.getUrl(), -1, targetDiaryModel.getState() );
                                            }
                                        }
                                    }
                                });
                                diaryWrapper.addView( customDiaryModel );
                            }
                        }
                    } else {

                        if( !ObjectUtils.isEmpty( getActivity() ) ) {
                            TextView textView = new TextView( getActivity() );
                            textView.setText( "리얼웨딩 글을 작성하시면 \n 포인트를 드려요 !! " );
                            textView.setTextSize( TypedValue.COMPLEX_UNIT_DIP, 14 );
                            textView.setGravity( Gravity.CENTER );
                            textView.setLayoutParams( new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, SizeConverter.dpToPx( 250 ) ) );
                            diaryWrapper.addView( textView );
                        }

                    }
                } else {
                    Log.e( "WQTEST", "userResponse.body() is null" );
                }
            }

            @Override
            public void onFailure(Call<UserContentResponse> call, Throwable t) {
                Log.e( "WQTEST", "RealWeddingFragment userCall Failed" );
                t.printStackTrace();
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if( isVisibleToUser ) {
            Answers.getInstance().logContentView(new ContentViewEvent()
                    .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                    .putContentType( "Fragment" )
                    .putContentName( "myrealwedding_list" ) );
        }
    }

    private void startFullscreenActivity( String url, long contentId, String state ) {
        Intent intent = new Intent( getActivity(), FullscreenActivity.class );
        intent.putExtra( "URL", url );
        intent.putExtra( "contentId", contentId );
        intent.putExtra( "state", state );
        getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY );
    }
}
