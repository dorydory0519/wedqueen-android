package com.jjlee.wedqueen.support;

import android.os.Build;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;


import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.utils.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

/**
 * Created by JJLEE on 2016. 4. 5..
 */
public class MyCookieJar implements CookieJar {

    private static List<Cookie> cookies = new ArrayList<>();
    private static CookieManager cookieManager;
    private static CookieSyncManager cookieSyncManager; // ONLY FOR PRE-LOLLIPOP DEVICES


    public MyCookieJar() {
        cookieManager = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            cookieSyncManager = CookieSyncManager.createInstance(MyApplication.getGlobalApplicationContext());
        }
    }

    @Override
    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {

        this.cookies.clear();

        // 로그인시 SET-COOKIE 로 들어오는 애들 webview 에도 세팅해줌
        List<Cookie> prefsCookies = new ArrayList<Cookie>();
        for(Cookie cookie : cookies) {
            this.cookies.add(cookie); // okhttp로 들어오는 요청의 쿠키 저장
            prefsCookies.add(cookie); // 전역변수 cookies를 사용하면 ConcurrentModificationException이 발생하므로 새로 객체 생성후 따로 담은 Cookie List를 Add
            String cookieString = cookie.name() + '=' + cookie.value() + "; domain=" + cookie.domain();
            cookieManager.setCookie(Constants.BASE_URL, cookieString);
//            Log.e( "WQTEST3", "[Response] url : " + url + "\ncookieString : " + cookieString );
        }
        MyApplication.getGlobalApplicationContext().setCookieData( prefsCookies );
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.getInstance().sync(); // LOLLIPOP
        }

    }

    @Override
    public List<Cookie> loadForRequest(HttpUrl url) {

        // okhttp로 나가는 요청에 쿠키 세팅
        if( cookies != null && cookies.size() > 0 ) {
//            for( Cookie cookie : cookies ) {
//                Log.e( "WQTEST3", "[Request] class cookie.name() : " + cookie.name() );
//                Log.e( "WQTEST3", "[Request] class cookie.value() : " + cookie.value() );
//            }
            return cookies;
        }

        if( url.toString().contains( "version" ) || url.toString().contains( "login" ) ) {
            return new ArrayList<Cookie>();
        } else {
            List<Cookie> cookieData = MyApplication.getGlobalApplicationContext().getCookieData();
            if( !ObjectUtils.isEmpty( cookieData ) ) {
                for( Cookie cookie : cookieData ) {
//                    Log.e( "WQTEST3", "[Request] prefs cookieData.name() : " + cookie.name() );
//                    Log.e( "WQTEST3", "[Request] prefs cookieData.value() : " + cookie.value() );
                }
                this.cookies = cookieData;
                return cookieData;
            }
        }

        return new ArrayList<Cookie>();
    }

    public static void removeSessionCookies() {
        cookies.clear();
        MyApplication.getGlobalApplicationContext().setCookieData( null );
        cookieManager.setCookie(Constants.BASE_URL, "REMEMBER_ME=");
        cookieManager.setCookie(Constants.BASE_URL, "JSESSIONID=");
        cookieManager.removeSessionCookie();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.getInstance().sync(); // LOLLIPOP
        }

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
