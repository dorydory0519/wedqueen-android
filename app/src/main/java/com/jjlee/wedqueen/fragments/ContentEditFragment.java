package com.jjlee.wedqueen.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjlee.wedqueen.Interface.MoveFragmentInterface;
import com.jjlee.wedqueen.R;

public class ContentEditFragment extends Fragment implements MoveFragmentInterface {

    private MoveFragmentInterface moveFragmentInterface;

    private ViewPager contenteditViewPager;
    private ContentEditAdapter contentEditAdapter;

    public ContentEditFragment() {} // Required empty public constructor

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contentedit, container, false);

//        moveFragmentInterface = this;

//        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.contentedit_tablayout);
//        contenteditViewPager = (ViewPager) view.findViewById(R.id.contentedit_viewpager);

//        contentEditAdapter = new ContentEditAdapter( getActivity().getSupportFragmentManager() );
//        contenteditViewPager.setAdapter( contentEditAdapter );
//        contenteditViewPager.setOffscreenPageLimit( 2 );
//        tabLayout.setupWithViewPager( contenteditViewPager );

        return view;
    }

    public void setEditContent( Uri uri ) {
        Fragment fragment = contentEditAdapter.getRegisteredFragment( 0 );
//        ((EditMainFragment)fragment).setEditContent( uri );
    }

    public class ContentEditAdapter extends FragmentPagerAdapter {

        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        public ContentEditAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch ( position ) {
                case 0 :
                    EditMainFragment editMainFragment = new EditMainFragment();
//                    editMainFragment.setMoveFragmentInterface( moveFragmentInterface );
                    return editMainFragment;
                case 1 :
                    EditDetailFragment editDetailFragment = new EditDetailFragment();
//                    editDetailFragment.setMoveFragmentInterface( moveFragmentInterface );
                    return editDetailFragment;

            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }
    }

    @Override
    public void moveFragment(int position) {
        if( contenteditViewPager != null ) {
            contenteditViewPager.setCurrentItem( position );
        }
    }
}
