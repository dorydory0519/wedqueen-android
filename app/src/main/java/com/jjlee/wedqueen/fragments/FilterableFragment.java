package com.jjlee.wedqueen.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.app.model.TabCategory;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.utils.SizeConverter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FilterableFragment extends PageFragment {
    public static final String TABTYPE = "tab_type";
    public static final String CURRENT_CATEGORY = "current_category";

    protected List<ViewGroup> categoryButtons = new ArrayList<>();
    protected int selectedCategoryIndex;
    protected ArrayList<TabCategory> tabCategories;
    protected String tabType;
    protected String currentCategory;

    protected View rootView;
    protected HorizontalScrollView categoryScrollView;

    private int mIndex;

    public FilterableFragment() {} // Required empty public constructor

    public static void putFilterRelatedArgument(Bundle args, String tabType, String initialCategory) {
        args.putString(TABTYPE, tabType);
        args.putString(CURRENT_CATEGORY, initialCategory);
    }

    public static FilterableFragment newInstance(String tabType, String initialCategory, int index) {
        FilterableFragment fragment = new FilterableFragment();
        Bundle args = new Bundle();
        putFilterRelatedArgument(args, tabType, initialCategory);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TABTYPE, tabType);
        outState.putString(CURRENT_CATEGORY, currentCategory);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();

        if(savedInstanceState == null) {
            tabType = args.getString(TABTYPE);
            currentCategory = args.getString(CURRENT_CATEGORY);
        } else {
            tabType = savedInstanceState.getString(TABTYPE, null);
            currentCategory = savedInstanceState.getString(CURRENT_CATEGORY, null);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_filterable, container, false);
        rootView = view;
        initiateNecessaryComponents((ViewGroup) view);

        LinearLayout tabCategoryWrapper = (LinearLayout) view.findViewById(R.id.filter_wrapper);
        categoryScrollView = (HorizontalScrollView) view.findViewById(R.id.filter_scoller);

        Bundle args = getArguments();

        String tabType = args.getString(TABTYPE);

        if( tabType != null && ( tabType.equals( "checklist" ) || tabType.equals( "mypage" ) || tabType.equals( "inquiry" ) || tabType.equals( "scrap" ) || tabType.equals( "invitation" ) ) ) {
            categoryScrollView.setVisibility( View.GONE );
        }

        if(savedInstanceState == null) {
            initiateTabCategories(tabCategoryWrapper, tabType, args.getString(CURRENT_CATEGORY));
        } else {
            initiateTabCategories(tabCategoryWrapper,
                savedInstanceState.getString(TABTYPE), savedInstanceState.getString(CURRENT_CATEGORY));
        }

        return view;
    }

    public void selectCategory(View view) {
        selectButton(view);
    }

    public void selectButton(View view) {
        int i = 0;
        int index = 0;

        for(ViewGroup categoryButton : categoryButtons ) {
            categoryButton.setSelected(false);
            categoryButton.findViewById(R.id.category_indicator).setVisibility(View.GONE);
//            categoryButton.findViewById(R.id.category_text).setBackgroundResource(R.drawable.border_bottom_gray);
            categoryButton.findViewById(R.id.category_text).setAlpha(0.2f);
//            ((TextView)categoryButton.findViewById(R.id.category_text)).setTypeface(
//                    Typeface.createFromAsset(getActivity().getAssets(), "NotoSansKR-Light-Hestia.otf"));

            if(categoryButton.equals(view)) {
                index = i;
            }
            i++;
        }

        selectedCategoryIndex = index;
        currentCategory = tabCategories.get(index).getCategoryName();

        int scrollOffset = (int)(view.getLeft() - (( rootView.getWidth() - view.getWidth()) * 0.5));
        categoryScrollView.smoothScrollTo(scrollOffset + SizeConverter.dpToPx(14), 0);
        view.findViewById(R.id.category_indicator).setVisibility(View.VISIBLE);
//        view.findViewById(R.id.category_text).setBackgroundResource(R.drawable.border_bottom_gold);
        view.findViewById(R.id.category_text).setAlpha(1.0f);
//        ((TextView)view.findViewById(R.id.category_text)).setTypeface(
//                Typeface.createFromAsset(getActivity().getAssets(), "NotoSansKR-Regular-Hestia.otf"));

        view.setSelected(true);
    }

    public void initiateTabCategories(LinearLayout tabCategoryWrapper, String tabType, String currentCategory) {
        Prefs prefs = new Prefs( getActivity() );
        HashMap<String, ArrayList<TabCategory>> allTabCategories = prefs.getCurrentTabCategories();
        tabCategories = allTabCategories.get(tabType);
        categoryButtons.clear();
        tabCategoryWrapper.removeAllViews();
        View.OnClickListener categoryClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                selectCategory(v);
            }
        };

        if(tabCategories != null) {
            if( (tabType.equals("mypage") && MyApplication.getGlobalApplicationContext().isLoggedIn())
                    || !tabType.equals("mypage")) {
                for (TabCategory tabCategory : tabCategories) {
                    FrameLayout categoryButton = (FrameLayout) getActivity().getLayoutInflater().inflate(R.layout.category_button, null);
                    TextView categoryButtonText = (TextView) categoryButton.findViewById(R.id.category_text);
                    categoryButtonText.setText(tabCategory.getCategoryNameKor());
                    categoryButton.setOnClickListener(categoryClickListener);
                    categoryButtons.add(categoryButton);
                    tabCategoryWrapper.addView(categoryButton);
                }

                if (currentCategory != null) {
                    selectedCategoryIndex = getCategoryIndex(currentCategory);
                } else {
                    selectedCategoryIndex = 0;
                }

                View view = categoryButtons.get(selectedCategoryIndex);
                 view.findViewById(R.id.category_indicator).setVisibility(View.VISIBLE);
//                view.findViewById(R.id.category_text).setBackgroundResource(R.drawable.border_bottom_gold);
                view.findViewById(R.id.category_text).setAlpha(1.0f);
//                ((TextView) view.findViewById(R.id.category_text)).setTypeface(
//                        Typeface.createFromAsset(getActivity().getAssets(), "NotoSansKR-Regular-Hestia.otf"));


                view.setSelected(true);
            } else {
                TabCategory notLoggedInCategory = new TabCategory(999l, "로그인이 필요합니다", "", "mypage", 1l);
                FrameLayout categoryButton = (FrameLayout) getActivity().getLayoutInflater().inflate(R.layout.category_button, null);
                TextView categoryButtonText = (TextView) categoryButton.findViewById(R.id.category_text);
                categoryButtonText.setText(notLoggedInCategory.getCategoryNameKor());
                categoryButton.setOnClickListener(categoryClickListener);
                categoryButtons.add(categoryButton);
                tabCategoryWrapper.addView(categoryButton);
                selectButton(categoryButton);
            }
        }
    }

    public int getCategoryIndex(String categoryName) {
        int i=0;
        for(TabCategory tabCategory : tabCategories) {
            if(tabCategory.getCategoryName().equals(categoryName)) {
                return i;
            }
            i++;
        }
        return 0;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    protected void startFullscreenActivity(String url) {
        Log.i("URL1", url);
        Intent intent = new Intent(getActivity(), FullscreenActivity.class);
        intent.putExtra("URL", url);
        getActivity().startActivityForResult(intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY);
    }

    @Override
    public void refresh() {
        // TODO: do refresh
    }
}
