package com.jjlee.wedqueen.rest.app.controller;


import com.jjlee.wedqueen.rest.app.model.TabCategoryResponse;
import com.jjlee.wedqueen.rest.app.model.VersionResponse;
import com.jjlee.wedqueen.rest.app.service.AppService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;


public class AppController extends RetrofitController {
    private AppService appService;

    public AppController() {
        super();
        appService = RETROFIT.create(AppService.class);
    }

    public Call<VersionResponse> checkVersion(String version) {
        return appService.checkVersion(version);
    }

    public Call<TabCategoryResponse> getTabCategories() {
        return appService.getTabCategories();
    }

}
