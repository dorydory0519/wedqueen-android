package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.BitmapUtil;
import com.jjlee.wedqueen.utils.ColorFilterGenerator;
import com.jjlee.wedqueen.utils.ObjectUtils;

/**
 * Created by esmac on 2017. 9. 9..
 */

public class CustomRealWeddingModel extends RelativeLayout {

    RequestManager mRequestManager;

    ImageView realweddingImg;
    ImageView realweddingColorFilter;
    TextView realweddingEditor;
    TextView realweddingTitle;

    BitmapUtil bitmapUtil;

    public CustomRealWeddingModel(Context context) {
        super(context);
        initView();

    }

    public CustomRealWeddingModel(Context context, RequestManager requestManager) {
        super(context);
        mRequestManager = requestManager;
        initView();
    }

    public CustomRealWeddingModel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomRealWeddingModel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_realweddings, this, false);
        addView(v);

        bitmapUtil = new BitmapUtil();

        realweddingImg = (ImageView)findViewById(R.id.realwedding_img);
        realweddingColorFilter = (ImageView)findViewById(R.id.realwedding_colorfilter);
        realweddingEditor = (TextView) findViewById(R.id.realwedding_editor);
        realweddingTitle = (TextView)findViewById(R.id.realwedding_title);

    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.RealWeddingModel);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.RealWeddingModel, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {

        String realweddingImg_resID = typedArray.getString(R.styleable.RealWeddingModel_realwedding_img);
        setRealweddingImg( realweddingImg_resID );

        String realweddingEditor_resID = typedArray.getString(R.styleable.RealWeddingModel_realwedding_editor);
        setRealweddingEditor( realweddingEditor_resID );

        String realweddingTitle_resID = typedArray.getString(R.styleable.RealWeddingModel_realwedding_title);
        setRealweddingTitle( realweddingTitle_resID );

        typedArray.recycle();
    }

    public void setRealweddingImg(String resUrl) {
        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load( resUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( realweddingImg );
        } else {
            Glide.with( getContext() ).load( resUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( realweddingImg );
        }
    }
    public void setRealweddingEditor(String resString) {
        realweddingEditor.setText( resString );
    }
    public void setRealweddingTitle(String resString) {
        realweddingTitle.setText( resString );
    }
    public void setRealweddingSize(int size) {
        realweddingImg.getLayoutParams().width = size;
        realweddingImg.getLayoutParams().height = size;
        realweddingColorFilter.getLayoutParams().width = size;
        realweddingColorFilter.getLayoutParams().height = size;
        realweddingTitle.getLayoutParams().width = size;
        realweddingEditor.getLayoutParams().width = size - 40;
    }
}
