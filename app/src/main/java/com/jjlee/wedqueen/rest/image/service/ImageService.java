package com.jjlee.wedqueen.rest.image.service;

import com.jjlee.wedqueen.rest.account.model.LoginCommonResponse;
import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.rest.image.model.ImageResponse;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by JJLEE on 2016. 5. 23..
 */
public interface ImageService {
    @Headers("Accept: application/json")
    @Multipart
    @POST
    Call<String> postImageToServer(@Url String url,
                                           @Part MultipartBody.Part image);

    @Headers("Accept: application/json")
    @Multipart
    @POST
    Call<ImageResponse> postImageUploadToServer(@Url String url,
                                                @Part MultipartBody.Part image);

    @Headers("Accept: application/json")
    @Multipart
    @POST
    Call<ImageResponse> postImagesUploadToServer(@Url String url,
                                                @Part List<MultipartBody.Part> images);

    @Headers("Accept: application/json")
    @Multipart
    @POST
    Call<ImageResponse> postImageToServerForProfile (@Url String url,
                                                     @Part MultipartBody.Part image);

}
