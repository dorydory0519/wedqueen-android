package com.jjlee.wedqueen.rest.payment.model;

import com.jjlee.wedqueen.rest.survey.model.SurveyAnswer;

import java.util.List;

/**
 * Created by esmac on 2018. 1. 11..
 */

public class PaymentResponse {
    private List<Payment> payments;

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }
}
