package com.jjlee.wedqueen.view;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.dday.model.CategoryDto;
import com.jjlee.wedqueen.utils.DecimalUtils;
import com.jjlee.wedqueen.utils.DrawableUtils;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.ViewUtils;

import java.util.List;

public class DragCategoryAdapter extends RecyclerView.Adapter<DragCategoryAdapter.CategoryViewHolder> implements DraggableItemAdapter<DragCategoryAdapter.CategoryViewHolder> {

    private interface Draggable extends DraggableItemConstants {}

    private ItemClick itemClick;
    public interface ItemClick {
        public void onClick( long categoryId );
    }
    private ItemCheck itemCheck;
    public interface ItemCheck {
        public void onCheck( long categoryId, boolean isCompleteCheck );
    }
    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }
    public void setItemCheck(ItemCheck itemCheck) { this.itemCheck = itemCheck; }

    private CategoryDataProvider mProvider;
    private Context mContext;

    public static class CategoryViewHolder extends AbstractDraggableItemViewHolder {

        public LinearLayout mContainer;
        public LinearLayout mLayoutCategoryData;
        public View mDragHandle;
        private CheckBox mCompleteCheck;
        private TextView mCategoryTitle;
        private TextView mCategoryStatus;
        private TextView mBudget;
        private TextView mExpense;

        public CategoryViewHolder( View v ) {
            super( v );

            mContainer = v.findViewById( R.id.container );
            mLayoutCategoryData = v.findViewById( R.id.layout_category_data );
            mDragHandle = v.findViewById( R.id.drag_handle );
            mCompleteCheck = v.findViewById( R.id.completeCheck );
            mCategoryTitle = v.findViewById( R.id.category_title );
            mCategoryStatus = v.findViewById( R.id.category_status );
            mBudget = v.findViewById( R.id.budget );
            mExpense = v.findViewById( R.id.expense );
        }
    }

    public DragCategoryAdapter( Context context, CategoryDataProvider provider ) {
        this.mContext = context;
        this.mProvider = provider;

        setHasStableIds( true );
    }

    @Override
    public long getItemId( int position ) {
        return mProvider.getItem( position ).getId();
    }

    public String getCategoryTitle( int position ) { return mProvider.getItem( position ).getTitle(); }

//    @Override
//    public int getItemViewType(int position) {
//        return mProvider.getItem( position ).getViewType();
//    }

    @Override
    public DragCategoryAdapter.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );
        View v = inflater.inflate( R.layout.list_item_checkcategory, parent, false );

        return new DragCategoryAdapter.CategoryViewHolder( v );
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        CategoryDataProvider.CategoryData item = mProvider.getItem( position );

        LinearLayout container = holder.mContainer;
        TextView categoryTitle = holder.mCategoryTitle;
        CheckBox completeCheck = holder.mCompleteCheck;
        TextView categoryStatus = holder.mCategoryStatus;

        if( item.isPinned() ) {
            holder.mDragHandle.setVisibility( View.INVISIBLE );
            holder.mDragHandle.setClickable( false );
            holder.mCompleteCheck.setVisibility( View.VISIBLE );
        } else {
            holder.mDragHandle.setVisibility( View.VISIBLE );
            holder.mDragHandle.setClickable( true );
            holder.mCompleteCheck.setVisibility( View.INVISIBLE );
        }


        // item Data Obtain
        String title = item.getTitle();
        boolean isUnTitled = false;
        if( ObjectUtils.isEmpty( title ) ) {
            title = "제목없음";
            isUnTitled = true;
        }
        boolean isEdited = false;
        if( item.getBudget() > 0 || item.getExpense() > 0 ) {
            isEdited = true;
        }
        boolean isCompleteCheck = item.isCompleteChecked();
        String status = item.getDDayStatus();


        // Set Data to ViewHolder
        if( !ObjectUtils.isEmpty( status ) ) {
            if( status.equals( "red" ) ) {
                categoryStatus.setVisibility( View.VISIBLE );
                categoryStatus.setText( "급해요!" );
                categoryStatus.setBackground( ContextCompat.getDrawable( mContext, R.drawable.category_status_emergency ) );
            } else if( status.equals( "orange" ) ) {
                categoryStatus.setVisibility( View.VISIBLE );
                categoryStatus.setText( "지금 해야해요!" );
                categoryStatus.setBackground( ContextCompat.getDrawable( mContext, R.drawable.category_status_now ) );
            } else if( status.equals( "yellow" ) ) {
                categoryStatus.setVisibility( View.VISIBLE );
                categoryStatus.setText( "여유있어요" );
                categoryStatus.setBackground( ContextCompat.getDrawable( mContext, R.drawable.category_status_spare ) );
            } else {
                categoryStatus.setVisibility( View.GONE );
            }
        }

        completeCheck.setChecked( isCompleteCheck );
        SpannableString spannableString = new SpannableString( title );
        if( isCompleteCheck ) {
            spannableString.setSpan( new StrikethroughSpan(), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
            categoryTitle.setText( spannableString );
            categoryTitle.setTextColor( ContextCompat.getColor( mContext, R.color.colorGray ) );
            categoryStatus.setVisibility( View.GONE );
            container.setBackgroundColor( ContextCompat.getColor( mContext, R.color.checked_category ) );
        } else {
            StrikethroughSpan[] strikethroughSpans = spannableString.getSpans( 0, spannableString.length(), StrikethroughSpan.class );
            for( StrikethroughSpan strikethroughSpan : strikethroughSpans ) {
                spannableString.removeSpan( strikethroughSpan );
            }
            categoryTitle.setText( spannableString );
            categoryTitle.setTextColor( Color.parseColor( "#000000" ) );
            categoryStatus.setVisibility( View.VISIBLE );

            container.setBackgroundColor( ContextCompat.getColor( mContext, R.color.colorWhite ) );
        }

        if( isUnTitled ) {
            categoryTitle.setTextColor( Color.parseColor( "#CCCCCC" ) );
        }

        if( isEdited ) {
            categoryStatus.setVisibility( View.GONE );
        }

        holder.mBudget.setText( DecimalUtils.convertDecimalFormat( (int)item.getBudget() ) );
        holder.mExpense.setText( DecimalUtils.convertDecimalFormat( (int)item.getExpense() ) );
        holder.mLayoutCategoryData.setTag( item.getCategoryId() );
        holder.mLayoutCategoryData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( itemClick != null ) {
                    itemClick.onClick( (long)view.getTag() );
                }
            }
        });
        completeCheck.setTag( position );
        completeCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                int checkedPosition = (int)compoundButton.getTag();
                setCompleteCheck( checkedPosition, b );

                if( itemCheck != null ) {
                    itemCheck.onCheck( getItemId( checkedPosition ), b );
                }
            }
        });

        // Drag 상태에 따른 backgroundColor 핸들링
//        int dragState = holder.getDragStateFlags();
//
//        if( ( dragState & Draggable.STATE_FLAG_IS_UPDATED ) != 0 ) {
//            int bgResId;
//
//            if( ( dragState & Draggable.STATE_FLAG_IS_ACTIVE ) != 0 ) {
//                bgResId = R.drawable.bg_item_dragging_active_state;
//
//                DrawableUtils.clearState( holder.mContainer.getForeground() );
//            } else if( ( dragState & Draggable.STATE_FLAG_DRAGGING ) != 0 ) {
//                bgResId = R.drawable.bg_item_dragging_state;
//            } else {
//                bgResId = R.drawable.bg_item_normal_state;
//            }
//
//            holder.mContainer.setBackgroundResource( bgResId );
//        }
    }

    private void setCompleteCheck( int position, boolean isCompleteCheck ) {
        mProvider.getItem( position ).setCompleteChecked( isCompleteCheck );
        notifyItemChanged( position );
    }

    public void setPinned( boolean pinned ) {

        if( !ObjectUtils.isEmpty( mProvider ) ) {
            for( int i = 0; i < mProvider.getCount(); i++ ) {
                CategoryDataProvider.CategoryData data = mProvider.getItem( i );
                data.setPinned( pinned );
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mProvider.getCount();
    }

    public void addItem( CategoryDto categoryDto ) {
        mProvider.addItem( categoryDto );
        notifyDataSetChanged();
    }

    @Override
    public void onMoveItem(int fromPosition, int toPosition) {

        Log.e( "WQTEST3", "여길 부르나? fromPosition : " + fromPosition + " toPosition : " + toPosition );

        mProvider.moveItem( fromPosition, toPosition );
    }

    @Override
    public boolean onCheckCanStartDrag(DragCategoryAdapter.CategoryViewHolder holder, int position, int x, int y) {
        View containerView = holder.mContainer;
        View dragHandleView = holder.mDragHandle;

        int offsetX = containerView.getLeft() + (int) ( containerView.getTranslationX() + 0.5f );
        int offsetY = containerView.getTop() + (int) ( containerView.getTranslationY() + 0.5f );

        return ViewUtils.hitTest( dragHandleView, x - offsetX, y - offsetY );
    }

    @Override
    public ItemDraggableRange onGetItemDraggableRange(DragCategoryAdapter.CategoryViewHolder holder, int position) {
        return null;
    }

    @Override
    public boolean onCheckCanDrop(int draggingPosition, int dropPosition) {
        return true;
    }

    @Override
    public void onItemDragStarted(int position) {
        notifyDataSetChanged();
    }

    @Override
    public void onItemDragFinished(int fromPosition, int toPosition, boolean result) {
        notifyDataSetChanged();
    }


}
