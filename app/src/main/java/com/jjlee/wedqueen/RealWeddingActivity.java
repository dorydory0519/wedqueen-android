package com.jjlee.wedqueen;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.GridLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.CustomViews.CustomRealWeddingModel;
import com.jjlee.wedqueen.rest.content.controller.ContentController;
import com.jjlee.wedqueen.rest.content.model.Content;
import com.jjlee.wedqueen.rest.content.model.ContentResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.CustomScrollView;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RealWeddingActivity extends AppCompatActivity {

    private RequestManager mGlideRequestManager;

    private int displayWidth;
    private int realweddingImgSize;

    private boolean isLoadingRealweddingData;
    private int targetTime;
    private int realWeddingPage;

    private String categoryName;
    private String orderType;

    private GridLayout realweddingWrapper;

    private TextView sortText;

    private Dialog filterSortDlg;

    private ContentController contentController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realwedding);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "realwedding_list" ) );

        mGlideRequestManager = Glide.with( this );

        categoryName = getIntent().getStringExtra( "categoryName" );
        orderType = "";

        isLoadingRealweddingData = false;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics( displayMetrics );
        displayWidth = displayMetrics.widthPixels;

        realWeddingPage = 1;
        targetTime = new Date().getSeconds();

        sortText = (TextView) findViewById( R.id.sort_text );

        initFilterSortDlg();

        findViewById( R.id.filter_sort ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFilterSortDlg();
            }
        });
        sortText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFilterSortDlg();
            }
        });

        // 리얼웨딩 Setting
        realweddingWrapper = (GridLayout) findViewById(R.id.realwedding_wrapper);
        realweddingImgSize = ( displayWidth - SizeConverter.dpToPx( 14 ) ) / 2;
        contentController = new ContentController();
        getContentsFromServer();
        ((CustomScrollView) findViewById(R.id.realwedding_scrollview)).setOnBottomReachedListener(new CustomScrollView.OnBottomReachedListener() {
            @Override
            public void onBottomReached() {
                if( !isLoadingRealweddingData ) {
                    getContentsFromServer();
                }
            }
        });

    }

    private void getContentsFromServer() {
        // 첫 Loading or Refresh
        if( realWeddingPage == 1 ) {
            realweddingWrapper.removeAllViews();
        }

        isLoadingRealweddingData = true;

        Call<ContentResponse> contentCall = contentController.getContentsBySort(categoryName, realWeddingPage++, targetTime, orderType);
        contentCall.enqueue(new Callback<ContentResponse>() {
            @Override
            public void onResponse(Call<ContentResponse> call, Response<ContentResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    ContentResponse contentResponse = response.body();

                    if( contentResponse.getContents().size() > 0 ) {
                        List<Content> contents = contentResponse.getContents();

                        for( Content content : contents ) {

                            if( !ObjectUtils.isEmpty( RealWeddingActivity.this ) ) {
                                CustomRealWeddingModel customRealWeddingModel = new CustomRealWeddingModel( RealWeddingActivity.this, mGlideRequestManager );
                                customRealWeddingModel.setRealweddingSize( realweddingImgSize );
                                customRealWeddingModel.setRealweddingImg( content.getImageUrl() );
                                customRealWeddingModel.setRealweddingEditor( "by " + content.getCompany().getName() );
                                customRealWeddingModel.setRealweddingTitle( content.getTitle() );
                                customRealWeddingModel.setTag( content.getUrl() );
                                customRealWeddingModel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startFullscreenActivity( "" + v.getTag() );
                                    }
                                });
                                realweddingWrapper.addView( customRealWeddingModel );
                            }
                        }
                    }
                } else {
                    Log.e( "WQTEST", "response body is null" );
                }

                isLoadingRealweddingData = false;

            }

            @Override
            public void onFailure(Call<ContentResponse> call, Throwable t) {
                t.printStackTrace();
                isLoadingRealweddingData = false;
            }
        });
    }


    private void initFilterSortDlg() {
        filterSortDlg = new Dialog( this );
        filterSortDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        filterSortDlg.setContentView(R.layout.dialog_filter_sort);

        filterSortDlg.findViewById( R.id.sort_recent ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderType = "recent";
                sortText.setText( "최신순" );
                realWeddingPage = 1;
                getContentsFromServer();
                filterSortDlg.dismiss();
            }
        });
        filterSortDlg.findViewById( R.id.sort_popular ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderType = "";
                sortText.setText( "인기순" );
                realWeddingPage = 1;
                getContentsFromServer();
                filterSortDlg.dismiss();
            }
        });
        filterSortDlg.findViewById( R.id.sort_view ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderType = "mostviewed";
                sortText.setText( "조회순" );
                realWeddingPage = 1;
                getContentsFromServer();
                filterSortDlg.dismiss();
            }
        });


    }

    private void showFilterSortDlg() {
        TextView sortRecent = (TextView) filterSortDlg.findViewById( R.id.sort_recent );
        TextView sortPopular = (TextView) filterSortDlg.findViewById( R.id.sort_popular );
        TextView sortView = (TextView) filterSortDlg.findViewById( R.id.sort_view );

        if( orderType.equals( "recent" ) ) {
            sortRecent.setTextColor( getResources().getColor( R.color.colorAccent ) );
            sortPopular.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortView.setTextColor( Color.parseColor( "#CCCCCC" ) );
        } else if( orderType.equals( "" ) ) {
            sortRecent.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortPopular.setTextColor( getResources().getColor( R.color.colorAccent ) );
            sortView.setTextColor( Color.parseColor( "#CCCCCC" ) );
        } else if( orderType.equals( "mostviewed" ) ) {
            sortRecent.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortPopular.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortView.setTextColor( getResources().getColor( R.color.colorAccent ) );
        }

        filterSortDlg.show();
    }



    private void startFullscreenActivity(String url) {
        if( MyApplication.getGlobalApplicationContext().canLoadDetailContent() ) {
            Intent intent = new Intent( this, FullscreenActivity.class);
            intent.putExtra("URL", url);
            startActivityForResult(intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY);
        } else {
            Intent intent = new Intent( this, AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
