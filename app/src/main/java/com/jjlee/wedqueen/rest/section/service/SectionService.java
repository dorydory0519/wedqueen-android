package com.jjlee.wedqueen.rest.section.service;

import com.jjlee.wedqueen.rest.section.model.SectionResponse;
import com.jjlee.wedqueen.rest.sector.model.SectorResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SectionService {
    @Headers("Accept: application/json")
    @GET("api/v1/section")
    Call<SectionResponse> getSection(@Query("tabType") String tabType);

}
