package com.jjlee.wedqueen.rest.user.model;

import com.jjlee.wedqueen.rest.content.model.Content;

import java.util.List;

public class UserResponse {

    private String ddayToString;
    private Long dday;
    private User user;
    private Boolean hasSubscription;
    private int followerCount;
    private List<Content> contents;

    public String getDdayToString() {
        return ddayToString;
    }

    public void setDdayToString(String ddayToString) {
        this.ddayToString = ddayToString;
    }

    public Long getDday() {
        return dday;
    }

    public void setDday(Long dday) {
        this.dday = dday;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getHasSubscription() {
        return hasSubscription;
    }

    public void setHasSubscription(Boolean hasSubscription) {
        this.hasSubscription = hasSubscription;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }
}
