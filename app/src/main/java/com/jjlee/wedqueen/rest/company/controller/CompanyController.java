package com.jjlee.wedqueen.rest.company.controller;


import com.jjlee.wedqueen.rest.company.model.CompanyResponse;
import com.jjlee.wedqueen.rest.company.service.CompanyService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class CompanyController extends RetrofitController {
    private CompanyService companyService;

    public CompanyController() {
        super();
        companyService = RETROFIT.create(CompanyService.class);
    }

    public Call<CompanyResponse> getPremiumsInTab(String tab, String category, int page) {
        return companyService.getPremiumsInTab(tab, category, page);
    }

    public Call<CompanyResponse> getPremiums(String category, int page) {
        return companyService.getPremiums(category, page, 3);
    }

    public Call<CompanyResponse> getPremiums(String category, int page, int contentCount) {
        return companyService.getPremiums(category, page, contentCount);
    }

    public Call<CompanyResponse> getCompanies(String category, int page) {
        return companyService.getCompanies(category, page);
    }

}
