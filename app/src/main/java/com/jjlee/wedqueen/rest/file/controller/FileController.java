package com.jjlee.wedqueen.rest.file.controller;

import com.google.gson.JsonObject;
import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.WebviewContainingActivity;
import com.jjlee.wedqueen.rest.file.service.FileService;
import com.jjlee.wedqueen.support.ProgressRequestBody;
import com.jjlee.wedqueen.support.RetrofitController;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Created by JJLEE on 2016. 5. 23..
 */
public class FileController extends RetrofitController {
    private FileService fileService;

    public FileController() {
        super();
        fileService = RETROFIT.create(FileService.class);
    }

    public Call<JsonObject> postLiveChatFileToServer(File file, String mimeType) {

        String mimeTypeString = "multipart/form-data";
        if(mimeType != null) {
            mimeTypeString = mimeType + "/*";
        }

        RequestBody requestFile =
                RequestBody.create(MediaType.parse(mimeTypeString), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        return fileService.postLiveChatFileToServer("group/livechat/file", body);
    }

    public Call<JsonObject> postInvitationVideoFileToServer(File file, String mimeType, FullscreenActivity callee) {

        String mimeTypeString = "multipart/form-data";
        if(mimeType != null) {
            mimeTypeString = mimeType;
        }

        ProgressRequestBody bodyPart = new ProgressRequestBody(mimeTypeString, file, callee);

        MultipartBody.Part filePart =
                MultipartBody.Part.createFormData("file", file.getName(), bodyPart);

        return fileService.postLiveChatFileToServer("aws/file/video", filePart);
    }

}