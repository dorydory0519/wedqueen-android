package com.jjlee.wedqueen.rest.tutorial.model;


import java.util.List;

public class Tutorial {
    private List<TutorialDetail> tutorialDetails;

    public List<TutorialDetail> getTutorialDetails() {
        return tutorialDetails;
    }

    public void setTutorialDetails(List<TutorialDetail> tutorialDetails) {
        this.tutorialDetails = tutorialDetails;
    }
}

