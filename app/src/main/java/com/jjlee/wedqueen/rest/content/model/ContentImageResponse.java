package com.jjlee.wedqueen.rest.content.model;

import java.util.List;

public class ContentImageResponse {

    private String title;
    private String url;
    private List<String> sources;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getSources() {
        return sources;
    }

    public void setSources(List<String> sources) {
        this.sources = sources;
    }
}
