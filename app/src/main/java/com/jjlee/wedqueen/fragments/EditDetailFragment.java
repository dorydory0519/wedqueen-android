package com.jjlee.wedqueen.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.google.android.flexbox.FlexboxLayout;
import com.jjlee.wedqueen.ContentEditActivity;
import com.jjlee.wedqueen.CustomViews.CustomHashTagImageModel;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.app.model.TabCategory;
import com.jjlee.wedqueen.rest.autocomplete.model.AutoCompleteSet;
import com.jjlee.wedqueen.rest.autocomplete.model.Company;
import com.jjlee.wedqueen.rest.autocomplete.model.HashTag;
import com.jjlee.wedqueen.rest.content.model.Content;
import com.jjlee.wedqueen.rest.content.model.ContentForFragment;
import com.jjlee.wedqueen.rest.content.model.PostContent;
import com.jjlee.wedqueen.rest.region.controller.RegionController;
import com.jjlee.wedqueen.rest.region.model.Region;
import com.jjlee.wedqueen.rest.region.model.RegionResponse;
import com.jjlee.wedqueen.rest.verification.model.Verification;
import com.jjlee.wedqueen.support.AutoCompleteAdapter;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

//import io.userhabit.service.Userhabit;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by esmac on 2017. 9. 20..
 */

public class EditDetailFragment extends Fragment {

    private TextView estimateGuideLabel;
    private ScrollView editDetailScrollView;

    private Dialog publishDlg;
//    private Dialog reviewPointDlg;
    private Dialog categoryDlg;

    private AutoCompleteAdapter autoCompleteCompanyAdapter;
    private AutoCompleteAdapter autoCompleteHashTagAdapter;

    private AutoCompleteTextView autocompleCompany;
    private AutoCompleteTextView autocompleHashTag;

    private FlexboxLayout hashTagWrapper;

    private List<String> selectedHashTags = new ArrayList<String>();

    private Spinner regionSpinner;
    private List<String> regionData;
    private RegionController regionController;
    private String selectedRegion;
    private String userRegion;

    private TextView categoryLabel;
    private ArrayList<String> categoryKorList;
    private ArrayList<Long> categoryIdList;
    private List<String> selectedCategory = new ArrayList<>();
    private Long selectedCategoryId;

    private int year_x, month_x, day_x;
    private TextView dateLabel;

    private EditText estimateLabel;

    private TextView saveBtn;

    private String surveyReviewParam;
    private String companyName;
    private String reservTime;
//    private boolean isWarningPoint = false;

    public static EditDetailFragment newInstance( String surveyReviewParam ) {
        EditDetailFragment fragment = new EditDetailFragment();
        Bundle args = new Bundle();
        args.putString( "surveyReviewParam", surveyReviewParam );
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        surveyReviewParam = args.getString( "surveyReviewParam" );
        if( !ObjectUtils.isEmpty( surveyReviewParam ) ) {
            String[] arrSurveyReviewParam = surveyReviewParam.split( ";" );

            if( !ObjectUtils.isEmpty( arrSurveyReviewParam ) && arrSurveyReviewParam.length > 0 ) {
                companyName = arrSurveyReviewParam[1];
                reservTime = arrSurveyReviewParam[2];
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_editdetail, container, false);

        Prefs prefs = new Prefs( getActivity() );

        if( !ObjectUtils.isEmpty( prefs.getCurrentTabCategories() ) ) {
            List<TabCategory> tabCategoryList = prefs.getCurrentTabCategories().get( "contentfeed" );
            if( !ObjectUtils.isEmpty( tabCategoryList ) ) {
                categoryKorList = new ArrayList<String>();
                categoryIdList = new ArrayList<Long>();
                for( int i = 0; i < tabCategoryList.size(); i++ ) {

                    if( !ObjectUtils.isEmpty( tabCategoryList.get( i ).getCategoryName() ) ) {
                        categoryKorList.add( tabCategoryList.get( i ).getCategoryNameKor() );
                        categoryIdList.add( tabCategoryList.get( i ).getId() );
                    }
                }
            }
        }

        // 이전 버튼 Click Action
        (view.findViewById(R.id.prev_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Move Prev Fragment
                ((ContentEditActivity)getActivity()).moveFragment( 0 );
            }
        });

        // 임시저장 버튼 Click Action
        saveBtn = (TextView)view.findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 작성한 내용들 저장 Action
                ((ContentEditActivity)getActivity()).postTempContent();
            }
        });

        // 발행 버튼 Click Action
        (view.findViewById(R.id.publish_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPublishDlg();
            }
        });


//        initReviewPointDlg();
        initCategoryDlg( 0 );


        // 지역 부분
        userRegion = prefs.getUserRegion();
        regionSpinner = (Spinner)view.findViewById(R.id.region_spinner);
        regionData = new ArrayList<>();
        regionController = new RegionController();
        Call<RegionResponse> regionCall = regionController.getRegions();
        regionCall.enqueue(new Callback<RegionResponse>() {
            @Override
            public void onResponse(Call<RegionResponse> call, Response<RegionResponse> response) {
                if( response.body() != null ) {
                    RegionResponse regionResponse = response.body();

                    if( regionResponse != null && regionResponse.getRegions().size() > 0 ) {
                        for( Region region : regionResponse.getRegions() ) {
                            if( region.getKorPrecinct() != "" ) {
                                regionData.add( region.getKorPrecinct() );
                            } else {
                                regionData.add( region.getKorRegion() );
                            }
                        }
                        setSpinnerAdapter();
                        regionSpinner.setSelection( regionData.indexOf( userRegion ) );
                    }
                }
            }

            @Override
            public void onFailure(Call<RegionResponse> call, Throwable t) {
                regionData.add( "서울" );
                regionData.add( "부산" );
                regionData.add( "대전" );
                regionData.add( "경기" );
                setSpinnerAdapter();
            }
        });


        // 업체명 부분
        autocompleCompany = (AutoCompleteTextView)view.findViewById(R.id.autocomple_company);
        autoCompleteCompanyAdapter = new AutoCompleteAdapter( getContext(), android.R.layout.simple_dropdown_item_1line, "company" );
        autocompleCompany.setAdapter( autoCompleteCompanyAdapter );
        autocompleCompany.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AutoCompleteSet autoCompleteSet = autoCompleteCompanyAdapter.getItem( position );
                if( autoCompleteSet instanceof Company ) {
                    autocompleCompany.setText( ((Company) autoCompleteSet).getName() );
                    autocompleCompany.setSelection( autocompleCompany.length() );

                    ((ContentEditActivity)getActivity()).setSaved( false );
                }
            }
        });


        // 카테고리 부분
        categoryLabel = (TextView) view.findViewById( R.id.category_label );
        categoryLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryDlg.show();
            }
        });


        // 준비날짜 부분
        Calendar today = Calendar.getInstance();
        year_x = today.get( Calendar.YEAR );
        month_x = today.get( Calendar.MONTH );
        day_x = today.get( Calendar.DATE );
        dateLabel = (TextView)view.findViewById(R.id.date_label);
        dateLabel.setText( "준비날짜 \t\t " + year_x + "년 " + ( month_x + 1 ) + "월 " + day_x + "일" );
        dateLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if( !ObjectUtils.isEmpty( reviewParam ) && !isWarningPoint ) {
//                    reviewPointDlg.show();
//                    isWarningPoint = true;
//                } else {
                    DatePickerDialog datePickerDialog = new DatePickerDialog( getActivity(), dPickerListener, year_x, month_x, day_x );
                    datePickerDialog.show();
//                    Userhabit.setScreen( datePickerDialog, "날짜선택 Dialog" );
//                }
            }
        });


        // 견적 부분
        estimateLabel = (EditText)view.findViewById(R.id.estimate_label);
        estimateLabel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    estimateLabel.removeTextChangedListener(this);
                    String value = s.toString();
                    if( !ObjectUtils.isEmpty( value ) ) {
                        String str = s.toString().replaceAll( ",", "" ).replaceAll( " ₩", "" ).replaceAll( "₩", "" );
                        DecimalFormat decimalFormat = new DecimalFormat( "###,###" );
                        double dEstimate = Double.parseDouble( str );

                        if( dEstimate > 2000000000 ) {
                            Toast.makeText( getActivity(), "범위가 너무 큽니다.", Toast.LENGTH_SHORT).show();
                            dEstimate = 2000000000;
                        }

                        str = decimalFormat.format( dEstimate );
                        if( str.length() > 13 ) {
                            Toast.makeText( getActivity(), "범위가 너무 큽니다.", Toast.LENGTH_SHORT).show();
                            str = decimalFormat.format( dEstimate / 10 );
                        }

                        estimateLabel.setText( str + " ₩" );
                        estimateLabel.setSelection( str.length() );

                    }
                    estimateLabel.addTextChangedListener(this);
                    ((ContentEditActivity)getActivity()).setSaved( false );
                } catch (Exception e) {
                    e.printStackTrace();
                    estimateLabel.addTextChangedListener(this);
                }
            }
        });


        // 해시태그 부분
        hashTagWrapper = (FlexboxLayout) view.findViewById( R.id.hashtag_wrapper );
        editDetailScrollView = (ScrollView) view.findViewById(R.id.editdetail_scrollview);
        estimateGuideLabel = (TextView)view.findViewById(R.id.estimate_guide_label);
        autocompleHashTag = (AutoCompleteTextView)view.findViewById(R.id.edit_hashtag);
//        EditText editHashtag = (EditText)view.findViewById(R.id.edit_hashtag);
//        autocompleHashTag.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if( hasFocus ) {
//                    estimateGuideLabel.setVisibility(View.GONE);
//                } else {
//                    estimateGuideLabel.setVisibility(View.VISIBLE);
//                }
//            }
//        });
        autocompleHashTag.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch ( actionId ) {
                    case EditorInfo.IME_ACTION_DONE:
                    case EditorInfo.IME_ACTION_NEXT:
                    case EditorInfo.IME_ACTION_GO:
                        addHashTag( v.getText().toString().replaceAll( "#", "" ) );
                        break;
                    default:
                        break;
                }
                return true;
            }
        });
        autoCompleteHashTagAdapter = new AutoCompleteAdapter( getContext(), android.R.layout.simple_dropdown_item_1line, "hashtag" );
        autocompleHashTag.setAdapter( autoCompleteHashTagAdapter );
        autocompleHashTag.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AutoCompleteSet autoCompleteSet = autoCompleteHashTagAdapter.getItem( position );
                if( autoCompleteSet instanceof HashTag ) {
                    addHashTag( ((HashTag) autoCompleteSet).getName().replace( "#", "" ) );
                }
            }
        });


        // 모든 기본데이터 세팅을 마친 후 reviewParam 관련 데이터를 세팅해줌
        if( !ObjectUtils.isEmpty( surveyReviewParam ) ) {
            if( !ObjectUtils.isEmpty( companyName ) ) {
                autocompleCompany.setText( companyName );
//                autocompleCompany.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        if( !isWarningPoint ) {
//                            reviewPointDlg.show();
//                            isWarningPoint = true;
//                        }
//                    }
//                });
            }
            if( !ObjectUtils.isEmpty( reservTime ) ) {

                String strPreparationDate = reservTime.split( " " )[0];
                String[] arrPreparationDate = strPreparationDate.split( "-" );

                year_x = Integer.parseInt( arrPreparationDate[0] );
                month_x = Integer.parseInt( arrPreparationDate[1] ) - 1;
                day_x = Integer.parseInt( arrPreparationDate[2] );

                dateLabel.setText( "준비날짜 \t\t " + year_x + "년 " + ( month_x + 1 ) + "월 " + day_x + "일" );
            }
        }


        return view;
    }

    private void setSpinnerAdapter() {

        if( !ObjectUtils.isEmpty( getActivity() ) ) {
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.custom_spinner_item, regionData);
            arrayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
            regionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedRegion = regionSpinner.getItemAtPosition(position).toString();

                    // Spinner가 너무 늦게 설정되서 임시저장값을 바꾸어버리므로 일단 주석처리
//                ((ContentEditActivity)getActivity()).setSaved( false );
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            regionSpinner.setAdapter( arrayAdapter );
        }
    }

    private void addHashTag( String strHashTag ) {

        autocompleHashTag.setText( "" );

        // 이미 동일한 이름의 해시태그가 존재하는 경우 NO ACTION
        if( selectedHashTags.indexOf( strHashTag ) > -1 ) {
            return;
        }

        CustomHashTagImageModel customHashTagImageModel = new CustomHashTagImageModel( getActivity() );
        customHashTagImageModel.setHashtagText( strHashTag );
        customHashTagImageModel.setTag( strHashTag );
        customHashTagImageModel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hashTagWrapper.removeView( v );
                selectedHashTags.remove( ((CustomHashTagImageModel)v).getHashtagText().replaceAll( "#", "" ) );

                ((ContentEditActivity)getActivity()).setSaved( false );
            }
        });

        selectedHashTags.add( strHashTag );

        hashTagWrapper.addView( customHashTagImageModel );

        editDetailScrollView.smoothScrollTo( 0, editDetailScrollView.getBottom() );

        ((ContentEditActivity)getActivity()).setSaved( false );

    }


    private DatePickerDialog.OnDateSetListener dPickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            year_x = year;
            month_x = monthOfYear;
            day_x = dayOfMonth;

            ((ContentEditActivity)getActivity()).setSaved( false );

            dateLabel.setText( "준비날짜 \t\t " + year_x + "년 " + ( month_x + 1 ) + "월 " + day_x + "일" );
        }
    };

    public void setGetContentData( ContentForFragment getContent ) {

        if( !getContent.getState().equals( "temp" ) ) {
            // 임시저장된 상태가 아니라면 임시저장 문구를 '수정'으로 변경
            saveBtn.setText( "수정" );
            saveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 수정메서드로 binding
                    ((ContentEditActivity)getActivity()).putModifyContent();
                }
            });
        }

        // 지역 Setting
        regionSpinner.setSelection( regionData.indexOf( getContent.getRegion() ) );

        // 업체명 Setting
        autocompleCompany.setText( getContent.getCompanyName() );

        // 준비날짜 Setting
        String strPreparationDate = getContent.getPreparationDate();
        strPreparationDate = strPreparationDate.split( " " )[0];
        String[] arrPreparationDate = strPreparationDate.split( "-" );

        year_x = Integer.parseInt( arrPreparationDate[0] );
        month_x = Integer.parseInt( arrPreparationDate[1] ) - 1;
        day_x = Integer.parseInt( arrPreparationDate[2] );

        dateLabel.setText( "준비날짜 \t\t " + year_x + "년 " + ( month_x + 1 ) + "월 " + day_x + "일" );

        // 견적 Setting
        estimateLabel.setText( getContent.getEstimate() + "" );

        // 해시태그 Setting
        List<String> hashTags = getContent.getHashTags();
        for( String hashTag : hashTags ) {
            addHashTag( hashTag );
        }

        Log.e( "WQTEST3", "getContent.getTabCategoryId() : " + getContent.getTabCategoryId() );
        int checkedItemIdx = 0;
        if( !ObjectUtils.isEmpty( getContent.getTabCategoryId() ) && getContent.getTabCategoryId() > 0 ) {
            for( int i = 0; i < categoryIdList.size(); i++ ) {
                if( categoryIdList.get( i ) == getContent.getTabCategoryId() ) {
                    selectedCategoryId = categoryIdList.get( i );
                    categoryLabel.setText( categoryKorList.get( i ) );
                    checkedItemIdx = i;
                }
            }
        }

        // 카테고리 선택 Dialog 새로 재구성
        initCategoryDlg( checkedItemIdx );

        ((ContentEditActivity)getActivity()).setSaved( true );

    }

    public void setPostContentData(PostContent postContent) {
        postContent.setTabCategoryId( selectedCategoryId );
        postContent.setRegion( selectedRegion );
        postContent.setCompanyName( autocompleCompany.getText().toString() );
        postContent.setPreparationDate( year_x + "." + String.format( "%02d", ( month_x + 1 ) ) + "." + String.format( "%02d", day_x ) );
        String strEstimate = estimateLabel.getText().toString();
        if( !ObjectUtils.isEmpty( strEstimate ) ) {
            strEstimate = strEstimate.replaceAll( ",", "" ).replaceAll( " ₩", "" ).replaceAll( "₩", "" ).trim();

            try {
                postContent.setEstimate( Integer.parseInt( strEstimate ) );
            } catch ( NumberFormatException ne ) {
                Crashlytics.log( "strEstimate : " + strEstimate );
                postContent.setEstimate( 0 );
            }
        }

        for( String selectedHashTag : selectedHashTags ) {
            Log.e( "WQTEST3", "selectedHashTag : " + selectedHashTag );
        }

        postContent.setHashTags( selectedHashTags );
    }


    public boolean checkDetailDataSet() {

        if( ObjectUtils.isEmpty( selectedCategoryId ) ) {
            Toast.makeText( getActivity(), "카테고리를 선택해주세요!", Toast.LENGTH_SHORT).show();
            ((ContentEditActivity)getActivity()).moveFragment( 1 );
            return false;
        }

        return true;

    }


    public void showPublishDlg() {
        publishDlg = new Dialog( getActivity() );
        publishDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        publishDlg.setContentView(R.layout.dialog_save);

        TextView textToShow = (TextView) publishDlg.findViewById(R.id.text_to_show);
        textToShow.setText( "※ 300자 이하, 이미지 1개 이하의 글은 포인트 적립이 되지 않습니다!" );

        // 서버에서 가져온 발행확인 Text와 Color가 존재하면 Set
        Verification verification = ((ContentEditActivity)getActivity()).getVerification();
        if( !ObjectUtils.isEmpty( verification ) ) {

            if( !ObjectUtils.isEmpty( verification.getMsg() ) ) {
                String targetVerifyMsg = verification.getMsg();
                targetVerifyMsg = targetVerifyMsg.replaceAll( "\\\\n", "\\\n" );
                textToShow.setText( targetVerifyMsg );
            }

            if( !ObjectUtils.isEmpty( verification.getColor() ) ) {
                textToShow.setTextColor( Color.parseColor( verification.getColor() ) );
            }
        }

        TextView okBtn = (TextView) publishDlg.findViewById(R.id.ok_btn);
        okBtn.setText( "다시쓰기" );
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                publishDlg.dismiss();
            }
        });

        TextView cancelBtn = (TextView) publishDlg.findViewById(R.id.cancel_btn);
        cancelBtn.setText( "발행" );
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ContentEditActivity)getActivity()).postPublishContent();
                publishDlg.dismiss();
            }
        });

        publishDlg.show();

    }


    private void initCategoryDlg( int checkedItemIdx ) {

        AlertDialog.Builder dlgBuilder = new AlertDialog.Builder( getActivity() );
        dlgBuilder.setTitle( "카테고리를 선택해주세요." );
        String[] arrCategoryKor = categoryKorList.toArray( new String[categoryKorList.size()] );
        dlgBuilder.setSingleChoiceItems(arrCategoryKor, checkedItemIdx, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selectedCategoryId = categoryIdList.get( i );
                categoryLabel.setText( categoryKorList.get( i ) );
            }
        });
//        dlgBuilder.setMultiChoiceItems( arrCategoryKor, null, new DialogInterface.OnMultiChoiceClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
//
//                if( isChecked ) {
//                    selectedCategory.add( categoryKorList.get( which ) );
//                    selectedCategoryId = categoryIdList[which];
//                } else {
//                    selectedCategory.remove( categoryKorList.get( which ) );
//                }
//                String categoryFullText = "";
//                for( String strCategory : selectedCategory ) {
//                    if( ObjectUtils.isEmpty( categoryFullText ) ) {
//                        categoryFullText += strCategory;
//                    } else {
//                        categoryFullText += ", " + strCategory;
//                    }
//                }
//                categoryLabel.setText( categoryFullText );
//            }
//        });
        dlgBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                categoryDlg.dismiss();
            }
        });
        categoryDlg = dlgBuilder.create();
    }


//    private void initReviewPointDlg() {
//        reviewPointDlg = new Dialog( getActivity() );
//        reviewPointDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        reviewPointDlg.setContentView(R.layout.dialog_reviewpoint);
//
//        reviewPointDlg.findViewById( R.id.reviewpoint_close ).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                reviewPointDlg.dismiss();
//            }
//        });
//    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if( isVisibleToUser ) {

            Answers.getInstance().logContentView(new ContentViewEvent()
                    .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                    .putContentType( "Fragment" )
                    .putContentName( "edit_detail" ) );

        }
    }

}
