package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class CustomHonorUserModel extends LinearLayout{

    RequestManager mRequestManager;

    private ImageView honorUserImg;
    private TextView honorUserName;
    private TextView honorUserReviewCount;
    private TextView followBtn;
    private GridLayout honorUserReviewWrapper;

    private long userId;
    private boolean isFollow;

    public CustomHonorUserModel(Context context) {
        super(context);
        initView();
    }

    public CustomHonorUserModel(Context context, RequestManager requestManager) {
        super(context);
        this.mRequestManager = requestManager;
        initView();
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService( infService );
        View v = li.inflate( R.layout.layout_honor_user, this, false );
        addView(v);

        honorUserImg = (ImageView) findViewById( R.id.honor_user_img );
        honorUserName = (TextView) findViewById( R.id.honor_user_name );
        honorUserReviewCount = (TextView) findViewById( R.id.honor_user_review_count );
        followBtn = (TextView) findViewById( R.id.follow_btn );
        honorUserReviewWrapper = (GridLayout) findViewById( R.id.honor_user_review_wrapper );

        isFollow = false;
    }

    public void setHonorUserImg( String resUrl ) {
        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load(resUrl).bitmapTransform( new CropCircleTransformation( getContext() ) ).placeholder( R.drawable.default_user_icon ).into(honorUserImg);
        } else {
            Glide.with( getContext() ).load( resUrl ).bitmapTransform( new CropCircleTransformation( getContext() ) ).placeholder( R.drawable.default_user_icon ).into( honorUserImg );
        }
    }

    public long getUserId() {
        return userId;
    }
    public void setUserId(long userId) {
        this.userId = userId;
    }
    public void setHonorUserName(String resString ) {
        honorUserName.setText( resString );
    }
    public void setHonorUserReviewCount( String resString ) {
        honorUserReviewCount.setText( resString );
    }
    public GridLayout getHonorUserReviewWrapper() {
        return honorUserReviewWrapper;
    }
    public TextView getFollowBtn() {
        return followBtn;
    }
    public boolean isFollow() {
        return isFollow;
    }
    public void setFollow( boolean isFollow ) {
        this.isFollow = isFollow;
        followBtn.setTag( isFollow );

        if( isFollow ) {
            followBtn.setBackgroundResource( R.drawable.border_gray_round_padding );
            followBtn.setTextColor( ContextCompat.getColor( getContext(), R.color.colorGray ) );
            followBtn.setText( "구독중" );
        } else {
            followBtn.setBackgroundResource( R.drawable.gradient_yellow_round );
            followBtn.setTextColor( ContextCompat.getColor( getContext(), R.color.colorWhite ) );
            followBtn.setText( "구독하기" );
        }
    }
}
