package com.jjlee.wedqueen;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;

import com.jjlee.wedqueen.rest.account.controller.AccountController;
import com.jjlee.wedqueen.rest.account.model.ConstantDto;
import com.jjlee.wedqueen.rest.account.model.GoogleOauthResponse;
import com.jjlee.wedqueen.rest.account.model.Login;
import com.jjlee.wedqueen.rest.account.model.LoginCommonResponse;
import com.jjlee.wedqueen.rest.app.model.TabCategory;
import com.jjlee.wedqueen.rest.dday.model.DDayDto;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.MyCookieJar;
import com.jjlee.wedqueen.support.Prefs;

import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;
import com.jjlee.wedqueen.view.SigninButton;
import com.kakao.auth.AuthType;
import com.kakao.auth.ISessionCallback;
import com.kakao.auth.Session;
import com.kakao.usermgmt.UserManagement;
import com.kakao.usermgmt.callback.LogoutResponseCallback;
import com.kakao.util.exception.KakaoException;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "ACCOUNT_ACTIVITY";
    private static final int GOOGLE_SIGNIN = 9055;
    private static final int VIEW_TERMS = 8314;

    private AccountController accountController;
    private Prefs prefs;

    //layout_progress
    private FrameLayout progressBarWrapper;

    private SigninButton mFacebookButton;
    private SigninButton mGoogleButton;
    private SigninButton mTwitterButton;
    private SigninButton mKakaoButton;
    private ImageView tooltip;

    private TextView usageButton;
    private TextView infoButton;

    // KAKAO RELATED FIELDS
    private SessionCallback myKakaoCallback;
    private String actionType;
    private String loginFrom;

    // FACEBOOK
    private LoginManager facebookLoginManager;
    private CallbackManager callbackManager;
    private AppEventsLogger logger;

    // TWITTER
    private TwitterAuthClient twitterAuthClient;

    // GOOGLE
    private GoogleApiClient mGoogleApiClient;
    private boolean shouldLogoutGoogle = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentType( "Activity" )
                .putContentName( "login" ) );

        MyApplication.getGlobalApplicationContext().setCurrentActivity(this);
        accountController = new AccountController();
        prefs = new Prefs(this);

        getAdvertisementId();

        // FACEBOOK initializations
        facebookLoginManager = LoginManager.getInstance();
        callbackManager = CallbackManager.Factory.create();
        facebookLoginManager.registerCallback(callbackManager, new MyFacebookCallback());
        logger = AppEventsLogger.newLogger(this);


        // KAKAO initializations
        myKakaoCallback = new SessionCallback();
        Session.getCurrentSession().addCallback(myKakaoCallback);

        // TWITTER initializations
        twitterAuthClient = new TwitterAuthClient();

        // GOOGLE initializations
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .requestServerAuthCode(this.getString(R.string.google_server_client_id))
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();


        Intent intent = getIntent();
        actionType = intent.getStringExtra(Constants.ACTION_TYPE);
        loginFrom = intent.getStringExtra( Constants.LOGIN_FROM );

        if( ObjectUtils.isEmpty( loginFrom ) ) {
            loginFrom = "isLoggedIn";
        }

        switch(actionType) {
            case Constants.ACTION_TYPE_AUTO_LOGIN:
                setContentView(R.layout.activity_account_blank);
                autoLogin(prefs.getLogin());
                break;
            case Constants.ACTION_TYPE_MANUAL_LOGIN:
                setContentView(R.layout.activity_account);
                initSnsButtons(prefs.getLastLogin());
                initTermButtons();
                progressBarWrapper = (FrameLayout)findViewById(R.id.layout_progress);
                break;
            case Constants.ACTION_TYPE_LOGOUT:
                setContentView(R.layout.activity_account_blank);
                logout(false);
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void animateActivatedButton(final ImageButton imageButton, int durationMultiplier) {
        imageButton.animate().setDuration(200 * durationMultiplier)
                .translationX(SizeConverter.dpToPx(0f))
                .translationY(SizeConverter.dpToPx(-170f))
                .scaleX(1.5f)
                .scaleY(1.5f)
                .alpha(1.0f)
                .start();
    }

    public void animateDeactivatedButton(ImageButton imageButton, float tx, int durationMultiplier) {
        imageButton.animate().setDuration(400 * durationMultiplier)
                .translationX(SizeConverter.dpToPx(tx))
                .translationY(SizeConverter.dpToPx(-30f))
                .scaleX(0.85f)
                .scaleY(0.85f)
                .alpha(0.5f)
                .start();
    }


    public void activateButton(int activatedButton, int durationMultiplier) {
        if( prefs.getLastLogin() == Constants.LOGIN_NULL ) {
            Drawable tooltipDrawable = ContextCompat.getDrawable(this, R.drawable.login_tooltip);
            tooltip.setImageDrawable(tooltipDrawable);
        } else {
            if(prefs.getLastLogin() == activatedButton) {
                Drawable tooltipDrawable = ContextCompat.getDrawable(this, R.drawable.login_tooltip_relogin);
                tooltip.setImageDrawable(tooltipDrawable);
            } else {
                Drawable tooltipDrawable = ContextCompat.getDrawable(this, R.drawable.login_tooltip_newlogin);
                tooltip.setImageDrawable(tooltipDrawable);
            }
        }

        tooltip.setAlpha(0.0f);
        tooltip.setVisibility(View.VISIBLE);
        tooltip.animate().setStartDelay(200 * durationMultiplier).setDuration(400 * durationMultiplier)
                .alpha(1.0f)
                .start();

        switch(activatedButton) {
            case Constants.SNS_TYPE_KAKAO:
                mKakaoButton.setIsActivated(true);
                mFacebookButton.setIsActivated(false);
                mTwitterButton.setIsActivated(false);
                mGoogleButton.setIsActivated(false);
                animateActivatedButton(mKakaoButton, durationMultiplier);
                animateDeactivatedButton(mFacebookButton, -65f, durationMultiplier);
                animateDeactivatedButton(mTwitterButton, 0f, durationMultiplier);
                animateDeactivatedButton(mGoogleButton, 65f, durationMultiplier);
                break;
            case Constants.SNS_TYPE_FACEBOOK:
                mKakaoButton.setIsActivated(false);
                mFacebookButton.setIsActivated(true);
                mTwitterButton.setIsActivated(false);
                mGoogleButton.setIsActivated(false);
                animateDeactivatedButton(mKakaoButton, -65f, durationMultiplier);
                animateActivatedButton(mFacebookButton, durationMultiplier);
                animateDeactivatedButton(mTwitterButton, 0f, durationMultiplier);
                animateDeactivatedButton(mGoogleButton, 65f, durationMultiplier);
                break;
            case Constants.SNS_TYPE_TWITTER:
                mKakaoButton.setIsActivated(false);
                mFacebookButton.setIsActivated(false);
                mTwitterButton.setIsActivated(true);
                mGoogleButton.setIsActivated(false);
                animateDeactivatedButton(mKakaoButton, -65f, durationMultiplier);
                animateDeactivatedButton(mFacebookButton, 0f, durationMultiplier);
                animateActivatedButton(mTwitterButton, durationMultiplier);
                animateDeactivatedButton(mGoogleButton, 65f, durationMultiplier);
                break;

            case Constants.SNS_TYPE_GOOGLE:
                mKakaoButton.setIsActivated(false);
                mFacebookButton.setIsActivated(false);
                mTwitterButton.setIsActivated(false);
                mGoogleButton.setIsActivated(true);
                animateDeactivatedButton(mKakaoButton, -65f, durationMultiplier);
                animateDeactivatedButton(mFacebookButton, 0f, durationMultiplier);
                animateDeactivatedButton(mTwitterButton, 65f, durationMultiplier);
                animateActivatedButton(mGoogleButton, durationMultiplier);
                break;
        }
    }
    public void initSnsButtons(int lastLogin) {

        tooltip = (ImageView) findViewById(R.id.tooltip);

        mFacebookButton = (SigninButton)findViewById(R.id.button_facebook);
        mGoogleButton = (SigninButton)findViewById(R.id.button_google);
        mTwitterButton = (SigninButton)findViewById(R.id.button_twitter);
        mKakaoButton = (SigninButton) findViewById(R.id.button_kakao);

        mFacebookButton.setInitialTranslationX(mFacebookButton.getTranslationX());
        mFacebookButton.setInitialTranslationY(mFacebookButton.getTranslationY());
        mGoogleButton.setInitialTranslationX(mGoogleButton.getTranslationX());
        mGoogleButton.setInitialTranslationY(mGoogleButton.getTranslationY());
        mTwitterButton.setInitialTranslationX(mTwitterButton.getTranslationX());
        mTwitterButton.setInitialTranslationY(mTwitterButton.getTranslationY());
        mKakaoButton.setInitialTranslationX(mKakaoButton.getTranslationX());
        mKakaoButton.setInitialTranslationY(mKakaoButton.getTranslationY());

        switch(lastLogin) {
            case Constants.SNS_TYPE_KAKAO:
                activateButton(Constants.SNS_TYPE_KAKAO, 0);
                break;
            case Constants.SNS_TYPE_FACEBOOK:
                activateButton(Constants.SNS_TYPE_FACEBOOK, 0);
                break;
            case Constants.SNS_TYPE_GOOGLE:
                activateButton(Constants.SNS_TYPE_GOOGLE, 0);
                break;
            case Constants.SNS_TYPE_TWITTER:
                activateButton(Constants.SNS_TYPE_TWITTER, 0);
                break;
            default:
                break;
        }

        mFacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mFacebookButton.isActivated()) {
                    showProgressBar();
                    facebookLogin();
                } else {
                    activateButton(Constants.SNS_TYPE_FACEBOOK, 1);
                }
            }
        });
        mGoogleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mGoogleButton.isActivated()) {
                    showProgressBar();
                    googleLogin();
                } else {
                    activateButton(Constants.SNS_TYPE_GOOGLE, 1);
                }
            }
        });
        mTwitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTwitterButton.isActivated()) {
                    showProgressBar();
                    twitterLogin();
                } else {
                    activateButton(Constants.SNS_TYPE_TWITTER, 1);
                }
            }
        });
        mKakaoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mKakaoButton.isActivated()) {
                    showProgressBar();
                    kakaoLogin();
                } else {
                    activateButton(Constants.SNS_TYPE_KAKAO, 1);
                }
            }
        });
    }

    public void initTermButtons() {
        TextView.OnClickListener ocl = new TextView.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AccountActivity.this, FullscreenActivity.class);
                if(v.getId() == R.id.terms_info ) {
                    intent.putExtra("URL", Constants.INFO_URL);
                } else {
                    intent.putExtra("URL", Constants.USAGE_URL);
                }
                intent.putExtra("NO_TOOL_BAR", true);
                startActivityForResult(intent, VIEW_TERMS);
            }
        };

        usageButton = (TextView)findViewById(R.id.terms_usage);
        infoButton = (TextView)findViewById(R.id.terms_info);
        usageButton.setOnClickListener(ocl);
        infoButton.setOnClickListener(ocl);

    }

    public void showProgressBar() {
        if(actionType.equals(Constants.ACTION_TYPE_MANUAL_LOGIN)) {
            progressBarWrapper.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgressBar() {
        if(actionType.equals(Constants.ACTION_TYPE_MANUAL_LOGIN)) {
            progressBarWrapper.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.getGlobalApplicationContext().setCurrentActivity(null);
        Runtime.getRuntime().gc();
        Session.getCurrentSession().removeCallback(myKakaoCallback);
    }


    private void facebookLogin() {
        kakaoLogout();
        googleLogout();
        twitterLogout();

        if(AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired()) {
            login(Constants.SNS_TYPE_FACEBOOK, AccessToken.getCurrentAccessToken().getToken(), null);
        } else {
            if(actionType.equals(Constants.ACTION_TYPE_AUTO_LOGIN)) { // 자동 로그인 시도라면
                login(Constants.LOGIN_NULL, null, null); // 익명 로그인
            } else { // 수동 로그인 시도라면
                facebookLoginManager.logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
            }
        }
    }
    private void facebookLogout() {
        facebookLoginManager.logOut();
    }

    private void getGoogleAccessToken(String serverAuthCode) {
        String clientId = getString(R.string.google_server_client_id);
        String clientSecret = getString(R.string.google_server_client_secret);
        String redirectUri = "";
        Call<GoogleOauthResponse> call = accountController
                .getGoogleToken("authorization_code", clientId, clientSecret, redirectUri, serverAuthCode);
        call.enqueue(new Callback<GoogleOauthResponse>() {
            @Override
            public void onResponse(Call<GoogleOauthResponse> call, Response<GoogleOauthResponse> response) {
                login(Constants.SNS_TYPE_GOOGLE, response.body().getAccess_token(), null);
            }

            @Override
            public void onFailure(Call<GoogleOauthResponse> call, Throwable t) {
                login(Constants.LOGIN_NULL, null, null); // 익명 로그인
            }
        });
    }

    private void googleLogin() {
        kakaoLogout();
        facebookLogout();
        twitterLogout();

        OptionalPendingResult<GoogleSignInResult> pendingResult =
                Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);

        if(pendingResult.isDone()) {
            GoogleSignInResult gsir = pendingResult.get();
            if(gsir.isSuccess()) {
                getGoogleAccessToken(gsir.getSignInAccount().getServerAuthCode());
            } else {
                if(actionType.equals(Constants.ACTION_TYPE_AUTO_LOGIN)) { // 자동 로그인 시도라면
                    login(Constants.LOGIN_NULL, null, null); // 익명 로그인
                } else { // 수동 로그인 시도라면
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, GOOGLE_SIGNIN);
                }
            }
        } else {
            pendingResult.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult result) {
                if(result.isSuccess()) {
                    getGoogleAccessToken(result.getSignInAccount().getServerAuthCode());
                } else {
                    if(actionType.equals(Constants.ACTION_TYPE_AUTO_LOGIN)) { // 자동 로그인 시도라면
                        login(Constants.LOGIN_NULL, null, null); // 익명 로그인
                    } else { // 수동 로그인 시도라면
                        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                        startActivityForResult(signInIntent, GOOGLE_SIGNIN);
                    }
                }
                }
            });
        }
    }

    private void googleLogout() {
        if(mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
        } else {
            shouldLogoutGoogle = true;
            mGoogleApiClient.connect();
        }

    }

    private void twitterLogin() {
        facebookLogout();
        kakaoLogout();
        googleLogout();

        if(Twitter.getSessionManager().getActiveSession() != null) {
            // twitter auth token never expires.
            TwitterSession ts = Twitter.getSessionManager().getActiveSession();
            String token = ts.getAuthToken().token;
            String secret = ts.getAuthToken().secret;
            login(Constants.SNS_TYPE_TWITTER, token, secret);
        } else {
            twitterAuthClient.authorize(this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
                @Override
                public void success(Result<TwitterSession> result) {
                    login(Constants.SNS_TYPE_TWITTER,
                            result.data.getAuthToken().token, result.data.getAuthToken().secret);
                }

                @Override
                public void failure(TwitterException exception) {
                    if(actionType.equals(Constants.ACTION_TYPE_AUTO_LOGIN)) { // 자동 로그인 시도인데 실패하면
                        login(Constants.LOGIN_NULL, null, null); // 익명 로그인
                    } else {
                        // 아마 유저가 취소한 경우일 확률이 높음. 따라서 아무것도 안함.
                    }
                }
            });
        }
    }
    private void twitterLogout() {
        Twitter.getSessionManager().clearActiveSession();
    }

    // kakao
    private void kakaoLogin() {
        facebookLogout();
        googleLogout();
        twitterLogout();

        if( Session.getCurrentSession().isOpened() ) {
            login(Constants.SNS_TYPE_KAKAO, Session.getCurrentSession().getAccessToken(), null );
        } else if( Session.getCurrentSession().isOpenable() ) {
            Session.getCurrentSession().implicitOpen();
        } else { // closed 라면
            if(actionType.equals(Constants.ACTION_TYPE_AUTO_LOGIN)) { // 자동 로그인 시도인데 실패하면
                login(Constants.LOGIN_NULL, null, null); // 익명 로그인
            } else { // 수동 로그인 시도라면
                Session.getCurrentSession().open(AuthType.KAKAO_TALK, this);
            }
        }
    }

    private void kakaoLogout() {
        UserManagement.requestLogout(new LogoutResponseCallback() {
            @Override
            public void onCompleteLogout() {
                Log.i(TAG, "Kakao successfully logged out.");
            }
        });
        Session.getCurrentSession().close();
    }

    private void anonymousLogin() {
        facebookLogout();
        googleLogout();
        twitterLogout();
        kakaoLogout();
        login(Constants.LOGIN_NULL, null, null);
    }

    public void autoLogin(int snsType) {
        Log.d("snsType", String.valueOf(snsType));
        switch(snsType) {
            case Constants.SNS_TYPE_FACEBOOK:
                facebookLogin();
                break;
            case Constants.SNS_TYPE_KAKAO:
                kakaoLogin();
                break;
            case Constants.SNS_TYPE_TWITTER:
                twitterLogin();
                break;
            case Constants.SNS_TYPE_GOOGLE:
                googleLogin();
                break;
            default:
                anonymousLogin();
                break;
        }
    }

    private void login(final int snsType, String accessToken, String secret) {

        Log.i(TAG, "로그인을 시도합니다.($login)");
        Call<LoginCommonResponse> call = null;

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 12);

        Login login;
        String deviceToken = prefs.getLastDeviceToken(); // 무조건 저장되어있는 디바이스 토큰 사용. 이후에 바로 갱신 한번 함.
        String expireTime = String.valueOf(cal.getTimeInMillis());
        String version = ((MyApplication)getApplication()).getVersionName();
        String advertisingId = prefs.getAdid();

//        Log.e( "WQTEST", "accessToken : " + accessToken );
//        Log.e( "WQTEST", "deviceToken : " + deviceToken );
//        Log.e( "WQTEST", "secret : " + secret );
//        Log.e( "WQTEST", "expireTime : " + expireTime );
//        Log.e( "WQTEST", "version : " + version );
//        Log.e( "WQTEST", "advertisingId : " + advertisingId );

        String registrationMethod = null; // only used for logging
        switch(snsType) {
            case Constants.SNS_TYPE_FACEBOOK:
                login = new Login(accessToken, deviceToken, expireTime, version, advertisingId);
                call = accountController.facebookLoggedIn(login);
                registrationMethod = "facebook";
                break;
            case Constants.SNS_TYPE_KAKAO:
                login = new Login(accessToken, deviceToken, expireTime, version, advertisingId);
                call = accountController.kakaoLoggedIn(login);
                registrationMethod = "kakao";
                break;
            case Constants.SNS_TYPE_TWITTER:
                login = new Login(accessToken, deviceToken, secret, expireTime, version, advertisingId);
                call = accountController.twitterLoggedIn(login);
                registrationMethod = "twitter";
                break;
            case Constants.SNS_TYPE_GOOGLE:
                login = new Login(accessToken, deviceToken, expireTime, version, advertisingId);
                call = accountController.googleLoggedIn(login);
                registrationMethod = "google";
                break;
            case Constants.LOGIN_NULL:
                login = new Login(null, deviceToken, null, version, advertisingId);
                call = accountController.anonymousLoggedIn(login);
                registrationMethod = "not_logged_in";
                break;
        }

//        Log.e( "WQTEST", "login METHOD" );
//        Log.e( "WQTEST", "snsType : " + snsType );

        if( call != null ) {
            final String tempRegMethod = registrationMethod;
            call.enqueue(new Callback<LoginCommonResponse>() {
                @Override
                public void onResponse(Call<LoginCommonResponse> call, Response<LoginCommonResponse> response) {
                    LoginCommonResponse loginResponse = response.body();
                    if (response.code() == 200) {

                        Bundle parameters = new Bundle();
                        parameters.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, tempRegMethod);
                        logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, parameters);

                        setTabCategoriesFromLoginResponseCategories(loginResponse.getTabCategories());
                        MyApplication.getGlobalApplicationContext().logUser(loginResponse.getUserId());
                        MyApplication.getGlobalApplicationContext().setBadgeCount(loginResponse.getNotificationCount());
                        MyApplication.getGlobalApplicationContext().setUserId(loginResponse.getUserId());
                        MyApplication.getGlobalApplicationContext().setUserImageUrl(loginResponse.getUserImageUrl());
                        MyApplication.getGlobalApplicationContext().setUserNickName(loginResponse.getNickname());

                        DDayDto dDayDto = loginResponse.getDday();

                        // 서버에서 DDay 정보를 가져온 경우 String을 
                        if( dDayDto != null ) {
                            prefs.setDdayDate( dDayDto.getdDayDate() );
                            prefs.setCBudget( (int) dDayDto.getcBudget() );
                        } else {
                            prefs.setDdayDate( "" );
                        }

                        if(snsType == Constants.LOGIN_NULL) {
                            MyApplication.getGlobalApplicationContext().setIsLoggedIn(false);

                            Answers.getInstance().logLogin(new LoginEvent()
                                    .putSuccess( false )
                                    .putMethod( loginFrom )
                                    .putCustomAttribute( "loginType", "anonymous" ) );
                        } else {
                            MyApplication.getGlobalApplicationContext().setIsLoggedIn(true);
                            prefs.setLogin(snsType);
                            prefs.setLastLogin(snsType);

                            Answers.getInstance().logLogin(new LoginEvent()
                                    .putSuccess( true )
                                    .putMethod( loginFrom )
                                    .putCustomAttribute( "loginType", tempRegMethod ) );
                        }

                        List<ConstantDto> constants = loginResponse.getConstants();
                        if( !ObjectUtils.isEmpty( constants ) ) {
                            for( ConstantDto constantDto : constants ) {
                                if( !ObjectUtils.isEmpty( constantDto.getKey() )
                                        && !ObjectUtils.isEmpty( constantDto.getValue() )
                                        && constantDto.getKey().equals( "ISLOGIN_CONTENT_DETAIL" ) ) {

                                    if( constantDto.getValue().equals( "Y" ) ) {
                                        MyApplication.getGlobalApplicationContext().setLoginContentDetail( true );
                                    } else if( constantDto.getValue().equals( "N" ) ) {
                                        MyApplication.getGlobalApplicationContext().setLoginContentDetail( false );
                                    }
                                }
                            }
                        }

                        if(actionType.equals(Constants.ACTION_TYPE_MANUAL_LOGIN )
                                || actionType.equals(Constants.ACTION_TYPE_LOGOUT )) {
                            MyApplication.getGlobalApplicationContext().setIsLoginSynchronized(false);
                        } else { // 자동 로그인이라면 첫 로딩시에 다 잘 가져오므로
                            MyApplication.getGlobalApplicationContext().setIsLoginSynchronized(true);
                        }

                        if( !ObjectUtils.isEmpty( loginResponse.getFirstLogin() ) && loginResponse.getFirstLogin().equals( "Y" ) ) {
                            MyApplication.getGlobalApplicationContext().setShowInviteCode( true );
                        }

                        loginSuccess(snsType, loginResponse);

//                        IgawAdbrix.Commerce.login( getApplicationContext(), Base64.encodeToString( loginResponse.getUserId().toString().getBytes(), Base64.DEFAULT ) );

                    } else {
                        setTabCategoriesFromLoginResponseCategories(null);
                        loginFailed(snsType, loginResponse, response.code());
                    }
                    hideProgressBar();
                }

                @Override
                public void onFailure(Call<LoginCommonResponse> call, Throwable t) {
                    t.printStackTrace();
                    setTabCategoriesFromLoginResponseCategories(null);
                    hideProgressBar();
                    loginFailed(snsType, new LoginCommonResponse("네트워크 에러 발생. 로그인 실패."), 400);

                }
            });
        }
    }

    // 실제적인 로그아웃은 sessionCookie만 지우면 됨.
    // 대신 /logout?deviceToken=?? 보다 세션 쿠키를 먼저 지우면 안됨

    private void clearAccountRelatedThings() {
        MyApplication.getGlobalApplicationContext().setIsLoggedIn(false);
        MyApplication.getGlobalApplicationContext().setIsLoginSynchronized(false);
        MyApplication.getGlobalApplicationContext().setUserId( "-1" );

        MyCookieJar.removeSessionCookies();
        // 이거 필요없을수도.
        // /logout(POST) 하면 자동으로 Set-Cookie 로 새로운 JSESSIONID 를 제공함.

        MyApplication.getGlobalApplicationContext().setBadgeCount(0);
        MyApplication.getGlobalApplicationContext().setUserImageUrl(null);
    }

    private void logout(boolean isHappenedDuringAutoLogin) {

        Log.i(TAG, "서버 및 SNS 로그아웃을 시도합니다.($logout)");
        logger.logEvent("유저가 로그아웃함");

        if(!isHappenedDuringAutoLogin) {
            int loginSnsType = prefs.getLogin();
            Call<Void> call = accountController.serverLogout(prefs.getLastDeviceToken());

            switch(loginSnsType) {
                case Constants.SNS_TYPE_FACEBOOK:
                    facebookLogout();
                    break;
                case Constants.SNS_TYPE_KAKAO:
                    kakaoLogout();
                    break;
                case Constants.SNS_TYPE_TWITTER:
                    twitterLogout();
                    break;
                case Constants.SNS_TYPE_GOOGLE:
                    googleLogout();
                    break;
            }
            prefs.setLogin(Constants.LOGIN_NULL);
            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                    clearAccountRelatedThings();
                    returnToCallerActivity();
                    // NOTE: MainActivity onResume 에서 배지 카운트를 업데이트 하므로 추가적인 작업 필요 없음
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    clearAccountRelatedThings();
                    returnToCallerActivity();
                    // NOTE: MainActivity onResume 에서 배지 카운트를 업데이트 하므로 추가적인 작업 필요 없음
                }
            });
        } else {
            clearAccountRelatedThings();
        }
    }


    public void loginSuccess(int snsType, LoginCommonResponse response) {
        switch(actionType) {
            case Constants.ACTION_TYPE_AUTO_LOGIN:
                returnToSplashActivityWithSuccess(snsType);
                break;
            case Constants.ACTION_TYPE_MANUAL_LOGIN:
                returnToCallerActivity();
                break;
        }

    }

    public void loginFailed(int snsType, LoginCommonResponse response, int responseCode) {
        switch(actionType) {
            case Constants.ACTION_TYPE_AUTO_LOGIN:
                returnToSplashActivityWithError(responseCode);
                break;
            case Constants.ACTION_TYPE_MANUAL_LOGIN:
                if(responseCode == 426) {
                    Toast.makeText(this, "최신 버전이 나왔어요! 업데이트 하셔야 로그인이 가능합니다.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "로그인을 실패하였습니다. 잠시 후 다시 시도해주세요! (" + responseCode + ")", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public void returnToSplashActivityWithSuccess(int snsType) {
        if(snsType == Constants.LOGIN_NULL) {
            //Toast.makeText(this, "디버깅 메세지: 익명 로그인. snsType: " + snsType, Toast.LENGTH_SHORT).show();
        }
        setResult(Activity.RESULT_OK);
        finish();
    }

    public void returnToSplashActivityWithError(int responseCode) {
        logout(true);
        setResult(responseCode);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        intent.putExtra(Constants.ACTION_TYPE, actionType);
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }

    public void returnToCallerActivity() {
        Intent intent = new Intent();
        intent.putExtra(Constants.ACTION_TYPE, actionType);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    /**
     * 각 탭들의 카테고리 설정 ( From Server )
     */

    private void setTabCategoriesFromLoginResponseCategories(List<TabCategory> tabCategories) {

        HashMap<String, Boolean> tabCategoriesOpenList = prefs.getTabCategoriesOpenList();

        HashMap<String, ArrayList<TabCategory>> allTabCategories = new HashMap<>();
        if(tabCategories == null || tabCategories.size() == 0 ) {
        } else {
            for(TabCategory tabCategory : tabCategories ) {
                if (tabCategory.getTabType().equals("contentfeed")
                        || tabCategory.getTabType().equals("weddingtalk")) {
                    ArrayList<TabCategory> tabCategoryList = allTabCategories.get(tabCategory.getTabType());
                    if(tabCategoryList == null) {
                        tabCategoryList = new ArrayList<>();
                        allTabCategories.put(tabCategory.getTabType(), tabCategoryList );
                    }

                    // isTipOpen 여부가 prefs에 저장되어있는 카테고리인 경우 set
                    if( tabCategory.getTabType().equals("contentfeed")
                            && !ObjectUtils.isEmpty( tabCategoriesOpenList )
                            && tabCategoriesOpenList.containsKey( tabCategory.getCategoryName() ) ) {
                        tabCategory.setTipOpen( tabCategoriesOpenList.get( tabCategory.getCategoryName() ) );
                    } else {
                        tabCategory.setTipOpen( true );
                    }

                    tabCategoryList.add(tabCategory);
                }
            }
            prefs.setCurrentTabCategories(allTabCategories);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("REQUESTCODE: ", requestCode + "");

        if (Session.getCurrentSession().handleActivityResult(requestCode, resultCode, data)) { // kakao
        } else if( callbackManager.onActivityResult(requestCode, resultCode, data) ) {
        } else if(requestCode == twitterAuthClient.getRequestCode() ) {
            twitterAuthClient.onActivityResult(requestCode, resultCode, data); // twitter
        } else if(requestCode == GOOGLE_SIGNIN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                getGoogleAccessToken(result.getSignInAccount().getServerAuthCode());
            } else {
                if(Constants.ACTION_TYPE_MANUAL_LOGIN.equals(actionType) ) {
                    Toast.makeText(this, "로그인을 실패하였습니다. 잠시 후 다시 시도해주세요!", Toast.LENGTH_SHORT).show();
                }
                hideProgressBar();
            }

        }

        super.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onConnected(Bundle bundle) {
        if(shouldLogoutGoogle) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            shouldLogoutGoogle = false;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    // KAKAO SessionCallback 구현체
    private class SessionCallback implements ISessionCallback {
        @Override
        public void onSessionOpened() {
            login(Constants.SNS_TYPE_KAKAO, Session.getCurrentSession().getAccessToken(), null);
        }

        @Override
        public void onSessionOpenFailed(KakaoException exception) {
            try {
                hideProgressBar();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class MyFacebookCallback implements FacebookCallback<LoginResult> {

        @Override
        public void onSuccess(LoginResult loginResult) {
            String accessToken = loginResult.getAccessToken().getToken();
            login(Constants.SNS_TYPE_FACEBOOK, accessToken, null);
        }

        @Override
        public void onCancel() {
            Log.d("FacebookLoginFailed", "로그인 중 사용자가 취소함.");
            hideProgressBar();
        }

        @Override
        public void onError(FacebookException error) {
            Log.d("FacebookLoginFailed", "실패.");
            hideProgressBar();
            error.printStackTrace();
        }
    }

    private void getAdvertisementId() {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    AdvertisingIdClient.Info adIdInfo = AdvertisingIdClient.getAdvertisingIdInfo( getApplicationContext() );
                    if( !ObjectUtils.isEmpty( adIdInfo ) && !adIdInfo.isLimitAdTrackingEnabled() ) {
                        prefs.setAdid( adIdInfo.getId() );
                    } else {
                        prefs.setAdid( null );
                    }
                } catch ( IOException | GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException e ) {
                    e.printStackTrace();
                }
            }
        });
    }
}
