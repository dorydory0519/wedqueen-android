package com.jjlee.wedqueen.rest.dday.model;

public class PositionDto {

    private Long positionableId;
    private Long currentPosition;
    private Long targetPosition;

    public PositionDto(Long positionableId, Long currentPosition, Long targetPosition) {
        this.positionableId = positionableId;
        this.currentPosition = currentPosition;
        this.targetPosition = targetPosition;
    }

    public Long getPositionableId() {
        return positionableId;
    }
    public void setPositionableId(Long positionableId) {
        this.positionableId = positionableId;
    }
    public Long getTargetPosition() {
        return targetPosition;
    }
    public void setTargetPosition(Long targetPosition) {
        this.targetPosition = targetPosition;
    }
    public Long getCurrentPosition() {
        return currentPosition;
    }
    public void setCurrentPosition(Long currentPosition) {
        this.currentPosition = currentPosition;
    }
}

