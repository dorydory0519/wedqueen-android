package com.jjlee.wedqueen.rest.dday.model;

import com.jjlee.wedqueen.rest.common.model.AppResponse;

public class DDayFormResponse extends AppResponse {

    private DDayForm dday;

    public DDayForm getDday() {
        return dday;
    }

    public void setDday(DDayForm dday) {
        this.dday = dday;
    }

}
