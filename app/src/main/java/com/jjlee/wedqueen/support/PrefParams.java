package com.jjlee.wedqueen.support;

import java.util.Calendar;

/**
 * Created by JJLEE on 2016. 4. 4..
 */
public class PrefParams {
    public static final String PREF_NAME = "JJLEE_PREFS" ;

    public static final String LOGIN = "Login";
    public static final String LAST_LOGIN = "LastLogin";
    public static final String ISLOGIN_CONTENTDETAIL = "isLoginContentDetail";
    public static final String LAST_DEVICE_TOKEN = "LastDeviceToken";
    public static final String LAUNCH_COUNT = "LaunchCount";
    public static final String CURRENT_TAB_CATEGORIES = "CurrentTabCategories";
    public static final String TAB_CATEGORIES_OPENLIST = "TabCategoriesOpenList";
    public static final String COOKIE_DATA = "CookieData";
    public static final String UUID = "UUID";
    public static final String ADID = "ADID";
    public static final String DDAY_DATE = "DdayDate";
    public static final String SHOW_POPUP = "ShowPopup";
    public static final String RESTRICTED_POPUP_LIST = "RestrictedPopupList";
    public static final String SHOW_TUTORIAL = "ShowTutorial";
    public static final String TUTORIAL_IMGURL = "TutorialImgUrl";
    public static final String SHOW_INSTANT_COUNT = "ShowInstantCount";
    public static final String SHOW_HOME_GUIDE_COUNT = "ShowHomeGuideCount";
    public static final String SHOW_WEDDINGTALK_GUIDE_COUNT = "ShowWeddingtalkGuideCount";
    public static final String SHOW_CHECKLIST_GUIDE_COUNT = "ShowChecklistGuideCount";
    public static final String SHOW_POINTSHOP_GUIDE_COUNT = "ShowPointshopGuideCount";
    public static final String SHOW_MYPAGE_GUIDE_COUNT = "ShowMypageGuideCount";
    public static final String USER_ID = "UserId";
    public static final String USER_NICKNAME = "UserNickname";
    public static final String USER_POINT = "UserPoint";
    public static final String USER_REGION = "UserRegion";
}
