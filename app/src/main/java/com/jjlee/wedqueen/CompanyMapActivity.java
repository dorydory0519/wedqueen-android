package com.jjlee.wedqueen;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tsengvn.typekit.TypekitContextWrapper;


public class CompanyMapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private String companyName;
    private double companyLatitude;
    private double companyLongitude;

    private GoogleMap mGoogleMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_companymap);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "company_map" ) );

        companyName = getIntent().getStringExtra( "companyName" );
        companyLatitude = getIntent().getDoubleExtra( "latitude", 0 );
        companyLongitude = getIntent().getDoubleExtra( "longitude", 0 );

        android.app.FragmentManager fragmentManager = getFragmentManager();
        MapFragment mapFragment = (MapFragment)fragmentManager
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;

        setCompanyMarker();

        mGoogleMap.animateCamera( CameraUpdateFactory.zoomTo( 13 ) );
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

//                for( int i = 0; i < detailCompanyWrapper.getChildCount(); i++ ) {
//
//                    if( detailCompanyWrapper.getChildAt( i ) instanceof ImageView ) {
//                        continue;
//                    }
//
//                    LinearLayout premiumModelLayout = (LinearLayout) detailCompanyWrapper.getChildAt( i );
//
//                    // Premium Model Layout의 Tag에 CompanyName을 담아놓았기 때문에 꺼내쓰면됨
//                    String companyName = (String)premiumModelLayout.getTag();
//
//                    if( marker.getTitle().equals( companyName ) ) {
//                        detailcompanyScrollview.smoothScrollTo( 0, premiumModelLayout.getTop() );
//                        marker.showInfoWindow();
//                        break;
//                    }
//                }

                return false;
            }
        });

    }

    private void setCompanyMarker() {

        if( companyLatitude != 0 && companyLongitude != 0 ) {
            LatLng latLng = new LatLng( companyLatitude, companyLongitude );

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title( companyName );
            markerOptions.icon( BitmapDescriptorFactory.fromResource( R.drawable.addr_picker ) );

            mGoogleMap.addMarker( markerOptions );
            mGoogleMap.moveCamera( CameraUpdateFactory.newLatLngZoom( latLng, 14 ) );
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
