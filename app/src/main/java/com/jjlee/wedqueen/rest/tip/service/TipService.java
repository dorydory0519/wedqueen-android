package com.jjlee.wedqueen.rest.tip.service;

import com.jjlee.wedqueen.rest.tip.model.TipResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface TipService {
    @Headers("Accept: application/json")
    @GET("api/v1/tip")
    Call<TipResponse> getTip(@Query("type") String type, @Query("page") int page, @Query("tabCategoryId") String tabCategoryId);

}
