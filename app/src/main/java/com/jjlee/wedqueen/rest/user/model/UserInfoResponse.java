package com.jjlee.wedqueen.rest.user.model;

import com.jjlee.wedqueen.rest.content.model.Content;

import java.util.List;

public class UserInfoResponse {

    private UserRestInfo userInfo;

    public UserRestInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserRestInfo userInfo) {
        this.userInfo = userInfo;
    }
}
