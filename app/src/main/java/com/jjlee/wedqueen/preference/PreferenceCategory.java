package com.jjlee.wedqueen.preference;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class PreferenceCategory extends android.preference.PreferenceCategory
{
	public PreferenceCategory(Context context)
	{
		super(context);
	}

	public PreferenceCategory(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public PreferenceCategory(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	@Override
	protected View onCreateView(ViewGroup parent)
	{
		TextView categoryTitle = (TextView)super.onCreateView(parent);
		categoryTitle.setTextColor(getContext().getResources().getColor(
				android.R.color.secondary_text_dark));

		return categoryTitle;
	}
}