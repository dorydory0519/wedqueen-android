package com.jjlee.wedqueen.rest.dday.model;

public class DDayForm {

    private Gender gender;

    private Region region;

    private String dDay;

    private Integer dDayType;

//    private Integer alarmTime;

    private String exceptions;

    public DDayForm( Gender gender, Region region, String dDay, Integer dDayType ) {
        this.gender = gender;
        this.region = region;
        this.dDay = dDay;
        this.dDayType = dDayType;
//        this.alarmTime = 20;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public String getdDay() {
        return dDay;
    }

    public void setdDay(String dDay) {
        this.dDay = dDay;
    }

    public Integer getdDayType() {
        return dDayType;
    }

    public void setdDayType(Integer dDayType) {
        this.dDayType = dDayType;
    }

//	public Integer getBudget() {
//		return budget;
//	}
//
//	public void setBudget(Integer budget) {
//		this.budget = budget;
//	}
//
//    public Integer getAlarmTime() {
//        return alarmTime;
//    }
//
//    public void setAlarmTime(Integer alarmTime) {
//        this.alarmTime = alarmTime;
//    }

    public String getExceptions() {
        return exceptions;
    }

    public void setExceptions(String exceptions) {
        this.exceptions = exceptions;
    }
}

