package com.jjlee.wedqueen.rest.premium.model;

public class PremiumAdLog {
    private Long premiumAdId;
    private String logType;
    private Long userId;

    public PremiumAdLog(Long premiumAdId, String logType, Long userId) {
        this.premiumAdId = premiumAdId;
        this.logType = logType;
        this.userId = userId;
    }

    public Long getPremiumAdId() {
        return premiumAdId;
    }

    public void setPremiumAdId(Long premiumAdId) {
        this.premiumAdId = premiumAdId;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
