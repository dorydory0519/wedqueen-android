package com.jjlee.wedqueen.rest.account.model;

/**
 * Created by JJLEE on 2016. 7. 8..
 */
public class Anonymous {
    private Long id;
    private String uuid;
    private String deviceToken;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }
}
