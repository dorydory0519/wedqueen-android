package com.jjlee.wedqueen.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.AccountActivity;
import com.jjlee.wedqueen.ContentEditActivity;
import com.jjlee.wedqueen.CustomViews.CustomBannerModel;
import com.jjlee.wedqueen.CustomViews.CustomPaymentModel;
import com.jjlee.wedqueen.CustomViews.CustomPremiumShopModel;
import com.jjlee.wedqueen.CustomViews.CustomReservModel;
import com.jjlee.wedqueen.CustomViews.CustomShopModel;
import com.jjlee.wedqueen.FriendInviteActivity;
import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MainActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.MyPaymentActivity;
import com.jjlee.wedqueen.MyReservActivity;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.invitation.controller.InvitationController;
import com.jjlee.wedqueen.rest.invitation.model.CheckInvitationResponse;
import com.jjlee.wedqueen.rest.payment.controller.PaymentController;
import com.jjlee.wedqueen.rest.payment.model.Payment;
import com.jjlee.wedqueen.rest.payment.model.PaymentResponse;
import com.jjlee.wedqueen.rest.point.controller.PointController;
import com.jjlee.wedqueen.rest.point.model.PointResponse;
import com.jjlee.wedqueen.rest.sector.controller.SectorController;
import com.jjlee.wedqueen.rest.sector.model.Sector;
import com.jjlee.wedqueen.rest.sector.model.SectorItem;
import com.jjlee.wedqueen.rest.sector.model.SectorItemAux;
import com.jjlee.wedqueen.rest.sector.model.SectorItemLog;
import com.jjlee.wedqueen.rest.sector.model.SectorItemLogResponse;
import com.jjlee.wedqueen.rest.sector.model.SectorResponse;
import com.jjlee.wedqueen.rest.shop.controller.ShopController;
import com.jjlee.wedqueen.rest.shop.model.Gift;
import com.jjlee.wedqueen.rest.shop.model.ShopResponse;
import com.jjlee.wedqueen.rest.survey.controller.SurveyController;
import com.jjlee.wedqueen.rest.survey.model.SurveyAnswer;
import com.jjlee.wedqueen.rest.survey.model.SurveyResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.DecimalUtils;
import com.jjlee.wedqueen.utils.ObjectUtils;

import java.text.DecimalFormat;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PointShopFragment extends Fragment {

    private final DecimalFormat decimalFormat = new DecimalFormat( "###,###" );

    private TextView myPoint;
    private TextView startLogin;
    private TextView enterFriendInvitation;

    private TextView pointLabel;
    private LinearLayout pointWrapper;
    private TextView pointHistoryBtn;
    private TextView couponListBtn;
    private TextView moreReservBtn;
    private TextView morePaymentBtn;

    private TextView premiumLabel;

    private CustomBannerModel inviteBannerWrapper;
    private LinearLayout reservWrapper;
    private LinearLayout paymentWrapper;
    private LinearLayout shopPremiumWrapper;
    private LinearLayout shopWrapper;

    private TextView filterSort;
    private String orderType;

    private SwipeRefreshLayout swipePointshop;

    private InvitationController invitationController;
    private PointController pointController;
    private SurveyController surveyController;
    private PaymentController paymentController;
    private ShopController shopController;
    private SectorController sectorController;

    private Dialog filterSortDlg;
    private Dialog pointHelpDlg;
    private Dialog shopDetailDlg;

    public PointShopFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pointshop, container, false);


        pointLabel = (TextView)view.findViewById( R.id.point_label );
        pointWrapper = (LinearLayout)view.findViewById( R.id.point_wrapper );
        pointHistoryBtn = (TextView)view.findViewById( R.id.point_history_btn );
        couponListBtn = (TextView)view.findViewById( R.id.coupon_list_btn );
        moreReservBtn = (TextView) view.findViewById( R.id.more_reserv_btn );
        morePaymentBtn = (TextView) view.findViewById( R.id.more_payment_btn );

        filterSort = view.findViewById( R.id.filter_sort );

        orderType = "";


        initFilterSortDlg();
        initPointHelpDlg();
        initShopDetailDlg();

        pointHistoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFullscreenActivity( Constants.POINTHISTORY_URL );
            }
        });
        couponListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( getActivity(), "기능 준비중입니다 ^^", Toast.LENGTH_SHORT).show();
            }
        });
        moreReservBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(), MyReservActivity.class );
                getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_MYRESERV_ACTIVITY );
            }
        });
        morePaymentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(), MyPaymentActivity.class );
                getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_MYPAYMENT_ACTIVITY );
            }
        });
        view.findViewById( R.id.invite_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFriendInviteActivity();
            }
        });
//        ((TextView)view.findViewById( R.id.invite_point )).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startFriendInviteActivity();
//            }
//        });
        filterSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterSortDlg.show();
            }
        });


        invitationController = new InvitationController();
        pointController = new PointController();
        surveyController = new SurveyController();
        paymentController = new PaymentController();
        shopController = new ShopController();
        sectorController = new SectorController();


        myPoint = (TextView)view.findViewById( R.id.my_point );
        startLogin = (TextView)view.findViewById( R.id.start_login );
        startLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(), AccountActivity.class );
                intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
                intent.putExtra( Constants.LOGIN_FROM, "pointshop_btn_point" );
                getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
            }
        });
        enterFriendInvitation = (TextView) view.findViewById( R.id.enter_friend_invitation );
        enterFriendInvitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).initInviteCodeDlg();
            }
        });
        premiumLabel = (TextView) view.findViewById( R.id.premium_label );
        inviteBannerWrapper = (CustomBannerModel)view.findViewById( R.id.invite_banner_wrapper );
        reservWrapper = (LinearLayout)view.findViewById( R.id.reserv_wrapper );
        paymentWrapper = (LinearLayout)view.findViewById( R.id.payment_wrapper );
        shopPremiumWrapper = (LinearLayout)view.findViewById( R.id.shop_premium_wrapper );
        shopWrapper = (LinearLayout)view.findViewById( R.id.shop_wrapper );


        // 서버에서 데이터 불러오기
        refresh();

        // refresh ACTION
        swipePointshop = (SwipeRefreshLayout)view.findViewById( R.id.swipe_pointshop );
        swipePointshop.setColorSchemeResources(
                R.color.colorPrimaryDark, R.color.colorAccent,
                R.color.colorPrimaryDark, R.color.colorAccent);
        swipePointshop.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
                swipePointshop.setRefreshing( false );
            }
        });

        return view;
    }


    private void postSectorItemLogToServer( long sectorItemId ) {

        String userId = MyApplication.getGlobalApplicationContext().getUserId();
        if( ObjectUtils.isEmpty( userId ) ) {
            userId = "-1";
        }

        SectorItemLog sectorItemLog = new SectorItemLog( sectorItemId, "click", Long.parseLong( userId ) );
        Call<SectorItemLogResponse> sectorCall = sectorController.postSectorItemLog( sectorItemLog );
        sectorCall.enqueue(new Callback<SectorItemLogResponse>() {
            @Override
            public void onResponse(Call<SectorItemLogResponse> call, Response<SectorItemLogResponse> response) {
            }

            @Override
            public void onFailure(Call<SectorItemLogResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private void initFilterSortDlg() {
        filterSortDlg = new Dialog( getActivity() );
        filterSortDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        filterSortDlg.setContentView(R.layout.dialog_filter_sort);

        filterSortDlg.findViewById( R.id.sort_view ).setVisibility( View.GONE );

        TextView sortRecent = (TextView) filterSortDlg.findViewById( R.id.sort_recent );
        TextView sortPopular = (TextView) filterSortDlg.findViewById( R.id.sort_popular );
        TextView sortLowPrice = (TextView) filterSortDlg.findViewById( R.id.sort_lowprice );
        TextView sortHighPrice = (TextView) filterSortDlg.findViewById( R.id.sort_highprice );

        sortRecent.setText( "최신등록순" );
        sortLowPrice.setVisibility( View.VISIBLE );
        sortHighPrice.setVisibility( View.VISIBLE );

        sortRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderType = "recent";
                filterSort.setText( "최신등록순" );
                selectSort();
                getShopDataFromServer();
                filterSortDlg.dismiss();
            }
        });
        sortPopular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderType = "";
                filterSort.setText( "인기순" );
                selectSort();
                getShopDataFromServer();
                filterSortDlg.dismiss();
            }
        });
        sortLowPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderType = "low-price";
                filterSort.setText( "낮은가격순" );
                selectSort();
                getShopDataFromServer();
                filterSortDlg.dismiss();
            }
        });
        sortHighPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderType = "high-price";
                filterSort.setText( "높은가격순" );
                selectSort();
                getShopDataFromServer();
                filterSortDlg.dismiss();
            }
        });
    }

    private void selectSort() {

        TextView sortRecent = (TextView) filterSortDlg.findViewById( R.id.sort_recent );
        TextView sortPopular = (TextView) filterSortDlg.findViewById( R.id.sort_popular );
        TextView sortLowPrice = (TextView) filterSortDlg.findViewById( R.id.sort_lowprice );
        TextView sortHighPrice = (TextView) filterSortDlg.findViewById( R.id.sort_highprice );

        if( orderType.equals( "recent" ) ) {
            sortRecent.setTextColor( getResources().getColor( R.color.colorAccent ) );
            sortPopular.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortLowPrice.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortHighPrice.setTextColor( Color.parseColor( "#CCCCCC" ) );
        } else if( orderType.equals( "" ) ) {
            sortRecent.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortPopular.setTextColor( getResources().getColor( R.color.colorAccent ) );
            sortLowPrice.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortHighPrice.setTextColor( Color.parseColor( "#CCCCCC" ) );
        } else if( orderType.equals( "low-price" ) ) {
            sortRecent.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortPopular.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortLowPrice.setTextColor( getResources().getColor( R.color.colorAccent ) );
            sortHighPrice.setTextColor( Color.parseColor( "#CCCCCC" ) );
        } else if( orderType.equals( "high-price" ) ) {
            sortRecent.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortPopular.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortLowPrice.setTextColor( Color.parseColor( "#CCCCCC" ) );
            sortHighPrice.setTextColor( getResources().getColor( R.color.colorAccent ) );
        }
    }


    private void initPointHelpDlg() {
        pointHelpDlg = new Dialog( getActivity() );
        pointHelpDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pointHelpDlg.setContentView(R.layout.dialog_pointhelp);

        pointHelpDlg.findViewById( R.id.pointhelp_close ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pointHelpDlg.dismiss();
            }
        });
    }

    private void showPointHelpDlg( String type, String surveyReviewParam, String strContractPoint, String strReviewPoint ) {
        if( !ObjectUtils.isEmpty( type ) ) {

            TextView pointHelpLabel = (TextView)pointHelpDlg.findViewById( R.id.pointhelp_label );
            LinearLayout pointHelpTextWrapper = (LinearLayout)pointHelpDlg.findViewById( R.id.pointhelp_text_wrapper );
            pointHelpTextWrapper.removeAllViews();

            TextView pointStartBtn = (TextView)pointHelpDlg.findViewById( R.id.point_start_btn );

            TextView helpTextView1 = new TextView( getActivity() );
            helpTextView1.setPadding( 0, 10, 0, 10 );
            helpTextView1.setLineSpacing( 0, 1.2f );

            TextView helpTextView2 = new TextView( getActivity() );
            helpTextView2.setPadding( 0, 10, 0, 10 );
            helpTextView2.setLineSpacing( 0, 1.2f );

            if( type.equals( "visit" ) ) {
                pointHelpLabel.setText( "방문 포인트" );

                helpTextView1.setText( "∙ 예약한 업체에서 방문여부 체크 후 포인트가 적립됩니다." );
                pointHelpTextWrapper.addView( helpTextView1 );

                helpTextView2.setText( "∙ 방문한지 3일 이상이 지났는데도 계속 포인트가 적립되지 않는다면 아래의 버튼을 클릭하여 카카오톡 플러스 친구 '웨딩의 여신'으로 말씀해주세요!" );
                pointHelpTextWrapper.addView( helpTextView2 );

                pointStartBtn.setText( "포인트 문의하러 가기" );
                pointStartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startFullscreenActivity( "http://pf.kakao.com/_exjFpl" );
                    }
                });

            } else if( type.equals( "contract" ) ) {
                pointHelpLabel.setText( "계약 포인트" );

                helpTextView1.setText( "∙ 간편예약을 통해 방문 후 계약시 " + strContractPoint + "를 적립해드립니다." );
                pointHelpTextWrapper.addView( helpTextView1 );

                helpTextView2.setText( "∙ 계약 후 아래의 버튼을 클릭하여 카카오톡 플러스 친구 ‘웨딩의 여신’으로 계약서 사진을 보내주세요!" );
                pointHelpTextWrapper.addView( helpTextView2 );

                pointStartBtn.setText( "계약서 보내러 가기" );
                pointStartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startFullscreenActivity( "http://pf.kakao.com/_exjFpl" );
                    }
                });

            } else if( type.equals( "review" ) ) {
                pointHelpLabel.setText( "후기 포인트" );

                helpTextView1.setText( "∙ 지금 바로 아래의 버튼을 눌러 후기를 작성해주세요." );
                pointHelpTextWrapper.addView( helpTextView1 );

                helpTextView2.setText( "∙ " + strReviewPoint + "를 적립해드립니다!" );
                pointHelpTextWrapper.addView( helpTextView2 );

                pointStartBtn.setText( "후기쓰러 가기" );
                pointStartBtn.setTag( surveyReviewParam );
                pointStartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pointHelpDlg.dismiss();
                        Intent intent = new Intent( getActivity(), ContentEditActivity.class );
                        intent.putExtra( "surveyReviewParam", v.getTag() + "" );
                        getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_CONTENTEDIT_ACTIVITY );
                    }
                });
            }

            pointHelpDlg.show();
        }
    }

    private void showPaymentHelpDlg( String type, String paymentReviewParam ) {
        if( !ObjectUtils.isEmpty( type ) ) {
            TextView pointHelpLabel = (TextView)pointHelpDlg.findViewById( R.id.pointhelp_label );
            LinearLayout pointHelpTextWrapper = (LinearLayout)pointHelpDlg.findViewById( R.id.pointhelp_text_wrapper );
            pointHelpTextWrapper.removeAllViews();

            TextView pointStartBtn = (TextView)pointHelpDlg.findViewById( R.id.point_start_btn );

            TextView helpTextView1 = new TextView( getActivity() );
            helpTextView1.setPadding( 0, 10, 0, 10 );
            helpTextView1.setLineSpacing( 0, 1.2f );

            TextView helpTextView2 = new TextView( getActivity() );
            helpTextView2.setPadding( 0, 10, 0, 10 );
            helpTextView2.setLineSpacing( 0, 1.2f );

            if( type.equals( "contract" ) ) {
                pointHelpLabel.setText( "결제 포인트" );

                helpTextView1.setText( "∙ 상품 결제시 포인트가 자동으로 적립됩니다." );
                pointHelpTextWrapper.addView( helpTextView1 );

                helpTextView2.setText( "∙ 포인트 적립에 오류가 있을 경우 아래의 버튼을 클릭하여 말씀해주세요." );
                pointHelpTextWrapper.addView( helpTextView2 );

                pointStartBtn.setText( "'웨딩의 여신' 플러스 친구와 대화하기" );
                pointStartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startFullscreenActivity( "http://pf.kakao.com/_exjFpl" );
                    }
                });

            } else if( type.equals( "review" ) ) {
                pointHelpLabel.setText( "후기 포인트" );

                helpTextView1.setText( "∙ 지금 바로 아래의 버튼을 눌러 후기를 작성해주세요." );
                pointHelpTextWrapper.addView( helpTextView1 );

                helpTextView2.setText( "∙ 1,000P를 적립해드립니다!" );
                pointHelpTextWrapper.addView( helpTextView2 );

                pointStartBtn.setText( "후기쓰러 가기" );
                pointStartBtn.setTag( paymentReviewParam );
                pointStartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pointHelpDlg.dismiss();
                        Intent intent = new Intent( getActivity(), ContentEditActivity.class );
                        intent.putExtra( "paymentReviewParam", v.getTag() + "" );
                        getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_CONTENTEDIT_ACTIVITY );
                    }
                });
            }
            pointHelpDlg.show();
        }
    }


    private void initShopDetailDlg() {
        shopDetailDlg = new Dialog( getActivity() );
        shopDetailDlg.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shopDetailDlg.setContentView(R.layout.dialog_shopdetail);

        shopDetailDlg.findViewById( R.id.shopdetail_close ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shopDetailDlg.dismiss();
            }
        });
    }


    private void showShopDetailDlg( String imgUrl, String brandName, String name, String desc ) {
        Glide.with( this ).load( imgUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( (ImageView)shopDetailDlg.findViewById( R.id.shopdetail_img ) );
        ((TextView)shopDetailDlg.findViewById( R.id.shopdetail_brandname )).setText( brandName );
        ((TextView)shopDetailDlg.findViewById( R.id.shopdetail_name )).setText( name );
        ((TextView)shopDetailDlg.findViewById( R.id.shopdetail_desc )).setText( desc + "\n" );

        shopDetailDlg.show();
    }


    private void refresh() {

        refreshMyData();
        getShopPremiumDataFromServer();
        getShopDataFromServer();

    }


    public void refreshMyData() {
        checkFriendInvitation();
        getInviteSectorFromServer();
        getMyPointFromServer();
        getReservDataFromServer();
        getPaymentDataFromServer();
    }

    private void checkFriendInvitation() {
        Call<CheckInvitationResponse> invitationCall = invitationController.checkInvitation();
        invitationCall.enqueue(new Callback<CheckInvitationResponse>() {
            @Override
            public void onResponse(Call<CheckInvitationResponse> call, Response<CheckInvitationResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    CheckInvitationResponse checkInvitationResponse = response.body();

                    boolean invited = checkInvitationResponse.isInvited();
                    // 친구초대코드를 입력한 적이 없는 경우 친구초대코드를 입력할 수 있는 Dialog를 띄워 줄 수 있도록 해당 버튼 ( enter_friend_invitation ) 활성화
                    if( !invited ) {
                        enterFriendInvitation.setVisibility( View.VISIBLE );
                    } else {
                        enterFriendInvitation.setVisibility( View.GONE );
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckInvitationResponse> call, Throwable t) {
                t.printStackTrace();
                enterFriendInvitation.setVisibility( View.GONE );
            }
        });
    }

    private void getInviteSectorFromServer() {

        inviteBannerWrapper.removeAllViews();

        Call<SectorResponse> inviteSectorCall = sectorController.getInviteSectors();
        inviteSectorCall.enqueue(new Callback<SectorResponse>() {
            @Override
            public void onResponse(Call<SectorResponse> call, Response<SectorResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    SectorResponse sectorResponse = response.body();

                    if( !ObjectUtils.isEmpty( sectorResponse.getSectors() ) && sectorResponse.getSectors().size() > 0 ) {
                        List<Sector> sectors = sectorResponse.getSectors();

                        for( Sector sector : sectors ) {
                            List<SectorItem> sectorItems = sector.getItems();

                            for( SectorItem sectorItem : sectorItems ) {
                                if( sectorItem.getType().equals( "invite" ) && !ObjectUtils.isEmpty( getActivity() ) ) {
                                    ImageView bannerImg = new ImageView( getActivity() );
                                    bannerImg.setPadding( 18, 18, 18, 18 );
                                    bannerImg.setAdjustViewBounds( true );
                                    Glide.with( getActivity() ).load( sectorItem.getImageUrl() ).placeholder( R.color.image_placeholder_color ).into( bannerImg );

                                    inviteBannerWrapper.setSectorItemId( sectorItem.getId() );
                                    inviteBannerWrapper.addView( bannerImg );
                                    inviteBannerWrapper.setTag( sectorItem.getLinkUrl() );
                                    inviteBannerWrapper.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            CustomBannerModel targetModel = (CustomBannerModel) v;
                                            postSectorItemLogToServer( targetModel.getSectorItemId() );
                                            startFullscreenActivity( v.getTag() + "" );
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SectorResponse> call, Throwable t) {

            }
        });
    }

    private void getMyPointFromServer() {

        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            Call<PointResponse> pointCall = pointController.getMyPoint();
            pointCall.enqueue(new Callback<PointResponse>() {
                @Override
                public void onResponse(Call<PointResponse> call, Response<PointResponse> response) {
                    if( !ObjectUtils.isEmpty( response.body() ) ) {
                        PointResponse pointResponse = response.body();
                        myPoint.setText( DecimalUtils.convertDecimalFormat( pointResponse.getPoint() ) );
                        pointLabel.setVisibility( View.VISIBLE );
                        pointWrapper.setVisibility( View.VISIBLE );
                        startLogin.setVisibility( View.GONE );
                    } else {
                        pointLabel.setVisibility( View.GONE );
                        pointWrapper.setVisibility( View.GONE );
                        startLogin.setVisibility( View.VISIBLE );
                    }
                }

                @Override
                public void onFailure(Call<PointResponse> call, Throwable t) {
                    pointLabel.setVisibility( View.GONE );
                    pointWrapper.setVisibility( View.GONE );
                    startLogin.setVisibility( View.VISIBLE );
                }
            });
        } else {
            pointLabel.setVisibility( View.GONE );
            pointWrapper.setVisibility( View.GONE );
            startLogin.setVisibility( View.VISIBLE );
        }
    }


    private void getReservDataFromServer() {

        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            Call<SurveyResponse> surveyCall = surveyController.getSurveyAnswer();
            surveyCall.enqueue(new Callback<SurveyResponse>() {
                @Override
                public void onResponse(Call<SurveyResponse> call, Response<SurveyResponse> response) {
                    if( !ObjectUtils.isEmpty( response.body() ) ) {
                        SurveyResponse surveyResponse = response.body();

                        if( surveyResponse.getResultCode() == 0 && !ObjectUtils.isEmpty( surveyResponse.getSurveyAnswers() ) && surveyResponse.getSurveyAnswers().size() > 0 ) {
                            List<SurveyAnswer> surveyAnswers = surveyResponse.getSurveyAnswers();

                            for( SurveyAnswer surveyAnswer : surveyAnswers ) {
                                appendReservModel( surveyAnswer, true );
                            }
                        } else {
                            appendReservModel( null, false );
                        }
                    } else {
                        appendReservModel( null, false );
                    }
                }

                @Override
                public void onFailure(Call<SurveyResponse> call, Throwable t) {
                    appendReservModel( null, false );
                }
            });
        } else {
            appendReservModel( null, false );
        }
    }

    private void getPaymentDataFromServer() {

        paymentWrapper.removeAllViews();

        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {

            Call<PaymentResponse> paymentCall = paymentController.getPayment();
            paymentCall.enqueue(new Callback<PaymentResponse>() {
                @Override
                public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                    if( !ObjectUtils.isEmpty( response.body() ) ) {
                        PaymentResponse paymentResponse = response.body();

                        if( !ObjectUtils.isEmpty( paymentResponse.getPayments() ) && paymentResponse.getPayments().size() > 0 ) {
                            List<Payment> payments = paymentResponse.getPayments();

                            for( Payment payment : payments ) {
                                appendPaymentModel( payment, true );
                            }
                        } else {
                            appendPaymentModel( null, false );
                        }
                    } else {
                        appendPaymentModel( null, false );
                    }
                }

                @Override
                public void onFailure(Call<PaymentResponse> call, Throwable t) {
                    appendPaymentModel( null, false );
                }
            });
        } else {
            appendPaymentModel( null, false );
        }

    }

    private void getShopPremiumDataFromServer() {

        shopPremiumWrapper.removeAllViews();

        Call<SectorResponse> sectorCall = shopController.getPremiumGifts();
        sectorCall.enqueue(new Callback<SectorResponse>() {
            @Override
            public void onResponse(Call<SectorResponse> call, Response<SectorResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    SectorResponse sectorResponse = response.body();

                    if (!ObjectUtils.isEmpty(sectorResponse.getSectors()) && sectorResponse.getSectors().size() > 0) {
                        List<Sector> sectors = sectorResponse.getSectors();

                        for( Sector sector : sectors ) {
                            List<SectorItem> sectorItems = sector.getItems();

                            premiumLabel.setText( sector.getTitle() );

                            for( SectorItem sectorItem : sectorItems ) {
                                if( sectorItem.getType().equals( "point" ) && !ObjectUtils.isEmpty( getActivity() ) ) {
                                    CustomPremiumShopModel customPremiumShopModel = new CustomPremiumShopModel( getActivity() );
                                    customPremiumShopModel.setSectorItemId( sectorItem.getId() );
                                    customPremiumShopModel.setPremiumShopImg( sectorItem.getImageUrl() );
                                    customPremiumShopModel.setPremiumShopName( sectorItem.getTitle() );
                                    String strPoint = "";
                                    List<SectorItemAux> itemAuxes = sectorItem.getItemAux();
                                    for( SectorItemAux itemAux : itemAuxes ) {
                                        if( itemAux.getAuxKey().equals( "point" ) ) {
                                            strPoint = DecimalUtils.convertDecimalFormat( itemAux.getAuxValue() ) + "P ";
                                        } else if( itemAux.getAuxKey().equals( "brandName" ) ) {
                                            customPremiumShopModel.setGiftBrandName( itemAux.getAuxValue() );
                                        } else if( itemAux.getAuxKey().equals( "description" ) ) {
                                            customPremiumShopModel.setGiftDesc( itemAux.getAuxValue() );
                                        }
                                    }
                                    customPremiumShopModel.setPremiumShopPoint( strPoint );
                                    customPremiumShopModel.setTag( sectorItem.getLinkUrl() );
                                    customPremiumShopModel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            CustomPremiumShopModel targetModel = (CustomPremiumShopModel) v;
                                            postSectorItemLogToServer( targetModel.getSectorItemId() );
                                            showShopDetailDlg( targetModel.getGiftImgUrl(), targetModel.getGiftBrandName(), targetModel.getGiftName(), targetModel.getGiftDesc() );
//                                            startFullscreenActivity( "" + v.getTag() );
                                        }
                                    });

                                    shopPremiumWrapper.addView( customPremiumShopModel );
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SectorResponse> call, Throwable t) {

            }
        });
    }

    private void getShopDataFromServer() {

        Call<ShopResponse> shopCall = shopController.getGifts( orderType );
        shopCall.enqueue(new Callback<ShopResponse>() {
            @Override
            public void onResponse(Call<ShopResponse> call, Response<ShopResponse> response) {

                shopWrapper.removeAllViews();

                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    ShopResponse shopResponse = response.body();

                    if (!ObjectUtils.isEmpty(shopResponse.getGifts()) && shopResponse.getGifts().size() > 0) {
                        List<Gift> gifts = shopResponse.getGifts();

                        for( Gift gift : gifts ) {
                            if( !ObjectUtils.isEmpty( getActivity() ) ) {
                                CustomShopModel customShopModel = new CustomShopModel( getActivity() );
                                customShopModel.setShopId( gift.getId() );
                                customShopModel.setShopImg( gift.getImage() );
                                customShopModel.setShopName( gift.getTitle() );
                                customShopModel.setShopPoint( DecimalUtils.convertDecimalFormat( gift.getPoint() ) + "P" );
                                customShopModel.setShopCompanyName( gift.getBrandName() );
                                customShopModel.setGiftDesc( gift.getDescription() );
                                customShopModel.getShopBuyBtn().setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startFullscreenActivity(
                                                Constants.POINTPAYMENT_URL +
                                                        ((CustomShopModel)v.getParent().getParent().getParent().getParent()).getShopId() +
                                                        "?amount=" +
                                                        ((CustomShopModel)v.getParent().getParent().getParent().getParent()).getShopCount() );
                                    }
                                });
                                customShopModel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        CustomShopModel targetModel = (CustomShopModel) v;
                                        showShopDetailDlg( targetModel.getGiftImgUrl(), targetModel.getGiftBrandName(), targetModel.getGiftName(), targetModel.getGiftDesc() );
                                    }
                                });
                                shopWrapper.addView( customShopModel );
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ShopResponse> call, Throwable t) {
                shopWrapper.removeAllViews();
            }
        });
    }


    private void appendReservModel( SurveyAnswer surveyAnswer, boolean reservOn ) {

        if( !ObjectUtils.isEmpty( getActivity() ) ) {

            reservWrapper.removeAllViews();

            if( reservOn ) {
                CustomReservModel customReservModel = new CustomReservModel( getActivity() );
                customReservModel.setReservOnOff( true );
                customReservModel.setReservCompanyThumb( surveyAnswer.getCompanyImageUrl() );

                customReservModel.setReservCompanyName( surveyAnswer.getCompanyTitle() );
                customReservModel.setReservTime( surveyAnswer.getReservationTime() );
                customReservModel.setReviewParam( surveyAnswer.getId(), surveyAnswer.getCompanyTitle(), surveyAnswer.getReservationTime() );
                customReservModel.setReservStatus( surveyAnswer.getStatus() );

                customReservModel.setVisit( surveyAnswer.isVisit(), surveyAnswer.getVisitPoint() );
                if( !surveyAnswer.isVisit() ) {
                    customReservModel.getVisitSection().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showPointHelpDlg( "visit"
                                    , ""
                                    , ""
                                    , "" );
                        }
                    });
                }

                customReservModel.setContract( surveyAnswer.isContract(), surveyAnswer.getContractPoint() );
                if( !surveyAnswer.isContract() ) {
                    customReservModel.getContractSection().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showPointHelpDlg( "contract"
                                    , ""
                                    , ((TextView)v.findViewById( R.id.contract_point )).getText().toString()
                                    , "" );
                        }
                    });
                }

                customReservModel.setReview( surveyAnswer.isReview(), surveyAnswer.getReviewPoint() );
                if( !surveyAnswer.isReview() ) {
                    customReservModel.getReviewSection().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showPointHelpDlg( "review"
                                    , v.getTag() + ""
                                    , ""
                                    , ((TextView)v.findViewById( R.id.review_point )).getText().toString() );
                        }
                    });
                }

                // 서버에서 랜딩될 url을 보내준 이후에 주석 제거
                customReservModel.setTag( surveyAnswer.getLinkUrl() );
                customReservModel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startFullscreenActivity( v.getTag() + "" );
                    }
                });

                reservWrapper.addView( customReservModel );
                moreReservBtn.setVisibility( View.VISIBLE );
            } else {
                CustomReservModel customReservModel = new CustomReservModel( getActivity() );
                customReservModel.setReservOnOff( false );
                customReservModel.getStartDetailCom().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startFullscreenActivity( Constants.BASE_URL + "content/9509" );
//                        Intent intent = new Intent( getActivity(), DetailCompanyActivity.class );
//                        intent.putExtra( "categoryName", "" );
//                        getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_DETAILCOMPANY_ACTIVITY );
                    }
                });
                reservWrapper.addView( customReservModel );
                moreReservBtn.setVisibility( View.GONE );
            }
        }
    }


    private void appendPaymentModel( Payment payment, boolean paymentOn ) {
        if( !ObjectUtils.isEmpty( getActivity() ) ) {

            if( paymentOn ) {
                CustomPaymentModel customPaymentModel = new CustomPaymentModel( getActivity() );
                customPaymentModel.setPaymentOnOff( true );
                customPaymentModel.setPaymentCompanyThumb( payment.getCompanyThumbnailUrl() );
                customPaymentModel.setPaymentCompanyName( payment.getCompanyName() + " | " + payment.getProductName() );
                customPaymentModel.setPaymentAmount( payment.getAmount() );
                customPaymentModel.setPaymentTime( payment.getUpdationTime() );
                customPaymentModel.setPaymentStatus( payment.getStatus() );
//                customPaymentModel.setPaymentStatusColorCode( payment.getStatusColorCode() );
                customPaymentModel.setReviewParam( payment.getId() );

                customPaymentModel.setContract( payment.isContract(), payment.getContractPoint() );
                if( !payment.isContract() ) {
                    customPaymentModel.getContractSection().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showPaymentHelpDlg( "contract", "" );
                        }
                    });
                }

                customPaymentModel.setReview( payment.isReview(), payment.getReviewPoint() );
                if( !payment.isReview() ) {
                    customPaymentModel.getReviewSection().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showPaymentHelpDlg( "review", v.getTag() + "" );
                        }
                    });
                }

                customPaymentModel.setTag( payment.getLinkUrl() );
                customPaymentModel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startFullscreenActivity( v.getTag() + "" );
                    }
                });

                paymentWrapper.addView( customPaymentModel );
                morePaymentBtn.setVisibility( View.VISIBLE );
            } else {

                CustomPaymentModel customPaymentModel = new CustomPaymentModel( getActivity() );
                customPaymentModel.setPaymentOnOff( false );
                paymentWrapper.addView( customPaymentModel );
                morePaymentBtn.setVisibility( View.GONE );
            }
        }
    }


    private void startFriendInviteActivity() {

        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            Intent intent = new Intent( getActivity(), FriendInviteActivity.class );
            startActivity( intent );
        } else {
            Intent intent = new Intent( getActivity(), AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            intent.putExtra( Constants.LOGIN_FROM, "pointshop_btn_invite_friend" );
            getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
        }
    }


    private void startFullscreenActivity(String url) {
        if( MyApplication.getGlobalApplicationContext().canLoadDetailContent() ) {
            Intent intent = new Intent(getActivity(), FullscreenActivity.class);
            intent.putExtra("URL", url);
            getActivity().startActivityForResult(intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY);
        } else {
            Intent intent = new Intent( getActivity(), AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if( isVisibleToUser ) {

            Answers.getInstance().logContentView(new ContentViewEvent()
                    .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                    .putContentType( "Fragment" )
                    .putContentName( "point_tab" ) );

        }
    }
}
