package com.jjlee.wedqueen.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.text.style.UnderlineSpan;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.h6ah4i.android.widget.advrecyclerview.animator.DraggableItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.h6ah4i.android.widget.advrecyclerview.draggable.RecyclerViewDragDropManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.WrapperAdapterUtils;
import com.jjlee.wedqueen.AccountActivity;
import com.jjlee.wedqueen.CustomViews.CustomCheckCategory;
import com.jjlee.wedqueen.CustomViews.CustomCheckedTextView;
import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MainActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.common.model.AppResponse;
import com.jjlee.wedqueen.rest.dday.controller.DDayController;
import com.jjlee.wedqueen.rest.dday.model.CategoryDto;
import com.jjlee.wedqueen.rest.dday.model.CategoryForm;
import com.jjlee.wedqueen.rest.dday.model.CategoryResponse;
import com.jjlee.wedqueen.rest.dday.model.DDayDto;
import com.jjlee.wedqueen.rest.dday.model.DDayForm;
import com.jjlee.wedqueen.rest.dday.model.DDayFormResponse;
import com.jjlee.wedqueen.rest.dday.model.DDayResponse;
import com.jjlee.wedqueen.rest.dday.model.Gender;
import com.jjlee.wedqueen.rest.dday.model.PositionDto;
import com.jjlee.wedqueen.rest.dday.model.Region;
import com.jjlee.wedqueen.rest.invitation.model.InvitationForm;
import com.jjlee.wedqueen.rest.premium.controller.PremiumController;
import com.jjlee.wedqueen.rest.user.controller.UserController;
import com.jjlee.wedqueen.rest.user.model.GetUserResponse;
import com.jjlee.wedqueen.rest.user.model.InvitationResponse;
import com.jjlee.wedqueen.rest.user.model.User;
import com.jjlee.wedqueen.rest.user.model.UserResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.utils.CalendarUtil;
import com.jjlee.wedqueen.utils.DecimalUtils;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.RegionConverter;
import com.jjlee.wedqueen.view.CategoryDataProvider;
import com.jjlee.wedqueen.view.DragCategoryAdapter;

import java.util.Calendar;
import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChecklistFragment extends Fragment {

    private RequestManager mGlideRequestManager;
    private Prefs prefs;

    private UserController userController;
    private PremiumController premiumController;
    private DDayController ddayController;

    private BottomSheetDialog ddaySettingDlg;   // 일정 설정 Dlg
    private Dialog ddayClearDlg;                // 일정 초기화 Dlg
    private Dialog ddayInfoSetDlg;              // 일정 설정 Dlg
    private Dialog coupleInvitationDlg;         // 반쪽초대 Dlg
    private Dialog coupleDisconnectDlg;         // 반쪽끊기 Dlg
    private Dialog ddayCategorySelectDlg;       // 일정Category 선택 Dlg
    private AlertDialog regionDlg;              // 지역 선택 Dlg

    private SwipeRefreshLayout checklistRefresh;
    private NestedScrollView checklistScrollview;

    private DDayForm dDayForm;
    private Calendar today;
    private String strToday;

    // 평균예산 데이터 View
    private TextView totalBudget;
    private TextView totalExpense;

    // 반쪽 데이터 View
    private TextView userNickname;
//    private ImageView userImg;
    private TextView coupleText;
    private LinearLayout coupleInvitationWrapper;
    private TextView myCoupleCode;
    private EditText coupleCode;
    private boolean isCouple = false;


    // 예산 데이터 View
    private LinearLayout emptyDday;
    private ViewGroup transitionsContainer;
    private LinearLayout settingBtn;
    private LinearLayout settingBox;
    private boolean settingBoxVisible;
    private LinearLayout moveCategory;
    private TextView addCategory;


    // 일정 설정 Dlg 변수
    private boolean successToLoadDdayInfo = false;
    private String[] regionListItems;
    private TextView genderFemale;
    private TextView genderMale;
    private TextView ddayRegion;
    private TextView ddayDate;
    private TextView ddayType365;
    private TextView ddayType180;
    private TextView ddayType100;


    // 일정 Category 선택 Dlg 변수
    private GridLayout checkedWrapper;


    // 카테고리 List 관련 View ( AdvancedRecyclerView )
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private DragCategoryAdapter mAdapter;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewDragDropManager mRecyclerViewragDropManager;
    private boolean isPinned;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGlideRequestManager = Glide.with( this );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checklist, container, false);

        prefs = new Prefs( getActivity() );

        regionListItems = getResources().getStringArray( R.array.region_item );
        today = Calendar.getInstance();
        strToday = CalendarUtil.getStringDate( today );

        isPinned = true;

        initDdaySettingDlg();
        initDdayClearDlg();
        initDdayInfoSetDlg();
        initCoupleInvitationDlg();
        initCoupleDisconnectDlg();
        initDdayCategorySelectDlg();
        initRegionSelectDlg();


        userController = new UserController();
//        userImg = view.findViewById( R.id.user_img );
        userNickname = view.findViewById( R.id.user_nickname );
        coupleText = view.findViewById( R.id.couple_text );


        ddayController = new DDayController();


        coupleInvitationWrapper = view.findViewById( R.id.couple_invitation_wrapper );
        coupleInvitationWrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
                    if( isCouple ) {
                        coupleDisconnectDlg.show();
                    } else {
                        Call<InvitationResponse> coupleCodeCall = userController.getCoupleInvitationCode();
                        coupleCodeCall.enqueue(new Callback<InvitationResponse>() {
                            @Override
                            public void onResponse(Call<InvitationResponse> call, Response<InvitationResponse> response) {

                                InvitationResponse coupleCodeResponse = response.body();
                                if( !ObjectUtils.isEmpty( coupleCodeResponse ) && !ObjectUtils.isEmpty( coupleCodeResponse.getCode() ) ) {
                                    myCoupleCode.setText( coupleCodeResponse.getCode() );
                                    coupleInvitationDlg.show();
                                } else {
                                    if( !ObjectUtils.isEmpty( coupleCodeResponse ) && !ObjectUtils.isEmpty( coupleCodeResponse.getRtnMsg() ) ) {
                                        Toast.makeText( getActivity(), coupleCodeResponse.getRtnMsg(), Toast.LENGTH_SHORT ).show();
                                    } else {
                                        Toast.makeText( getActivity(), "내 반쪽코드를 불러오지 못했습니다. 관리자에게 문의해주세요.", Toast.LENGTH_SHORT ).show();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<InvitationResponse> call, Throwable t) {
                                t.printStackTrace();
                                Toast.makeText( getActivity(), "내 반쪽코드를 불러오지 못했습니다. 인터넷 연결을 확인해주세요.", Toast.LENGTH_SHORT ).show();
                            }
                        });
                    }
                } else {
                    startAccountActivity();
                }
            }
        });


        emptyDday = view.findViewById( R.id.empty_dday );
        view.findViewById( R.id.create_dday ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( !MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
                    startAccountActivity();
                } else {
                    ddayInfoSetDlg.show();
                }
            }
        });
        transitionsContainer = view.findViewById( R.id.transition_container );
        settingBox = transitionsContainer.findViewById( R.id.setting_box );
        settingBoxVisible = false;
        settingBtn = view.findViewById( R.id.setting_btn );
        settingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                TransitionManager.beginDelayedTransition( transitionsContainer );
//                settingBoxVisible = !settingBoxVisible;
//                if( settingBoxVisible ) {
//                    settingBox.setVisibility( View.VISIBLE );
//                    settingBtn.setBackgroundColor( Color.parseColor( "#000000" ) );
//                } else {
//                    settingBox.setVisibility( View.GONE );
//                    settingBtn.setBackgroundColor( Color.parseColor( "#F6F6F6" ) );
//                }
                ddaySettingDlg.show();
            }
        });
        view.findViewById( R.id.statistics_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAnotherActivity( FullscreenActivity.class, Constants.CHECKLIST_STATS_URL, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY, true );
            }
        });
        view.findViewById( R.id.budget_avg_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startAnotherActivity( FullscreenActivity.class, Constants.AVG_BUDGET_URL, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY, true );
            }
        });
        moveCategory = view.findViewById( R.id.move_category );
        moveCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( !ObjectUtils.isEmpty( mAdapter ) ) {
                    isPinned = !isPinned;
                    mAdapter.setPinned( isPinned );
                    mRecyclerViewragDropManager.setInitiateOnLongPress( !isPinned );
                } else {
                    Toast.makeText( getActivity(), "정보를 불러오는 중입니다. 잠시 후에 다시 시도해주세요!", Toast.LENGTH_SHORT ).show();
                }
            }
        });
        addCategory = view.findViewById( R.id.add_category );
        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveCategory();
            }
        });

        totalBudget = view.findViewById( R.id.total_budget );
        totalExpense = view.findViewById( R.id.total_expense );


        mRecyclerView = view.findViewById( R.id.checkcategory_wrapper_new );
        mRecyclerView.setNestedScrollingEnabled( false );
        mLayoutManager = new LinearLayoutManager( getActivity() );


        getUserDataFromServer();
        getDdayInfoFromServer();
        getCheckCategoriesFromServer();


        DraggableItemAnimator generalItemAnimator = new DraggableItemAnimator();
//        generalItemAnimator.setDebug( true );
//        generalItemAnimator.setMoveDuration( 2000 );
        mRecyclerView.setItemAnimator( generalItemAnimator );

        checklistScrollview = view.findViewById( R.id.checklist_scrollview );

        checklistRefresh = view.findViewById( R.id.checklist_refresh );
        checklistRefresh.setColorSchemeResources(
                R.color.colorPrimaryDark, R.color.colorAccent,
                R.color.colorAccent, R.color.colorAccent);
        checklistRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
                checklistRefresh.setRefreshing( false );
            }
        });

        return view;
    }


    private void postDdayDataToServer( boolean includeExceptions ) {

        if( ObjectUtils.isEmpty( dDayForm ) ) {
            dDayForm = new DDayForm( Gender.female, Region.seoul, strToday, 365 );
        }

        if( includeExceptions ) {
            String exceptions = "";
            for( int i = 0; i < checkedWrapper.getChildCount(); i++ ) {
                CustomCheckedTextView customCheckedTextView = (CustomCheckedTextView)checkedWrapper.getChildAt( i );

                if( !customCheckedTextView.isSelected() ) {
                    exceptions += customCheckedTextView.getCategoryValue() + "|";
                }
            }

            if( !ObjectUtils.isEmpty( exceptions ) ) {
                dDayForm.setExceptions( exceptions.substring( 0, exceptions.length() - 1 ) );
            }
        }

        Log.e( "WQTEST3", "dDayForm.getGender() : " + dDayForm.getGender() );
        Log.e( "WQTEST3", "dDayForm.getRegion() : " + dDayForm.getRegion() );
        Log.e( "WQTEST3", "dDayForm.getdDay() : " + dDayForm.getdDay() );
        Log.e( "WQTEST3", "dDayForm.getdDayType() : " + dDayForm.getdDayType() );
        Log.e( "WQTEST3", "dDayForm.getExceptions() : " + dDayForm.getExceptions() );

        Call<AppResponse> ddayCreateCall = ddayController.createDDay( dDayForm );
        ddayCreateCall.enqueue(new Callback<AppResponse>() {
            @Override
            public void onResponse(Call<AppResponse> call, Response<AppResponse> response) {
                Log.e( "WQTEST3", "createDday Success" );
                refreshData();

                if( !ObjectUtils.isEmpty( dDayForm.getdDay() ) ) {

                    prefs.setDdayDate( dDayForm.getdDay().replaceAll( "\\.", "-" ) );

                    // Home TAB에도 Dday를 표기하는 부분이 있기 때문에 refresh 해주어야 함
                    ((MainActivity)getActivity()).setShouldRefresh( 0 );
                }
            }

            @Override
            public void onFailure(Call<AppResponse> call, Throwable t) {
                Log.e( "TEST", "createDday Failed" );
                Toast.makeText( getActivity(), "결혼정보 설정에 실패하였습니다", Toast.LENGTH_SHORT ).show();
            }
        });
    }


    private void deleteDDay() {

        toggleDdayView( false );

        Call<AppResponse> ddayDeleteCall = ddayController.deleteDDay();
        ddayDeleteCall.enqueue(new Callback<AppResponse>() {
            @Override
            public void onResponse(Call<AppResponse> call, Response<AppResponse> response) {
                refreshData();
            }

            @Override
            public void onFailure(Call<AppResponse> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText( getActivity(), "결혼정보 초기화에 실패하였습니다. 인터넷 연결을 확인해주세요.", Toast.LENGTH_SHORT ).show();
            }
        });
    }

    private void toggleCoupleView( boolean isCouple ) {
        if( isCouple ) {
            coupleText.setText( "반쪽 끊기" );
        } else {
            coupleText.setText( "반쪽 초대" );
        }
    }

    private void toggleDdayView( boolean isExistCategory ) {
        if( isExistCategory ) {
            emptyDday.setVisibility( View.GONE );
            transitionsContainer.setVisibility( View.VISIBLE );
        } else {

            totalBudget.setText( "?" );
            totalExpense.setText( "?" );

            emptyDday.setVisibility( View.VISIBLE );
            transitionsContainer.setVisibility( View.GONE );
        }
    }


    public void refreshData() {
        settingBoxVisible = false;
        settingBox.setVisibility( View.GONE );
//        settingBtn.setBackgroundColor( Color.parseColor( "#F6F6F6" ) );

        // refresh 하면서 dlg 데이터들도 초기화해야함
        initDdayInfoSetDlg();
        initDdayCategorySelectDlg();
        initRegionSelectDlg();

        getUserDataFromServer();
        getDdayInfoFromServer();
        getCheckCategoriesFromServer();
    }




    // Dialog들 초기화 부분
    private void initDdayClearDlg() {
        ddayClearDlg = new Dialog( getActivity() );
        ddayClearDlg.requestWindowFeature( Window.FEATURE_NO_TITLE );
        ddayClearDlg.setContentView( R.layout.dialog_clearconfirm );

        ddayClearDlg.findViewById( R.id.cancel_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ddayClearDlg.dismiss();
            }
        });
        ddayClearDlg.findViewById( R.id.clear_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ddayClearDlg.dismiss();
                deleteDDay();
            }
        });
    }

    private void initDdayInfoSetDlg() {
        ddayInfoSetDlg = new Dialog( getActivity() );
        ddayInfoSetDlg.requestWindowFeature( Window.FEATURE_NO_TITLE );
        ddayInfoSetDlg.setContentView( R.layout.dialog_ddayset );

        genderFemale = ddayInfoSetDlg.findViewById( R.id.gender_female );
        genderMale = ddayInfoSetDlg.findViewById( R.id.gender_male );
        ddayRegion = ddayInfoSetDlg.findViewById( R.id.dday_region );
        ddayDate = ddayInfoSetDlg.findViewById( R.id.dday_date );
        ddayType365 = ddayInfoSetDlg.findViewById( R.id.dday_type_365 );
        ddayType180 = ddayInfoSetDlg.findViewById( R.id.dday_type_180 );
        ddayType100 = ddayInfoSetDlg.findViewById( R.id.dday_type_100 );

        genderFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( !v.isSelected() ) {
                    genderFemale.setSelected( true );
                    genderMale.setSelected( false );
                    dDayForm.setGender( Gender.female );
                }
            }
        });
        genderMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( !v.isSelected() ) {
                    genderFemale.setSelected( false );
                    genderMale.setSelected( true );
                    dDayForm.setGender( Gender.male );
                }
            }
        });

        ddayRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regionDlg.show();
            }
        });
        ddayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog( getActivity(), dPickerListener, today.get( Calendar.YEAR ), today.get( Calendar.MONTH ), today.get( Calendar.DATE ) );
                datePickerDialog.show();
            }
        });
        ddayType365.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( !v.isSelected() ) {
                    ddayType365.setSelected( true );
                    ddayType180.setSelected( false );
                    ddayType100.setSelected( false );
                    dDayForm.setdDayType( 365 );
                }
            }
        });
        ddayType180.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( !v.isSelected() ) {
                    ddayType365.setSelected( false );
                    ddayType180.setSelected( true );
                    ddayType100.setSelected( false );
                    dDayForm.setdDayType( 180 );
                }
            }
        });
        ddayType100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( !v.isSelected() ) {
                    ddayType365.setSelected( false );
                    ddayType180.setSelected( false );
                    ddayType100.setSelected( true );
                    dDayForm.setdDayType( 100 );
                }
            }
        });

        // DEFAULT Data Set
        genderFemale.setSelected( true );
        ddayType365.setSelected( true );
        ddayDate.setText( strToday );

        ddayInfoSetDlg.findViewById( R.id.confirm_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ddayInfoSetDlg.dismiss();

                if( successToLoadDdayInfo ) {
                    postDdayDataToServer( false );
                } else {
                    ddayCategorySelectDlg.show();
                }
            }
        });
        ddayInfoSetDlg.findViewById( R.id.cancel_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ddayInfoSetDlg.dismiss();
            }
        });
    }

    private void initDdaySettingDlg() {
        ddaySettingDlg = new BottomSheetDialog( getActivity() );
        ddaySettingDlg.setContentView( getLayoutInflater().inflate( R.layout.dialog_checklist_setting, null ) );

        ddaySettingDlg.findViewById( R.id.setting_close ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ddaySettingDlg.dismiss();
            }
        });
        ddaySettingDlg.findViewById( R.id.dday_date_set ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ddaySettingDlg.dismiss();
                if( !MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
                    startAccountActivity();
                } else {
                    ddayInfoSetDlg.show();
                }
            }
        });
        ddaySettingDlg.findViewById( R.id.dday_clear ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ddaySettingDlg.dismiss();
                ddayClearDlg.show();
            }
        });
    }

    private void initCoupleInvitationDlg() {
        coupleInvitationDlg = new Dialog( getActivity() );
        coupleInvitationDlg.requestWindowFeature( Window.FEATURE_NO_TITLE );
        coupleInvitationDlg.setContentView( R.layout.dialog_couple_invitation );

        myCoupleCode = coupleInvitationDlg.findViewById( R.id.my_couple_code );
        coupleCode = coupleInvitationDlg.findViewById( R.id.couple_code );
        coupleInvitationDlg.findViewById( R.id.check_couple_code ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e( "TEST", "coupleCode : " + coupleCode.getText().toString() );
                Call<AppResponse> matchCodeCall = userController.matchCoupleInvitation( new InvitationForm( coupleCode.getText().toString() ) );
                matchCodeCall.enqueue(new Callback<AppResponse>() {
                    @Override
                    public void onResponse(Call<AppResponse> call, Response<AppResponse> response) {

                        AppResponse matchCodeResponse = response.body();

                        if( !ObjectUtils.isEmpty( matchCodeResponse.getRtnKey() ) ) {
                            if( matchCodeResponse.getRtnKey().equals( "DAOK" ) ) {
                                // Has-Data ( 연결하려는 반쪽과 나 둘다 일정표가 존재함 )
                                Toast.makeText( getActivity(), "두 분 모두 일정표를 생성하신 상태에요! 두 분 중 한 분은 일정표를 초기화해주세요.", Toast.LENGTH_SHORT ).show();
                            } else if( matchCodeResponse.getRtnKey().equals( "CMOK" ) ) {
                                // Success To Match
                                refreshData();
                            }
                        }
                        if( !ObjectUtils.isEmpty( matchCodeResponse.getRtnMsg() ) ) {
                            Log.e( "TEST", "msg is not null" );
                            Toast.makeText( getActivity(), matchCodeResponse.getRtnMsg(), Toast.LENGTH_SHORT ).show();
                        }
                        coupleInvitationDlg.dismiss();
                    }

                    @Override
                    public void onFailure(Call<AppResponse> call, Throwable t) {
                        t.printStackTrace();
                        Toast.makeText( getActivity(), "반쪽 매칭에 실패하였습니다. 인터넷 연결을 확인해주세요.", Toast.LENGTH_SHORT ).show();
                        coupleInvitationDlg.dismiss();
                    }
                });

            }
        });
        coupleInvitationDlg.findViewById( R.id.close_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coupleInvitationDlg.dismiss();
            }
        });
    }

    private void initCoupleDisconnectDlg() {
        coupleDisconnectDlg = new Dialog( getActivity() );
        coupleDisconnectDlg.requestWindowFeature( Window.FEATURE_NO_TITLE );
        coupleDisconnectDlg.setContentView( R.layout.dialog_couple_disconnect );

        coupleDisconnectDlg.findViewById( R.id.disconnect_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Call<AppResponse> coupleDisconnectCall = userController.disconnectCouple();
                coupleDisconnectCall.enqueue(new Callback<AppResponse>() {
                    @Override
                    public void onResponse(Call<AppResponse> call, Response<AppResponse> response) {

                        Log.e( "TEST", "coupleDisconnectCall Success" );

                        AppResponse disconnectResponse = response.body();
                        coupleDisconnectDlg.dismiss();

                        if( !ObjectUtils.isEmpty( disconnectResponse ) ) {
                            if( !ObjectUtils.isEmpty( disconnectResponse.getRtnKey() ) && disconnectResponse.getRtnKey().equals( "CMOK" ) ) {
                                refreshData();
                            } else if( !ObjectUtils.isEmpty( disconnectResponse.getRtnMsg() ) ) {
                                Toast.makeText( getActivity(), disconnectResponse.getRtnMsg(), Toast.LENGTH_SHORT ).show();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AppResponse> call, Throwable t) {
                        t.printStackTrace();
                        Log.e( "TEST", "coupleDisconnectCall Failed" );
                        Toast.makeText( getActivity(), "반쪽 초대끊기에 실패하였습니다. 인터넷 연결을 확인해주세요.", Toast.LENGTH_SHORT ).show();
                        coupleDisconnectDlg.dismiss();
                        refreshData();
                    }
                });
            }
        });
        coupleDisconnectDlg.findViewById( R.id.close_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                coupleDisconnectDlg.dismiss();
            }
        });
    }

    private void initDdayCategorySelectDlg() {
        ddayCategorySelectDlg = new Dialog( getActivity() );
        ddayCategorySelectDlg.requestWindowFeature( Window.FEATURE_NO_TITLE );
        ddayCategorySelectDlg.setContentView( R.layout.dialog_select_category );

        checkedWrapper = ddayCategorySelectDlg.findViewById( R.id.checked_wrapper );
        ddayCategorySelectDlg.findViewById( R.id.category_select_confirm ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postDdayDataToServer( true );
                ddayCategorySelectDlg.dismiss();
            }
        });
    }

    private void initRegionSelectDlg() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder( getActivity() );
        mBuilder.setTitle( "지역을 선택해주세요" );
        mBuilder.setSingleChoiceItems( getResources().getStringArray( R.array.region_item ), -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ddayRegion.setText( regionListItems[which] );
                dDayForm.setRegion( RegionConverter.convertRegionFromKor( regionListItems[which] ) );
                dialog.dismiss();
            }
        });
        regionDlg = mBuilder.create();
    }




    // 서버와의 통신 부분
    private void getUserDataFromServer() {

        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            Call<GetUserResponse> userCall = userController.getUser();
            userCall.enqueue(new Callback<GetUserResponse>() {
                @Override
                public void onResponse(Call<GetUserResponse> call, Response<GetUserResponse> response) {
                    GetUserResponse userResponse = response.body();
                    if( !ObjectUtils.isEmpty( userResponse ) ) {

                        isCouple = userResponse.isCouple();
//                        Log.e( "TEST", "isCouple : " + isCouple );

                        toggleCoupleView( isCouple );

                        User user = userResponse.getUser();

                        if( !ObjectUtils.isEmpty( user ) ) {
//                            mGlideRequestManager.load( user.getImageUrl() ).bitmapTransform( new CropCircleTransformation( getActivity() ) ).placeholder( R.drawable.default_user_icon ).into( userImg );
                            userNickname.setText( user.getNickname() );
                        } else {
//                            mGlideRequestManager.load( R.drawable.default_user_icon ).bitmapTransform( new CropCircleTransformation( getActivity() ) ).into( userImg );
                            userNickname.setText( "-" );
                        }
                    } else {
//                        mGlideRequestManager.load( R.drawable.default_user_icon ).bitmapTransform( new CropCircleTransformation( getActivity() ) ).into( userImg );
                        userNickname.setText( "-" );
                    }
                }

                @Override
                public void onFailure(Call<GetUserResponse> call, Throwable t) {
                    t.printStackTrace();

                    toggleCoupleView( false );
//                    mGlideRequestManager.load( R.drawable.default_user_icon ).bitmapTransform( new CropCircleTransformation( getActivity() ) ).into( userImg );
                    userNickname.setText( "-" );
                }
            });
        } else {
            toggleCoupleView( false );
//            mGlideRequestManager.load( R.drawable.default_user_icon ).bitmapTransform( new CropCircleTransformation( getActivity() ) ).into( userImg );
        }

    }

    private void getDdayInfoFromServer() {

        Call<DDayFormResponse> ddayInfoCall = ddayController.getdDayInfo();
        ddayInfoCall.enqueue(new Callback<DDayFormResponse>() {
            @Override
            public void onResponse(Call<DDayFormResponse> call, Response<DDayFormResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {

                    successToLoadDdayInfo = true;

                    dDayForm = response.body().getDday();

//                    Log.e( "WQTEST3", "dDayForm.getGender() : " + dDayForm.getGender() );
//                    Log.e( "WQTEST3", "dDayForm.getRegion() : " + dDayForm.getRegion() );
//                    Log.e( "WQTEST3", "dDayForm.getdDay() : " + dDayForm.getdDay() );
//                    Log.e( "WQTEST3", "dDayForm.getdDayType() : " + dDayForm.getdDayType() );

                    if( !ObjectUtils.isEmpty( dDayForm.getGender() ) && dDayForm.getGender().equals( Gender.male ) ) {
                        genderFemale.setSelected( false );
                        genderMale.setSelected( true );
                    } else {
                        genderFemale.setSelected( true );
                        genderMale.setSelected( false );
                    }
                    ddayRegion.setText( RegionConverter.convertRegionFromEngToKor( dDayForm.getRegion().name() ) );
                    String strDDay = dDayForm.getdDay();
                    ddayDate.setText( strDDay );
                    if( dDayForm.getdDayType() == 365 ) {
                        ddayType365.setSelected( true );
                        ddayType180.setSelected( false );
                        ddayType100.setSelected( false );
                    } else if( dDayForm.getdDayType() == 180 ) {
                        ddayType365.setSelected( false );
                        ddayType180.setSelected( true );
                        ddayType100.setSelected( false );
                    } else {
                        ddayType365.setSelected( false );
                        ddayType180.setSelected( false );
                        ddayType100.setSelected( true );
                    }

                } else {
                    successToLoadDdayInfo = false;
                    dDayForm = new DDayForm( Gender.female, Region.seoul, strToday, 365 );
                }
            }

            @Override
            public void onFailure(Call<DDayFormResponse> call, Throwable t) {
                Log.e("TEST", "ddayInfoCall Failed ( Maybe No data )" );
                successToLoadDdayInfo = false;
                dDayForm = new DDayForm( Gender.female, Region.seoul, strToday, 365 );
            }
        });
    }

    private void getCheckCategoriesFromServer() {

        Call<DDayResponse> ddayCall = ddayController.getdDay();
        ddayCall.enqueue(new Callback<DDayResponse>() {
            @Override
            public void onResponse(Call<DDayResponse> call
                    , Response<DDayResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    DDayResponse ddayResponse = response.body();
                    DDayDto dDayDto = ddayResponse.getdDay();

                    totalBudget.setText( DecimalUtils.convertDecimalFormat( (int)dDayDto.getcBudget() ) );
                    totalExpense.setText( DecimalUtils.convertDecimalFormat( (int)dDayDto.getExpense() ) );

                    int dDay = dDayDto.getdDay();
                    List<CategoryDto> categoryDtos = dDayDto.getCategories();

                    if( !ObjectUtils.isEmpty( categoryDtos ) && !ObjectUtils.isEmpty( getActivity() ) ) {
                        toggleDdayView( true );

                        refreshDropManager();

                        mAdapter = new DragCategoryAdapter( getActivity(), new CategoryDataProvider( categoryDtos, dDay ) );
                        mAdapter.setItemClick(new DragCategoryAdapter.ItemClick() {
                            @Override
                            public void onClick(long categoryId) {
                                startAnotherActivity( FullscreenActivity.class, Constants.CHECKLIST_CATEGORY_URL + categoryId, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY, false );
                            }
                        });
                        mAdapter.setItemCheck(new DragCategoryAdapter.ItemCheck() {
                            @Override
                            public void onCheck(long categoryId, boolean isCompleteCheck) {
                                updateCompleteCheckToServer( categoryId, isCompleteCheck );
                            }
                        });
                        mWrappedAdapter = mRecyclerViewragDropManager.createWrappedAdapter( mAdapter );
                        mRecyclerView.setLayoutManager( mLayoutManager );
                        mRecyclerView.setAdapter( mWrappedAdapter );

                        mRecyclerViewragDropManager.attachRecyclerView( mRecyclerView );
                        mRecyclerViewragDropManager.setOnItemDragEventListener(new RecyclerViewDragDropManager.OnItemDragEventListener() {
                            @Override
                            public void onItemDragStarted(int position) {
                            }
                            @Override
                            public void onItemDragPositionChanged(int fromPosition, int toPosition) {
                            }
                            @Override
                            public void onItemDragFinished(int fromPosition, int toPosition, boolean result) {
                                if( result ) {

                                    Log.e( "WQTEST3", "Move Category : " + mAdapter.getCategoryTitle( toPosition ) );
                                    updateCheckCategoryPositionToServer( mAdapter.getItemId( toPosition ), fromPosition, toPosition );
                                }
                            }
                            @Override
                            public void onItemDragMoveDistanceUpdated(int offsetX, int offsetY) {
                            }
                        });
                        checklistScrollview.smoothScrollTo( 0, 0 );      // 동작을 안하는듯 ??

                    } else {
                        toggleDdayView( false );
                    }


                } else {
                    Log.e("TEST", "DDayResponse body is null" );
                    toggleDdayView( false );
                }
            }

            @Override
            public void onFailure(Call<DDayResponse> call, Throwable t) {
                Log.e("TEST", "ddayCall Failed ( Maybe No data )" );
                toggleDdayView( false );
            }
        });
    }

    private void saveCategory() {
        Call<CategoryResponse> categoryCall = ddayController.saveCategory( new CategoryForm() );
        categoryCall.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {

                CategoryResponse categoryResponse = response.body();
                if( !ObjectUtils.isEmpty( categoryResponse ) ) {

                    CategoryDto categoryDto = new CategoryDto();
                    categoryDto.setId( categoryResponse.getId() );

                    mAdapter.addItem( categoryDto );

                } else {
                    Log.e( "TEST", "add failed" );
                    Toast.makeText(getActivity(), "항목추가에 실패하였습니다", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                Log.e( "TEST", "add failed" );
                t.printStackTrace();
                Toast.makeText( getActivity(), "항목추가에 실패하였습니다. 인터넷 연결을 확인해주세요.", Toast.LENGTH_SHORT ).show();
            }
        });
    }

    private void setCompleteCheck( CustomCheckCategory customCheckCategory, boolean isChecked ) {
        TextView categoryTitle = customCheckCategory.getCategoryTitle();
        TextView categoryStatus = customCheckCategory.getCategoryStatus();
        LinearLayout categoryItem = customCheckCategory.getCategoryItem();

        SpannableString spannableString = new SpannableString( categoryTitle.getText() );
        if( isChecked ) {
            spannableString.setSpan( new StrikethroughSpan(), 0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE );
            categoryTitle.setText( spannableString );
            categoryTitle.setTextColor( ContextCompat.getColor( getContext(), R.color.colorGray ) );
            categoryStatus.setTextColor( ContextCompat.getColor( getContext(), R.color.colorGray ) );
            categoryItem.setBackgroundColor( ContextCompat.getColor( getContext(), R.color.checked_category ) );
        } else {
            StrikethroughSpan[] strikethroughSpans = spannableString.getSpans( 0, spannableString.length(), StrikethroughSpan.class );
            for( StrikethroughSpan strikethroughSpan : strikethroughSpans ) {
                spannableString.removeSpan( strikethroughSpan );
            }
            categoryTitle.setText( spannableString );
            categoryTitle.setTextColor( Color.parseColor( "#000000" ) );
            categoryStatus.setTextColor( Color.parseColor( "#FFFFFF" ) );

            categoryItem.setBackgroundColor( ContextCompat.getColor( getContext(), R.color.colorWhite ) );
        }

        updateCompleteCheckToServer( customCheckCategory.getCategoryId(), isChecked );
    }


    private void updateCompleteCheckToServer( long categoryId, boolean isChecked ) {
        Call<AppResponse> updateCategoryCall = ddayController.updateCategory( categoryId, isChecked );
        updateCategoryCall.enqueue(new Callback<AppResponse>() {
            @Override
            public void onResponse(Call<AppResponse> call, Response<AppResponse> response) {
            }

            @Override
            public void onFailure(Call<AppResponse> call, Throwable t) {
            }
        });
    }


    private void updateCheckCategoryPositionToServer( long positionableId, int fromPosition, int toPosition ) {

        // 기존 position 설정 로직에서 toPosition 보내주는 부분이 from에 대한 index도 잡고있는 상태로 toPosition을 계산해서 동작하기 때문에 상단의 category를 하단으로 이동시키려는 경우 toPosition을 +1 시켜주어야 함
        if( fromPosition < toPosition ) {
            toPosition += 1;
        }

        Log.e( "WQTEST", "positionableId : " + positionableId );
        Log.e( "WQTEST", "fromPosition : " + fromPosition );
        Log.e( "WQTEST", "toPosition : " + toPosition );

        Call<AppResponse> positionSetCall = ddayController.setPosition( new PositionDto( positionableId, (long)fromPosition, (long)toPosition ) );
        positionSetCall.enqueue(new Callback<AppResponse>() {
            @Override
            public void onResponse(Call<AppResponse> call, Response<AppResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    AppResponse appResponse = response.body();

                    Log.e( "WQTEST3", "Success Position Change" );

                    if( !ObjectUtils.isEmpty( appResponse.getRtnKey() ) && appResponse.getRtnKey().equals( "CMOK" ) ) {
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    Log.e( "WQTEST3", "response Body is null" );
                }
            }

            @Override
            public void onFailure(Call<AppResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    private String calculateDdayStatus( int dday, int dDayStart, int dDayEnd ) {

        String retStr = "green";

        if( dday <= dDayEnd ) {
            retStr = "red";
        } else if( dDayStart >= dday && dday > dDayEnd ) {
            retStr = "orange";
        } else if( dday > 7 && dDayStart  >= dday - 7 ) {
            retStr = "yellow";
        }

        return retStr;
    }


    private DatePickerDialog.OnDateSetListener dPickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

            String strMonth = String.format( "%02d", ( month + 1 ) );
            String strDay = String.format( "%02d", dayOfMonth );

            dDayForm.setdDay( year + "." + strMonth + "." + strDay );
            ddayDate.setText( year + "." + strMonth + "." + strDay );
        }
    };


    private void startAnotherActivity( Class<?> targetActivityClass, String urlExtra, int reqCode, boolean checkLogIn ) {
        if( checkLogIn && !MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            startAccountActivity();
        } else {
            Intent intent = new Intent( getActivity(), targetActivityClass );
            if( !ObjectUtils.isEmpty( urlExtra ) ) {
                intent.putExtra( "URL", urlExtra );
            }
            if( reqCode > 0 ) {
                getActivity().startActivityForResult( intent, reqCode );
            } else {
                startActivity( intent );
            }
        }
    }

    private void startAccountActivity() {
        Intent intent = new Intent( getActivity(), AccountActivity.class );
        intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
        getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
    }


    private void refreshDropManager() {
        if( !ObjectUtils.isEmpty( mRecyclerViewragDropManager ) ) {
            mRecyclerViewragDropManager.release();
        }
        mRecyclerViewragDropManager = new RecyclerViewDragDropManager();
        mRecyclerViewragDropManager.setDraggingItemShadowDrawable( (NinePatchDrawable) ContextCompat.getDrawable( getActivity(), R.drawable.material_shadow_z3 ) );
        mRecyclerViewragDropManager.setInitiateOnLongPress( true );
        mRecyclerViewragDropManager.setInitiateOnMove( false );
    }

    @Override
    public void onPause() {
        if( mRecyclerViewragDropManager != null ) {
            mRecyclerViewragDropManager.cancelDrag();
        }

        super.onPause();
    }

    @Override
    public void onDestroy() {
        if( mRecyclerViewragDropManager != null ) {
            mRecyclerViewragDropManager.release();
            mRecyclerViewragDropManager = null;
        }

        if( mRecyclerView != null ) {
            mRecyclerView.setItemAnimator( null );
            mRecyclerView.setAdapter( null );
        }

        if( mWrappedAdapter != null ) {
            WrapperAdapterUtils.releaseAll( mWrappedAdapter );
            mWrappedAdapter = null;
        }
        mAdapter = null;
        mLayoutManager = null;

        super.onDestroy();
    }
}
