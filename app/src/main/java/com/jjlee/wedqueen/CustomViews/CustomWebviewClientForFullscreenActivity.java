package com.jjlee.wedqueen.CustomViews;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.jjlee.wedqueen.AccountActivity;
import com.jjlee.wedqueen.ExternalWebviewActivity;
import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.support.ConfirmDialog;
import com.jjlee.wedqueen.support.Constants;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by JJLEE on 2016. 4. 15..
 */
public class CustomWebviewClientForFullscreenActivity extends WebViewClient {
    private Map<String, String> customHeader = new HashMap<>(); // deviceType

    private FullscreenActivity fullscreenActivity;
    private WebView mTarget;

    private AlertDialog alertIsp, alertKFTC;
    final String KFTC_LINK = "market://details?id=com.kftc.bankpay.android";			//	금융결제원 설치 링크

    public CustomWebviewClientForFullscreenActivity(FullscreenActivity strongActivity, WebView target) {
        super();
        mTarget = target;
        fullscreenActivity = strongActivity;
        customHeader.put("deviceType", "android");
        customHeader.put("version", MyApplication.getGlobalApplicationContext().getVersionName());

//        createISPPopup();
//        createKFTCPopup();
    }

    public CustomWebviewClientForFullscreenActivity(FullscreenActivity strongActivity) {
        super();
        fullscreenActivity = strongActivity;
        customHeader.put("deviceType", "android");
        customHeader.put("version", MyApplication.getGlobalApplicationContext().getVersionName());
    }


    public void nullStrongActivity() {
        fullscreenActivity = null;
    }


    private void createISPPopup() {
        this.alertIsp = new AlertDialog.Builder(fullscreenActivity)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("알림")
                .setMessage("모바일 ISP어플리케이션이 설치되어있지 않습니다. \n설치를 눌러 진행해주십시요.\n취소를 누르면 결제가 취소됩니다.")
                .setPositiveButton("설치", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // ISP
                        CustomWebviewClientForFullscreenActivity.this.mTarget.loadUrl("http://mobile.vpay.co.kr/jsp/MISP/andown.jsp");
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(CustomWebviewClientForFullscreenActivity.this.fullscreenActivity, "(-1)결제가 취소되었습니다..", Toast.LENGTH_SHORT).show();
                    }
                }).create();
    }

    private void createKFTCPopup() {
        this.alertKFTC = new AlertDialog.Builder(fullscreenActivity)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("알림")
                .setMessage("계좌이체 결제를 하기 위해서는 BANKPAY 앱이 필요합니다.\n설치 페이지로  진행하시겠습니까?")
                .setPositiveButton("설치", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // KFTC
                        //NiceWebViewClient.this.target.loadUrl("http://mobile.vpay.co.kr/jsp/MISP/andown.jsp");
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(KFTC_LINK));
                        fullscreenActivity.startActivity(intent);
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(CustomWebviewClientForFullscreenActivity.this.fullscreenActivity, "(-1)결제가 취소되었습니다..", Toast.LENGTH_SHORT).show();
                    }
                }).create();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {

        Log.e( "WQTEST3", "shouldOverrideUrlLoading url : " + url );

        if (!url.startsWith("http://") && !url.startsWith("https://") && !url.startsWith("javascript:")) {
            Intent intent = null;

            try {
                intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
            } catch (URISyntaxException ex) {
                return false;
            }

            if(url.startsWith("kftc-bankpay")){
                if( !isPackageInstalled("com.kftc.bankpay.android")) {
                    alertKFTC.show();
                    return true;
                }
            }

            Uri uri = Uri.parse(intent.getDataString());
            intent = new Intent(Intent.ACTION_VIEW, uri);

            try {
                fullscreenActivity.startActivity(intent);
                return true;
            } catch (ActivityNotFoundException e) {
                if (url.startsWith("ispmobile://")) {
                    alertIsp.show();
                    return true;
                } else if (url.startsWith("kftc-bankpay")) {
                    alertKFTC.show();
                    return true;
                } else if( url.startsWith("intent://") || url.startsWith("intent:")) {//intent 형태의 스키마 처리
                    try {
                        Intent excepIntent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                        String packageNm = excepIntent.getPackage();
                        excepIntent = new Intent(Intent.ACTION_VIEW);
                        excepIntent.setData(Uri.parse("market://search?q=pname:"+ packageNm));
                        fullscreenActivity.startActivity(excepIntent);

                        return true;
                    } catch (URISyntaxException e1) {
                    }
                } else if (url != null	&& (url.contains("vguard")
                        || url.contains("droidxantivirus")
                        || url.contains("lottesmartpay")
                        || url.contains("smshinhancardusim://")
                        || url.contains("shinhan-sr-ansimclick")
                        || url.contains("v3mobile")
                        || url.endsWith(".apk")
                        || url.contains("smartwall://")
                        || url.contains("appfree://")
                        || url.contains("market://")
                        || url.contains("ansimclick://")
                        || url.contains("ansimclickscard")
                        || url.contains("ansim://")
                        || url.contains("mpocket")
                        || url.contains("mvaccine")
                        || url.contains("market.android.com")
                        || url.contains("http://m.ahnlab.com/kr/site/download"))) {
                    try{
                        try {
                            intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                        } catch (URISyntaxException ex) {
                            Log.i("NICE", "Bad URI " + url + ":" + ex.getMessage());
                            return false;
                        }

                        intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        fullscreenActivity.startActivity(intent);
                        return true;
                    } catch (ActivityNotFoundException ee) {
                        e.printStackTrace();
                        return false;
                    } catch (Exception ee) {
                        return false;
                    }
                }
            }
            return false;
        } else if(url.equals(Constants.BACK_URL)) {
            fullscreenActivity.finish();
            return true;
        } else if (url.equals(Constants.REFRESH_URL)) {
            view.reload();
            return true;
        } else if (url.equals(Constants.BACK_REFRESH_URL)) {
            Intent intent = new Intent();
            intent.putExtra("shouldRefresh", true);
            fullscreenActivity.setResult(Activity.RESULT_OK, intent);
            fullscreenActivity.finish();
            return true;
        } else if (url.contains(Constants.SIGN_IN_URL)) {
            Intent intent = new Intent(fullscreenActivity, AccountActivity.class);
            intent.putExtra(Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN);
            intent.putExtra( Constants.LOGIN_FROM, "action_webview" );
            fullscreenActivity.startActivityForResult(intent, Constants.REQ_CODE_LOGIN);

            return true;
        }
        else if (!url.contains(Constants.BASE_URL)) {
            Intent intent = new Intent(fullscreenActivity, ExternalWebviewActivity.class);
            intent.putExtra("url", url);

            if(url.contains("nobar=1")) {
                intent.putExtra("NO_TOOL_BAR", true);
            }

            fullscreenActivity.startActivity(intent);
            fullscreenActivity.overridePendingTransition(R.anim.slide_up, R.anim.no_change);
//            Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            try {
//                intent.setPackage("com.android.chrome");
//                fullscreenActivity.startActivity(intent);
//            } catch (Exception e) { // ActivityNotFoundException ex
//                // Chrome browser presumably not installed so allow user to choose instead
//                intent.setPackage(null);
//                fullscreenActivity.startActivity(intent);
//            }

            return true;
        } else {
            view.loadUrl("javascript:androidPauseAllVideo()");
            Intent intent = new Intent(fullscreenActivity, FullscreenActivity.class);
            intent.putExtra("URL", url);
            if(url.contains("nobar=1")) {
                intent.putExtra("NO_TOOL_BAR", true);
            }
            fullscreenActivity.startActivityForResult(intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY);
            return true;
        }
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description,
                                String failingUrl) {
        super.onReceivedError(view, errorCode, description, failingUrl);

        Crashlytics.log( "FullscreenActivity onReceivedError errorCode : " + errorCode );

        if (fullscreenActivity != null) {
            fullscreenActivity.showNetworkError();
            String customErrorPageHtml = "<html></html>";
            view.loadData(customErrorPageHtml, "text/html", null);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError err) {
        super.onReceivedError(view, req, err);
        if(fullscreenActivity != null) {
            fullscreenActivity.showNetworkError();
            String customErrorPageHtml = "<html></html>";
            view.loadData(customErrorPageHtml, "text/html", null);
        }
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon)
    {
        if (url.contains("hanacard")) {
            final WebView tempView = view;
            final ConfirmDialog dialog = new ConfirmDialog(view.getContext());
            dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dia) {
                    dialog.setText("해당 카드는 지원하지 않습니다. 죄송합니다.");
                }
            });
            dialog.show();

            final Button confirmButton = (Button)dialog.findViewById(R.id.confirm_button);

            confirmButton.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    shouldOverrideUrlLoading(tempView, Constants.BACK_URL);
                    dialog.dismiss();
                    return false;
                }
            });

        }
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url){
        super.onPageFinished(view, url);
        if(fullscreenActivity != null) {
            fullscreenActivity.hideProgressBar();
            //특정 아이디 또는 클래스만 자동 재생되게 구현해야함
            if (url.contains("app.wedqueen.com")) {
                view.loadUrl("javascript:(function() { androidFunction.playVideo(); })()");
            }
        }
    }


    private boolean isPackageInstalled(String pkgName) {
        try {
            fullscreenActivity.getPackageManager().getPackageInfo(pkgName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return false;
    }

}