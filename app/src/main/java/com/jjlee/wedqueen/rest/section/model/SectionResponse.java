package com.jjlee.wedqueen.rest.section.model;

import java.util.List;

/**
 * Created by esmac on 2018. 3. 21..
 */

public class SectionResponse {
    private List<Section> section;

    public List<Section> getSection() {
        return section;
    }

    public void setSection(List<Section> section) {
        this.section = section;
    }
}
