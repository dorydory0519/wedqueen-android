package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jjlee.wedqueen.R;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class CustomFollowModel extends LinearLayout {

    private ImageView userImg;
    private TextView userDday;
    private TextView userNickname;
    private LinearLayout followBtn;
    private ImageView followImg;

    public CustomFollowModel(Context context) {
        super(context);
        initView();

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_follow, this, false);
        addView(v);

        userImg = (ImageView) findViewById( R.id.user_img );
        userDday = (TextView) findViewById( R.id.user_dday );
        userNickname = (TextView) findViewById( R.id.user_nickname );
        followBtn = (LinearLayout) findViewById( R.id.follow_btn );
        followImg = (ImageView) findViewById( R.id.follow_img );
    }

    public void setUserImg( String resUrl ) {
        Glide.with( getContext() ).load( resUrl ).bitmapTransform( new CropCircleTransformation( getContext() ) ).placeholder( R.drawable.default_user_icon ).into( userImg );
    }
    public void setUserDday( String resString ) {
        userDday.setText( resString );
    }
    public void setUserNickname( String resString ) {
        userNickname.setText( resString );
    }
    public void setFollow( boolean isFollow ) {
        followBtn.setTag( isFollow );

        if( isFollow ) {
            followBtn.setBackgroundResource( R.drawable.green_rounding );
            followImg.setImageResource( R.drawable.follow_on );
        } else {
            followBtn.setBackgroundResource( R.drawable.red_rounding );
            followImg.setImageResource( R.drawable.follow_off );
        }
    }
    public LinearLayout getFollowBtn() {
        return followBtn;
    }
}
