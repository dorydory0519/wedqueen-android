package com.jjlee.wedqueen.CustomViews;

import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.jjlee.wedqueen.fragments.FilterableWebviewFragment;
import com.jjlee.wedqueen.fragments.PageFragment;
import com.jjlee.wedqueen.fragments.WebviewFragment;

/**
 * Created by JJLEE on 2016. 3. 28..
 */
public class CustomWebChromeClient<T extends PageFragment> extends WebChromeClient {

    private T pageFragment;

    public CustomWebChromeClient(T fragment) {
        super();
        pageFragment = fragment;
    }

    @Override
    public void onProgressChanged(WebView view, int newProgress) {
        if(pageFragment instanceof FilterableWebviewFragment) {
            ((FilterableWebviewFragment) pageFragment).setLoadingProgress(newProgress);
        }

        if( newProgress > 75 ) {
            pageFragment.hideProgressBar();
        } else {
            pageFragment.showProgressBar();
        }

    }
}
