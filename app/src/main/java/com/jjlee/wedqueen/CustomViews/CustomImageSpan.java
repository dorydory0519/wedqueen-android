package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.style.ImageSpan;

/**
 * Created by esmac on 2017. 10. 12..
 */

public class CustomImageSpan extends ImageSpan {

    private String serverFileName;

    public CustomImageSpan(Context context, Bitmap b, int verticalAlignment, String serverFileName) {
        super(context, b, verticalAlignment);
        this.serverFileName = serverFileName;
    }

    public CustomImageSpan(Drawable d, int verticalAlignment, String serverFileName) {
        super(d, verticalAlignment);
        this.serverFileName = serverFileName;
    }

    public String getServerFileName() {
        return serverFileName;
    }

    public void setServerFileName(String serverFileName) {
        this.serverFileName = serverFileName;
    }
}
