package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;

public class CustomCategoryIcon extends LinearLayout {

    RequestManager mRequestManager;

    private LinearLayout categoryGridItem;
    private ImageView categoryImg;
    private TextView categoryText;

    public CustomCategoryIcon(Context context) {
        super(context);
        initView();
    }

    public CustomCategoryIcon(Context context, RequestManager requestManager) {
        super(context);
        this.mRequestManager = requestManager;
        initView();
    }

    private void initView() {
        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService( infService );
        View v = li.inflate( R.layout.layout_category_icon, this, false );
        addView(v);

        categoryGridItem = (LinearLayout) findViewById( R.id.category_grid_item );
        categoryImg = (ImageView) findViewById( R.id.category_img );
        categoryText = (TextView) findViewById( R.id.category_text );
    }

    public void setCategoryImg( String resUrl ) {
        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load(resUrl).centerCrop().placeholder( R.color.colorWhite ).into( categoryImg );
        } else {
            Glide.with( getContext() ).load( resUrl ).centerCrop().placeholder( R.color.colorWhite ).into( categoryImg );
        }
    }
    public void setCategoryImgSize(int size) {
        categoryGridItem.getLayoutParams().width = size;
        categoryGridItem.getLayoutParams().height = size;
    }

    public void setCategoryText( String resString ) {
        categoryText.setText( resString );
    }

}
