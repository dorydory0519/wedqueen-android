package com.jjlee.wedqueen.rest.common.model;

/**
 * Created by JJLEE on 2016. 7. 11..
 */
public class AppResponse {

    protected String rtnKey;
    protected String rtnMsg;

    public String getRtnKey() {
        return rtnKey;
    }

    public void setRtnKey(String rtnKey) {
        this.rtnKey = rtnKey;
    }

    public String getRtnMsg() {
        return rtnMsg;
    }

    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }
}
