package com.jjlee.wedqueen.rest.sector.model;

import java.util.List;

/**
 * Created by esmac on 2017. 12. 14..
 */

public class SectorResponse {
    private List<Sector> sectors;

    public List<Sector> getSectors() {
        return sectors;
    }

    public void setSectors(List<Sector> sectors) {
        this.sectors = sectors;
    }
}
