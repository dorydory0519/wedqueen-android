package com.jjlee.wedqueen;

import android.Manifest;
import android.app.Activity;
import android.app.AppOpsManager;
import android.app.Dialog;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.support.v4.widget.DrawerLayout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.facebook.appevents.AppEventsLogger;
import com.jjlee.wedqueen.CustomViews.CustomInstantMessage;
import com.jjlee.wedqueen.CustomViews.CustomViewPager;
import com.jjlee.wedqueen.fragments.ChecklistFragment;
import com.jjlee.wedqueen.fragments.FilterableFragment;
import com.jjlee.wedqueen.fragments.FilterableWebviewFragment;
import com.jjlee.wedqueen.fragments.HomeFragment;
import com.jjlee.wedqueen.fragments.MyPageFragment;
import com.jjlee.wedqueen.fragments.NewHomeFragment;
import com.jjlee.wedqueen.fragments.PointShopFragment;
import com.jjlee.wedqueen.fragments.WebviewFragment;
import com.jjlee.wedqueen.rest.instance.controller.InstanceController;
import com.jjlee.wedqueen.rest.instance.model.Instance;
import com.jjlee.wedqueen.rest.instance.model.InstanceResponse;
import com.jjlee.wedqueen.rest.invitation.controller.InvitationController;
import com.jjlee.wedqueen.rest.invitation.model.InvitationForm;
import com.jjlee.wedqueen.rest.invitation.model.InvitationSaveResponse;
import com.jjlee.wedqueen.rest.popup.controller.PopupController;
import com.jjlee.wedqueen.rest.popup.model.Popup;
import com.jjlee.wedqueen.rest.popup.model.PopupResponse;
import com.jjlee.wedqueen.rest.tutorial.controller.TutorialController;
import com.jjlee.wedqueen.rest.tutorial.model.TutorialDetail;
import com.jjlee.wedqueen.rest.tutorial.model.TutorialResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.utils.CalendarUtil;
import com.jjlee.wedqueen.utils.CustomScrollView;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;
import com.transitionseverywhere.Fade;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;
import com.transitionseverywhere.extra.Scale;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends WebviewContainingActivity {
    private SectionsPagerAdapter mSectionsPagerAdapter;

    private static final int PAGE_COUNT = 5;
    private static final int HOMETAB_POSITION = 0;
//    private static final int SEARCHTAB_POSITION = 1;
    private static final int WEDDINGTALKTAB_POSITION = 1;
//    private static final int EDITTAB_POSITION = 2;
    private static final int CHECKLIST_POSITION = 2;
    private static final int POINTSHOPTAB_POSITION = 3;
    private static final int MY_PAGE_POSITION = 4;

    private static final int SHOW_TAB_GUIDE_MAX = 3;

    private int prevSelectedTabPosition = HOMETAB_POSITION;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private CustomViewPager mViewPager;
    private DrawerLayout mDrawerLayout;
//    private TextView notiCountView;
//    private ImageButton alarmButton;

    private TabLayout tabLayout;
    public static MainActivity instance;

    private BroadcastReceiver mReceiver;

    private boolean isToastShown = false;

    private Prefs prefs;
    private Popup selectedPopup;
    private Dialog popupDialog;
    private ImageView popupImg;
    private TextView popupCloseForeverBtn;
    private View popupCloseSeparate;

    private Dialog inviteCodeDlg;
    private AppCompatDialog progressDialog;

//    private int prefsShowInstanceCount;
//    private CustomInstantMessage customInstantMessage;

    private int tabWidth;
    private FrameLayout tabGuideContainer;
    private TextView tabGuide;

    private void initViewPager() {
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (CustomViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(PAGE_COUNT - 1);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setSelectedTabIndicatorHeight(0);
        setTabIcons();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics( displayMetrics );
        int displayWidth = displayMetrics.widthPixels;
        tabWidth = ( displayWidth - SizeConverter.dpToPx( 7 ) ) / PAGE_COUNT;

        tabGuideContainer = findViewById( R.id.tab_guide_container );
        tabGuideContainer.getLayoutParams().width = tabWidth;
        tabGuide = findViewById( R.id.tab_guide );
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                tabGuide.setVisibility( View.INVISIBLE );

                boolean isShowTabGuide = false;

                switch ( tab.getPosition() ) {
                    case HOMETAB_POSITION :
                        int showHomeGuideCount = prefs.getShowHomeGuideCount();
                        if( showHomeGuideCount < SHOW_TAB_GUIDE_MAX ) {
                            isShowTabGuide = true;
                            prefs.setShowHomeGuideCount( showHomeGuideCount + 1 );
                        }
                        break;
                    case WEDDINGTALKTAB_POSITION :
                        int showWeddingtalkGuideCount = prefs.getShowWeddingtalkGuideCount();
                        if( showWeddingtalkGuideCount < SHOW_TAB_GUIDE_MAX ) {
                            isShowTabGuide = true;
                            prefs.setShowWeddingtalkGuideCount( showWeddingtalkGuideCount + 1 );
                        }
                        break;
                    case CHECKLIST_POSITION :
                        int showChecklistGuideCount = prefs.getShowChecklistGuideCount();
                        if( showChecklistGuideCount < SHOW_TAB_GUIDE_MAX ) {
                            isShowTabGuide = true;
                            prefs.setShowChecklistGuideCount( showChecklistGuideCount + 1 );
                        }
                        break;
                    case POINTSHOPTAB_POSITION :
                        int showPointshopGuideCount = prefs.getShowPointshopGuideCount();
                        if( showPointshopGuideCount < SHOW_TAB_GUIDE_MAX ) {
                            isShowTabGuide = true;
                            prefs.setShowPointshopGuideCount( showPointshopGuideCount + 1 );
                        }
                        break;
                    case MY_PAGE_POSITION :
                        int showMypageGuideCount = prefs.getShowMypageGuideCount();
                        if( showMypageGuideCount < SHOW_TAB_GUIDE_MAX ) {
                            isShowTabGuide = true;
                            prefs.setShowMypageGuideCount( showMypageGuideCount + 1 );
                        }
                        break;
                }

                if( !ObjectUtils.isEmpty( tab.getCustomView() ) && isShowTabGuide ) {

                    tabGuide.setText( Constants.TAB_NAMES[tab.getPosition()] );

                    tabGuideContainer.setTranslationX( SizeConverter.dpToPx( 7 ) + tabWidth * tab.getPosition() );

                    TransitionSet set = new TransitionSet()
                            .addTransition( new Scale( 0 ) )
//                            .addTransition( new Fade() )
                            .setDuration( 800 )
                            .addListener(new Transition.TransitionListener() {
                                @Override
                                public void onTransitionStart(@NonNull Transition transition) {

                                }

                                @Override
                                public void onTransitionEnd(@NonNull Transition transition) {
                                    TransitionSet endSet = new TransitionSet()
                                                .addTransition( new Scale( 0 ) )
                                            .setDuration( 300 )
                                            .setInterpolator( new FastOutLinearInInterpolator() );
                                    TransitionManager.beginDelayedTransition( tabGuideContainer, endSet );
                                    tabGuide.setVisibility( View.INVISIBLE );
                                }

                                @Override
                                public void onTransitionCancel(@NonNull Transition transition) {
                                    tabGuide.setVisibility( View.INVISIBLE );
                                }

                                @Override
                                public void onTransitionPause(@NonNull Transition transition) {
                                }

                                @Override
                                public void onTransitionResume(@NonNull Transition transition) {
                                    tabGuide.setVisibility( View.INVISIBLE );
                                }
                            })
                            .setInterpolator( new LinearOutSlowInInterpolator() );

                    TransitionManager.beginDelayedTransition( tabGuideContainer, set );
                    tabGuide.setVisibility( View.VISIBLE );
                }

                // 글쓰기 TAB은 새로운 Activity를 띄워줌
//                if( tab.getPosition() == EDITTAB_POSITION ) {
//                    startContentEditActivity();
//                } else {
                    // 선택한 TAB이 MYPAGE인데 로그인이 안된 경우
//                    if( tab.getPosition() == MY_PAGE_POSITION && !MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
//                        Intent intent = new Intent(MainActivity.this, AccountActivity.class);
//                        intent.putExtra(Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN);
//                        startActivityForResult(intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY);
//                    } else {
                        prevSelectedTabPosition = tab.getPosition();

                        View myPageTabView = tab.getCustomView();
                        if( !ObjectUtils.isEmpty( myPageTabView ) ) {
                            myPageTabView.findViewById(R.id.icon).setBackgroundResource(Constants.TAB_ICONS_ON[tab.getPosition()]);
                        }
                        mViewPager.setCurrentItem(tab.getPosition());
//                    }
//                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View myPageTabView = tab.getCustomView();
                myPageTabView.findViewById(R.id.icon).setBackgroundResource(Constants.TAB_ICONS_OFF[tab.getPosition()]);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // 선택된 Tab에 해당하는 Fragment Refresh 작업
                // @@@@@
                if( tab.getPosition() == HOMETAB_POSITION ) {
                    Fragment fragment = mSectionsPagerAdapter.getRegisteredFragment( HOMETAB_POSITION );
                    if( !ObjectUtils.isEmpty( fragment ) ) {

                        ((NewHomeFragment)fragment).setNewHomeScrollToTop();
                    }
                }
//                else if( tab.getPosition() == HOMETAB_POSITION ) {
//                    Fragment fragment = mSectionsPagerAdapter.getRegisteredFragment( HOMETAB_POSITION );
//                    if( !ObjectUtils.isEmpty( fragment ) ) {
//                        CustomScrollView customScrollView = ((HomeFragment) fragment).getHometabScrollview();
//                        if( !ObjectUtils.isEmpty( customScrollView ) ) {
//                            customScrollView.smoothScrollTo( 0, 0 );
//                        }
//                    }
//                }
//                else if( tab.getPosition() == SEARCHTAB_POSITION ) {
//                    // 스크롤 상단으로 이동시키는 부분 주석
//                    Fragment fragment = mSectionsPagerAdapter.getRegisteredFragment( SEARCHTAB_POSITION );
//                    if( !ObjectUtils.isEmpty( fragment ) ) {
//                        CustomScrollView customScrollView = ((SearchFragment) fragment).getSearchtabScrollview();
//                        if( !ObjectUtils.isEmpty( customScrollView ) ) {
//                            customScrollView.smoothScrollTo( 0, 0 );
//                        }
//                    }
//                }
            }
        });

    }

    private void initSearchAction() {
//        final Typeface sansSerifThin = Typeface.create("sans-serif-thin", Typeface.NORMAL);
//        final CustomEditText searchInput = ((CustomEditText)findViewById(R.id.search_input));
//        searchInput.setTypeface(sansSerifThin);
//
//        searchInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//
//                switch (actionId) {
//                    case EditorInfo.IME_ACTION_SEARCH:
//                        Intent intent = new Intent(MainActivity.this, FullscreenActivity.class);
//                        String keyword = v.getText().toString();
//
//                        String type = null;
//                        int curPos = mViewPager.getCurrentItem();
//                        switch (curPos) {
//                            case 0:
//                                type = "content";
//                                break;
//                            case 1:
//                                type = "weddingTalk";
//                                break;
//                            case 2:
//                                type = "content";
//                                break;
//                            case 3:
//                                type = "content";
//                                break;
//                            case 4:
//                                type = "content";
//                                break;
//                            default:
//                                type = "content";
//                                break;
//                        }
//                        keyword = keyword.replaceAll("#", "%23");
//                        intent.putExtra("URL", Constants.SEARCH_URL + "?query=" + keyword + "&type=" + type);
//                        startActivity(intent);
//
//                        return true;
//                    default:
//                        return false;
//                }
//            }
//        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        instance = this;
        setContentView(R.layout.activity_main);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "main" ) );

        prefs = new Prefs(this);

        initPopupDialog();

        // 인스턴스 메세지 작업 ( OFF 상태 )
//        prefsShowInstanceCount = prefs.getShowInstantCount();
//        customInstantMessage = (CustomInstantMessage) findViewById(R.id.custom_instant_message);
//        customInstantMessage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Animation fadeOutAnimation = AnimationUtils.loadAnimation( MainActivity.this, R.anim.fade_out );
//                fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//                        customInstantMessage.setVisibility( View.GONE );
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//                    }
//                });
//                customInstantMessage.startAnimation( fadeOutAnimation );
//                prefs.setShowInstantCount( prefsShowInstanceCount + 1 );
//            }
//        });
//        InstanceController instanceController = new InstanceController();
//        Call<InstanceResponse> instanceCall = instanceController.getInstances();
//        instanceCall.enqueue(new Callback<InstanceResponse>() {
//            @Override
//            public void onResponse(Call<InstanceResponse> call, Response<InstanceResponse> response) {
//                if( !ObjectUtils.isEmpty( response.body() ) ) {
//                    InstanceResponse instanceResponse = response.body();
//
//                    if( !ObjectUtils.isEmpty( instanceResponse.getInstances() ) && instanceResponse.getInstances().size() > 0 ) {
//                        List<Instance> instances = instanceResponse.getInstances();
//
//                        int showInstanceCount = instances.get( 0 ).getShowCount();
//
//                        if( showInstanceCount < 1 || showInstanceCount >= prefsShowInstanceCount ) {
//                            customInstantMessage.setInstantMessage( instances.get( 0 ).getText() );
//                            customInstantMessage.setVisibility( View.VISIBLE );
//                        }
//                    }
//                } else {
//                    Log.e( "WQTEST3", "instance response body is null" );
//                }
//            }
//
//            @Override
//            public void onFailure(Call<InstanceResponse> call, Throwable t) {
//                t.printStackTrace();
//            }
//        });


        if(Constants.ACTION_TYPE_AUTO_LOGIN.equals(getIntent().getStringExtra(Constants.ACTION_TYPE)) ) {
            getIntent().removeExtra(Constants.ACTION_TYPE);
        }

        ((MyApplication)MyApplication.getGlobalApplicationContext()).setCurrentActivity(this);

        mDrawerLayout = (DrawerLayout)findViewById(R.id.layout_drawer);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        // User Notification Count
//        alarmButton = (ImageButton) findViewById(R.id.alarm_button);
//        alarmButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (MyApplication.getGlobalApplicationContext().isLoggedIn()) {
//                    Intent intent = new Intent(MainActivity.this, FullscreenActivity.class);
//                    intent.putExtra("URL", Constants.NOTI_URL);
//                    MyApplication.getGlobalApplicationContext().setBadgeCount(0);
//                    setBadgeView(0);
//                    startActivity(intent);
//                } else {
//                    Intent intent = new Intent(MainActivity.this, AccountActivity.class);
//                    intent.putExtra(Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN);
//                    startActivityForResult(intent, Constants.REQ_CODE_LOGIN);
//                }
//            }
//        });
//        notiCountView = (TextView)findViewById(R.id.badge_notification);
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(Constants.BADGE_COUNT_CHANGED)) {
                    View myPageTabView = tabLayout.getTabAt(MY_PAGE_POSITION).getCustomView();

                    TextView notiCountView = (TextView) myPageTabView.findViewById(R.id.badge_notification);
                    setBadgeView( notiCountView, MyApplication.getGlobalApplicationContext().getBadgeCount() );

                }
            }

        };

        // Permission for drawing over another app ( API Level >= 23 )
//        checkDrawOverlayPermission();

        initViewPager();


        // 튜토리얼 페이지 로드
        if( prefs.getShowTutorial() ) {
            TutorialController tutorialController = new TutorialController();
            Call<TutorialResponse> tutorialCall = tutorialController.getTutorial();
            tutorialCall.enqueue(new Callback<TutorialResponse>() {
                @Override
                public void onResponse(Call<TutorialResponse> call, Response<TutorialResponse> response) {
                    if( !ObjectUtils.isEmpty( response.body() ) ) {
                        TutorialResponse tutorialResponse = response.body();

                        if( !ObjectUtils.isEmpty( tutorialResponse.getTutorial() )
                                && !ObjectUtils.isEmpty( tutorialResponse.getTutorial().getTutorialDetails() )
                                && tutorialResponse.getTutorial().getTutorialDetails().size() > 0 ) {
                            List<TutorialDetail> tutorialDetails = tutorialResponse.getTutorial().getTutorialDetails();
                            List<String> tutorialImgUrls = new ArrayList<>();
                            for( TutorialDetail tutorialDetail : tutorialDetails ) {
                                tutorialImgUrls.add( tutorialDetail.getUrl() );
                            }
                            prefs.setTutorialImgUrl( tutorialImgUrls );

                            prefs.setShowTutorial( false );

                            Intent intent = new Intent(MainActivity.this, TutorialActivity.class );
                            startActivityForResult( intent, Constants.REQ_CODE_OPEN_TUTORIAL_ACTIVITY );
                        } else {
                            getPopupFromServer();
                        }
                    }
                }

                @Override
                public void onFailure(Call<TutorialResponse> call, Throwable t) {
                    t.printStackTrace();
                    getPopupFromServer();
                }
            });
        } else {
            getPopupFromServer();
        }
    }
    private void setTabIcons() {
        for (int i = 0; i < PAGE_COUNT; i++) {
            View view = getLayoutInflater().inflate(R.layout.custom_tab, null);
            if( i == HOMETAB_POSITION ) {
                view.findViewById(R.id.icon).setBackgroundResource(Constants.TAB_ICONS_ON[i]);
            } else {
                view.findViewById(R.id.icon).setBackgroundResource(Constants.TAB_ICONS_OFF[i]);
            }
            tabLayout.getTabAt(i).setCustomView(view);
        }
        setMypageIcon();
    }

    public void setMypageIcon() {
//        int wh = SizeConverter.dpToPx(50);
        View myPageTabView = tabLayout.getTabAt(MY_PAGE_POSITION).getCustomView();
//        myPageTabView.findViewById(R.id.icon).setLayoutParams(new FrameLayout.LayoutParams(wh, wh, Gravity.CENTER));
//        myPageTabView.findViewById(R.id.icon).setBackgroundResource(Constants.TAB_ICONS[MY_PAGE_POSITION]);

        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            // myPage Tab Icon에 읽지않은 Noti Count 표시
            TextView notiCountView = (TextView) myPageTabView.findViewById(R.id.badge_notification);
            setBadgeView( notiCountView, MyApplication.getGlobalApplicationContext().getBadgeCount() );
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppEventsLogger.activateApp(this);

        View myPageTabView = tabLayout.getTabAt(MY_PAGE_POSITION).getCustomView();
        TextView notiCountView = (TextView) myPageTabView.findViewById(R.id.badge_notification);
        setBadgeView( notiCountView, MyApplication.getGlobalApplicationContext().getBadgeCount() );

        setMypageIcon();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Constants.BADGE_COUNT_CHANGED);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, intentFilter);

        if(getIntent().getStringExtra("URL") != null) {
            processNoti(getIntent());
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();

        // searchFragment의 searchView에서 Focus를 제거 ( 일부 device에서 onResume 시에 keyboard가 뜸 )
        // searchActivity로 이동
//        if( !ObjectUtils.isEmpty( mSectionsPagerAdapter ) ) {
//            SearchFragment searchFragment = ((SearchFragment)mSectionsPagerAdapter.getRegisteredFragment( SEARCHTAB_POSITION ));
//            if( !ObjectUtils.isEmpty( searchFragment ) ) {
//                if( !ObjectUtils.isEmpty( searchFragment.getSearchView() ) ) {
//                    searchFragment.getSearchView().clearFocus();
//                }
//            }
//        }

        AppEventsLogger.deactivateApp(this); // 이게 좀 애매...
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if( popupDialog != null ) {
            popupDialog.dismiss();
            popupDialog = null;
        }
    }

    @Override
    protected void onDestroy() {

        // MainActivity 종료시 Cache데이터 삭제
        clearApplicationData();

        super.onDestroy();

        instance = null;
        ((MyApplication)MyApplication.getGlobalApplicationContext()).setCurrentActivity(null);
        Runtime.getRuntime().gc();
    }

    public void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File( cache.getParent() );
        if( appDir.exists() ) {
            String[] children = appDir.list();
            for( String s : children ) {
                if( s.equals( "cache" ) ) {
                    deleteDir( new File( appDir, s ) );
                }
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if( dir != null && dir.isDirectory() ) {
            String[] children = dir.list();
            for( int i = 0; i < children.length; i++ ) {
                boolean success = deleteDir( new File( dir, children[i] ) );
                if( !success ) {
                    return false;
                }
            }
        }

        return dir.delete();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {
        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch(position) {
                case 0:
                    return new NewHomeFragment();
//                    return HomeFragment.newInstance( "contentfeed" );
                case 1:
                    return FilterableWebviewFragment.newInstance("weddingtalk", "publicTalk", Constants.WEDDING_TALK_URL, position);
                case 2:
                    return new ChecklistFragment();
                case 3:
                    return new PointShopFragment();
                case 4:
                    return MyPageFragment.newInstance("mypage");
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show PAGE_COUNT total pages.
            return PAGE_COUNT;
        }


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        @Override
        public int getItemPosition(Object item) {
            Fragment fragment = (Fragment) item;

            if( fragment instanceof HomeFragment ) {
                return POSITION_NONE;
            } else {
                return super.getItemPosition( item );
            }

        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }

        public SparseArray<Fragment> getRegisteredFragments() {
            return registeredFragments;
        }
    }

    public void toggleDrawerLayout () {
        if(mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
        {
            mDrawerLayout.closeDrawer(Gravity.RIGHT);
        }
        else
        {
            mDrawerLayout.openDrawer(Gravity.RIGHT);
        }
    }

    public void setShouldRefresh( int targetPosition ) {

        Fragment pf = mSectionsPagerAdapter.getRegisteredFragment( targetPosition );

        if( pf instanceof MyPageFragment ) {
            ((MyPageFragment)pf).refresh();
        } else if( pf instanceof HomeFragment ) {
            ((HomeFragment)pf).refresh();
        } else if( pf instanceof  PointShopFragment ) {
            ((PointShopFragment)pf).refreshMyData();
        } else if( pf instanceof ChecklistFragment ) {
            ((ChecklistFragment)pf).refreshData();
        } else if( pf instanceof NewHomeFragment ) {
            ((NewHomeFragment)pf).refreshData();
        }
    }

    public void setShouldRefreshAll() {
        int curPosition;
        curPosition = mViewPager.getCurrentItem();
        for(int i=0; i<PAGE_COUNT; i++) {
            Fragment pf = mSectionsPagerAdapter.getRegisteredFragment(i);

            if(pf instanceof WebviewFragment ) {
                if(i == curPosition) {
                    ((WebviewFragment) pf).setmFirstLoaded(true);
                    ((WebviewFragment) pf).refresh();
                } else {
                    ((WebviewFragment) pf).setmFirstLoaded(false);
                }
            }

            if(pf instanceof FilterableWebviewFragment) {
                if(i == curPosition) {
                    ((FilterableWebviewFragment) pf).setmFirstLoaded(true);
                    ((FilterableWebviewFragment) pf).refresh();
                } else {
                    ((FilterableWebviewFragment) pf).setmFirstLoaded(false);
                }
            }

            if( pf instanceof MyPageFragment ) {
                ((MyPageFragment)pf).refresh();
            } else if( pf instanceof PointShopFragment ) {
                ((PointShopFragment)pf).refreshMyData();
            } else if( pf instanceof ChecklistFragment ) {
                ((ChecklistFragment)pf).refreshData();
            } else if( pf instanceof NewHomeFragment ) {
                ((NewHomeFragment)pf).refreshData();
            }
        }
    }

    public void processAfterLoginStateChanged() {
        setMypageIcon();
        setShouldRefreshAll();
        MyApplication.getGlobalApplicationContext().setIsLoginSynchronized(true);
    }

    @Override
    protected void onNewIntent(Intent intent) { // FullscreenActivitiy 에서 homeAsUp 누르면 이거 탐
        super.onNewIntent(intent);
        if(!MyApplication.getGlobalApplicationContext().isLoginSynchronized()) { // 전체 리프레쉬 필요
            processAfterLoginStateChanged();
        }

        if(intent.getStringExtra("URL") != null) {
            processNoti(intent);
        }

//        IgawCommon.registerReferrer( MainActivity.this );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( requestCode == Constants.REQ_CODE_OPEN_TUTORIAL_ACTIVITY ) {
            getPopupFromServer();
        }

        if( MyApplication.getGlobalApplicationContext().isShowInviteCode() ) {
            initInviteCodeDlg();
            MyApplication.getGlobalApplicationContext().setShowInviteCode( false );
        }

        // MainActivity 내에서의 수동로그인 시도인 경우 이전 TAB으로 이동
        if( requestCode == Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY ) {
            tabLayout.getTabAt( prevSelectedTabPosition ).select();
        }

        // checklist는 항상 refresh
        setShouldRefresh( CHECKLIST_POSITION );

        if( !ObjectUtils.isEmpty( data ) && data.getBooleanExtra( "refreshMain", false ) ) {
            Log.e( "WQTEST3", "refreshMain" );
            setShouldRefresh( HOMETAB_POSITION );
        }

        // DetailCompany 에서 돌아온 경우 resultCode와 상관없이 HOME과 MYPAGE를 refresh해주고 return
        if( requestCode == Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY
                || requestCode == Constants.REQ_CODE_OPEN_DETAILCOMPANY_ACTIVITY ) {
            setShouldRefresh( HOMETAB_POSITION );
//            setShouldRefresh( CHECKLIST_POSITION );
            setShouldRefresh( POINTSHOPTAB_POSITION );
            setShouldRefresh( MY_PAGE_POSITION );
            return;
        }

        if( resultCode == Activity.RESULT_CANCELED ) {
            return;
        }

        if( !MyApplication.getGlobalApplicationContext().isLoginSynchronized()) { // 전체 리프레쉬 필요
            processAfterLoginStateChanged();
            Fragment pf = mSectionsPagerAdapter.getRegisteredFragment(MY_PAGE_POSITION);
            if(pf != null && pf instanceof FilterableFragment) {
                LinearLayout tabCategoryWrapper = (LinearLayout) pf.getView().findViewById(R.id.filter_wrapper);
                if(MyApplication.getGlobalApplicationContext().isLoggedIn()) {
                    ((FilterableFragment) pf).initiateTabCategories(tabCategoryWrapper, "mypage", "dday");
                } else {
                    ((FilterableFragment) pf).initiateTabCategories(tabCategoryWrapper, "mypage", "");
                }
            }
        } else if(data.getBooleanExtra("shouldRefresh", false)) { // 하나만 리프레시 필요

            Log.e( "WQTEST3" ,"shouldRefresh in Main" );

            int curPosition = mViewPager.getCurrentItem();
            Fragment pf = mSectionsPagerAdapter.getRegisteredFragment(curPosition);
            if (pf instanceof FilterableWebviewFragment) {
                ((FilterableWebviewFragment) pf).setmFirstLoaded(true);
                ((FilterableWebviewFragment)pf).refresh();
            } else if( pf instanceof MyPageFragment ) {
                ((MyPageFragment) pf).refreshWeddingDiary();
            } else if( pf instanceof ChecklistFragment ) {
                ((ChecklistFragment) pf).refreshData();
            } else if( pf instanceof NewHomeFragment ) {
                ((NewHomeFragment) pf).refreshData();
            }
        }

        switch(requestCode) {

            case Constants.REQ_CODE_OPEN_CONTENTEDIT_ACTIVITY :

                // ContentEditActivity에서 MainActivity로 돌아온 경우 저장해둔 이전 TAB으로 이동
                tabLayout.getTabAt( prevSelectedTabPosition ).select();

                // 포인트가 지급될 여지가 있으므로 MYPAGE와 POINTSHOP TAB refresh 필요
                setShouldRefresh( MY_PAGE_POSITION );
                setShouldRefresh( POINTSHOPTAB_POSITION );

                break;

            // 로그인이 성공해서 MainActivity로 돌아온 경우
            case Constants.REQ_CODE_LOGIN:

                setShouldRefreshAll();

                break;

            // 로그아웃이 성공해서 MainActivity로 돌아온 경우
            case Constants.REQ_CODE_LOGOUT:

                setShouldRefresh( HOMETAB_POSITION );
                setShouldRefresh( MY_PAGE_POSITION );
                // Home TAB으로 이동시킴
                tabLayout.getTabAt( 0 ).select();

                break;

            case Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY:

                long contentId = data.getLongExtra( "contentId", -1 );
                if( contentId > -1 ) {
                    Intent intent = new Intent(MainActivity.this, ContentEditActivity.class);
                    intent.putExtra( "contentId", contentId );
                    startActivityForResult( intent, Constants.REQ_CODE_OPEN_CONTENTEDIT_ACTIVITY );
                }

                break;

            case Constants.REQ_CODE_OPEN_DDAYSETTING_ACTIVITY:
            case Constants.REQ_CODE_OPEN_MYRESERV_ACTIVITY:
            case Constants.REQ_CODE_OPEN_MYPAYMENT_ACTIVITY:

                setShouldRefresh( HOMETAB_POSITION );
                setShouldRefresh( POINTSHOPTAB_POSITION );
                setShouldRefresh( MY_PAGE_POSITION );
                break;
        }
    }

    @Override
    public void onBackPressed() {

        if(mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            mDrawerLayout.closeDrawer(Gravity.RIGHT);
        } else {
            if ( isToastShown ) {
                super.onBackPressed();
                //android.os.Process.killProcess(android.os.Process.myPid());
            } else {
                Toast.makeText(this, R.string.press_back_button_again_to_exit,
                        Toast.LENGTH_LONG).show();
                isToastShown = true;
            }
        }
    }

    public void changeViewPagerTo(int index) {
        mViewPager.setCurrentItem(index, true);
    }

    public void checkDrawOverlayPermission() {
        int launchCount = prefs.getLaunchCount();

        /** check if we already  have permission to draw over other apps */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ( (!Settings.canDrawOverlays(this) || !Settings.System.canWrite(this) ) && launchCount % 3 == 1) { // 세번에 한번씩 물어봄
                /** if not construct intent to request permission */

                /** request permission via start activity for result */

                if(!Settings.canDrawOverlays(this)) {
                    Intent intent1 = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                            Uri.parse("package:" + getPackageName()));
                    startActivity(intent1);
                }

                if(!Settings.System.canWrite(this)) {
                    Intent intent2 = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS,
                            Uri.parse("package:" + getPackageName()));
                    startActivity(intent2);
                }


            }
        }
    }

//    public void setBadgeView(int badgeCount) {
//        notiCountView.setText(badgeCount+"");
//        if(badgeCount == 0) {
//            notiCountView.setVisibility(View.GONE);
//            alarmButton.setAlpha(0.3f);
//        } else {
//            notiCountView.setVisibility(View.VISIBLE);
//            alarmButton.setAlpha(1.0f);
//        }
//    }

    public void setBadgeView(TextView badgeView, int badgeCount) {

        badgeView.setText(badgeCount+"");
        if(badgeCount == 0) {
            badgeView.setVisibility(View.GONE);
        } else {
            badgeView.setVisibility(View.VISIBLE);
        }
    }

    public void processNoti(Intent curIntent) {
        String url = curIntent.getStringExtra("URL");
        String notiId = curIntent.getStringExtra("NOTI_ID");
        int viewPagerPosition = curIntent.getIntExtra("SHOULD_CHANGE_PAGE_TO", -1);
        curIntent.removeExtra("URL");
        curIntent.removeExtra("NOTI_ID");
        curIntent.removeExtra("SHOULD_CHANGE_PAGE_TO");

        if( viewPagerPosition == -1 ) {
            Intent intent = new Intent(this, FullscreenActivity.class);
            intent.putExtra("URL", url);
            startActivity(intent);
        } else {
            changeViewPagerTo(viewPagerPosition);
            // URL parameter에 대한 처리는 아직 안함.
            // TODO: ex) /hotdeal?listType=company 해도 hotdeal로만 감.
        }

        MyApplication.getGlobalApplicationContext().requestCountDownSingleNoti(notiId);

    }


    private void getPopupFromServer() {

        Log.e( "WQTEST3", CalendarUtil.getStringDate( prefs.getRestrictedPopupDateMilli() ) + " 까지 팝업 제한됨" );

        if( Calendar.getInstance().getTimeInMillis() > prefs.getRestrictedPopupDateMilli() ) {
            PopupController popupController = new PopupController();
            Call<PopupResponse> popupCall = popupController.getPopupV2();
            popupCall.enqueue(new Callback<PopupResponse>() {
                @Override
                public void onResponse(Call<PopupResponse> call, Response<PopupResponse> response) {
                    if( !ObjectUtils.isEmpty( response.body() ) ) {
                        PopupResponse popupResponse = response.body();
                        if( !ObjectUtils.isEmpty( popupResponse.getPopups() ) && popupResponse.getPopups().size() > 0 ) {

                            // 리스트를 돌면서 lastPopupId가 있는지 찾아보고 있으면 그 다음 idx POPUP을, 없으면 첫번째 idx에 해당하는 POPUP id를 lastPopupId에 새로 넣음
                            int lastPopupId = prefs.getLastPopupId();
                            boolean selectNextPopup = false;
                            for( Popup popup : popupResponse.getPopups() ) {
                                if( ObjectUtils.isEmpty( selectedPopup ) ) {
                                    selectedPopup = popup;
                                }

                                if( selectNextPopup ) {
                                    selectedPopup = popup;
                                    break;
                                }

                                if( lastPopupId == popup.getId() ) {
                                    selectNextPopup = true;
                                }
                            }

                            // 보여질 팝업정보가 있는지 다시한번 체크하고 정보 PUT & SHOW
                            if( !ObjectUtils.isEmpty( selectedPopup ) && !ObjectUtils.isEmpty( popupDialog ) ) {

                                prefs.setLastPopupId( selectedPopup.getId() );

                                Glide.with( MainActivity.this ).load( selectedPopup.getImageUrl() ).centerCrop().into( popupImg );
                                popupImg.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        popupDialog.dismiss();
                                        Intent intent = new Intent( MainActivity.this, FullscreenActivity.class );
                                        intent.putExtra("URL", selectedPopup.getLinkUrl() );
                                        startActivity( intent );
                                    }
                                });

                                if( popupResponse.getTerm() > 0 ) {
                                    popupCloseForeverBtn.setText( popupResponse.getMent() );
                                    popupCloseForeverBtn.setTag( popupResponse.getTerm() );
                                    popupCloseForeverBtn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            prefs.setRestrictedPopupDateMilli( CalendarUtil.getFixedDateMilliFromNowToAfter( (int)view.getTag() ) );
                                            popupDialog.dismiss();
                                        }
                                    });
                                } else {
                                    popupCloseForeverBtn.setVisibility( View.INVISIBLE );
                                    popupCloseSeparate.setVisibility( View.INVISIBLE );
                                }

                                popupDialog.show();
                            }
                        }
                    } else {
                        Log.e( "WQTEST3", "popup response body is null" );
                    }
                }

                @Override
                public void onFailure(Call<PopupResponse> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    private void initPopupDialog() {
        popupDialog = new Dialog( this );
        popupDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        popupDialog.getWindow().setBackgroundDrawable( new ColorDrawable( android.graphics.Color.TRANSPARENT ) );
        popupDialog.getWindow().setDimAmount( 0.77f );
        popupDialog.setContentView( R.layout.layout_popup );
        popupDialog.setCancelable( false );

        popupImg = popupDialog.findViewById( R.id.popup_img );
        popupDialog.findViewById( R.id.close_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDialog.dismiss();
            }
        });
        popupCloseForeverBtn = popupDialog.findViewById( R.id.close_forever_btn );
    }


    public void initInviteCodeDlg() {
        inviteCodeDlg = new Dialog( this );
        inviteCodeDlg.requestWindowFeature( Window.FEATURE_NO_TITLE );
        inviteCodeDlg.setContentView( R.layout.dialog_invite_code );
        inviteCodeDlg.setCancelable( false );

        inviteCodeDlg.findViewById( R.id.close_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inviteCodeDlg.dismiss();
            }
        });
        inviteCodeDlg.findViewById( R.id.ok_btn ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strInviteCode = ((TextView)inviteCodeDlg.findViewById( R.id.invite_code )).getText().toString();

                progressON( MainActivity.this, "초대코드 확인중..." );

                // 초대 코드를 가지고 save 통신
                InvitationController invitationController = new InvitationController();
                Call<InvitationSaveResponse> invitationSaveCall = invitationController.saveInvitationCode( new InvitationForm( strInviteCode ) );
                invitationSaveCall.enqueue(new Callback<InvitationSaveResponse>() {
                    @Override
                    public void onResponse(Call<InvitationSaveResponse> call, Response<InvitationSaveResponse> response) {

                        progressOFF();

                        if( !ObjectUtils.isEmpty( response.body() ) ) {
//                            Result result = response.body().getResult();
                            InvitationSaveResponse result = response.body();

                            if( !ObjectUtils.isEmpty( result ) ) {
                                if( result.isStatus() ) {
                                    MyApplication.getGlobalApplicationContext().setShowInviteCode( false );
                                    setShouldRefresh( POINTSHOPTAB_POSITION );
                                    inviteCodeDlg.dismiss();
                                }
                                Toast.makeText( MainActivity.this, result.getMsg(), Toast.LENGTH_SHORT ).show();
                            } else {
                                Log.e( "WQTEST3", "result is null" );
                                Toast.makeText( MainActivity.this, "통신에 실패하였습니다. 관리자에게 문의해주세요.", Toast.LENGTH_SHORT ).show();
                            }
                        } else {
                            Log.e( "WQTEST3", "invitation save response body is null" );
                            Toast.makeText( MainActivity.this, "통신에 실패하였습니다. 관리자에게 문의해주세요.", Toast.LENGTH_SHORT ).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<InvitationSaveResponse> call, Throwable t) {
                        progressOFF();
                        t.printStackTrace();
                        Log.e( "WQTEST3", "invitation save 통신 error" );
                        Toast.makeText( MainActivity.this, "통신에 실패하였습니다. 관리자에게 문의해주세요.", Toast.LENGTH_SHORT ).show();
                    }
                });
            }
        });
        inviteCodeDlg.show();
    }


    // 로딩 Dialog
    public void progressON(Activity activity, String message) {

        if (activity == null || activity.isFinishing()) {
            return;
        }


        if (progressDialog != null && progressDialog.isShowing()) {
            progressSET(message);
        } else {
            progressDialog = new AppCompatDialog(activity);
            progressDialog.setCancelable(false);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.layout_frame_loading);
            progressDialog.show();
        }


        final ImageView img_loading_frame = (ImageView) progressDialog.findViewById(R.id.iv_frame_loading);
        final AnimationDrawable frameAnimation = (AnimationDrawable) img_loading_frame.getBackground();
        img_loading_frame.post(new Runnable() {
            @Override
            public void run() {
                frameAnimation.start();
            }
        });

        TextView tv_progress_message = (TextView) progressDialog.findViewById(R.id.tv_progress_message);
        if (!TextUtils.isEmpty(message)) {
            tv_progress_message.setText(message);
        }
    }

    public void progressSET(String message) {

        if (progressDialog == null || !progressDialog.isShowing()) {
            return;
        }

        TextView tv_progress_message = (TextView) progressDialog.findViewById(R.id.tv_progress_message);
        if (!TextUtils.isEmpty(message)) {
            tv_progress_message.setText(message);
        }

    }

    public void progressOFF() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }


    public void startContentEditActivity() {

        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            Intent intent = new Intent(MainActivity.this, ContentEditActivity.class);
            startActivityForResult( intent, Constants.REQ_CODE_OPEN_CONTENTEDIT_ACTIVITY );
        } else {
            Intent intent = new Intent( this, AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            intent.putExtra( Constants.LOGIN_FROM, "tab_editor" );
            startActivityForResult(intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY);
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
