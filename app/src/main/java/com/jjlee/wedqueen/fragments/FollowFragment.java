package com.jjlee.wedqueen.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjlee.wedqueen.CustomViews.CustomFollowModel;
import com.jjlee.wedqueen.OtherUserDiaryActivity;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.subscription.controller.SubscriptionController;
import com.jjlee.wedqueen.rest.subscription.model.SubscriptionDto;
import com.jjlee.wedqueen.rest.subscription.model.SubscriptionGetResponse;
import com.jjlee.wedqueen.rest.subscription.model.SubscriptionResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.ObjectUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by esmac on 2018. 3. 9..
 */

public class FollowFragment extends Fragment {

    private String followType;

    private TextView followLabel;
    private LinearLayout followWrapper;
    private SubscriptionController subscriptionController;

    public static FollowFragment newInstance( String followType ) {
        FollowFragment fragment = new FollowFragment();
        Bundle args = new Bundle();
        args.putString( "followType", followType );
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        followType = args.getString( "followType" );

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_follow, container, false);

        followLabel = (TextView) view.findViewById( R.id.follow_label );
        followWrapper = (LinearLayout) view.findViewById( R.id.follow_wrapper );
        subscriptionController = new SubscriptionController();
        getFollowDataFromServer();

        return view;
    }

    private void getFollowDataFromServer() {
//        followWrapper.removeAllViews();

        Call<SubscriptionGetResponse> subscriptionCall;

        if( followType.equals( "following" ) ) {
            followLabel.setText( "팔로잉" );
            subscriptionCall = subscriptionController.getMyFollowing( "user" );
        } else {
            followLabel.setText( "팔로워" );
            subscriptionCall = subscriptionController.getMyFollower( "user" );
        }

        subscriptionCall.enqueue(new Callback<SubscriptionGetResponse>() {
            @Override
            public void onResponse(Call<SubscriptionGetResponse> call, Response<SubscriptionGetResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    SubscriptionGetResponse subscriptionGetResponse = response.body();

                    if( !ObjectUtils.isEmpty( subscriptionGetResponse.getUsers() ) && subscriptionGetResponse.getUsers().size() > 0 ) {
                        List<SubscriptionDto> users = subscriptionGetResponse.getUsers();

                        for( SubscriptionDto user : users ) {
                            if( !ObjectUtils.isEmpty( getActivity() ) ) {
                                CustomFollowModel customFollowModel = new CustomFollowModel( getActivity() );
                                customFollowModel.setUserImg( user.getThumbnailUrl() );
                                customFollowModel.setUserDday( user.getDdayToString() );
                                customFollowModel.setUserNickname( user.getName() );
                                customFollowModel.setFollow( user.isHasSubscription() );
                                customFollowModel.setTag( user.getSubscriptionableId() );
                                customFollowModel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // 해당 유저 Diary 확인 Activity 오픈
                                        Intent intent = new Intent( getActivity(), OtherUserDiaryActivity.class );
                                        intent.putExtra( "userId", (int)v.getTag() );
                                        getActivity().startActivityForResult( intent, Constants.REQ_CODE_OPEN_OTHERUSERDIARY_ACTIVITY );
                                    }
                                });
                                customFollowModel.getFollowBtn().setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        boolean isFollow = (boolean)v.getTag();
                                        CustomFollowModel targetModel = (CustomFollowModel)v.getParent().getParent();
                                        int targetUserId = (int) targetModel.getTag();

                                        setFollowUser( isFollow, targetUserId );
                                        targetModel.setFollow( !isFollow );
                                    }
                                });

                                followWrapper.addView( customFollowModel );
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SubscriptionGetResponse> call, Throwable t) {

            }
        });
    }

    private void setFollowUser( boolean isFollow, int targetUserId ) {
        if( isFollow ) {
            Call<SubscriptionResponse> subscriptionCall = subscriptionController.delSubscription( "user", targetUserId );
            subscriptionCall.enqueue(new Callback<SubscriptionResponse>() {
                @Override
                public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                }

                @Override
                public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                }
            });
        } else {
            Call<SubscriptionResponse> subscriptionCall = subscriptionController.postSubscription( "user", targetUserId );
            subscriptionCall.enqueue(new Callback<SubscriptionResponse>() {
                @Override
                public void onResponse(Call<SubscriptionResponse> call, Response<SubscriptionResponse> response) {
                }

                @Override
                public void onFailure(Call<SubscriptionResponse> call, Throwable t) {
                }
            });
        }
    }

}
