package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;

public class CustomRealReviewModel extends RelativeLayout {

    private RequestManager mRequestManager;

    private ImageView realreviewImg;
    private TextView realreviewTitle;
    private TextView realreviewDot;
    private TextView realreviewEditor;
    private TextView realreviewViewcount;
    private ImageView realreviewScrap;

    private LinearLayout realreviewDesc;

    private long contentId;
    private boolean isLoved;

    public CustomRealReviewModel(Context context) {
        super(context);
        initView();

    }

    public CustomRealReviewModel(Context context, RequestManager requestManager) {
        super(context);
        mRequestManager = requestManager;
        initView();
    }

    public CustomRealReviewModel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_realreviews, this, false);
        addView(v);

        realreviewImg = (ImageView) findViewById( R.id.realreview_img );
        realreviewTitle = (TextView) findViewById( R.id.realreview_title );
        realreviewDot = (TextView) findViewById( R.id.realreview_dot );
        realreviewEditor = (TextView) findViewById( R.id.realreview_editor );
        realreviewViewcount = (TextView) findViewById( R.id.realreview_viewcount );
        realreviewScrap = (ImageView) findViewById( R.id.realreview_scrap );
        realreviewDesc = (LinearLayout) findViewById( R.id.realreview_desc );

        isLoved = false;
    }

    public long getContentId() {
        return contentId;
    }
    public void setContentId(long contentId) {
        this.contentId = contentId;
    }
    public void setRealreviewImg(String resUrl ) {
        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load( resUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( realreviewImg );
        } else {
            Glide.with( getContext() ).load( resUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( realreviewImg );
        }
    }
    public void setRealreviewTitle( String resString ) {
        realreviewTitle.setText( resString );
    }
    public void setRealreviewEditor( String resString ) {
        realreviewDot.setVisibility( VISIBLE );
        realreviewEditor.setText( resString );
    }
    public void setRealreviewViewcount( String resString ) {
        realreviewViewcount.setText( resString );
    }
    public void setRealreviewSize( int size ) {
        realreviewImg.getLayoutParams().width = size;
        realreviewImg.getLayoutParams().height = 2 * size / 3;
        realreviewTitle.getLayoutParams().width = size;
        realreviewEditor.setMaxWidth( size - SizeConverter.dpToPx( 68 ) );
        realreviewDesc.getLayoutParams().width = size;
    }

    public ImageView getRealreviewScrap() {
        return realreviewScrap;
    }
    public boolean isLoved() {
        return isLoved;
    }
    public void setRealreviewLoved( boolean isLoved ) {
        this.isLoved = isLoved;
        if( isLoved ) {
            realreviewScrap.setImageResource( R.drawable.scrap_on );
        } else {
            realreviewScrap.setImageResource( R.drawable.scrap_off );
        }
    }
}
