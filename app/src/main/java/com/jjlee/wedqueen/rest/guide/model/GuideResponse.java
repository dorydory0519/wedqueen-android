package com.jjlee.wedqueen.rest.guide.model;

import com.jjlee.wedqueen.rest.recommend.model.User;

import java.util.List;

public class GuideResponse {
    private KeywordGuide keywordGuide;

    public KeywordGuide getKeywordGuide() {
        return keywordGuide;
    }

    public void setKeywordGuide(KeywordGuide keywordGuide) {
        this.keywordGuide = keywordGuide;
    }
}
