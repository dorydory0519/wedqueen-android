package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexboxLayout;
import com.jjlee.wedqueen.R;

import java.util.List;

/**
 * Created by esmac on 2017. 12. 13..
 */

public class CustomImageGoodsModel extends LinearLayout {

    private RelativeLayout relativeImgGoods;
    private ImageView goodsImg;
    private TextView goodsTitle;
    private FlexboxLayout goodsHashTagWrapper;
    private long sectorItemId;

    public CustomImageGoodsModel(Context context) {
        super(context);
        initView();
    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_image_goods, this, false);
        addView(v);

        relativeImgGoods = (RelativeLayout) findViewById( R.id. relative_img_goods );
        goodsImg = (ImageView) findViewById( R.id.goods_img );
        goodsTitle = (TextView) findViewById( R.id.goods_title );
        goodsHashTagWrapper = (FlexboxLayout) findViewById( R.id.goods_hashTag_wrapper );

    }

    public void setGoodsImg( String resUrl ) {
        Glide.with( getContext() ).load( resUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( goodsImg );
    }
    public void setGoodsImg( String resUrl, int imgHeight, float imgRatio ) {
        relativeImgGoods.getLayoutParams().width = (int)( imgHeight / imgRatio );
        relativeImgGoods.getLayoutParams().height = imgHeight;
        Glide.with( getContext() ).load( resUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( goodsImg );
    }

    public void setGoodsTitle( String resString ) {
        goodsTitle.setText( resString );
    }

    public void setGoodsHashTagWrapper( List<String> hashTags ) {

        for( String hashTag : hashTags ) {

            CustomEventTagModel customEventTagModel = new CustomEventTagModel( getContext() );
            customEventTagModel.setEventTag( hashTag );

            goodsHashTagWrapper.addView( customEventTagModel );

        }
    }

    public long getSectorItemId() {
        return sectorItemId;
    }

    public void setSectorItemId(long sectorItemId) {
        this.sectorItemId = sectorItemId;
    }
}
