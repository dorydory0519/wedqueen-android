package com.jjlee.wedqueen.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageButton;

/**
 * Created by JJLEE on 2016. 5. 26..
 */
public class SigninButton extends ImageButton {

    private float initialTranslationX;
    private float initialTranslationY;
    private boolean isActivated = false;

    public SigninButton(Context context) {
        super(context);
    }

    public SigninButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SigninButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public float getInitialTranslationX() {
        return initialTranslationX;
    }

    public void setInitialTranslationX(float initialTranslationX) {
        this.initialTranslationX = initialTranslationX;
    }

    public float getInitialTranslationY() {
        return initialTranslationY;
    }

    public void setInitialTranslationY(float initialTranslationY) {
        this.initialTranslationY = initialTranslationY;
    }

    @Override
    public boolean isActivated() {
        return isActivated;
    }

    public void setIsActivated(boolean isActivated) {
        this.isActivated = isActivated;
    }
}
