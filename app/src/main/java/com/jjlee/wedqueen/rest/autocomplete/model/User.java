package com.jjlee.wedqueen.rest.autocomplete.model;

/**
 * Created by esmac on 2017. 9. 21..
 */

public class User extends AutoCompleteSet {
    private int userId;
    private String name;
    private String image;

    public User( int userId, String name, String image ) {
        this.userId = userId;
        this.name = name;
        this.image = image;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
