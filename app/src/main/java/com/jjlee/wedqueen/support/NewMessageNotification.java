package com.jjlee.wedqueen.support;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.jjlee.wedqueen.R;

/**
 * Helper class for showing and canceling new message
 * notifications.
 * <p/>
 * This class makes heavy use of the {@link NotificationCompat.Builder} helper
 * class to create notifications in a backward-compatible way.
 */
public class NewMessageNotification {
    /**
     * The unique identifier for this type of notification.
     */
    public static final int NOTIFICATION_ID = 1;
    private static final String NOTIFICATION_TAG = "NewMessage";
    private static final String PUSH_CHANNEL_ID = "wedqueen_channel_01";

    /**
     * Shows the notification, or updates a previously shown notification of
     * this type, with the given parameters.
     *
     * @see #cancel(Context)
     */
    public static void notify(final Context context, final PendingIntent contentIntent,
                              final String msg, final int number, final boolean isNotiAll) {
        final Resources res = context.getResources();

        // This image is used as the notification's large icon (thumbnail).
        final Bitmap picture = BitmapFactory.decodeResource(res, R.drawable.notification_large_new);

        final String ticker = msg;
        final String title = res.getString(R.string.notification_title);
        final String text = msg;

        int notiType = isNotiAll ? 0 : Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND;
        long[] vibratePattern = new long[] {0, 50};

        NotificationCompat.Builder builder;
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ) {
            builder = new NotificationCompat.Builder(context)
                    // Set appropriate defaults for the notification light, sound,
                    // and vibration.
                    .setDefaults(notiType)
                    .setLights(0xFFAAFF00, 1500, 250)
                    // Set required fields, including the small icon, the
                    // notification title, and text.
                    .setSmallIcon(R.drawable.ic_stat_new_message)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setLargeIcon(picture)
                    .setTicker(ticker)
                    .setNumber(number)
                    .setAutoCancel(true)
                    // Set the pending intent to be initiated when the user touches the notification.
                    .setContentIntent(contentIntent)
//                .setVibrate(vibratePattern)
                    // Android Oreo Version부터 채널을 추가해주어야 함
                    .setChannelId( PUSH_CHANNEL_ID )
                    // Show expanded text content on devices running Android 4.1 or later.
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(text)
                            .setBigContentTitle(title)
                            .setSummaryText(""));
        } else {
            builder = new NotificationCompat.Builder(context)
                    // Set appropriate defaults for the notification light, sound,
                    // and vibration.
                    .setDefaults(notiType)
                    .setLights(0xFFAAFF00, 1500, 250)
                    // Set required fields, including the small icon, the
                    // notification title, and text.
                    .setSmallIcon(R.drawable.ic_stat_new_message)
                    .setContentTitle(title)
                    .setContentText(text)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setLargeIcon(picture)
                    .setTicker(ticker)
                    .setNumber(number)
                    .setAutoCancel(true)
                    // Set the pending intent to be initiated when the user touches the notification.
                    .setContentIntent(contentIntent)
//                .setVibrate(vibratePattern)
                    // Show expanded text content on devices running Android 4.1 or later.
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(text)
                            .setBigContentTitle(title)
                            .setSummaryText(""));
        }


        Notification noti = builder.build();
        noti.flags |= Notification.FLAG_SHOW_LIGHTS;
        notify(context, noti);
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    private static void notify(final Context context, final Notification notification) {
        final NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ) {
            NotificationChannel channel = new NotificationChannel( PUSH_CHANNEL_ID, "wedqueen_channel_01", NotificationManager.IMPORTANCE_DEFAULT );
            nm.createNotificationChannel( channel );
        }

        nm.notify(NOTIFICATION_TAG, 0, notification);
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    public static void cancel(final Context context) {
        final NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(NOTIFICATION_TAG, 0);
    }
}
