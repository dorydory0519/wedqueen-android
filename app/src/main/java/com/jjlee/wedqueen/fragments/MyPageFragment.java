package com.jjlee.wedqueen.fragments;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.AccountActivity;
import com.jjlee.wedqueen.ContentEditActivity;
import com.jjlee.wedqueen.DDaySettingActivity;
import com.jjlee.wedqueen.FollowActivity;
import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MainActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.user.controller.UserController;
import com.jjlee.wedqueen.rest.user.model.UserInfoResponse;
import com.jjlee.wedqueen.rest.user.model.UserRestDto;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.utils.CalendarUtil;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyPageFragment extends Fragment {

    private RequestManager mGlideRequestManager;

    public static final String TABTYPE = "tab_type";

    protected String tabType;
    private TextView contenteditBtn;

    private UserController userController;

    private MyPageAdapter myPageAdapter;

    protected View rootView;

    private Prefs prefs;
    private ImageView userImg;
    private TextView userNickName;
    private TextView userDday;
//    private TextView userPoint;
    private TextView badgeNoti;

    private TextView followerCnt;
    private TextView followingCnt;

    public MyPageFragment() {} // Required empty public constructor

    public static void putFilterRelatedArgument(Bundle args, String tabType) {
        args.putString(TABTYPE, tabType);
    }

    public static MyPageFragment newInstance(String tabType) {
        MyPageFragment fragment = new MyPageFragment();
        Bundle args = new Bundle();
        putFilterRelatedArgument(args, tabType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TABTYPE, tabType);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGlideRequestManager = Glide.with( this );

        Bundle args = getArguments();

        if(savedInstanceState == null) {
            tabType = args.getString(TABTYPE);
        } else {
            tabType = savedInstanceState.getString(TABTYPE, null);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mypage, container, false);
        rootView = view;

        userController = new UserController();

        userImg = (ImageView) view.findViewById(R.id.user_img);
        userImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOtherActivity( DDaySettingActivity.class, Constants.REQ_CODE_OPEN_DDAYSETTING_ACTIVITY );
            }
        });

        userNickName = (TextView) view.findViewById(R.id.user_nickname);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics( displayMetrics );
        userNickName.setMaxWidth( displayMetrics.widthPixels - SizeConverter.dpToPx( 220 ) );
        userNickName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOtherActivity( DDaySettingActivity.class, Constants.REQ_CODE_OPEN_DDAYSETTING_ACTIVITY );
            }
        });

        userDday = (TextView) view.findViewById(R.id.user_dday);
        userDday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOtherActivity( DDaySettingActivity.class, Constants.REQ_CODE_OPEN_DDAYSETTING_ACTIVITY );
            }
        });

//        userPoint = (TextView) view.findViewById(R.id.user_point);

        badgeNoti = (TextView) view.findViewById(R.id.badge_noti);

        followerCnt = (TextView) view.findViewById( R.id.follower_cnt );
        followingCnt = (TextView) view.findViewById( R.id.following_cnt );

        prefs = new Prefs( getActivity() );
        getUserData();


        view.findViewById(R.id.noti_link).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                badgeNoti.setVisibility( View.GONE );
                MyApplication.getGlobalApplicationContext().setBadgeCount( 0 );
                startFullscreenActivity( Constants.NOTI_URL );
            }
        });


//        view.findViewById(R.id.pointshop_link).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startFullscreenActivity( Constants.POINTSHOP_URL );
//            }
//        });

        view.findViewById( R.id.followlist_link ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOtherActivity( FollowActivity.class );
            }
        });


        contenteditBtn = (TextView)view.findViewById(R.id.contentedit_btn);
        contenteditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOtherActivity( ContentEditActivity.class );
            }
        });


        view.findViewById(R.id.drawer_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).toggleDrawerLayout();
            }
        });


        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.mypage_tablayout);
        ViewPager mypageViewPager = (ViewPager) view.findViewById(R.id.mypage_viewpager);

        myPageAdapter = new MyPageAdapter( getActivity().getSupportFragmentManager() );
        mypageViewPager.setAdapter( myPageAdapter );
        mypageViewPager.setOffscreenPageLimit( 2 );
        mypageViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                switch ( position ) {
                    case 0 :
                        contenteditBtn.setVisibility( View.VISIBLE );
                        break;
                    case 1 :
                        contenteditBtn.setVisibility( View.GONE );
                        break;
                    case 2 :
                        contenteditBtn.setVisibility( View.GONE );
                        break;
//                    case 3 :
//                        contenteditBtn.setVisibility( View.GONE );
//                        break;
//                    case 4 :
//                        contenteditBtn.setVisibility( View.GONE );
//                        break;
                }
                super.onPageSelected(position);
            }
        });

        tabLayout.setupWithViewPager( mypageViewPager );
//        tabLayout.getTabAt( 0 ).setText( "예산" );
        tabLayout.getTabAt( 0 ).setText( "리얼웨딩" );
        tabLayout.getTabAt( 1 ).setText( "청첩장" );
        tabLayout.getTabAt( 2 ).setText( "저장" );
//        tabLayout.getTabAt( 4 ).setText( "예약" );

        return view;
    }

    public void getUserData() {

        if( ObjectUtils.isEmpty( userController ) ) {
            userController = new UserController();
        }

        Call<UserInfoResponse> userRestInfoCall = userController.getUserInfo();
        userRestInfoCall.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {

                    UserInfoResponse userInfoResponse = response.body();

                    UserRestDto userRestDto = userInfoResponse.getUserInfo().getUser();

                    // User Image Setting
                    String userImageUrl = userRestDto.getImageUrl();
                    mGlideRequestManager.load( userImageUrl ).bitmapTransform( new CropCircleTransformation( getActivity() ) ).placeholder( R.drawable.default_user_icon ).into( userImg );
                    MyApplication.getGlobalApplicationContext().setUserImageUrl( userImageUrl );

                    // User Nickname Setting
                    String userNickname = userRestDto.getNickname();
                    userNickName.setText( userNickname );
                    MyApplication.getGlobalApplicationContext().setUserNickName( userNickname );

                    // User Dday Setting
                    String userDdayString = CalendarUtil.getdDayString( userInfoResponse.getUserInfo().getMarriageDate() );
                    SpannableString content = new SpannableString( userDdayString );
                    content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                    userDday.setText(content);

                    // User Point Setting
                    int point = userRestDto.getPoint();
//                    userPoint.setText( point + "포인트" );
                    MyApplication.getGlobalApplicationContext().setUserPoint( point );

                    followerCnt.setText( userInfoResponse.getUserInfo().getFollowerCount() + "" );
                    followingCnt.setText( userInfoResponse.getUserInfo().getFollowingCount() + "" );

                    // User NotiCount Setting
                    int notiCount = userRestDto.getNotificationCount();
                    badgeNoti.setText( notiCount + "" );
                    if( notiCount < 1 ) {
                        badgeNoti.setVisibility( View.GONE );
                    } else {
                        badgeNoti.setVisibility( View.VISIBLE );
                    }

                } else {
                    Log.e( "WQTEST", "response body is null" );
                    setDefaultUserData();
                }
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                setDefaultUserData();
            }
        });
    }

    private void setDefaultUserData() {

        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            // get UserImage
            if( !ObjectUtils.isEmpty( MyApplication.getGlobalApplicationContext().getUserImageUrl() ) ) {
                mGlideRequestManager.load( MyApplication.getGlobalApplicationContext().getUserImageUrl() ).bitmapTransform( new CropCircleTransformation( getActivity() ) ).placeholder( R.drawable.default_user_icon ).into( userImg );
            }

            // get UserNickname
            String strUserNickName = MyApplication.getGlobalApplicationContext().getUserNickName();
            if( !ObjectUtils.isEmpty( strUserNickName ) ) {
                userNickName.setText( strUserNickName );
            }

            // get UserDday
            String userDdayString = "";
            if( !ObjectUtils.isEmpty( prefs.getDdayDate() ) ) {
                userDdayString = CalendarUtil.getdDayString( prefs.getDdayDate() );
            }
            SpannableString content = new SpannableString( userDdayString );
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            userDday.setText(content);

            // get UserPoint
            int iUserPoint = MyApplication.getGlobalApplicationContext().getUserPoint();
//                userPoint.setText( iUserPoint + "포인트" );

            // get notification badge
            int notiCount = MyApplication.getGlobalApplicationContext().getBadgeCount();
            badgeNoti.setText( notiCount + "" );
            if( notiCount < 1 ) {
                badgeNoti.setVisibility( View.GONE );
            } else {
                badgeNoti.setVisibility( View.VISIBLE );
            }
        } else {
            mGlideRequestManager.load( R.drawable.default_user_icon ).bitmapTransform( new CropCircleTransformation( getActivity() ) ).into( userImg );

            userNickName.setText( "웨딩의 여신" );

            SpannableString content = new SpannableString( "D-100" );
            content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
            userDday.setText(content);

        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void refresh() {

        getUserData();
        refreshWeddingDiary();
        refreshInnerWebview();

//        FragmentTransaction transaction = getFragmentManager().beginTransaction();
//        transaction.detach( this ).attach( this ).commit();

    }

    public void refreshWeddingDiary() {
        // Wedding Diary TAB Refresh
        if( !ObjectUtils.isEmpty( myPageAdapter ) ) {
            ((RealWeddingFragment)myPageAdapter.getRegisteredFragment( 0 )).getMyRealWeddingsFromServer();
        }
    }

    public void refreshInnerWebview() {
        if( !ObjectUtils.isEmpty( myPageAdapter ) ) {
            // CheckList TAB refresh
//            ((FilterableWebviewFragment)myPageAdapter.getRegisteredFragment( 0 )).refresh();
            // Invitation TAB refresh
            ((FilterableWebviewFragment)myPageAdapter.getRegisteredFragment( 1 )).refresh();
            // Scrap TAB refresh
            ((FilterableWebviewFragment)myPageAdapter.getRegisteredFragment( 2 )).refresh();
            // SurveyList TAB refresh
//            ((FilterableWebviewFragment)myPageAdapter.getRegisteredFragment( 4 )).refresh();
        }
    }


    public class MyPageAdapter extends FragmentStatePagerAdapter {

        SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

        public MyPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch ( position ) {
//                case 0 : // CheckList TAB
//                    return FilterableWebviewFragment.newInstance("checklist", "", Constants.CHECKLIST_URL, position);
                case 0 : // Wedding Diary TAB
                    return RealWeddingFragment.newInstance( "Wedding Diary TAB" );
                case 1 : // Invitation TAB
                    return FilterableWebviewFragment.newInstance( "invitation", "", Constants.MOBILE_INVITATION_URL, position );
                case 2 : // Scrap TAB
                    return FilterableWebviewFragment.newInstance("scrap", "", Constants.SCRAP_URL, position);
//                case 4 : // SurveyList TAB
//                    return FilterableWebviewFragment.newInstance("inquiry", "", Constants.INQUIRY_URL, position);

            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }

        @Override
        public int getItemPosition(Object item) {
            Fragment fragment = (Fragment) item;

            return POSITION_NONE;

        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // 화면이 보여질 때마다 새로 통신
        if( isVisibleToUser ) {
            getUserData();

            Answers.getInstance().logContentView(new ContentViewEvent()
                    .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                    .putContentType( "Fragment" )
                    .putContentName( "myinfo_tab" ) );
        }
    }

    private void startFullscreenActivity(String url) {
        Intent intent = new Intent(getActivity(), FullscreenActivity.class);
        intent.putExtra("URL", url);
        getActivity().startActivityForResult(intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY);
    }

    private void startOtherActivity( Class<?> activityClass ) {
        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            Intent intent = new Intent( getActivity(), activityClass );
            startActivity( intent );
        } else {
            Intent intent = new Intent( getActivity(), AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
        }
    }

    private void startOtherActivity( Class<?> activityClass, int requestCode ) {
        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            Intent intent = new Intent( getActivity(), activityClass );
            getActivity().startActivityForResult( intent, requestCode );
        } else {
            Intent intent = new Intent( getActivity(), AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            intent.putExtra( Constants.LOGIN_FROM, "mypage_btn_profile_image" );
            getActivity().startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
        }
    }

}
