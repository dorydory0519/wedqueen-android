package com.jjlee.wedqueen.rest.test.service;

import com.jjlee.wedqueen.rest.test.model.Test;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by LeeJungYi on 2016-04-01.
 */
public interface TestService {
    @Headers("Accept: application/json")

    @POST("crud/test")
    Call<Test> save(@Body Test test);

//    @GET
//    Call<Test> findById(@Url String url);

    @GET("crud/test/{id}")
    Call<Test> findById(@Path("id") Long id);
}
