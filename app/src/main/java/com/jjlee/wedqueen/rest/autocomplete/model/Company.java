package com.jjlee.wedqueen.rest.autocomplete.model;

/**
 * Created by esmac on 2017. 9. 21..
 */

public class Company extends AutoCompleteSet {
    private String name;
    private String image;
    private String address;
    private String url;

    public Company(String name, String image, String address, String url) {
        this.name = name;
        this.image = image;
        this.address = address;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
