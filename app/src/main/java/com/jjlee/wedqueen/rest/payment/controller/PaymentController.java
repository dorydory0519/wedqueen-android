package com.jjlee.wedqueen.rest.payment.controller;

import com.jjlee.wedqueen.rest.payment.model.PaymentResponse;
import com.jjlee.wedqueen.rest.payment.service.PaymentService;
import com.jjlee.wedqueen.rest.survey.model.SurveyResponse;
import com.jjlee.wedqueen.rest.survey.service.SurveyService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class PaymentController extends RetrofitController {
    private PaymentService paymentService;

    public PaymentController() {
        super();
        paymentService = RETROFIT.create(PaymentService.class);
    }

    public Call<PaymentResponse> getPayment() {
        return paymentService.getPayment( "pointtab" );
    }

    public Call<PaymentResponse> getPayments() {
        return paymentService.getPayments();
    }

}
