package com.jjlee.wedqueen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import com.jjlee.wedqueen.CustomViews.CustomPremiumAdModel;
import com.jjlee.wedqueen.rest.premium.controller.PremiumController;
import com.jjlee.wedqueen.rest.premium.model.PremiumAd;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PremiumAdsActivity extends AppCompatActivity {
    private LinearLayout bannersWrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_premiumads );

        bannersWrapper = findViewById( R.id.ads_wrapper );
        PremiumController premiumController = new PremiumController();
        Call<List<PremiumAd>> call = premiumController.getPremiumAds();
        call.enqueue(new Callback<List<PremiumAd>>() {
            @Override
            public void onResponse(Call<List<PremiumAd>> call, Response<List<PremiumAd>> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    List<PremiumAd> premiumAds = response.body();

                    if( premiumAds.size() > 0 ) {
                        for( PremiumAd premiumAd : premiumAds ) {
                            CustomPremiumAdModel customPremiumAdModel = new CustomPremiumAdModel( PremiumAdsActivity.this );
                            customPremiumAdModel.setAdImg( premiumAd.getImage() );
                            customPremiumAdModel.setAdTitle( premiumAd.getTitle() );
                            customPremiumAdModel.setAdAux( premiumAd.getAuxName() );
                            customPremiumAdModel.setTag( premiumAd.getLinkUrl() );
                            customPremiumAdModel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent( PremiumAdsActivity.this, FullscreenActivity.class );
                                    intent.putExtra( "URL", v.getTag() + "" );
                                    startActivityForResult( intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY );
                                }
                            });

                            bannersWrapper.addView( customPremiumAdModel );
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PremiumAd>> call, Throwable t) {

            }
        });

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
