package com.jjlee.wedqueen.rest.region.controller;

import com.jjlee.wedqueen.rest.region.model.RegionResponse;
import com.jjlee.wedqueen.rest.region.service.RegionService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class RegionController extends RetrofitController {
    private RegionService regionService;

    public RegionController() {
        super();
        regionService = RETROFIT.create(RegionService.class);
    }

    public Call<RegionResponse> getRegions() {
        return regionService.getRegions();
    }

}
