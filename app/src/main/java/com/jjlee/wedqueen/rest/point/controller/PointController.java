package com.jjlee.wedqueen.rest.point.controller;

import com.jjlee.wedqueen.rest.point.model.PointResponse;
import com.jjlee.wedqueen.rest.point.service.PointService;
import com.jjlee.wedqueen.rest.popup.model.PopupResponse;
import com.jjlee.wedqueen.rest.popup.service.PopupService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class PointController extends RetrofitController {
    private PointService pointService;

    public PointController() {
        super();
        pointService = RETROFIT.create(PointService.class);
    }

    public Call<PointResponse> getMyPoint() {
        return pointService.getMyPoint();
    }

}
