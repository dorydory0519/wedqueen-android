package com.jjlee.wedqueen.rest.tip.model;

import java.util.List;

public class TipResponse {

    private Tip tip;

    public Tip getTip() {
        return tip;
    }

    public void setTip(Tip tip) {
        this.tip = tip;
    }
}
