package com.jjlee.wedqueen.rest.autocomplete.model;

/**
 * Created by esmac on 2017. 9. 21..
 */

public class HashTag extends AutoCompleteSet {
    private String name;
    private int count;
    private String url;

    public HashTag(String name, int count, String url) {
        this.name = name;
        this.count = count;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
