package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;

import jp.wasabeef.glide.transformations.CropCircleTransformation;


public class CustomRecommendUserModel extends RelativeLayout {

    private RequestManager mRequestManager;

    private ImageView recommendUserImg;
    private TextView recommendUserNickName;

    public CustomRecommendUserModel(Context context) {
        super(context);
        initView();

    }

    public CustomRecommendUserModel(Context context, RequestManager mRequestManager) {
        super(context);
        this.mRequestManager = mRequestManager;
        initView();

    }

    public CustomRecommendUserModel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomRecommendUserModel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_recommenduser, this, false);
        addView(v);

        recommendUserImg = (ImageView) findViewById(R.id.recommenduser_img);
        recommendUserNickName = (TextView)findViewById(R.id.recommenduser_nickname);

    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.RecommendUserModel);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.RecommendUserModel, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {

        String recommendUserImg_resID = typedArray.getString(R.styleable.RecommendUserModel_recommenduser_img);
        setRecommendUserImg( recommendUserImg_resID );

        String recommendUserNickName_resID = typedArray.getString(R.styleable.RecommendUserModel_recommenduser_nickname);
        setRecommendUserNickName( recommendUserNickName_resID );

        typedArray.recycle();
    }

    public void setRecommendUserImg(String resUrl) {
        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load(resUrl).bitmapTransform(new CropCircleTransformation(getContext())).placeholder( R.drawable.default_user_icon ).into(recommendUserImg);
        }
    }
    public void setRecommendUserNickName(String resString) {
        recommendUserNickName.setText( resString );
    }

}
