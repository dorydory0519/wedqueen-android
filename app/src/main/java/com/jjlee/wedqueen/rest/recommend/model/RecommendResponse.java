package com.jjlee.wedqueen.rest.recommend.model;

import com.jjlee.wedqueen.rest.region.model.Region;

import java.util.List;

public class RecommendResponse {
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
