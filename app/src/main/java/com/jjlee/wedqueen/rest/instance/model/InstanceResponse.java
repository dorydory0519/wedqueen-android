package com.jjlee.wedqueen.rest.instance.model;


import com.jjlee.wedqueen.rest.tutorial.model.TutorialDetail;

import java.util.List;

public class InstanceResponse {
    private List<Instance> instances;

    public List<Instance> getInstances() {
        return instances;
    }

    public void setInstances(List<Instance> instances) {
        this.instances = instances;
    }
}

