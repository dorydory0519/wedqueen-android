package com.jjlee.wedqueen.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.AccountActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.app.model.TabCategory;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.CustomViews.CustomWebChromeClient;
import com.jjlee.wedqueen.CustomViews.CustomWebviewClient;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.support.WebviewJavascriptInterface;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;
import com.jjlee.wedqueen.view.NestedWebview;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by JJLEE on 2016. 4. 24..
 */

public class FilterableWebviewFragment extends FilterableFragment {

    private static final String INITIAL_URL = "initial_url";

    private static final Map<String, String> customHeader = new HashMap<>();

    private Prefs prefs;

//    private NestedWebview mWebview;
    private WebView mWebview;
    private String initialUrl;
    private String currentUrl;

    private boolean mFirstLoaded = false; // 처음 로딩이 되었는지 여부 판단
    public boolean isFullyLoaded = false; // 처음 로딩이 모두 되었는지 여부 판단

    private View fabButton;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(INITIAL_URL, initialUrl);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState != null) {
            initialUrl  = savedInstanceState.getString(INITIAL_URL, null);
        }

    }

    public static FilterableWebviewFragment newInstance(String tabType, String initialCategory, String initialUrl, int index) {
        FilterableWebviewFragment filterableWebviewFragment = new FilterableWebviewFragment();
        Bundle args = new Bundle();
        putFilterRelatedArgument(args, tabType, initialCategory);
        args.putString(INITIAL_URL, initialUrl);
        filterableWebviewFragment.setArguments(args);
        return filterableWebviewFragment;
    }

    @Override
    public void selectCategory(View view) {
        if(isFullyLoaded) {
            super.selectCategory(view);
            switch(tabType) {
                case "contentfeed":
                    mWebview.loadUrl("javascript:androidContentFeedCategoryClickAction('"
                            + tabCategories.get(selectedCategoryIndex).getCategoryName()
                            + "');");
                    break;

                case "hotdeal":
                    mWebview.loadUrl("javascript:androidContentHotdealCategoryClickAction('"
                            + tabCategories.get(selectedCategoryIndex).getCategoryName()
                            + "');");
                    break;

                case "weddingtalk":
                    mWebview.loadUrl("javascript:androidContentWeddingtalkCategoryClickAction('"
                            + tabCategories.get(selectedCategoryIndex).getCategoryName()
                            + "');");

                    if(tabCategories.get(selectedCategoryIndex).getCategoryName().equals("groupTalk")) {
                        fabButton.setVisibility(View.GONE);
                    } else { // public talk
                        fabButton.setVisibility(View.VISIBLE);
                    }
                    break;

                case "mypage":
                    mWebview.loadUrl("javascript:androidMypageCategoryClickAction('"
                            + tabCategories.get(selectedCategoryIndex).getCategoryName()
                            + "');");
                    break;

                case "estimation":
                    mWebview.loadUrl("javascript:androidEstimationPageCategoryClickAction('"
                            + tabCategories.get(selectedCategoryIndex).getCategoryName()
                            + "');");
                    break;

                default:
                    break;
            }
        }
//        else {
//            Snackbar.make(this.getView().findViewById(R.id.tab_main_content_wrapper), "페이지가 로딩중입니다. 잠시만 기다려주세요!", Snackbar.LENGTH_SHORT).show();
//        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        initialUrl = args.getString(INITIAL_URL);
        currentUrl = initialUrl;

        prefs = new Prefs( getActivity() );

        customHeader.put("deviceType", "android");
        customHeader.put("version", MyApplication.getGlobalApplicationContext().getVersionName() );
//        mWebview = new NestedWebview(getContext());
        mWebview = new WebView( getContext() );

        mWebview.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT < 19) {
            mWebview.getSettings().setPluginState(WebSettings.PluginState.ON);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            mWebview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        mWebview.setWebViewClient(new CustomWebviewClient(this));
        mWebview.setWebChromeClient(new CustomWebChromeClient(this));

        if( Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT ) {
            mWebview.setWebContentsDebuggingEnabled(true);
        }

        WeakReference wr = new WeakReference(getActivity());
        mWebview.addJavascriptInterface(new WebviewJavascriptInterface(wr), "androidJSI");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        if( !ObjectUtils.isEmpty( mWebview ) && ObjectUtils.isEmpty( mWebview.getParent() ) ) {
            mSwipeRefreshLayout.addView(mWebview);
        }

        if("weddingtalk".equals(tabType)) {

            fabButton = view.findViewById(R.id.fab_button);
            fabButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(MyApplication.getGlobalApplicationContext().isLoggedIn()) {
                        startFullscreenActivity(Constants.WEDDING_TALK_URL + "/new");
                    } else {
                        Intent intent = new Intent(getActivity(), AccountActivity.class);
                        intent.putExtra(Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN);
                        getActivity().startActivityForResult(intent, Constants.REQ_CODE_LOGIN);
                    }
                }
            });

            if("groupTalk".equals(currentCategory)) {
                fabButton.setVisibility(View.GONE);
            } else { // public talk
                fabButton.setVisibility(View.VISIBLE);
            }


        }

        view.findViewById(R.id.retry_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideNetworkError();
                refresh();
            }
        });

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                hideNetworkError();
                mWebview.loadUrl(mWebview.getUrl(), customHeader);
                mWebview.scrollTo( 0, 0 );
                //refresh();
            }
        });
        mSwipeRefreshLayout.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if( mWebview.getScrollY() == 0 ) {
                    mSwipeRefreshLayout.setEnabled(true);
                } else {
                    mSwipeRefreshLayout.setEnabled(false);
                }
            }
        });


        return view;
    }

    /**
     * 유저에게 이 프래그먼트(페이지)가 보이는 순간을
     * 이 메서드에서 캐치한다
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && !mFirstLoaded) { // 이 프래그먼트가 보이는 순간에 로딩이 안되었다면
            refresh();
            mFirstLoaded = true;
        }
    }

    public void setLoadingProgress(int progress) {
        if(progress == 100) {
            isFullyLoaded = true;
        } else {
            isFullyLoaded = false;
        }
    }

    public void setCurrentUrl(String currentUrl) {
        this.currentUrl = currentUrl;
    }

    public String getShouldRefreshTo(String tabType, int selectedCategoryIndex) {
        if( ObjectUtils.isEmpty( prefs ) ) {
            return Constants.MY_PAGE_URL;
        }

        HashMap<String, ArrayList<TabCategory>> allTabCategories = prefs.getCurrentTabCategories();

        String categoryName;

        try {
            categoryName = allTabCategories.get(tabType).get(selectedCategoryIndex).getCategoryName();
        } catch (NullPointerException e) {
            if("checklist".equals(tabType)) {
                return Constants.CHECKLIST_URL;
            } else if("contentfeed".equals(tabType)) {
                return Constants.CONTENT_FEED_URL;
            } else if("weddingtalk".equals(tabType)) {
                return Constants.WEDDING_TALK_URL;
            } else if("estimation".equals(tabType)) {
                return Constants.ESTIMATION_URL;
            } else if("scrap".equals(tabType)) {
                return Constants.SCRAP_URL;
            } else if("inquiry".equals(tabType)) {
                return Constants.INQUIRY_URL;
            } else if("invitation".equals(tabType)) {
                return Constants.MOBILE_INVITATION_URL;
            } else { // mypage
                return Constants.MY_PAGE_URL + "/main";
            }
        }

        if("mypage".equals(tabType) && !MyApplication.getGlobalApplicationContext().isLoggedIn()) {
            return Constants.MY_PAGE_URL + "/main";
        }

        if("checklist".equals(tabType)) {
            return Constants.CHECKLIST_URL;
        } else if("contentfeed".equals(tabType)) {
            return Constants.CONTENT_FEED_URL + "?category=" + categoryName;
        } else if("weddingtalk".equals(tabType)) {
            return Constants.WEDDING_TALK_URL + "?talkType=" + categoryName;
        } else if("estimation".equals(tabType)) {
            return Constants.ESTIMATION_URL + "?category=" + categoryName;
        } else if("scrap".equals(tabType)) {
            return Constants.SCRAP_URL;
        } else if("inquiry".equals(tabType)) {
            return Constants.INQUIRY_URL;
        } else if("invitation".equals(tabType)) {
            return Constants.MOBILE_INVITATION_URL;
        } else { // mypage
            if( categoryName != null && categoryName.equals( "dday" ) ) {
                categoryName = "main";
            }
            return Constants.MY_PAGE_URL + "/" + categoryName;
        }
    }

    public void setmFirstLoaded(boolean mFirstLoaded) {
        this.mFirstLoaded = mFirstLoaded;
    }

    @Override
    public void refresh() {
        String requestedUrl = getShouldRefreshTo(tabType, selectedCategoryIndex);
        if( !ObjectUtils.isEmpty( mWebview ) ) {
            Log.e( "WQTEST3", "error here ( requestedUrl : " + requestedUrl + " ) " );
            mWebview.loadUrl( requestedUrl, customHeader );
        }

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Fragment" )
                .putContentName( "filterable_webview" )
                .putCustomAttribute( "requestedUrl", requestedUrl ) );
    }

    public WebView getWebview() {
        return mWebview;
    }

}
