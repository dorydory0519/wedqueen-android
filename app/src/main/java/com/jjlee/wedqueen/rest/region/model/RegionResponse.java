package com.jjlee.wedqueen.rest.region.model;

import java.util.List;

public class RegionResponse {
    private List<Region> region;

    public List<Region> getRegions() {
        return region;
    }

    public void setRegions(List<Region> regions) {
        this.region = region;
    }
}
