package com.jjlee.wedqueen.rest.love.service;

import com.jjlee.wedqueen.rest.common.model.CommonResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by JJLEE on 2016. 5. 7..
 */
public interface LoveService {
    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("love")
    Call<CommonResponse> loveRequest (@Field("lovableId")Long lovableId, @Field("type")String lovableType, @Field("_method")String method);

}
