package com.jjlee.wedqueen.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by JJLEE on 2016. 3. 30..
 */
public class NetworkUtil {
    public static final boolean CheckInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            NetworkInfo ni = cm.getActiveNetworkInfo();

            if (ni != null && ni.isAvailable() && ni.isConnected() ) {
                return true;
            } else {
                return false;
            }
        } catch( Exception e ) {
            return false;
        }
    }
}
