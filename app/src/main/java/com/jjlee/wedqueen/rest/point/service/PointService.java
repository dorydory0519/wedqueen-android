package com.jjlee.wedqueen.rest.point.service;

import com.jjlee.wedqueen.rest.point.model.PointResponse;
import com.jjlee.wedqueen.rest.popup.model.PopupResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface PointService {

    @Headers("Accept: application/json")
    @GET("api/v1/point")
    Call<PointResponse> getMyPoint();

}
