package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;


public class CustomPopularImgModel extends RelativeLayout {

    RequestManager mRequestManager;

    ImageView popularImgImg;
    ImageView popularImgColorFilter;
    TextView popularImgTitle;
    TextView popularImgHashTag;

    public CustomPopularImgModel(Context context) {
        super(context);
        initView();

    }

    public CustomPopularImgModel(Context context, RequestManager mRequestManager) {
        super(context);
        this.mRequestManager = mRequestManager;
        initView();

    }

    public CustomPopularImgModel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomPopularImgModel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_popularimg, this, false);
        addView(v);

        popularImgImg = (ImageView)findViewById(R.id.popularimg_img);
        popularImgColorFilter = (ImageView)findViewById(R.id.popular_colorfilter);
        popularImgTitle = (TextView) findViewById(R.id.popularimg_title);
        popularImgHashTag = (TextView)findViewById(R.id.popularimg_hashtag);

    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.PopularImgModel);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.PopularImgModel, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {

        String popularImgImg_resID = typedArray.getString(R.styleable.PopularImgModel_popularimg_img);
        setPopularImgImg( popularImgImg_resID );

        String popularImgTitle_resID = typedArray.getString(R.styleable.PopularImgModel_popularimg_title);
        setPopularImgTitle( popularImgTitle_resID );

        String popularImgHashTag_resID = typedArray.getString(R.styleable.PopularImgModel_popularimg_hashtag);
        setPopularImgHashTag( popularImgHashTag_resID );

        typedArray.recycle();
    }

    public void setPopularImgImg(String resUrl) {
        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load(resUrl).centerCrop().placeholder( R.color.image_placeholder_color ).into( popularImgImg );
        }
    }
    public void setPopularImgTitle(String resString) {
        popularImgTitle.setText( resString );
    }
    public void setPopularImgHashTag(String resString) {
        popularImgHashTag.setText( resString );
    }
    public void setPopularImgSize(int size) {
        popularImgImg.getLayoutParams().width = size;
        popularImgImg.getLayoutParams().height = size;
        popularImgTitle.getLayoutParams().width = size - SizeConverter.dpToPx( 20 );
        popularImgHashTag.getLayoutParams().width = size - SizeConverter.dpToPx( 20 );
        popularImgColorFilter.getLayoutParams().width = size;
        popularImgColorFilter.getLayoutParams().height = size;
    }
}
