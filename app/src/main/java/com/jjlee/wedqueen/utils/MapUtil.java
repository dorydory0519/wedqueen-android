package com.jjlee.wedqueen.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by JJLEE on 2016. 4. 4..
 */
public class MapUtil {
    public static Map<String, String> getMapFromObject(Object obj) {
        Map<String, String> map = new HashMap<>();

        Field[] fields = obj.getClass().getDeclaredFields();
        if (fields != null) {
            for (Field field : fields) {
                try {
                    field.setAccessible(true);
                    map.put(field.getName(), field.get(obj).toString());
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                    return null;
                } catch (NullPointerException e) { }
            }
        }

        return map;
    }

}
