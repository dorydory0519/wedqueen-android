package com.jjlee.wedqueen.rest.content.model;

import java.util.List;

public class PostContent {

    private List<ContentFragment> fragments;
    private String title;
    private List<String> hashTags;
    private Boolean isAutoSave;
    private String region;
    private String thumbnail;
    private String companyName;
    private String preparationDate;
    private int estimate;
    private String state;

    private Long surveyAnswerId;
    private Long paymentId;
    private Long tabCategoryId;


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<ContentFragment> getFragments() {
        return fragments;
    }

    public void setFragments(List<ContentFragment> fragments) {
        this.fragments = fragments;
    }

    public Boolean getAutoSave() {
        return isAutoSave;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getHashTags() {
        return hashTags;
    }

    public void setHashTags(List<String> hashTags) {
        this.hashTags = hashTags;
    }

    public Boolean isAutoSave() {
        return isAutoSave;
    }

    public void setAutoSave(Boolean autoSave) {
        isAutoSave = autoSave;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPreparationDate() {
        return preparationDate;
    }

    public void setPreparationDate(String preparationDate) {
        this.preparationDate = preparationDate;
    }

    public int getEstimate() {
        return estimate;
    }

    public void setEstimate(int estimate) {
        this.estimate = estimate;
    }

    public Long getSurveyAnswerId() {
        return surveyAnswerId;
    }

    public void setSurveyAnswerId(Long surveyAnswerId) {
        this.surveyAnswerId = surveyAnswerId;
    }

    public Long getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Long paymentId) {
        this.paymentId = paymentId;
    }

    public Long getTabCategoryId() {
        return tabCategoryId;
    }

    public void setTabCategoryId(Long tabCategoryId) {
        this.tabCategoryId = tabCategoryId;
    }

    @Override
    public String toString() {
        return "PostContent{" +
                "fragments=" + fragments +
                ", title='" + title + '\'' +
                ", hashTags=" + hashTags +
                ", isAutoSave=" + isAutoSave +
                ", region='" + region + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", companyName='" + companyName + '\'' +
                ", preparationDate='" + preparationDate + '\'' +
                ", estimate='" + estimate + '\'' +
                '}';
    }
}
