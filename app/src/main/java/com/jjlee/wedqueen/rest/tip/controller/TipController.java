package com.jjlee.wedqueen.rest.tip.controller;

import com.jjlee.wedqueen.rest.tip.model.TipResponse;
import com.jjlee.wedqueen.rest.tip.service.TipService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class TipController extends RetrofitController {
    private TipService tipService;

    public TipController() {
        super();
        tipService = RETROFIT.create(TipService.class);
    }

    public Call<TipResponse> getTip(String type, int page, String tabCategoryId) {
        return tipService.getTip(type, page, tabCategoryId);
    }

}
