package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjlee.wedqueen.R;

public class CustomCheckedTextView extends LinearLayout {

    private ImageView categoryCheck;
    private TextView categoryName;

    private String categoryValue;
    private boolean isSelected;

    public CustomCheckedTextView(Context context) {
        super(context);
        initView();
    }

    public CustomCheckedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomCheckedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CheckCategoryBtn );
        setTypeArray(typedArray);
    }
    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CheckCategoryBtn, defStyle, 0);
        setTypeArray(typedArray);
    }
    private void setTypeArray(TypedArray typedArray) {

        String strCategoryName = typedArray.getString( R.styleable.CheckCategoryBtn_category_name );
        categoryName.setText( strCategoryName );
        categoryValue = typedArray.getString( R.styleable.CheckCategoryBtn_category_value );

        typedArray.recycle();
    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService( infService );
        View v = li.inflate( R.layout.layout_checked_textview, this, false );

        isSelected = true;
        categoryCheck = v.findViewById( R.id.category_check );
        categoryName = v.findViewById( R.id.category_name );
        v.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isSelected = !isSelected;

                if( isSelected ) {
                    categoryCheck.setVisibility( VISIBLE );
                } else {
                    categoryCheck.setVisibility( INVISIBLE );
                }
            }
        });

        addView( v );

    }

    @Override
    public boolean isSelected() {
        return isSelected;
    }

    public String getCategoryValue() {
        return categoryValue;
    }
}