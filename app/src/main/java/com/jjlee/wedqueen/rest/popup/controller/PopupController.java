package com.jjlee.wedqueen.rest.popup.controller;

import com.jjlee.wedqueen.rest.popup.model.PopupResponse;
import com.jjlee.wedqueen.rest.popup.service.PopupService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class PopupController extends RetrofitController {
    private PopupService popupService;

    public PopupController() {
        super();
        popupService = RETROFIT.create(PopupService.class);
    }

    public Call<PopupResponse> getPopup() {
        return popupService.getPopup( "android" );
    }

    public Call<PopupResponse> getPopupV2() {
        return popupService.getPopupV2( "android" );
    }

}
