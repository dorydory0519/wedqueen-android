package com.jjlee.wedqueen.rest.recommend.service;

import com.jjlee.wedqueen.rest.recommend.model.RecommendResponse;
import com.jjlee.wedqueen.rest.region.model.RegionResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface RecommendService {
    @Headers("Accept: application/json")

    @GET("api/v1/user/recommend")
    Call<RecommendResponse> getRecommendUsers();

    @GET("api/v2/user/recommend")
    Call<RecommendResponse> getHomeRecommendUsers(@Query("filter") String filter);

}
