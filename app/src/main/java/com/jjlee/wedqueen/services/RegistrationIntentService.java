package com.jjlee.wedqueen.services;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmReceiver;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.account.controller.AccountController;
import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.Prefs;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 이 파일은 Instance ID (deviceToken) 를 가지고 토큰을 가져오는 작업을 한다.
 *
 */

public class RegistrationIntentService extends IntentService {

    private AccountController accountController;

    private static final String TAG = "RegistrationIntentService";
    private GcmReceiver mGcmReceiver;

    public RegistrationIntentService() {
        super(TAG);
    }


    @Override
    public void onCreate() {
        super.onCreate();
        accountController = new AccountController();

        mGcmReceiver = new GcmReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals(Constants.REGISTRATION_READY)) { // 액션이 READY일 경우
                } else if (action.equals(Constants.REGISTRATION_GENERATING)) { // 액션이 GENERATING일 경우
                } else if (action.equals(Constants.REGISTRATION_COMPLETE)) { // 액션이 COMPLETE일 경우
                    String token = intent.getStringExtra("token");
                    Prefs prefs = new Prefs(RegistrationIntentService.this);
                    if(prefs.getLastDeviceToken() == null || !prefs.getLastDeviceToken().equals(token)) {
                        prefs.setLastDeviceToken(token);
                        String adid = prefs.getAdid();
                        sendDeviceTokenToServer(token, adid);
                    } else {
                        stopSelf();
                    }
                }
            }
        };

//        mBroadcastReceiver = new WakefulBroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                String action = intent.getAction();
//                if (action.equals(Constants.REGISTRATION_READY)) { // 액션이 READY일 경우
//                } else if (action.equals(Constants.REGISTRATION_GENERATING)) { // 액션이 GENERATING일 경우
//                } else if (action.equals(Constants.REGISTRATION_COMPLETE)) { // 액션이 COMPLETE일 경우
//                    String token = intent.getStringExtra("token");
//                    Prefs prefs = new Prefs(RegistrationIntentService.this);
//                    if(prefs.getLastDeviceToken() == null || !prefs.getLastDeviceToken().equals(token)) {
//                        prefs.setLastDeviceToken(token);
//                        String adid = prefs.getAdid();
//                        sendDeviceTokenToServer(token, adid);
//                    } else {
//                        stopSelf();
//                    }
//                }
//            }
//        };
    }

    private void sendDeviceTokenToServer(String deviceToken, String advertisingId) {
        Call<CommonResponse> call = accountController.sendDeviceTokenToServer(deviceToken, advertisingId);
        call.enqueue(new Callback<CommonResponse>() {
            @Override
            public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                stopSelf();
            }

            @Override
            public void onFailure(Call<CommonResponse> call, Throwable t) {
                stopSelf();
            }
        });
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        LocalBroadcastManager.getInstance(this).registerReceiver( mGcmReceiver,
                new IntentFilter( Constants.REGISTRATION_READY ) );
        LocalBroadcastManager.getInstance(this).registerReceiver( mGcmReceiver,
                new IntentFilter( Constants.REGISTRATION_GENERATING ) );
        LocalBroadcastManager.getInstance(this).registerReceiver( mGcmReceiver,
                new IntentFilter( Constants.REGISTRATION_COMPLETE ) );
        return super.onStartCommand(intent, flags, startId);

    }


    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance( this ).unregisterReceiver( mGcmReceiver );
        super.onDestroy();
    }

    /**
     * GCM을 위한 Instance ID의 토큰을 생성하여 가져온다.
     * @param intent
     */
    @SuppressLint("LongLogTag")
    @Override
    protected void onHandleIntent(Intent intent) {
        // GCM Instance ID의 토큰을 가져오는 작업이 시작되면 LocalBroadcast로 GENERATING 액션을 알려준다.
        LocalBroadcastManager.getInstance(this)
                .sendBroadcast(new Intent(Constants.REGISTRATION_GENERATING));

        // GCM을 위한 Instance ID를 가져온다.
        InstanceID instanceID = InstanceID.getInstance(this);
        String token = null;
        try {
            synchronized (TAG) {
                // GCM 앱을 등록하고 획득한 설정파일인 google-services.json을 기반으로 SenderID를 자동으로 가져온다.
                String default_senderId = getString(R.string.gcm_sender_id);
                // GCM 기본 scope는 "GCM"이다.
                String scope = GoogleCloudMessaging.INSTANCE_ID_SCOPE;
                // Instance ID에 해당하는 토큰을 생성하여 가져온다.
                token = instanceID.getToken(default_senderId, scope, null);

                Log.i(TAG, "GCM Registration Token: " + token);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // GCM Instance ID에 해당하는 토큰을 획득하면 LocalBroadcast에 COMPLETE 액션을 알린다.
        // 이때 토큰을 함께 넘겨주어서 토큰 정보를 활용할 수 있도록 했다.
        Intent registrationComplete = new Intent( Constants.REGISTRATION_COMPLETE );
        registrationComplete.putExtra( "token", token );
        LocalBroadcastManager.getInstance(this).sendBroadcast( registrationComplete );
    }
}