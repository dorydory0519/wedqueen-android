package com.jjlee.wedqueen.rest.sector.model;

/**
 * Created by esmac on 2017. 12. 19..
 */

public class SectorItemLog {
    private Long sectorItemId;
    private String logType;
    private Long userId;

    public SectorItemLog() {
    }

    public SectorItemLog(Long sectorItemId, String logType, Long userId) {
        this.sectorItemId = sectorItemId;
        this.logType = logType;
        this.userId = userId;
    }

    public Long getSectorItemId() {
        return sectorItemId;
    }

    public void setSectorItemId(Long sectorItemId) {
        this.sectorItemId = sectorItemId;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
