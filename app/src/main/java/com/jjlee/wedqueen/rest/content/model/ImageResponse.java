package com.jjlee.wedqueen.rest.content.model;

import java.util.List;

/**
 * Created by JJLEE on 2016. 4. 23..
 */
public class ImageResponse {
    private List<Image> images;

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}
