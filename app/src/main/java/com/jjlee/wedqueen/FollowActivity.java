package com.jjlee.wedqueen;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.fragments.FollowFragment;
import com.tsengvn.typekit.TypekitContextWrapper;

/**
 * Created by esmac on 2018. 3. 9..
 */

public class FollowActivity extends AppCompatActivity {

    private ViewPager followViewPager;
    private FollowAdapter followAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "follow_list" ) );

        TabLayout tabLayout = (TabLayout) findViewById( R.id.follow_tablayout );
        followViewPager = (ViewPager) findViewById( R.id.follow_viewpager );

        followAdapter = new FollowAdapter( getSupportFragmentManager() );
        followViewPager.setAdapter( followAdapter );
        tabLayout.setupWithViewPager( followViewPager );
        tabLayout.getTabAt( 0 ).setText( "팔로워" );
        tabLayout.getTabAt( 1 ).setText( "팔로잉" );

    }

    public class FollowAdapter extends FragmentPagerAdapter {

        public FollowAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch ( position ) {
                case 0 :
                    return FollowFragment.newInstance( "follower" );
                case 1 :
                    return FollowFragment.newInstance( "following" );

            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
