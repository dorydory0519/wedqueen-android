package com.jjlee.wedqueen.rest.company.model;

import java.util.List;

/**
 * Created by JJLEE on 2016. 4. 23..
 */
public class CompanyResponse {
    private Boolean lastPage;
    private List<Company> companies;

    public Boolean getLastPage() {
        return lastPage;
    }

    public void setLastPage(Boolean lastPage) {
        this.lastPage = lastPage;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }
}
