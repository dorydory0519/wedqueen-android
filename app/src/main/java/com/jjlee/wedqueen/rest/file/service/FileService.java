package com.jjlee.wedqueen.rest.file.service;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

/**
 * Created by JJLEE on 2016. 5. 23..
 */
public interface FileService {
    @Multipart
    @POST
    Call<JsonObject> postLiveChatFileToServer(@Url String url, @Part MultipartBody.Part file);

}
