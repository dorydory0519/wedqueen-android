package com.jjlee.wedqueen.rest.test.controller;


import com.jjlee.wedqueen.rest.test.model.Test;
import com.jjlee.wedqueen.rest.test.service.TestService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

/**
 * Created by LeeJungYi on 2016-04-01.
 *
 *
 */

public class TestController extends RetrofitController {
    private TestService testService;

    public TestController() {
        super();
        testService = RETROFIT.create(TestService.class);
    }

    public Call<Test> getTest(long id) {
        return testService.findById(id);
    }

    public Call<Test> saveTest(Test test) {
        return testService.save(test);
//        return testService.save(new com.jjlee.wedqueen.rest.test.model.Test("wedding"));
    }
}
