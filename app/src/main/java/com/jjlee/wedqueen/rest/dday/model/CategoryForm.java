package com.jjlee.wedqueen.rest.dday.model;

public class CategoryForm {

    public CategoryForm() {
        this.dDayStart = 0;
        this.dDayEnd = 0;
    }

    private String title;

    private Type type;

    private Integer dDayStart;

    private Integer dDayEnd;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Integer getdDayStart() {
        return dDayStart;
    }

    public void setdDayStart(Integer dDayStart) {
        this.dDayStart = dDayStart;
    }

    public Integer getdDayEnd() {
        return dDayEnd;
    }

    public void setdDayEnd(Integer dDayEnd) {
        this.dDayEnd = dDayEnd;
    }
}
