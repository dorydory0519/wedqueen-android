package com.jjlee.wedqueen.receivers;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.RemoteViews;

import com.jjlee.wedqueen.MainActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.SplashActivity;
import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.rest.dday.controller.DDayController;
import com.jjlee.wedqueen.rest.dday.model.DDayDto;
import com.jjlee.wedqueen.rest.dday.model.DDayResponse;
import com.jjlee.wedqueen.rest.love.controller.LoveController;
import com.jjlee.wedqueen.support.Prefs;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by esmac on 2017. 5. 29..
 */

public class WidgetVer1Provider extends AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        appWidgetIds = appWidgetManager.getAppWidgetIds( new ComponentName( context, getClass() ) );
        for( int i=0; i < appWidgetIds.length; i++ ) {
            updateAppWidget( context, appWidgetManager, appWidgetIds[i] );
        }
    }

    public void updateAppWidget( Context context, AppWidgetManager appWidgetManager, int appWidgetId ) {

        RemoteViews updateViews = new RemoteViews( context.getPackageName(), R.layout.dday_widget_v1 );

        // 결혼예정일을 꺼내서 DDay 계산
//        Calendar targetDate = MyApplication.getGlobalApplicationContext().getdDayDate();
        Prefs prefs = new Prefs( context );
        Calendar targetDate = prefs.getDdayDate();
        if( targetDate != null ) {
            String sDday = "";
            Calendar nowDate = Calendar.getInstance();
            nowDate.set( Calendar.HOUR, 0 );
            nowDate.set( Calendar.MINUTE, 0 );
            nowDate.set( Calendar.SECOND, 0 );
            nowDate.set( Calendar.MILLISECOND, 0 );

            long nowMilli = nowDate.getTimeInMillis();
            long targetMilli = targetDate.getTimeInMillis();

            long subDate = ( targetMilli - nowMilli ) / ( 24 * 60 * 60 * 1000 );

            int iDday = (int)subDate;

            if( iDday > 0 ) {
                sDday = String.format( "D-%d", iDday );
            } else if( iDday < 0 ) {
                sDday = String.format( "D+%d", Math.abs( iDday ) );
            } else {
                sDday = "D-Day";
            }

            updateViews.setTextViewText( R.id.dday_text, sDday );
            updateViews.setTextViewTextSize( R.id.dday_text, TypedValue.COMPLEX_UNIT_DIP, 28 );
        } else {
            // 기본적으로 Widget을 추가할 때 Dday가 설정되어있지 않을 경우 튕겨주지만 여기도 설정가이드 추가
            updateViews.setTextViewText( R.id.dday_text, "DDay를 설정해주세요!" );
            updateViews.setTextViewTextSize( R.id.dday_text, TypedValue.COMPLEX_UNIT_DIP, 12 );
        }

        updateViews.setImageViewResource( R.id.dday_logo, R.drawable.logo_white );

        // Widget Click시 MainActivity로 이동하는 PendingIntent 설정
        Intent intent = new Intent( context, SplashActivity.class );
        PendingIntent pendingIntent = PendingIntent.getActivity( context, 0, intent, 0 );
        updateViews.setOnClickPendingIntent( R.id.dday_layout, pendingIntent );

        appWidgetManager.updateAppWidget( appWidgetId, updateViews );
    }
}
