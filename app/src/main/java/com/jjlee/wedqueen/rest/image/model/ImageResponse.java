package com.jjlee.wedqueen.rest.image.model;

import java.util.List;

/**
 * Created by esmac on 2017. 9. 19..
 */

public class ImageResponse {
    private List<String> fileNames;

    public List<String> getFileNames() {
        return fileNames;
    }

    public void setFileNames(List<String> fileNames) {
        this.fileNames = fileNames;
    }
}
