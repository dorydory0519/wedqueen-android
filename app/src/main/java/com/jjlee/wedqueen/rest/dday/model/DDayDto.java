package com.jjlee.wedqueen.rest.dday.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by esmac on 2017. 5. 29..
 */

public class DDayDto {

//    private Long id;

    private int dDay;

    private String dDayDate;

    private String remainingDay;

    private String ddayToString;

    private int progress;

    private long cBudget;

    private long expense;

    private int talkCount;

    private List<CategoryDto> categories = new ArrayList<>();

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

    public int getdDay() {
        return dDay;
    }

    public void setdDay(int dDay) {
        this.dDay = dDay;
    }

    public String getdDayDate() {
        return dDayDate;
    }

    public void setdDayDate(String dDayDate) {
        this.dDayDate = dDayDate;
    }

    public String getRemainingDay() {
        return remainingDay;
    }

    public void setRemainingDay(String remainingDay) {
        this.remainingDay = remainingDay;
    }

    public String getDdayToString() {
        return ddayToString;
    }

    public void setDdayToString(String ddayToString) {
        this.ddayToString = ddayToString;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public long getcBudget() {
        return cBudget;
    }

    public void setcBudget(long cBudget) {
        this.cBudget = cBudget;
    }

    public long getExpense() {
        return expense;
    }

    public void setExpense(long expense) {
        this.expense = expense;
    }

    public int getTalkCount() {
        return talkCount;
    }

    public void setTalkCount(int talkCount) {
        this.talkCount = talkCount;
    }

    public List<CategoryDto> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDto> categories) {
        this.categories = categories;
    }


}

