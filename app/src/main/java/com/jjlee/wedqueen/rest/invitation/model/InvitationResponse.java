package com.jjlee.wedqueen.rest.invitation.model;

/**
 * Created by esmac on 2018. 3. 21..
 */

public class InvitationResponse {
    private String code;
    private int invitationCount;
    private int invitePoint;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getInvitationCount() {
        return invitationCount;
    }

    public void setInvitationCount(int invitationCount) {
        this.invitationCount = invitationCount;
    }

    public int getInvitePoint() {
        return invitePoint;
    }

    public void setInvitePoint(int invitePoint) {
        this.invitePoint = invitePoint;
    }
}
