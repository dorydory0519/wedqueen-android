package com.jjlee.wedqueen.rest.account.service;
import com.jjlee.wedqueen.rest.account.model.Anonymous;
import com.jjlee.wedqueen.rest.account.model.GoogleOauthResponse;
import com.jjlee.wedqueen.rest.account.model.LoginCommonResponse;
import com.jjlee.wedqueen.rest.account.model.NotificationCountResponse;
import com.jjlee.wedqueen.rest.common.model.CommonResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by JJLEE on 2016. 4. 4..
 */

public interface AccountService {
    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("login/kakao")
    Call<LoginCommonResponse> kakaoLoggedIn (@FieldMap Map<String, String> map);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("login/facebook")
    Call<LoginCommonResponse> facebookLoggedIn(@FieldMap Map<String, String> map);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("login/google")
    Call<LoginCommonResponse> googleLoggedIn(@FieldMap Map<String, String> map);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("login/twitter")
    Call<LoginCommonResponse> twitterLoggedIn(@FieldMap Map<String, String> map);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("login/version")
    Call<LoginCommonResponse> anonymousLoggedIn(@FieldMap Map<String, String> map);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("account/logout")
    Call<Void> serverLogout(@Field("deviceToken") String deviceToken);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("account/deviceToken")
    Call<CommonResponse> sendDeviceTokenToServer(@Field("deviceToken") String deviceToken);

    @Headers("Accept: application/json")
    @FormUrlEncoded
    @POST("account/deviceToken")
    Call<CommonResponse> sendDeviceTokenToServer(@Field("deviceToken") String deviceToken, @Field("advertisingId") String advertisingId);

    @Headers("Accept: application/json")
    @POST("account/delete")
    Call<CommonResponse> unjoinUser();

    @Headers("Accept: application/json")
    @GET("noti/{notiId}")
    Call<NotificationCountResponse> countDownSingleNotification(@Path("notiId") String notiId);

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("anonymous")
    Call<Anonymous> postAnonymousDeviceToken(@Field("deviceToken") String deviceToken);

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @DELETE("anonymous")
    Call<CommonResponse> deleteAnonymousDeviceToken(@Field("uuid") String uuid);

    @FormUrlEncoded
    @Headers("Accept: application/json")
    @PUT("anonymous")
    Call<CommonResponse> updateAnonymousDeviceToken(@Field("uuid") String uuid, @Field("deviceToken") String deviceToken);

    @FormUrlEncoded
    @POST("https://www.googleapis.com/oauth2/v4/token")
    Call<GoogleOauthResponse> getGoogleToken(
            @Field("grant_type") String grantType, @Field("client_id") String clientId,
            @Field("client_secret") String clientSecret, @Field("redirect_uri") String redirectUri,
            @Field("code") String authCode );



}
