package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.RequestManager;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;


public class CustomDetailPreminumModel extends LinearLayout {

    RequestManager mRequestManager;

    CustomImageView companyThumbnail;
    ImageView companyPicker;
    TextView companyIdx;
    TextView companyName;
    TextView companyDetail;
    TextView companyAddress;

    TextView companyShowmap;

    TextView realWeddingCnt;
    TextView surveyCnt;
    TextView paymentCnt;

    String linkUrl;

    LinearLayout companyContentWrapper;

    public CustomDetailPreminumModel(Context context) {
        super(context);
        initView();

    }

    public CustomDetailPreminumModel(Context context, RequestManager requestManager) {
        super(context);
        this.mRequestManager = requestManager;
        initView();

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_detail_premium, this, false);
        addView(v);

        companyThumbnail = (CustomImageView)findViewById(R.id.company_thumbnail);
        companyPicker = (ImageView)findViewById(R.id.company_picker);
        companyIdx = (TextView)findViewById(R.id.company_idx);
        companyName = (TextView)findViewById(R.id.company_name);
        companyDetail = (TextView)findViewById(R.id.company_detail);
        companyAddress = (TextView)findViewById(R.id.company_address);
        companyShowmap = (TextView)findViewById(R.id.company_showmap);
        realWeddingCnt = (TextView)findViewById(R.id.realwedding_cnt);
        surveyCnt = (TextView)findViewById(R.id.survey_cnt);
        paymentCnt = (TextView)findViewById(R.id.payment_cnt);
        companyContentWrapper = (LinearLayout)findViewById(R.id.company_content_wrapper);
    }

    public void setCompanyThumbnail(String resUrl) {
        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load( resUrl ).placeholder( R.color.image_placeholder_color ).into( companyThumbnail );
        }
    }
    public void setCompanyIdx(String resString) {
        companyIdx.setText( resString );
    }
    public void setCompanyName(String resString) {
        companyName.setText( resString );
        setTag( resString );
        companyPicker.setTag( resString );
        companyShowmap.setTag( resString );
    }
    public void setCompanyDetail(String resString) {
        companyDetail.setText( resString );
    }
    public void setCompanyAddress(String resString) {
        companyAddress.setText( resString );
    }
    public void setRealWeddingCnt(int resInt) {
        if( resInt > 9 ) {
            realWeddingCnt.setVisibility( View.VISIBLE );
            realWeddingCnt.setText( "리얼웨딩 " + resInt );
        }
    }
    public void setSurveyCnt(int resInt) {
        if( resInt > 9 ) {
            surveyCnt.setVisibility( View.VISIBLE );
            surveyCnt.setText( "예약 " + resInt );
        }
    }
    public void setPaymentCnt(int resInt) {
        if( resInt > 9 ) {
            paymentCnt.setVisibility( View.VISIBLE );
            paymentCnt.setText( "구매 " + resInt );
        }
    }
    public void addCompanyContent( CustomCompanyModel customCompanyModel ) {
        companyContentWrapper.addView( customCompanyModel );
    }
    public void addCompanyContent( TextView moreBtn ) {
        companyContentWrapper.addView( moreBtn );
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public ImageView getCompanyPicker() {
        return companyPicker;
    }

    public void setCompanyPicker(ImageView companyPicker) {
        this.companyPicker = companyPicker;
    }

    public TextView getCompanyShowmap() {
        return companyShowmap;
    }

    public void setCompanyShowmap(TextView companyShowmap) {
        this.companyShowmap = companyShowmap;
    }
}
