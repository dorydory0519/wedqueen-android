package com.jjlee.wedqueen.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.v4.content.ContextCompat;

import com.jjlee.wedqueen.R;

/**
 * Created by JJLEE on 2016. 5. 18..
 */
public class BitmapUtil {

    public static Bitmap getProfileBitmap(Bitmap bitmap) {
        Bitmap output;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        } else {
            output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(output);

        final int white = 0xFFFFFFFF;
        final int gold = 0xFFB8A97D;
        final int color = 0xFF424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(r, r, r, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(SizeConverter.dpToPx(4));
        paint.setColor(gold);
        canvas.drawCircle(r, r, r, paint);

        bitmap.recycle();
        return output;

    }

    public Bitmap addGradient(Bitmap originalBitmap) {
        int width = originalBitmap.getWidth();
        int height = originalBitmap.getHeight();
        Bitmap updatedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(updatedBitmap);

        canvas.drawBitmap(originalBitmap, 0, 0, null);

        Paint paint = new Paint();
        LinearGradient shader = new LinearGradient(0, 0, 0, height, Color.parseColor("#FFFFFF"), Color.parseColor("#000000"), Shader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY));
        canvas.drawRect(0, 0, width, height, paint);

        return updatedBitmap;
    }

    public static int getColorCodeFromString( Context context, String sColorCode ) {
        int iColorCode = ContextCompat.getColor( context, R.color.edittext_black );

        if( sColorCode.equals( "red" ) ) {
            iColorCode = ContextCompat.getColor( context, R.color.edittext_red );
        } else if( sColorCode.equals( "orange" ) ) {
            iColorCode = ContextCompat.getColor( context, R.color.edittext_orange );
        } else if( sColorCode.equals( "yellow" ) ) {
            iColorCode = ContextCompat.getColor( context, R.color.edittext_yellow );
        } else if( sColorCode.equals( "green" ) ) {
            iColorCode = ContextCompat.getColor( context, R.color.edittext_green );
        } else if( sColorCode.equals( "blue" ) ) {
            iColorCode = ContextCompat.getColor( context, R.color.edittext_blue );
        } else if( sColorCode.equals( "purple" ) ) {
            iColorCode = ContextCompat.getColor( context, R.color.edittext_purple );
        }

        return iColorCode;
    }
}
