package com.jjlee.wedqueen.CustomViews;

import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.fragments.WebviewFragment;

import java.lang.ref.WeakReference;

/**
 * Created by JJLEE on 2016. 3. 28..
 */
public class CustomWebChromeClientForFullscreenActivity extends WebChromeClient {

    private FullscreenActivity fullscreenActivity;

    public CustomWebChromeClientForFullscreenActivity(FullscreenActivity activity) {
        super();
        this.fullscreenActivity = activity;
    }

    public void nullStrongActivity() {
        fullscreenActivity = null;
    }

//    @Override
//    public void onProgressChanged(WebView view, int newProgress) {
//        if(fullscreenActivity != null) {
//            if(newProgress == 100) {
//                fullscreenActivity.hideProgressBar();
//            } else {
//                fullscreenActivity.showProgressBar();
//            }
//        }
//    }


}
