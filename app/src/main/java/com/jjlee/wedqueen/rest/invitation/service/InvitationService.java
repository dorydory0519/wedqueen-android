package com.jjlee.wedqueen.rest.invitation.service;

import com.jjlee.wedqueen.rest.invitation.model.CheckInvitationResponse;
import com.jjlee.wedqueen.rest.invitation.model.InvitationForm;
import com.jjlee.wedqueen.rest.invitation.model.InvitationResponse;
import com.jjlee.wedqueen.rest.invitation.model.InvitationSaveResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by esmac on 2018. 3. 21..
 */

public interface InvitationService {
    @Headers("Accept: application/json")
    @GET("api/v1/user/invitation")
    Call<InvitationResponse> getMyInvitationInfo();

    @Headers("Accept: application/json")
    @GET("api/v2/user/invitation/check")
    Call<CheckInvitationResponse> checkInvitation();

    @Headers("Accept: application/json")
    @POST("api/v1/user/invitation")
    Call<InvitationSaveResponse> saveInvitationCode(@Body InvitationForm invitationForm );
}
