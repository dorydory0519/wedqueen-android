package com.jjlee.wedqueen.rest.guide.controller;

import com.jjlee.wedqueen.rest.guide.model.GuideResponse;
import com.jjlee.wedqueen.rest.guide.service.GuideService;
import com.jjlee.wedqueen.rest.recommend.model.RecommendResponse;
import com.jjlee.wedqueen.rest.recommend.service.RecommendService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class GuideController extends RetrofitController {
    private GuideService guideService;

    public GuideController() {
        super();
        guideService = RETROFIT.create(GuideService.class);
    }

    public Call<GuideResponse> getKeywordGuide() {
        return guideService.getKeywordGuide();
    }

}
