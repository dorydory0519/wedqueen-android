package com.jjlee.wedqueen.rest.user.model;

import com.jjlee.wedqueen.rest.common.model.AppResponse;

public class InvitationResponse extends AppResponse {
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
