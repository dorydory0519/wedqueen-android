package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;


public class CustomDetailCompanyModel extends LinearLayout {

    RequestManager mRequestManager;

    CustomImageView companyThumbnail;
    TextView companyName;

    TextView realWeddingCnt;
    TextView paymentCnt;
    TextView surveyCnt;

    public CustomDetailCompanyModel(Context context) {
        super(context);
        initView();

    }

    public CustomDetailCompanyModel(Context context, RequestManager requestManager) {
        super(context);
        this.mRequestManager = requestManager;
        initView();

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_detail_company, this, false);
        addView(v);

        companyThumbnail = (CustomImageView)findViewById(R.id.company_thumbnail);
        companyName = (TextView)findViewById(R.id.company_name);
        realWeddingCnt = (TextView)findViewById(R.id.realwedding_cnt);
        paymentCnt = (TextView)findViewById(R.id.payment_cnt);
        surveyCnt = (TextView)findViewById(R.id.survey_cnt);
    }

    public void setCompanyThumbnail(String resUrl) {

        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load( resUrl ).placeholder( R.color.image_placeholder_color ).into( companyThumbnail );
        } else {
            Glide.with( getContext() ).load( resUrl ).placeholder( R.color.image_placeholder_color ).into( companyThumbnail );
        }
    }
    public void setCompanyName(String resString) {
        companyName.setText( resString );
    }

    public void setRealWeddingCnt(int resInt) {
        if( resInt > 9 ) {
            realWeddingCnt.setVisibility( View.VISIBLE );
            realWeddingCnt.setText( "리얼웨딩 " + resInt );
        }
    }
    public void setPaymentCnt(int resInt) {
        if( resInt > 9 ) {
            paymentCnt.setVisibility( View.VISIBLE );
            paymentCnt.setText( "판매 " + resInt );
        }
    }
    public void setSurveyCnt(int resInt) {
        if( resInt > 9 ) {
            surveyCnt.setVisibility( View.VISIBLE );
            surveyCnt.setText( "예약 " + resInt );
        }
    }
}
