package com.jjlee.wedqueen.rest.dday.controller;

import com.jjlee.wedqueen.rest.common.model.AppResponse;
import com.jjlee.wedqueen.rest.dday.model.BudgetResponse;
import com.jjlee.wedqueen.rest.dday.model.CategoryForm;
import com.jjlee.wedqueen.rest.dday.model.CategoryResponse;
import com.jjlee.wedqueen.rest.dday.model.DDayForm;
import com.jjlee.wedqueen.rest.dday.model.DDayFormResponse;
import com.jjlee.wedqueen.rest.dday.model.DDayResponse;
import com.jjlee.wedqueen.rest.dday.model.DDayV2Response;
import com.jjlee.wedqueen.rest.dday.model.PositionDto;
import com.jjlee.wedqueen.rest.dday.service.DDayService;
import com.jjlee.wedqueen.support.RetrofitController;
import com.jjlee.wedqueen.utils.MapUtil;

import java.util.Map;

import retrofit2.Call;

public class DDayController extends RetrofitController {

    private DDayService ddayService;

    public DDayController() {
        super();
        ddayService = RETROFIT.create(DDayService.class);
    }

    public Call<BudgetResponse> getBudgetAvg() {
        return ddayService.getBudgetAvg();
    }

    public Call<DDayResponse> getdDay() {
        return ddayService.getdDay();
    }

    /**
     * 설정 > 알림설정 시 return 을 받는 데이터 중에
     * alarmTime이 공백이면, 알람끄기 체크
     * 8은 아침, 12는 점심, 20은 저녁으로 체크
     * @return
     */
    public Call<DDayFormResponse> getdDayInfo() {
        return ddayService.getdDayInfo();
    }

    /**
     * 알람 설정 시 getdDayInfo로 받은 데이터를 form 에 맞게 넣어야 함
     * @param form
     * @return
     */
    public Call<AppResponse> createDDay(DDayForm form ) {
        Map<String, String> map = MapUtil.getMapFromObject( form );
        return ddayService.createDDay( map );
    }

    /**
     * 알람 초기화
     * @return
     */
    public Call<AppResponse> deleteDDay() {
        return ddayService.deleteDDay();
    }

    /**
     * 항목 추가
     * @param form
     * @return
     */
    public Call<CategoryResponse> saveCategory(CategoryForm form ) {
        Map<String, String> map = MapUtil.getMapFromObject( form );
        return ddayService.saveCategory( map );
    }

    /**
     * 항목 완료
     * /user/dday/category/{id}/complete/update
     */
    public Call<AppResponse> updateCategory(Long id
            , boolean complete ) {
        return ddayService.updateCategory( id, complete );
    }

    /**
     * 항목 이동
     * /user/dday/position
     */
    public Call<AppResponse> setPosition(PositionDto form ) {
        return ddayService.setPosition( form );
    }


    public Call<DDayV2Response> getDDayV2() {
        return ddayService.getDDayV2();
    }

}
