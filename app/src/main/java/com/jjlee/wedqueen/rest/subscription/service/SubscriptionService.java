package com.jjlee.wedqueen.rest.subscription.service;

import com.jjlee.wedqueen.rest.subscription.model.SubscriptionGetResponse;
import com.jjlee.wedqueen.rest.subscription.model.SubscriptionResponse;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SubscriptionService {
    @Headers("Accept: application/json")
    @POST("api/v1/subscription/{type}/{id}")
    Call<SubscriptionResponse> postSubscription(@Path("type") String type, @Path("id") int id);

    @Headers("Accept: application/json")
    @DELETE("api/v1/subscription/{type}/{id}")
    Call<SubscriptionResponse> delSubscription(@Path("type") String type, @Path("id") int id);

    @Headers("Accept: application/json")
    @GET("api/v1/subscription/{type}")
    Call<SubscriptionGetResponse> getMyFollowing(@Path("type") String type);

    @Headers("Accept: application/json")
    @GET("api/v1/subscription/{type}/subscriber")
    Call<SubscriptionGetResponse> getMyFollower(@Path("type") String type);

}
