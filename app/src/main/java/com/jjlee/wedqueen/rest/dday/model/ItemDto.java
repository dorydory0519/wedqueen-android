package com.jjlee.wedqueen.rest.dday.model;

/**
 * Created by esmac on 2017. 5. 29..
 */

public class ItemDto {

    private Long id;

    private String title;

    private boolean checked;

    private String memo;

    private Long mBudget;

    private Long wBudget;

    private Long cBudget;

    private Long expense;

    private String owner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Long getmBudget() {
        return mBudget;
    }

    public void setmBudget(Long mBudget) {
        this.mBudget = mBudget;
    }

    public Long getwBudget() {
        return wBudget;
    }

    public void setwBudget(Long wBudget) {
        this.wBudget = wBudget;
    }

    public Long getcBudget() {
        return cBudget;
    }

    public void setcBudget(Long cBudget) {
        this.cBudget = cBudget;
    }

    public Long getExpense() {
        return expense;
    }

    public void setExpense(Long expense) {
        this.expense = expense;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

}