package com.jjlee.wedqueen.rest.content.model;

import java.util.List;

/**
 * Created by JJLEE on 2016. 4. 23..
 */
public class PostContentResponse {
    private String msg;
    private int code;
    private String httpStatus;
    private Result result;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(String httpStatus) {
        this.httpStatus = httpStatus;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
