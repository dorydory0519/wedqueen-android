package com.jjlee.wedqueen.rest.invitation.model;

/**
 * Created by esmac on 2018. 3. 23..
 */

public class InvitationForm {
    private String code;

    public InvitationForm(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "InvitationForm{" +
                " code='" + code + '\'' +
                '}';
    }
}
