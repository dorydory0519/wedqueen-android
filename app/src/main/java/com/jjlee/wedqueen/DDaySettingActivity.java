package com.jjlee.wedqueen;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.CustomViews.CustomCategoryButton;
import com.jjlee.wedqueen.fragments.EditMainFragment;
import com.jjlee.wedqueen.rest.app.model.TabCategory;
import com.jjlee.wedqueen.rest.content.model.PostContentResponse;

import com.jjlee.wedqueen.rest.image.controller.ImageController;
import com.jjlee.wedqueen.rest.image.model.ImageResponse;
import com.jjlee.wedqueen.rest.user.controller.UserController;
import com.jjlee.wedqueen.rest.user.model.PostUserInfoResponse;
import com.jjlee.wedqueen.rest.user.model.RegionDto;
import com.jjlee.wedqueen.rest.user.model.TabCategoryDto;
import com.jjlee.wedqueen.rest.user.model.UserInfoForm;
import com.jjlee.wedqueen.rest.user.model.UserInfoResponse;
import com.jjlee.wedqueen.rest.user.model.UserRestInfo;
import com.jjlee.wedqueen.rest.user.model.WeddingStyleDto;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.utils.FileUtils;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.jjlee.wedqueen.utils.SizeConverter;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

//import io.userhabit.service.Userhabit;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by esmac on 2017. 9. 11..
 */

public class DDaySettingActivity extends AppCompatActivity {

    private InputMethodManager inputMethodManager;

    private int year_x, month_x, day_x;
    private ImageView profileImageView;
    private LinearLayout nicknameWrapper;
    private ImageView nicknameImageView;
    private EditText nicknameLabel;
    private boolean isNicknameEditMode = false;
    private EditText ddayLabel;
    private Button confirmButton;
    private Spinner regionSpinner;
    private Prefs prefs;
    private UserController userController;
    private List<String> regionData;
    private List<String> regionDataEng;
//    private String selectedRegion;
    private GridLayout categoryWrapper;

    private Uri imgUri;
    private ImageController imageController;

    private RadioGroup weddingstyleGroup;
    private int[] weddingstyles;

    private List<Long> selectedCategoryIds = new ArrayList<Long>();

    private int selectedRegionIdx = 0;
    private boolean hasDday;
    private UserInfoForm userInfoForm;

    private AppCompatDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ddaysetting);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "set_myinfo" ) );

        imageController = new ImageController();

        inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        hasDday = false;
        userInfoForm = new UserInfoForm();

        // dday Default Setting
        prefs = new Prefs(this);
        Calendar dday = prefs.getDdayDate();
        if( dday == null ) {
            dday = Calendar.getInstance();
        }
        year_x = dday.get( Calendar.YEAR );
        month_x = dday.get( Calendar.MONTH );
        day_x = dday.get( Calendar.DATE );

        confirmButton = (Button) findViewById(R.id.button4);
        profileImageView = (ImageView) findViewById(R.id.user_photo);
        nicknameImageView = (ImageView) findViewById(R.id.btn_nickname_edit);
        nicknameLabel = (EditText) findViewById(R.id.user_nickname);

        ddayLabel = (EditText) findViewById(R.id.dday_date);
        ddayLabel.setText( year_x + "년 " + ( month_x + 1 ) + "월 " + day_x + "일" );
        ddayLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog( DDaySettingActivity.this, dPickerListener, year_x, month_x, day_x );
                datePickerDialog.show();
//                Userhabit.setScreen( datePickerDialog, "날짜선택 Dialog" );
            }
        });

        // region Default Setting
        regionSpinner = (Spinner)findViewById(R.id.region_spinner);
        regionData = new ArrayList<>();
        regionDataEng = new ArrayList<>();

        weddingstyleGroup = (RadioGroup)findViewById(R.id.weddingstyle_group);
        weddingstyles = new int[]{ R.id.traditional_wedding, R.id.general_wedding, R.id.house_wedding };

        // tabCategory Default Setting
        categoryWrapper = (GridLayout)findViewById(R.id.category_wrapper);

        userController = new UserController();
        Call<UserInfoResponse> userRestInfoCall = userController.getUserInfo();
        progressON( DDaySettingActivity.this, "Load..." );
        userRestInfoCall.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call
                    , Response<UserInfoResponse> response) {

                progressOFF();

                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    UserInfoResponse userInfoResponse = response.body();
                    UserRestInfo userRestInfo = userInfoResponse.getUserInfo();
                    if( !ObjectUtils.isEmpty( userRestInfo ) ){

                        // user ( profileImage & nickName )
                        if( !ObjectUtils.isEmpty( userRestInfo.getUser() ) ) {
                            if( !ObjectUtils.isEmpty( DDaySettingActivity.this ) ) {
                                Glide.with( DDaySettingActivity.this ).load( userRestInfo.getUser().getImageUrl() ).bitmapTransform( new CropCircleTransformation( DDaySettingActivity.this ) ).placeholder( R.drawable.default_user_icon ).into( profileImageView );
                            }
                            nicknameLabel.setText( userRestInfo.getUser().getNickname() );
                        }

                        // user dday
                        if( !ObjectUtils.isEmpty( userRestInfo.getMarriageDate() ) ) {
                            hasDday = true;

                            userInfoForm.setMarriageDate( userRestInfo.getMarriageDate() );

                            String year = userRestInfo.getMarriageDate().substring(0, 4);
                            String month = userRestInfo.getMarriageDate().substring(5, 7);
                            if (month.substring(0, 1).equals("0")) {
                                month = month.substring(1, 2);
                            }
                            String day = userRestInfo.getMarriageDate().substring(8, 10);
                            if (day.substring(0, 1).equals("0")) {
                                day = day.substring(1, 2);
                            }
                            ddayLabel.setText( year + "년 " + month + "월 " + day + "일" );
                        }

                        // user region
                        if( userRestInfo.getRegions().size() > 0 ) {
                            int index = 0;
                            for( RegionDto region : userRestInfo.getRegions() ) {
                                if( region.getKorPrecinct() != "" ) {
                                    regionData.add( region.getKorPrecinct() );
                                    regionDataEng.add( region.getEngPrecinct() );
                                } else {
                                    regionData.add( region.getKorRegion() );
                                    regionDataEng.add( region.getEngRegion() );
                                }

                                if (region.isSelected()) {
                                    selectedRegionIdx = index;
                                }
                                index++;
                            }
                            setSpinnerAdapter();
                        }
                        regionSpinner.setSelection( selectedRegionIdx );

                        // user weddingStyle
                        if( userRestInfo.getWeddingStyles().size() > 0 ) {
                            // position 값과 name, color코드를 모두 가져오지만 일단 기본적으로 앱단에서 미리 세팅해놓은 것을 가져다쓰도록 구현함
                            boolean isCheckWeddingStyle = false;
                            for(WeddingStyleDto weddingStyle : userRestInfo.getWeddingStyles() ) {
                                if( weddingStyle.isSelected() ) {
                                    weddingstyleGroup.check( weddingstyles[ (int)(weddingStyle.getPosition() - 1) ] );
                                    isCheckWeddingStyle = true;
                                    break;
                                }
                            }

                            // 체크된 웨딩스타일이 없으면 '일반적인' 웨딩스타일
                            if( !isCheckWeddingStyle ) {
                                weddingstyleGroup.check( weddingstyles[1] );
                            }
                        }

                        // user tabCategories
                        if( userRestInfo.getTabcategories().size() > 0 ) {
                            for( TabCategoryDto tabCategory : userRestInfo.getTabcategories() ) {
                                CustomCategoryButton categoryButton = new CustomCategoryButton( DDaySettingActivity.this );
                                categoryButton.setCategoryButton( tabCategory.getCategoryNameKor() );
                                categoryButton.setSelected( tabCategory.isSelected() );
                                categoryButton.setCategoryId( tabCategory.getId() );
                                categoryButton.setPosition( tabCategory.getPosition() );
                                categoryButton.setCategoryName( tabCategory.getCategoryName() );
                                categoryButton.setCategoryNameKor( tabCategory.getCategoryNameKor() );
                                categoryButton.setTabType( tabCategory.getTabType() );
                                categoryButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        CustomCategoryButton selectedCategory = (CustomCategoryButton)v;

                                        if( selectedCategory.getCategoryId() == 9 ) {
                                            Toast.makeText( DDaySettingActivity.this, "'" + selectedCategory.getCategoryButton().getText() + "' 카테고리는 필수입니다!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            selectedCategory.setSelected( !selectedCategory.isSelected() );
                                        }
                                    }
                                });

                                categoryWrapper.addView( categoryButton );
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call
                    , Throwable t) {
                progressOFF();
                t.printStackTrace();
            }
        });

        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        && ContextCompat.checkSelfPermission( DDaySettingActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions( DDaySettingActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                            Constants.REQ_EXTERNAL_STORAGE_PERMISSION);
                } else {
                    openFileChooser( Constants.REQ_CODE_OPEN_PROFILE_IMG );
                }
            }
        });

        nicknameWrapper = (LinearLayout)findViewById(R.id.nickname_wrapper);
        nicknameLabel.setMaxWidth( getWindowManager().getDefaultDisplay().getWidth() - SizeConverter.dpToPx( 145 ) );
        nicknameImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // 모드변경
                isNicknameEditMode = !isNicknameEditMode;

                nicknameLabel.setFocusable( isNicknameEditMode );
                nicknameLabel.setClickable( isNicknameEditMode );
                nicknameLabel.setFocusableInTouchMode( isNicknameEditMode );

                LinearLayout.LayoutParams wrapperLayoutParams = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT );
                wrapperLayoutParams.gravity = Gravity.CENTER_VERTICAL;

                if( isNicknameEditMode ) {

                    ((ImageView)v).setImageResource( R.drawable.icon_nickname_confirm );

                    wrapperLayoutParams.weight = 1.0f;
                    nicknameWrapper.setLayoutParams( wrapperLayoutParams );
                    nicknameLabel.setLayoutParams( new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT ) );
                    nicknameLabel.requestFocus();
                    nicknameLabel.setSelection( nicknameLabel.length() );
                    showKeyBoard();
                } else {

                    ((ImageView)v).setImageResource( R.drawable.icon_nickname_edit );

                    nicknameWrapper.setLayoutParams( wrapperLayoutParams );
                    nicknameLabel.setLayoutParams( new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT ) );
                    hideKeyBoard();
                }

            }
        });
        nicknameLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( !isNicknameEditMode ) {
                    isNicknameEditMode = true;

                    v.setFocusable( true );
                    v.setClickable( true );
                    v.setFocusableInTouchMode( true );

                    LinearLayout.LayoutParams wrapperLayoutParams = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT );
                    wrapperLayoutParams.gravity = Gravity.CENTER_VERTICAL;
                    wrapperLayoutParams.weight = 1.0f;
                    nicknameWrapper.setLayoutParams( wrapperLayoutParams );
                    v.setLayoutParams( new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT ) );
                    v.requestFocus();
                    showKeyBoard();
                }
            }
        });


        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressON( DDaySettingActivity.this, "Save..." );

                // 닉네임
                userInfoForm.setNickname( nicknameLabel.getText().toString() );

                // 웨딩스타일
                long selectedWeddingStyleId = 1;
                for( int i = 0; i < weddingstyles.length; i++ ) {
                    if( weddingstyleGroup.getCheckedRadioButtonId() == weddingstyles[i] ) {
                        selectedWeddingStyleId = i;
                        break;
                    }
                }
                userInfoForm.setWeddingStyleId( selectedWeddingStyleId + 1 );

                // 탭 카테고리
                ArrayList<TabCategory> newTabCategories = new ArrayList<>();
                for( int i = 0; i < categoryWrapper.getChildCount(); i++ ) {
                    CustomCategoryButton customCategoryButton = (CustomCategoryButton) categoryWrapper.getChildAt( i );
                    if( customCategoryButton.isSelected() ) {
                        selectedCategoryIds.add( customCategoryButton.getCategoryId() );
                        Log.e( "WQTEST1", "CategoryId : " + customCategoryButton.getCategoryId() + "  CategoryName : " + customCategoryButton.getCategoryButton().getText() );

                        newTabCategories.add( customCategoryButton.getTabCategory() );

                    }
                }
                userInfoForm.setTabCategoryIds( selectedCategoryIds );
                HashMap<String, ArrayList<TabCategory>> allTabCategories = prefs.getCurrentTabCategories();
                allTabCategories.put( "contentfeed", newTabCategories );
                prefs.setCurrentTabCategories( allTabCategories );

                if (hasDday) {
                    //put
                    Call<PostUserInfoResponse> call = userController.putUserInfo( userInfoForm );
                    call.enqueue(new Callback<PostUserInfoResponse>() {
                        @Override
                        public void onResponse(Call<PostUserInfoResponse> call
                                , Response<PostUserInfoResponse> response) {

                            progressOFF();

                            Log.e( "WQTEST3", "onResponse putUserInfo" );

                            if( !ObjectUtils.isEmpty( response.body() ) ) {
                                PostUserInfoResponse postUserInfoResponse = response.body();

                                if( postUserInfoResponse.getCode() == 200 ) {

                                    // 내정보 App내에 Setting
                                    setUserInfoFormToApp( postUserInfoResponse.getResult().getImageUrl() );

                                    Intent intent = new Intent();
                                    setResult(RESULT_OK, intent);
                                    finish();
                                } else {
                                    Toast.makeText( DDaySettingActivity.this, postUserInfoResponse.getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Log.e( "WQTEST", "response's body is null" );
                            }
                        }

                        @Override
                        public void onFailure(Call<PostUserInfoResponse> call, Throwable t) {
                            progressOFF();
                            t.printStackTrace();
                            Toast.makeText( DDaySettingActivity.this, "내 정보 저장에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                else {
                    //post
                    //디데이, 지역은 무조건 절대로 받아야함
                    if (userInfoForm.getMarriageDate() == null) {
                        progressOFF();
                        Toast.makeText( DDaySettingActivity.this, "D-Day 입력이 필요합니다.", Toast.LENGTH_SHORT).show();
                    }
                    else if (userInfoForm.getRegion() == null) {
                        progressOFF();
                        Toast.makeText( DDaySettingActivity.this, "지역 정보 입력이 필요합니다.", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Call<PostUserInfoResponse> call = userController.postUserInfo( userInfoForm );
                        call.enqueue(new Callback<PostUserInfoResponse>() {
                            @Override
                            public void onResponse(Call<PostUserInfoResponse> call
                                    , Response<PostUserInfoResponse> response) {
                                if( !ObjectUtils.isEmpty( response.body() ) ) {

                                    progressOFF();

                                    PostUserInfoResponse postUserInfoResponse = response.body();

                                    if( postUserInfoResponse.getCode() == 200 ) {

                                        // 내정보 App내에 Setting
                                        setUserInfoFormToApp( postUserInfoResponse.getResult().getImageUrl() );

                                        Intent intent = new Intent();
                                        setResult(RESULT_OK, intent);
                                        finish();
                                    } else {
                                        Toast.makeText( DDaySettingActivity.this, postUserInfoResponse.getMsg(), Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Log.e( "WQTEST", "response's body is null" );
                                }
                            }

                            @Override
                            public void onFailure(Call<PostUserInfoResponse> call, Throwable t) {
                                progressOFF();
                                t.printStackTrace();
                                Toast.makeText( DDaySettingActivity.this, "내 정보 저장에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }
        });
    }

    private void setUserInfoFormToApp( String userImgUrl ) {
        // 디데이 Set
        String strMarriageDate = userInfoForm.getMarriageDate();
        prefs.setDdayDate( strMarriageDate.substring( 0, 4 ) + "-" + strMarriageDate.substring( 5, 7 ) + "-" + strMarriageDate.substring( 8, 10 ) );

        // 닉네임 Set
        MyApplication.getGlobalApplicationContext().setUserNickName( nicknameLabel.getText().toString() );

        // 프로필사진 Set
        MyApplication.getGlobalApplicationContext().setUserImageUrl( userImgUrl );
    }

    private void setSpinnerAdapter() {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.custom_spinner_item, regionData);
        arrayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        regionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                selectedRegion = regionSpinner.getItemAtPosition(position).toString();
                userInfoForm.setRegion(regionDataEng.get(position));
                prefs.setUserRegion( regionData.get( position ) );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        regionSpinner.setAdapter( arrayAdapter );
    }

    private DatePickerDialog.OnDateSetListener dPickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            year_x = year;
            month_x = monthOfYear;
            day_x = dayOfMonth;

            ddayLabel.setText( year_x + "년 " + ( month_x + 1 ) + "월 " + day_x + "일" );
            String marriageDate = "";
            if (month_x+1 < 10) {
                marriageDate = year+".0"+(month_x+1);
            }
            else {
                marriageDate = year+"."+(month_x+1);
            }
            if (day_x < 10) {
                marriageDate += ".0"+dayOfMonth;
            }
            else {
                marriageDate += "."+dayOfMonth;
            }
            userInfoForm.setMarriageDate(marriageDate);
        }
    };


    public void openFileChooser( int openType ) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult( Intent.createChooser( intent, "파일을 고르세요" ), openType );
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if( resultCode == Activity.RESULT_CANCELED ) {
            return;
        }

        imgUri = data == null || resultCode != Activity.RESULT_OK ? null : data.getData();

        File file = FileUtils.getFile(this, imgUri);

        int i = -1;
        if( file != null && file.getName() != null ) {
            i = file.getName().lastIndexOf('.');
        }

        switch(requestCode) {
            case Constants.REQ_CODE_OPEN_PROFILE_IMG :

                if( i > 0 && (
                        "jpg".equals(file.getName().substring(i+1).toLowerCase())
                                || "jpeg".equals(file.getName().substring(i+1).toLowerCase())
                                || "png".equals(file.getName().substring(i+1).toLowerCase())
                )) {
                    progressON( this, "Upload..." );

                    Call<ImageResponse> call = imageController.postImageUploadToServer( file, "general" );

                    call.enqueue(new Callback<ImageResponse>() {
                        @Override
                        public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                            ImageResponse imageResponse = response.body();

                            userInfoForm.setFilename( imageResponse.getFileNames().get( 0 ) );
                            profileImageView.setImageURI( imgUri );
                            Glide.with( DDaySettingActivity.this ).load( imgUri ).bitmapTransform( new CropCircleTransformation( DDaySettingActivity.this ) ).placeholder( R.drawable.default_user_icon ).into( profileImageView );
                            progressOFF();
                        }

                        @Override
                        public void onFailure(Call<ImageResponse> call, Throwable t) {
                            progressOFF();
                            t.printStackTrace();
                            Log.e("WQTEST", t.getMessage());
                            Toast.makeText( DDaySettingActivity.this, "이미지 업로드에 실패하였습니다. 잠시 후 다시 시도해주세요.", Toast.LENGTH_LONG ).show();
                        }
                    });
                } else {
                    Toast.makeText( this, "이미지 파일만 업로드 가능합니다.", Toast.LENGTH_LONG ).show();
                }

                break;
        }
    }

    // 로딩 Dialog
    public void progressON(Activity activity, String message) {

        if (activity == null || activity.isFinishing()) {
            return;
        }


        if (progressDialog != null && progressDialog.isShowing()) {
            progressSET(message);
        } else {

            progressDialog = new AppCompatDialog(activity);
            progressDialog.setCancelable(false);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.layout_frame_loading);
            progressDialog.show();
//            Userhabit.setScreen( progressDialog, "내정보 로딩 Dialog" );

        }


        final ImageView img_loading_frame = (ImageView) progressDialog.findViewById(R.id.iv_frame_loading);
        final AnimationDrawable frameAnimation = (AnimationDrawable) img_loading_frame.getBackground();
        img_loading_frame.post(new Runnable() {
            @Override
            public void run() {
                frameAnimation.start();
            }
        });

        TextView tv_progress_message = (TextView) progressDialog.findViewById(R.id.tv_progress_message);
        if (!TextUtils.isEmpty(message)) {
            tv_progress_message.setText(message);
        }
    }

    public void progressSET(String message) {

        if (progressDialog == null || !progressDialog.isShowing()) {
            return;
        }

        TextView tv_progress_message = (TextView) progressDialog.findViewById(R.id.tv_progress_message);
        if (!TextUtils.isEmpty(message)) {
            tv_progress_message.setText(message);
        }

    }

    public void progressOFF() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void showKeyBoard() {
        inputMethodManager.showSoftInput( nicknameLabel, 0 );
    }

    public void hideKeyBoard() {
        inputMethodManager.hideSoftInputFromWindow( nicknameLabel.getWindowToken(), 0 );
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

}
