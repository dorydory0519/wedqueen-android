package com.jjlee.wedqueen.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.jjlee.wedqueen.AccountActivity;
import com.jjlee.wedqueen.ExternalWebviewActivity;
import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MainActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.TutorialActivity;
import com.jjlee.wedqueen.rest.account.controller.AccountController;
import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.MyCookieJar;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.utils.ObjectUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsFragment extends PreferenceFragment {

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.settings);

//		Preference loginPreference = findPreference( "pref_login" );
//		if( !ObjectUtils.isEmpty( loginPreference ) ) {
//			if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
//				loginPreference.setTitle( R.string.signout );
//			} else {
//				loginPreference.setTitle( R.string.signin );
//			}
//		}

		checkVersion();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View view = super.onCreateView(inflater, container, savedInstanceState);
		view.setBackgroundColor(0xFFFFFF);

		return view;
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference)
	{
		switch(preference.getTitleRes()) {
			case R.string.signout: {
				// 로그아웃 : /account/logout으로 deviceToken=XXXXXX... 담아서 POST.
				// 즉, 로그아웃 하면 그 순간부터 앱이 꺼지기 전까진 알림을 받을 수 없음.

				Prefs prefs = new Prefs(getActivity());
				prefs.setCookieData(null);
				prefs.setDdayDate(null);
				prefs.setCBudget(0);

				((MainActivity) getActivity()).toggleDrawerLayout();
				Intent intent = new Intent(getActivity(), AccountActivity.class);
				intent.putExtra(Constants.ACTION_TYPE, Constants.ACTION_TYPE_LOGOUT);
				getActivity().startActivityForResult(intent, Constants.REQ_CODE_LOGOUT);
				return true;
			}
			case R.string.about:
			{
				Intent intent = new Intent(getActivity(), ExternalWebviewActivity.class);
				intent.putExtra("url", Constants.BASE_URL + "about");
				startActivity(intent);
				return true;
			}
			case R.string.tutorial:
			{
				Prefs prefs = new Prefs(getActivity());
				List<String> tutorialImgs = prefs.getTutorialImgUrl();

				if( !ObjectUtils.isEmpty( tutorialImgs ) && tutorialImgs.size() > 0 ) {
					Intent intent = new Intent(getActivity(), TutorialActivity.class);
					startActivity(intent);
				} else {
					Toast.makeText(getActivity(),
							"튜토리얼 준비중입니다!",
							Toast.LENGTH_SHORT).show();
				}

				return true;
			}
			case R.string.inquiry:
			{
				Intent intent = new Intent(getActivity(), FullscreenActivity.class);
				intent.putExtra("URL", Constants.INQUIRY_URL);
				startActivity(intent);

				return true;
			}
			case R.string.request:
			{
				Intent intent = new Intent(getActivity(), ExternalWebviewActivity.class);
				intent.putExtra("url", "http://goo.gl/eUIfhq");
				startActivity(intent);

				return true;
			}
			case R.string.report:
			{
				Intent intent = new Intent(getActivity(), ExternalWebviewActivity.class);
				intent.putExtra("url", "http://pf.kakao.com/_ceBJC");
				startActivity(intent);

				return true;
			}
			case R.string.suggest:
			{
				Intent intent = new Intent(getActivity(), ExternalWebviewActivity.class);
				intent.putExtra("url", "http://pf.kakao.com/_ceBJC");
				startActivity(intent);

				return true;
			}
			case R.string.version:
			{
				if(MyApplication.getGlobalApplicationContext().isLatestVersion()) {
					// 아무것도 안함
				} else {
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.app_url)));
					startActivity(intent);
				}

				return true;
			}
            case R.string.unjoin:
            {
                new AlertDialog.Builder(this.getActivity())
                        .setMessage("탈퇴하실 경우 회원님의 모든 데이터가 삭제되며 복구가 불가능합니다. 정말 탈퇴하시겠습니까?")
                        .setCancelable(false)
                        .setPositiveButton("탈퇴", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
								Prefs prefs = new Prefs(getActivity());
								prefs.setLastLogin(Constants.LOGIN_NULL);
								prefs.setLogin(Constants.LOGIN_NULL);

								AccountController accountController = new AccountController();
								Call<CommonResponse> call = accountController.unjoinUser();
								call.enqueue(new Callback<CommonResponse>() {
									@Override
									public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
										Toast.makeText(getActivity(),
												"탈퇴 처리 되었습니다. 웨딩의 여신을 사용해주셔서 감사합니다!",
												Toast.LENGTH_LONG).show();

										MyCookieJar.removeSessionCookies();
										MyApplication.getGlobalApplicationContext().setBadgeCount(0);
										MyApplication.getGlobalApplicationContext().setIsLoggedIn(false);
										MyApplication.getGlobalApplicationContext().setUserId( "-1" );
										((MainActivity)getActivity()).processAfterLoginStateChanged();
										((MainActivity)getActivity()).toggleDrawerLayout();
									}

									@Override
									public void onFailure(Call<CommonResponse> call, Throwable t) {
										Toast.makeText(getActivity(),
											"탈퇴 중 문제가 발생했습니다. 계속해서 문제가 발생한다면 mewed@naver.com 으로 탈퇴신청을 해주세요.",
											Toast.LENGTH_LONG).show();
										((MainActivity)getActivity()).toggleDrawerLayout();
									}
								});

                            }
                        })
                        .setNegativeButton("취소", null)
                        .show();
                return true;
            }
			default:
			{
				return false;
			}
		}
	}


	private void checkVersion() {
		MyApplication app = MyApplication.getGlobalApplicationContext();

		PreferenceCategory category = (PreferenceCategory)getPreferenceScreen().getPreference(0);
		Preference preference = category.getPreference(0);
		preference.setSummary(app.getVersionName());
		preference.setWidgetLayoutResource(app.isLatestVersion() ? 0 : R.layout.settings_version_new);
	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);

		if( isVisibleToUser ) {

			Answers.getInstance().logContentView(new ContentViewEvent()
					.putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
					.putContentType( "Fragment" )
					.putContentName( "setting" ) );

		}
	}

}
