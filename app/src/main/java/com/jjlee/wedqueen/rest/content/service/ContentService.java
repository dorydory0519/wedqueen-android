package com.jjlee.wedqueen.rest.content.service;

import com.jjlee.wedqueen.rest.company.model.CompanyResponse;
import com.jjlee.wedqueen.rest.content.model.Content;
import com.jjlee.wedqueen.rest.content.model.ContentImageResponse;
import com.jjlee.wedqueen.rest.content.model.ContentResponse;
import com.jjlee.wedqueen.rest.content.model.GetContentResponse;
import com.jjlee.wedqueen.rest.content.model.ImageResponse;
import com.jjlee.wedqueen.rest.content.model.PostContent;
import com.jjlee.wedqueen.rest.content.model.PostContentResponse;
import com.jjlee.wedqueen.rest.content.model.RealWeddingContentDto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ContentService {
    @Headers("Accept: application/json")
    @GET("api/v1/content")
    Call<ContentResponse> getContents(@Query("category") String category, @Query("page") int page, @Query("targetTime") int targetTime);

    @Headers("Accept: application/json")
    @GET("api/v1/content")
    Call<ContentResponse> getContentsBySort(@Query("category") String category, @Query("page") int page, @Query("targetTime") int targetTime, @Query("orderType") String orderType);

    @Headers("Accept: application/json")
    @GET("api/v1/content/{id}")
    Call<GetContentResponse> getContent(@Path("id") long id);

    @Headers("Accept: application/json")
    @DELETE("api/v1/content/{id}")
    Call<PostContentResponse> deleteContent(@Path("id") long id);

    @Headers("Accept: application/json")
    @PUT("api/v1/content/{id}/state/{state}")
    Call<PostContentResponse> putContentState(@Path("id") long id, @Path("state") String state);

//    @Headers("Accept: application/json")
//    @POST("api/v1/content")
//    Call<PostContentResponse> postContent(@Body PostContent postContent);
//
//    @Headers("Accept: application/json")
//    @PUT("api/v1/content/{id}")
//    Call<PostContentResponse> putContent(@Body PostContent putContent, @Path("id") long id);

    @Headers("Accept: application/json")
    @POST("api/v2/content")
    Call<PostContentResponse> postContentV2(@Body PostContent postContent);

    @Headers("Accept: application/json")
    @PUT("api/v2/content/{id}")
    Call<PostContentResponse> putContentV2(@Body PostContent putContent, @Path("id") long id);

    @Headers("Accept: application/json")
    @GET("api/v1/content/image")
    Call<ImageResponse> getImages(@Query("page") int page);

    @Headers("Accept: application/json")
    @GET("api/v1/content/{id}/images")
    Call<ContentImageResponse> getContentImages(@Path("id") long contentId);

    @Headers("Accept: application/json")
    @GET("api/v1/best/realwedding")
    Call<ContentResponse> getBestContent();

    @Headers("Accept: application/json")
    @GET("api/v2/content")
    Call<List<RealWeddingContentDto>> getV2Contents(@Query("category") String category, @Query("page") int page, @Query("targetTime") int targetTime);

    @GET("api/v3/premium")
    Call<List<RealWeddingContentDto>> getPremiums();
}
