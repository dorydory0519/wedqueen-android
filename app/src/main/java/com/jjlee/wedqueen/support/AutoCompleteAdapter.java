package com.jjlee.wedqueen.support;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jjlee.wedqueen.CustomViews.CustomImageView;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.rest.autocomplete.model.AutoCompleteSet;
import com.jjlee.wedqueen.rest.autocomplete.model.Company;
import com.jjlee.wedqueen.rest.autocomplete.model.HashTag;
import com.jjlee.wedqueen.rest.autocomplete.model.User;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by esmac on 2017. 10. 10..
 */

public class AutoCompleteAdapter extends ArrayAdapter implements Filterable{

    private String autoCompleType;
    private List<AutoCompleteSet> autoCompleteSetList;

    public AutoCompleteAdapter(Context context, int resource, String autoCompleType) {
        super(context, resource);
        autoCompleteSetList = new ArrayList<>();
        this.autoCompleType = autoCompleType;
    }

    @Override
    public int getCount() {
        return autoCompleteSetList.size();
    }

    @Override
    public AutoCompleteSet getItem(int position) {

        if( autoCompleteSetList.size() > position ) {
            return autoCompleteSetList.get( position );
        }

        return null;
    }

    @Override
    public Filter getFilter() {

        Filter myFilter = new Filter() {


            // 백그라운드에서 동작한다 그런데 이 함수 내에 Adapter의 데이터를 변경하는 코드가 들어있으면 Adapter의 데이터를 변경하는 시점과 notify되는 시점이 엉킬 경우 오류가 발생함
            // ( 키워드를 입력하거나 지우는 등의 변경이 빠르게 일어날 경우 데이터의 변경이 빈번하게 일어나면서 오류가 발생 )
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                List<AutoCompleteSet> queryResults;
                if( constraint != null && constraint.length() != 0 ){
                    queryResults = getParseJson( autoCompleType, constraint.toString() );
                } else {
                    queryResults = new ArrayList<>();
                }

                filterResults.values = queryResults;
                filterResults.count = queryResults.size();

                return filterResults;
            }

            @Override
            protected void publishResults(final CharSequence constraint, final FilterResults results) {

                autoCompleteSetList = (ArrayList<AutoCompleteSet>)results.values;

                if( results.count > 0 ) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return myFilter;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate( R.layout.layout_search_dropdown, parent, false );

        int searchCompanyCnt = 0;
        int searchHashTagCnt = 0;
        int searchUserCnt = 0;

        if( autoCompleteSetList.size() > position ) {
            AutoCompleteSet autoCompleteSet = getItem( position );

            LinearLayout companySection = (LinearLayout) view.findViewById(R.id.search_company_section);
            CustomImageView companyImg = (CustomImageView) view.findViewById(R.id.search_company_img);
            TextView companyName = (TextView) view.findViewById(R.id.search_company_name);
            TextView companyAddress = (TextView) view.findViewById(R.id.search_company_address);

            LinearLayout userSection = (LinearLayout) view.findViewById(R.id.search_user_section);
            ImageView userImg = (ImageView) view.findViewById(R.id.search_user_img);
            TextView userName = (TextView) view.findViewById(R.id.search_user_name);

            LinearLayout textSection = (LinearLayout) view.findViewById(R.id.search_text_section);
            TextView itemText = (TextView) view.findViewById(R.id.search_text_name);

            if( autoCompleType.equals( "company" ) || autoCompleType.equals( "hashtag" ) ) {
                companySection.setVisibility( View.GONE );
                userSection.setVisibility( View.GONE );

                if( autoCompleteSet instanceof Company ) {
                    itemText.setText( ((Company)autoCompleteSet).getName() );
                } else if( autoCompleteSet instanceof HashTag ) {
                    itemText.setText( ((HashTag)autoCompleteSet).getName() );
                }
            } else if( autoCompleType.equals( "search" ) ) {

                if( autoCompleteSet instanceof Company ) {
                    companyName.setText( ((Company)autoCompleteSet).getName() );
                    companyAddress.setText( ((Company)autoCompleteSet).getAddress() );
                    Glide.with(getContext()).load( ((Company)autoCompleteSet).getImage() ).centerCrop().placeholder( R.color.image_placeholder_color ).into( companyImg );
                    searchCompanyCnt++;
                } else if( autoCompleteSet instanceof HashTag ) {
                    itemText.setText( ((HashTag)autoCompleteSet).getName() );
                    searchHashTagCnt++;
                } else if( autoCompleteSet instanceof User ){
                    Glide.with( getContext() ).load( ((User) autoCompleteSet).getImage() ).bitmapTransform( new CropCircleTransformation( getContext() ) ).placeholder( R.drawable.default_user_icon ).into( userImg );
                    userName.setText( ((User) autoCompleteSet).getName() );
                    searchUserCnt++;
                }

                if( searchCompanyCnt < 1 ) {
                    companySection.setVisibility( View.GONE );
                }
                if( searchUserCnt < 1 ) {
                    userSection.setVisibility( View.GONE );
                }
                if( searchHashTagCnt < 1 ) {
                    textSection.setVisibility( View.GONE );
                }
            }
        }

        return view;
    }

    public List<AutoCompleteSet> getParseJson( String type, String term ) {
        List<AutoCompleteSet> listData = new ArrayList<AutoCompleteSet>();

        try{
            term = term.replace(" ", "%20");
            URL js = new URL( "http://app.wedqueen.com/api/v1/" + type + "/autocomplete?keyword=" + term );
            URLConnection jc = js.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(jc.getInputStream()));
            String line = reader.readLine();
            JSONObject jsonResponse = new JSONObject( line );

            if( type.equals( "company" ) || type.equals( "search" ) ) {
                JSONArray companies = jsonResponse.getJSONArray("companies");
                for(int i = 0; i < companies.length(); i++){
                    JSONObject r = companies.getJSONObject(i);
                    Company company = new Company( r.getString( "name" ), r.getString( "image" ), r.getString( "address" ), r.getString( "url" ) );
                    listData.add( company );
                }
            }

            if( type.equals( "search" ) ) {
                JSONArray users = jsonResponse.getJSONArray("users");
                for(int i = 0; i < users.length(); i++){
                    JSONObject r = users.getJSONObject(i);
                    User user = new User( r.getInt( "userId" ), r.getString( "name" ), r.getString( "image" ) );
                    listData.add( user );
                }
            }

            if( type.equals( "hashtag" ) || type.equals( "search" ) ) {
                JSONArray hashTags = jsonResponse.getJSONArray("hashTags");
                for(int i = 0; i < hashTags.length(); i++){
                    JSONObject r = hashTags.getJSONObject(i);
                    HashTag hashTag = new HashTag( r.getString( "name" ), r.getInt( "count" ), r.getString( "url") );
                    listData.add( hashTag );
                }
            }

        }catch (Exception e){
            Log.e( "WQTEST", "EXCEPTION " + e );
        }

        return listData;
    }
}
