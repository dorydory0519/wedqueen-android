package com.jjlee.wedqueen.rest.shop.controller;

import com.jjlee.wedqueen.rest.sector.model.SectorItemLog;
import com.jjlee.wedqueen.rest.sector.model.SectorItemLogResponse;
import com.jjlee.wedqueen.rest.sector.model.SectorResponse;
import com.jjlee.wedqueen.rest.sector.service.SectorLogService;
import com.jjlee.wedqueen.rest.sector.service.SectorService;
import com.jjlee.wedqueen.rest.shop.model.ShopResponse;
import com.jjlee.wedqueen.rest.shop.service.ShopService;
import com.jjlee.wedqueen.support.RetrofitController;

import java.util.List;

import retrofit2.Call;

public class ShopController extends RetrofitController {
    private ShopService shopService;

    public ShopController() {
        super();
        shopService = RETROFIT.create(ShopService.class);
    }

    public Call<ShopResponse> getGifts( String filter ) {
        return shopService.getGifts( filter );
    }

    public Call<SectorResponse> getPremiumGifts() {
        return shopService.getPremiumGifts();
    }

}
