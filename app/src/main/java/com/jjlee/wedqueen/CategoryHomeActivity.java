package com.jjlee.wedqueen;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjlee.wedqueen.rest.app.model.TabCategory;
import com.jjlee.wedqueen.rest.tabCategory.controller.TabCategoryController;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.Prefs;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.ArrayList;
import java.util.HashMap;

public class CategoryHomeActivity  extends AppCompatActivity {

    private Prefs prefs;

    private TabLayout tabLayout;
    private CategoryHomeAdapter categoryHomeAdapter;
    private ViewPager categoryHomeViewPager;
    private HashMap<String, ArrayList<TabCategory>> allTabCategories;
    private HashMap<String, Boolean> isTipOpenList;
    private ArrayList<TabCategory> tabCategories;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_home);

        prefs = new Prefs( this );
        allTabCategories = prefs.getCurrentTabCategories();
        tabCategories = allTabCategories.get( "contentfeed" );
        isTipOpenList = new HashMap<String, Boolean>();

        tabLayout = findViewById( R.id.category_home_tablayout );
        categoryHomeViewPager = findViewById( R.id.category_home_viewpager );

        categoryHomeAdapter = new CategoryHomeAdapter( getSupportFragmentManager() );

        initViewPager();


        findViewById( R.id.navbar_back ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById( R.id.fab_realreview ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startOtherActivity( ContentEditActivity.class );
            }
        });

    }


    private void initViewPager() {
        categoryHomeViewPager.setAdapter( categoryHomeAdapter );
        categoryHomeViewPager.setOffscreenPageLimit( 1 );
        tabLayout.setupWithViewPager( categoryHomeViewPager );

        String selectedCategory = getIntent().getStringExtra( "categoryName" );
        int categoryIdx = 0;
        int selectedIdx = 0;

        for( TabCategory tabCategory : tabCategories ) {
            tabLayout.newTab().setText( tabCategory.getCategoryNameKor() );
            isTipOpenList.put( tabCategory.getCategoryName(), tabCategory.isTipOpen() );

            if( tabCategory.getCategoryName().equals( selectedCategory ) ) {
                selectedIdx = categoryIdx;
            }
            categoryIdx++;
        }

        for( int i = 0; i < tabLayout.getTabCount(); i++ ) {
            TabLayout.Tab tab = tabLayout.getTabAt( i );

            if( !ObjectUtils.isEmpty( tab ) ) {
                TextView tabTextView = new TextView( this );
                tab.setCustomView( tabTextView );
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT );
                layoutParams.weight = 0f;

                tabTextView.setLayoutParams( layoutParams );
                tabTextView.setText( tabCategories.get( i ).getCategoryNameKor() );
                if( i == 0 ) {
                    tabTextView.setTextColor( ContextCompat.getColor( this, R.color.tab_text_selected ) );
                    tabTextView.setTextSize( TypedValue.COMPLEX_UNIT_DIP, 16 );
                    tabTextView.setTypeface( Typeface.createFromAsset( getAssets(), "NanumSquareOTFBold.otf" ), Typeface.BOLD );
                } else {
                    tabTextView.setTextColor( ContextCompat.getColor( this, R.color.tab_text ) );
                    tabTextView.setTextSize( TypedValue.COMPLEX_UNIT_DIP, 13 );
                    tabTextView.setTypeface( Typeface.createFromAsset( getAssets(), "NanumSquareOTFRegular.otf" ), Typeface.NORMAL );
                }

            }

            tabLayout.getTabAt( i ).setText( tabCategories.get( i ).getCategoryNameKor() );
        }
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                TextView tabTextView = (TextView) tab.getCustomView();

                if( !ObjectUtils.isEmpty( tabTextView ) ) {
                    tabTextView.setTextColor( ContextCompat.getColor( CategoryHomeActivity.this, R.color.tab_text_selected ) );
                    tabTextView.setTextSize( TypedValue.COMPLEX_UNIT_DIP, 16 );
                    tabTextView.setTypeface( Typeface.createFromAsset( getAssets(), "NanumSquareOTFBold.otf" ), Typeface.BOLD );
                }
                categoryHomeViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                TextView tabTextView = (TextView) tab.getCustomView();

                if( !ObjectUtils.isEmpty( tabTextView ) ) {
                    tabTextView.setTextColor( ContextCompat.getColor( CategoryHomeActivity.this, R.color.tab_text ) );
                    tabTextView.setTextSize( TypedValue.COMPLEX_UNIT_DIP, 13 );
                    tabTextView.setTypeface( Typeface.createFromAsset( getAssets(), "NanumSquareOTFRegular.otf" ), Typeface.NORMAL );
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        categoryHomeViewPager.setCurrentItem( selectedIdx );
    }



    public class CategoryHomeAdapter extends FragmentStatePagerAdapter {

        public CategoryHomeAdapter( FragmentManager fragmentManager ) {
            super( fragmentManager );
        }

        @Override
        public Fragment getItem(int position) {
            return NewCategoryFragment.getInstance( tabCategories.get( position ).getCategoryName(), tabCategories.get( position ).getId() );
        }

        @Override
        public int getCount() {
            return tabCategories.size();
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
//            super.restoreState(state, loader);
        }
    }


    private void startOtherActivity( Class<?> activityClass ) {
        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            Intent intent = new Intent( this, activityClass );
            startActivity( intent );
        } else {
            Intent intent = new Intent( this, AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            startActivityForResult( intent, Constants.REQ_CODE_LOGIN_IN_MAINACTIVITY );
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
