package com.jjlee.wedqueen.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.gms.gcm.GcmListenerService;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.NotificationActivity;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.receivers.RoutingReceiver;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.support.NewMessageNotification;
import java.net.URLDecoder;

/**
 * Created by JJLEE on 2016. 3. 30..
 *
 * 이 파일은 GCM으로 메시지가 도착하면 디바이스에 받은 메세지를 어떻게 사용할지에 대한 내용을 정의하는 클래스이다.
 *
 * * onMessageReceived() : GCM으로부터 이 함수를 통해 메세지를 받는다.
 *                         이때 전송한 SenderID와 Set 타입의 데이터 컬렉션 형태로 받게된다.
 *
 * * sendNotification() : GCM으로부터 받은 메세지를 디바이스에 알려주는 함수이다.
 *
 */
public class MyGcmListenerService extends GcmListenerService {

    private PowerManager.WakeLock wl;
    public static final int NOTIFICATION_ID = 1;
    private static final String TAG = "MyGcmListenerService";

    /**
     *
     * @param from SenderID 값을 받아온다.
     * @param data Set형태로 GCM으로 받은 데이터 payload이다.
     */
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String url = data.getString("url");
        String msg = data.getString("msg");
        String badge = data.getString("badge");
        String notiId = data.getString("notiId");

//        android.os.Debug.waitForDebugger();

        // GCM으로 받은 메세지를 디바이스에 알려주는 sendNotification()을 호출한다.
        sendNotification(url, msg, badge, notiId);
    }

    public void showNotiActivity(final String msg, final PendingIntent pendingIntent) {
        startActivity(
                new Intent(this, NotificationActivity.class)
                        .putExtra("msg", msg)
                        .putExtra("pendingIntent", pendingIntent)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        );
    }

    public void showNotiMessage(final String msg, final PendingIntent pendingIntent) {
        final Handler mainHandler = new Handler(this.getMainLooper());
        mainHandler.post(new Runnable() {
            @Override
            public void run() {

                final WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
                final NotificationManager nm = (NotificationManager) MyApplication
                        .getGlobalApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

                LayoutInflater li = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                final View notiView = li.inflate(R.layout.noti_message, null);

                WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.TYPE_PHONE,
                        WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                        PixelFormat.TRANSLUCENT);
                params.windowAnimations = android.R.style.Animation_Toast;

                params.gravity = Gravity.TOP | Gravity.CENTER;
                params.x = 0;
                params.y = 130;


                final Button cancelButton = (Button)notiView.findViewById(R.id.cancel_button);
                final Button confirmButton = (Button)notiView.findViewById(R.id.confirm_button);
                ((TextView) notiView.findViewById(R.id.text_to_show)).setText(msg);

                if ( (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        && Settings.canDrawOverlays(MyGcmListenerService.this) )
                        || Build.VERSION.SDK_INT < Build.VERSION_CODES.M ) {

                    wm.addView(notiView, params);

                    cancelButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            confirmButton.setOnClickListener(null);
                            cancelButton.setOnClickListener(null);
                            wm.removeView(notiView);
                        }
                    });

                    confirmButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                pendingIntent.send();
                            } catch (PendingIntent.CanceledException e) {
                                e.printStackTrace();
                            }
                            confirmButton.setOnClickListener(null);
                            cancelButton.setOnClickListener(null);
                            wm.removeView(notiView);
                            nm.cancelAll();
                        }
                    });

                    mainHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                confirmButton.setOnClickListener(null);
                                cancelButton.setOnClickListener(null);
                                wm.removeView(notiView);
                            } catch (Exception e) {
                                Log.i("RemovingNotiViewFailed", "notiView Already Removed!");
                            }
                        }
                    }, 3000);
                }
            }
        });


    }

    /**
     * 실제 디바에스에 GCM으로부터 받은 메세지를 알려주는 함수이다.
     * notify()메소드가 디바이스의 Notification Center에 알림을 올린다.
     * @param url
     * @param msg
     */
    private void sendNotification(String url, String msg, String badge, String notiId) {

        PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);

        Intent newIntent = new Intent(this, RoutingReceiver.class);
        String decodedUrl=null, decodedMsg=null, decodedBadge=null, decodedNotiId=null;

        try {
            if(url != null) { decodedUrl = URLDecoder.decode(url, "UTF-8"); }
            if(msg != null) { decodedMsg = URLDecoder.decode(msg, "UTF-8"); }
            if(badge != null) { decodedBadge = URLDecoder.decode(badge, "UTF-8"); }
            if(notiId != null) { decodedNotiId = URLDecoder.decode(notiId, "UTF-8"); }

            if(decodedUrl.startsWith("/")) {
                decodedUrl = decodedUrl.substring(1);
            }

            newIntent.putExtra("URL", Constants.BASE_URL + decodedUrl);
            newIntent.putExtra("NOTI_ID", decodedNotiId);
            newIntent.putExtra( "MSG", decodedMsg );

            PendingIntent contentIntent = PendingIntent.getBroadcast(this, NOTIFICATION_ID, newIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);

            int badgeCount;
            if(decodedBadge == null || decodedBadge.isEmpty()) {
                badgeCount = MyApplication.getGlobalApplicationContext().getBadgeCount();
            } else {
                badgeCount = Integer.parseInt(decodedBadge);
            }

            NewMessageNotification.notify(this, contentIntent, decodedMsg, badgeCount, notiId == null);

            if(MyApplication.getGlobalApplicationContext().isLoggedIn()) {
                MyApplication.getGlobalApplicationContext().setBadgeCount(badgeCount);
                Intent badgeCountChanged = new Intent(Constants.BADGE_COUNT_CHANGED);
                LocalBroadcastManager.getInstance(this).sendBroadcast(badgeCountChanged);
            }

            // 알림 Dialog 띄우지 않도록 제한
            if(MyApplication.isScreenOn(this)) {
//                showNotiMessage(decodedMsg, contentIntent);
                wl = pm.newWakeLock( PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, TAG);
                wl.acquire(2000);
            } else {
//                showNotiActivity(decodedMsg, contentIntent);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}