package com.jjlee.wedqueen.services;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;
import com.jjlee.wedqueen.support.Prefs;

/**
 * Created by JJLEE on 2016. 3. 30..
 *
 * 이 파일은 Instance ID를 획득하기 위한 리스너를 상속받아서 토큰을 갱신하는 코드를 추가한다.
 *
 */
public class MyInstanceIdListenerService extends InstanceIDListenerService{
    private static final String TAG = "MyInstanceIDLS";

    @Override
    public void onTokenRefresh() {
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }
}
