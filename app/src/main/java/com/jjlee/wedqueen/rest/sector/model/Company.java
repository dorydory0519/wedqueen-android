package com.jjlee.wedqueen.rest.sector.model;

/**
 * Created by esmac on 2017. 12. 18..
 */

public class Company {
    private String name;
    private String url;
    private String thumbnailUrl;
    private String description;
    private int paymentCount;
    private int surveyCount;
    private int realWeddingCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPaymentCount() {
        return paymentCount;
    }

    public void setPaymentCount(int paymentCount) {
        this.paymentCount = paymentCount;
    }

    public int getSurveyCount() {
        return surveyCount;
    }

    public void setSurveyCount(int surveyCount) {
        this.surveyCount = surveyCount;
    }

    public int getRealWeddingCount() {
        return realWeddingCount;
    }

    public void setRealWeddingCount(int realWeddingCount) {
        this.realWeddingCount = realWeddingCount;
    }
}
