package com.jjlee.wedqueen.utils;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;

/**
 * Created by esmac on 2017. 9. 5..
 */

public class CustomScrollView extends NestedScrollView {

    OnBottomReachedListener mListener;

    public CustomScrollView(Context context) {
        super(context);
    }

    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {

        View view = (View) getChildAt(getChildCount()-1);

        int diff = (view.getBottom()-(getHeight()+getScrollY()));

        if( !ObjectUtils.isEmpty( mListener ) && diff < 500 ) {
            mListener.onBottomReached();
        }

        super.onScrollChanged(l, t, oldl, oldt);
    }

    public OnBottomReachedListener getOnBottomReachedListener() {
        return mListener;
    }

    public void setOnBottomReachedListener(
            OnBottomReachedListener onBottomReachedListener) {
        mListener = onBottomReachedListener;
    }

    public interface OnBottomReachedListener{
        public void onBottomReached();
    }
}
