package com.jjlee.wedqueen.rest.account.model;

import com.jjlee.wedqueen.rest.common.model.AppResponse;

/**
 * Created by JJLEE on 2016. 7. 11..
 */
public class NotificationCountResponse extends AppResponse {

    private Integer notificationCount;

    public Integer getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(Integer notificationCount) {
        this.notificationCount = notificationCount;
    }
}
