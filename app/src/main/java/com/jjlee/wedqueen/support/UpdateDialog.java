package com.jjlee.wedqueen.support;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;

import com.jjlee.wedqueen.R;

/**
 * Created by JJLEE on 2016. 4. 12..
 */
public class UpdateDialog {
    public static AlertDialog createDialog(final Context context) {
        AlertDialog.Builder ab = new AlertDialog.Builder(context);
        ab.setTitle("앱 업데이트 필요");
        ab.setMessage("중요한 변경 사항이 있어 필수적으로 업데이트를 해야 앱을 사용하실 수 있습니다. 확인 버튼을 누르시면 앱 업데이트를 위해 구글 플레이스토어로 이동합니다.");
        ab.setCancelable(false);
//        ab.setIcon(context.getResources().getDrawable(R.drawable.ic_launcher));

        ab.setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(context.getString(R.string.app_url)) );
                context.startActivity(intent);
            }
        });
        return ab.create();
    }
}
