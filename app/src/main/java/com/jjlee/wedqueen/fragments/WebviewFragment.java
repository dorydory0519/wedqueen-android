package com.jjlee.wedqueen.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.CustomViews.CustomWebChromeClient;
import com.jjlee.wedqueen.CustomViews.CustomWebviewClient;
import com.jjlee.wedqueen.support.WebviewJavascriptInterface;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by JJLEE on 2016. 3. 28..
 * 뷰페이져의 Page로 사용될 웹뷰 프래그먼트
 * 네이티브로 개발되기 전까지는 이 프래그먼트로 하이브리드 앱을 구현한다.
 */

public class WebviewFragment extends PageFragment {
    private static String TAG = "WebviewFragment";
    private static String PAGE_INDEX = "page_index";
    private static String ROOT_URL = "root_url";

    Map<String, String> customHeader = new HashMap<>();

    private WebView mWebview;
    private ViewTreeObserver.OnScrollChangedListener mOnScrollChangedListener;

    private String mRootUrl;
    private boolean mFirstLoaded = false; // 처음 로딩이 되었는지 여부 판단
    private int mIndex; // 뷰페이저에서의 인덱스


    public static WebviewFragment newInstance(String rootUrl, int index) {
        WebviewFragment webviewFragment = new WebviewFragment();
        Bundle args = new Bundle();
        args.putString(ROOT_URL, rootUrl);
        args.putInt(PAGE_INDEX, index);
        webviewFragment.setArguments(args);
        return webviewFragment;
    }


    /**
     * 유저에게 이 프래그먼트(페이지)가 보이는 순간을
     * 이 메서드에서 캐치한다
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !mFirstLoaded) { // 이 프래그먼트가 보이는 순간에 로딩이 안되었다면
            mWebview.loadUrl(mRootUrl, customHeader);
            mFirstLoaded = true;
        } else {

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mWebview = new WebView(context);
        mWebview.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT < 19) {
            mWebview.getSettings().setPluginState(WebSettings.PluginState.ON);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            mWebview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        mWebview.setWebViewClient(new CustomWebviewClient(this));
        mWebview.setWebChromeClient(new CustomWebChromeClient(this));
        WeakReference wr = new WeakReference(context);
        mWebview.addJavascriptInterface(new WebviewJavascriptInterface(wr), "androidJSI");

        customHeader.put("deviceType", "android");
        customHeader.put("version", MyApplication.getGlobalApplicationContext().getVersionName());

        Bundle args = getArguments();
        if (args != null) {
            mRootUrl = args.getString(ROOT_URL);
            mIndex = args.getInt(PAGE_INDEX);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // 루트 뷰 인플레이션
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_webview, container, false);
        initiateNecessaryComponents(rootView);

        rootView.findViewById(R.id.retry_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideNetworkError();
                mWebview.loadUrl(mRootUrl, customHeader);
            }
        });

        mSwipeRefreshLayout.addView(mWebview);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                hideNetworkError();
                mWebview.loadUrl(mRootUrl, customHeader);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        mSwipeRefreshLayout.getViewTreeObserver().addOnScrollChangedListener(mOnScrollChangedListener =
                new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {
                        if (mWebview.getScrollY() == 0)
                            mSwipeRefreshLayout.setEnabled(true);
                        else
                            mSwipeRefreshLayout.setEnabled(false);

                    }
                });
    }


    @Override
    public void onStop() {
        mSwipeRefreshLayout.getViewTreeObserver().removeOnScrollChangedListener(mOnScrollChangedListener);
        super.onStop();
    }

    public void setmFirstLoaded(boolean mFirstLoaded) {
        this.mFirstLoaded = mFirstLoaded;
    }

    @Override
    public void refresh() {
        mWebview.reload();
    }

    public WebView getWebview() {
        return mWebview;
    }
}
