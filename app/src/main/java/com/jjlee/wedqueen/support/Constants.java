package com.jjlee.wedqueen.support;

import com.jjlee.wedqueen.R;

/**
 * Created by JJLEE on 2016. 3. 28..
 */
public class Constants {
//    public static final String BASE_URL = "http://192.168.0.13:8080/wedding/";

    public static final String BASE_URL = "http://app.wedqueen.com/";
//    public static String BASE_URL = "http://172.30.1.55:8080/wedding/";
//    public static String BASE_URL = "http://13.209.19.152/";
//    public static String BASE_URL = "http://13.209.5.108/";
//    public static String BASE_URL = "http://wedqueen-1574303488.ap-northeast-2.elb.amazonaws.com/";

    public static String LOG_BASE_URL = "http://wedqueen-log-1583723447.ap-northeast-2.elb.amazonaws.com/";
//    public static String LOG_BASE_URL = "http://10.130.104.221:8080/";
//    public static String LOG_BASE_URL = "http://172.30.1.13:8080/";

    public static final String CONTENT_FEED_URL = BASE_URL + "content/feed";

    public static final String CONTENT_URL = BASE_URL + "content/";
    public static final String WEDDING_TALK_URL = BASE_URL + "weddingtalk";
    public static final String HOTDEAL_URL = BASE_URL + "hotdeal";
    public static final String CHECKLIST_URL = BASE_URL + "user/checklist";
    public static final String CHECKLIST_STATS_URL = BASE_URL + "user/dday/stats";
    public static final String CHECKLIST_CATEGORY_URL = BASE_URL + "user/dday/category/";
    public static final String AVG_BUDGET_URL = BASE_URL + "user/budget/states";
    public static final String MY_PAGE_URL = BASE_URL + "user";
    public static final String NOTI_URL = BASE_URL + "noti";
    public static final String ESTIMATION_URL = BASE_URL + "estimation";
    public static final String INQUIRY_URL = BASE_URL + "user/inquiry";
    public static final String SCRAP_URL = BASE_URL + "user/lovedContent";

    public static final String POINTSHOP_URL = BASE_URL + "shop/point";
    public static final String POINTHISTORY_URL = BASE_URL + "shop/history";
    public static final String POINTPAYMENT_URL = BASE_URL + "shop/payment/";
    public static final String MOBILE_INVITATION_URL = BASE_URL + "user/mobile-invitation";

    public static final String USAGE_URL = BASE_URL + "/terms/usage";
    public static final String INFO_URL = BASE_URL + "/terms/info";

    public static final String SEARCH_URL = BASE_URL + "/search";

    public static final String SIGN_IN_URL = BASE_URL + "signin";
    public static final String BACK_REFRESH_URL = BASE_URL + "back-refresh";
    public static final String BACK_URL = BASE_URL + "back";
    public static final String REFRESH_URL = BASE_URL + "refresh";

    // Badge 관련 intent action 상수(Main Activity Receiver Action)
    public static final String BADGE_COUNT_CHANGED = "badgeCountChanged";

    // GCM 관련 intent action 상수들
    public static final String REGISTRATION_READY = "registrationReady";
    public static final String REGISTRATION_GENERATING = "registrationGenerating";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    // 로그인 / 로그아웃 관련 상수들
    public static final String ACTION_TYPE = "action_type";
    public static final String ACTION_TYPE_AUTO_LOGIN = "action_type_auto_login";
    public static final String ACTION_TYPE_MANUAL_LOGIN = "action_type_manual_login";
    public static final String ACTION_TYPE_LOGOUT = "action_type_logout";

    public static final String LOGIN_FROM = "login_from";

    // Airbridge App Name & Token
//    public static final String airBridgeAppName = "wedqueen";
//    public static final String airBridgeAppToken = "39623c10541e4c329529232ed6501ef0";

    // SNS 관련 상수들
    public static final int LOGIN_NULL = -1;
    public static final int SNS_TYPE_FACEBOOK = 0;
    public static final int SNS_TYPE_TWITTER = 1;
    public static final int SNS_TYPE_GOOGLE = 2;
    public static final int SNS_TYPE_KAKAO = 3;

    // REQUEST CODES
    public static final int FILE_CHOOSE = 9000;
    public static final int REQ_CODE_OPEN_FULLSCREEN_ACTIVITY = 9001;
    public static final int REQ_CODE_OPEN_DDAYSETTING_ACTIVITY = 9002;
    public static final int REQ_CODE_OPEN_CONTENTEDIT_ACTIVITY = 9003;
    public static final int REQ_CODE_OPEN_DETAILCOMPANY_ACTIVITY = 9004;
    public static final int REQ_CODE_OPEN_OTHERUSERDIARY_ACTIVITY = 9005;
    public static final int REQ_CODE_OPEN_TUTORIAL_ACTIVITY = 9006;
    public static final int REQ_CODE_OPEN_MYRESERV_ACTIVITY = 9007;
    public static final int REQ_CODE_OPEN_MYPAYMENT_ACTIVITY = 9008;
    public static final int REQ_CODE_OPEN_REALWEDDING_ACTIVITY = 9009;

    public static final int REQ_CODE_OPEN_THUMBNAIL_IMG = 9011;
    public static final int REQ_CODE_OPEN_CONTENT_IMG = 9012;
    public static final int REQ_CODE_OPEN_PROFILE_IMG = 9013;

    public static final int REQ_CODE_LOGIN = 9050;
    public static final int REQ_CODE_LOGOUT = 9051;
    public static final int REQ_CODE_LOGIN_IN_MAINACTIVITY = 9052;

    public static final int REQ_EXTERNAL_STORAGE_PERMISSION = 9;

    public static final String[] TAB_NAMES =
            { "리얼후기", "웨딩톡", "내예산", "포인트샵", "내정보" };

    // TAB ICONS
    public static final int[] TAB_ICONS_ON =
            {R.drawable.tab_review_on, R.drawable.tab_talk_on, R.drawable.tab_checklist_on,
                    R.drawable.tab_point_on, R.drawable.tab_mypage_on};

    public static final int[] TAB_ICONS_OFF =
            {R.drawable.tab_review_off, R.drawable.tab_talk_off, R.drawable.tab_checklist_off,
                    R.drawable.tab_point_off, R.drawable.tab_mypage_off};

    // public static final HashMap<String, ArrayList<TabCategory>> TAB_CATEGORIES_NAME = new HashMap<>();

}


