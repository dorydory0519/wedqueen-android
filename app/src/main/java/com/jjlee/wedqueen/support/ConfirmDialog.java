package com.jjlee.wedqueen.support;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.jjlee.wedqueen.R;

public class ConfirmDialog extends Dialog {

    private String text;

    public ConfirmDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_confirm);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        ((TextView)this.findViewById(R.id.text_to_show)).setText(text);
    }
}
