package com.jjlee.wedqueen.utils;

import java.text.DecimalFormat;

public class DecimalUtils {
    private static final DecimalFormat decimalFormat = new DecimalFormat( "###,###" );

    public static String convertDecimalFormat( String resStr ) {
        String result = "";

        try {
            int resInt = Integer.parseInt( resStr );
            result = decimalFormat.format( resInt );
        } catch ( NumberFormatException ne ) {
        } catch ( Exception e ) {
        }

        return result;
    }

    public static String convertDecimalFormat( int resInt ) {
        String result = "";

        try {
            result = decimalFormat.format( resInt );
        } catch ( NumberFormatException ne ) {
        } catch ( Exception e ) {
        }

        return result;
    }
}
