package com.jjlee.wedqueen.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CalendarUtil {

    public static String getStringDate( long milli ) {

        String resultStr = "";

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis( milli );
        resultStr += c.get( Calendar.YEAR );
        resultStr += ".";
        resultStr += String.format( "%02d", ( c.get( Calendar.MONTH ) + 1 ) );
        resultStr += ".";
        resultStr += String.format( "%02d", c.get( Calendar.DATE ) );
        resultStr += " ";
        resultStr += String.format( "%02d", c.get( Calendar.HOUR ) );
        resultStr += ":";
        resultStr += String.format( "%02d", c.get( Calendar.MINUTE ) );
        resultStr += ":";
        resultStr += String.format( "%02d", c.get( Calendar.SECOND ) );

        return resultStr;
    }

    public static String getStringDate( Calendar date ) {

        String resultStr = "";
        resultStr += date.get( Calendar.YEAR );
        resultStr += ".";
        resultStr += String.format( "%02d", ( date.get( Calendar.MONTH ) + 1 ) );
        resultStr += ".";
        resultStr += String.format( "%02d", date.get( Calendar.DATE ) );

        return resultStr;
    }

    public static String getdDayString(Calendar date) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, date.get(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, date.get(Calendar.MINUTE));
        c.set(Calendar.SECOND, date.get(Calendar.SECOND));
        c.set(Calendar.MILLISECOND, date.get(Calendar.MILLISECOND));

        String resultStr = "";
        int remainDay = (int) ((date.getTimeInMillis() - c.getTimeInMillis()) / 86400000);

        if( remainDay < 0 ) {
            resultStr = "D+" + Math.abs( remainDay );
        } else if( remainDay > 0 ) {
            resultStr = "D-" + remainDay;
        } else {
            resultStr = "D-Day";
        }

        return resultStr;
    }

    // yyyy.MM.dd
    public static String getdDayString(String date) {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat( "yyyy.MM.dd" );
        try {
            c.setTime( simpleDateFormat.parse( date ) );
        } catch (ParseException e) {
            return "D-?";
        }

        return getdDayString( c );
    }


    // after 일이 지난 날의 0시에 해당하는 millisecond를 구함
    public static long getFixedDateMilliFromNowToAfter( int after ) {

        Calendar cal = Calendar.getInstance();
        cal.add( Calendar.DATE, after );
        cal.set( Calendar.HOUR, 0 );
        cal.set( Calendar.MINUTE, 0 );
        cal.set( Calendar.SECOND, 0 );
        cal.set( Calendar.MILLISECOND, 0 );

        return cal.getTimeInMillis();
    }
}
