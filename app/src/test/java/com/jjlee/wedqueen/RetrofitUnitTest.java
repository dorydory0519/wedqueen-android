package com.jjlee.wedqueen;

import com.jjlee.wedqueen.rest.account.controller.AccountController;
import com.jjlee.wedqueen.rest.account.model.Login;
import com.jjlee.wedqueen.rest.account.model.LoginCommonResponse;

import junit.framework.Assert;

import org.junit.Test;

import java.io.IOException;

import retrofit2.Call;

/**
 * Created by leejungyi on 2016. 4. 1..
 */
public class RetrofitUnitTest {
    @Test
    public void retrofit2Test() throws IOException {
        //rest test (지우셔도 되요!!!)
        //get
        //TestService testService = new TestController().getTestService();

        MyApplication myApplication = new MyApplication();


        String deviceToken = "ABCDE"; // 무조건 저장되어있는 디바이스 토큰 사용. 이후에 바로 갱신 한번 함.
        String expireTime = "123456";
        String version = "1.4.0";
        String accessToken = "ababab";
        Login login = new Login(accessToken, deviceToken, expireTime, version);
        AccountController lc = new AccountController();
        Call<LoginCommonResponse> call = lc.kakaoLoggedIn(login);

//        TestController testController = new TestController();
//        Call<com.jjlee.wedqueen.rest.test.model.Test> call = testController.getTest(1);

        try {

//            com.jjlee.wedqueen.rest.test.model.Test test = call.execute().body();
            LoginCommonResponse lr = call.execute().body();
            System.out.println("@@@ login POST Response @@@ Success");
//            System.out.println("acquired test name :" + lr.getHttpStatus());
            Assert.assertTrue(lr != null);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        call.enqueue(new Callback<com.jjlee.wedqueen.rest.test.model.Test>() {
//            @Override
//            public void onResponse(Response<com.jjlee.wedqueen.rest.test.model.Test> response) {
//                Log.d("@@@ test Response @@@", "Success");
//
//                if (response.isSuccess()) {
//                    com.jjlee.wedqueen.rest.test.model.Test test = response.body();
//                    Log.d("acquired test", test.getName());
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable throwable) {
//            }
//        });

        //post
//        call = testController.saveTest();
//        com.jjlee.wedqueen.rest.test.model.Test test = call.execute().body();
//        System.out.println("@@@ test POST Response @@@ Success");
//        System.out.println("acquired test name :" + test.getName());
//        Assert.assertTrue(test != null);

//        call.enqueue(new Callback<com.jjlee.wedqueen.rest.test.model.Test>() {
//            @Override
//            public void onResponse(Response<com.jjlee.wedqueen.rest.test.model.Test> response) {
//                Log.d("@@@ test Response @@@", "Success");
//                if (response.isSuccess()) {
//                    com.jjlee.wedqueen.rest.test.model.Test test = response.body();
//                    Log.d("acquired test", test.getName());
//                    Assert.assertTrue(test.getName() == null);
//                }
//            }
//
//            @Override
//            public void onFailure(Throwable throwable) {
//            }
//        });

    }
}
