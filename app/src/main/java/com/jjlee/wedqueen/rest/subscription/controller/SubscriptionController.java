package com.jjlee.wedqueen.rest.subscription.controller;

import com.jjlee.wedqueen.rest.subscription.model.SubscriptionGetResponse;
import com.jjlee.wedqueen.rest.subscription.model.SubscriptionResponse;
import com.jjlee.wedqueen.rest.subscription.service.SubscriptionService;
import com.jjlee.wedqueen.rest.user.model.UserContentResponse;
import com.jjlee.wedqueen.rest.user.model.UserResponse;
import com.jjlee.wedqueen.rest.user.service.UserService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class SubscriptionController extends RetrofitController {
    private SubscriptionService subscriptionService;

    public SubscriptionController() {
        super();
        subscriptionService = RETROFIT.create(SubscriptionService.class);
    }

    public Call<SubscriptionResponse> postSubscription(String type, int id) {
        return subscriptionService.postSubscription(type, id);
    }

    public Call<SubscriptionResponse> delSubscription(String type, int id) {
        return subscriptionService.delSubscription(type, id);
    }

    public Call<SubscriptionGetResponse> getMyFollowing( String type ) {
        return subscriptionService.getMyFollowing( type );
    }

    public Call<SubscriptionGetResponse> getMyFollower( String type ) {
        return subscriptionService.getMyFollower( type );
    }

}
