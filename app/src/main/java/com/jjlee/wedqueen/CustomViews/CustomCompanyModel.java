package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.google.android.flexbox.FlexboxLayout;
import com.jjlee.wedqueen.R;
import com.jjlee.wedqueen.utils.ObjectUtils;

import java.util.List;

/**
 * Created by esmac on 2017. 9. 9..
 */

public class CustomCompanyModel extends LinearLayout {

    RequestManager mRequestManager;

    RelativeLayout companyContentSection;
    CustomImageView companyThumbnail;
    TextView companyName;
    TextView companyPostScript;
    ImageView contentImg;

    FlexboxLayout contentHashTagWrapper;
    TextView contentPrice;
    TextView contentTitle;

    TextView realWeddingCnt;
    TextView surveyCnt;
    TextView paymentCnt;

    LinearLayout companyInfo;

    private long sectorItemId;

    public CustomCompanyModel(Context context) {
        super(context);
        initView();

    }

    public CustomCompanyModel(Context context, RequestManager requestManager) {
        super(context);
        this.mRequestManager = requestManager;
        initView();

    }

    public CustomCompanyModel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomCompanyModel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_companies, this, false);
        addView(v);

        companyContentSection = (RelativeLayout)findViewById(R.id.company_content_section);
        companyThumbnail = (CustomImageView)findViewById(R.id.company_thumbnail);
        companyName = (TextView)findViewById(R.id.company_name);
        companyPostScript = (TextView)findViewById(R.id.company_postscript);
        contentImg = (ImageView)findViewById(R.id.content_img);
        contentHashTagWrapper = (FlexboxLayout)findViewById(R.id.content_hashTag_wrapper);
        contentPrice = (TextView)findViewById(R.id.content_price);
        contentTitle = (TextView)findViewById(R.id.content_title);
        realWeddingCnt = (TextView)findViewById(R.id.realwedding_cnt);
        surveyCnt = (TextView)findViewById(R.id.survey_cnt);
        paymentCnt = (TextView)findViewById(R.id.payment_cnt);

        companyInfo = (LinearLayout)findViewById(R.id.company_info);
    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CompanyModel);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CompanyModel, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {

        String companyThumbnail_resID = typedArray.getString(R.styleable.CompanyModel_company_thumbnail);
        setCompanyThumbnail(companyThumbnail_resID);

        String companyName_resID = typedArray.getString(R.styleable.CompanyModel_company_name);
        setCompanyName( companyName_resID );

        String companyPostScript_resID = typedArray.getString(R.styleable.CompanyModel_company_postscript);
        setCompanyPostScript( companyPostScript_resID );

        String contentImg_resID = typedArray.getString(R.styleable.CompanyModel_content_img);
        setContentImg( contentImg_resID );

        typedArray.recycle();
    }

    public void setCompanyThumbnail(String resUrl) {
        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load(resUrl).placeholder( R.color.image_placeholder_color ).into( companyThumbnail );
        }
    }
    public void setCompanyName(String resString) {
        companyName.setText( resString );
    }
    public void setCompanyPostScript(String resString) {
        companyPostScript.setText( resString );
    }
    public void setContentImg(String resUrl) {
        if( !ObjectUtils.isEmpty( mRequestManager ) ) {
            mRequestManager.load( resUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( contentImg );
        }
    }

    public void setContentPrice(int resInt) {
        if( resInt > 0 ) {
            contentPrice.setText( resInt + "원" );
        }
    }

    public void setContentTitle(String resString) {
        contentTitle.setText( resString );
    }

    public void setRealWeddingCnt(int resInt) {
        if( resInt > 9 ) {
            realWeddingCnt.setVisibility( View.VISIBLE );
            realWeddingCnt.setText( "리얼웨딩 " + resInt );
        }
    }
    public void setSurveyCnt(int resInt) {
        if( resInt > 9 ) {
            surveyCnt.setVisibility( View.VISIBLE );
            surveyCnt.setText( "예약 " + resInt );
        }
    }
    public void setPaymentCnt(int resInt) {
        if( resInt > 9 ) {
            paymentCnt.setVisibility( View.VISIBLE );
            paymentCnt.setText( "구매 " + resInt );
        }
    }

    public void setCompanyInfoGone() {
        companyInfo.setVisibility( View.GONE );
    }

    public RelativeLayout getCompanyContentSection() {
        return companyContentSection;
    }

    public void setContentHashTagWrapper( List<String> hashTags ) {

        for( String hashTag : hashTags ) {

            CustomEventTagModel customEventTagModel = new CustomEventTagModel( getContext() );
            customEventTagModel.setEventTag( hashTag );

            contentHashTagWrapper.addView( customEventTagModel );

        }
    }

    public long getSectorItemId() {
        return sectorItemId;
    }

    public void setSectorItemId(long sectorItemId) {
        this.sectorItemId = sectorItemId;
    }
}
