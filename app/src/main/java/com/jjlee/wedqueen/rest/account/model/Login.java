package com.jjlee.wedqueen.rest.account.model;

import java.io.Serializable;

/**
 * Created by JJLEE on 2016. 4. 4..
 */
public class Login implements Serializable {

    private String accessToken;
    private String deviceToken;
    private String secret;
    private String expireTime;
    private String version;
    private String advertisingId;

    public Login(String accessToken, String deviceToken, String expireTime, String version, String advertisingId) {
        this(accessToken, deviceToken, null, expireTime, version, advertisingId);
    }

    public Login(String accessToken, String deviceToken, String secret, String expireTime, String version, String advertisingId) {
        this.accessToken = accessToken;
        this.deviceToken = deviceToken;
        this.secret = secret;
        this.expireTime = expireTime;
        this.version = version;
        this.advertisingId = advertisingId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
