package com.jjlee.wedqueen.rest.sector.service;

import com.jjlee.wedqueen.rest.sector.model.SectorItemLog;
import com.jjlee.wedqueen.rest.sector.model.SectorResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SectorService {
    @Headers("Accept: application/json")
    @GET("api/v1/sector/tabcategory/{tabCategoryId}")
    Call<SectorResponse> getSectors(@Path("tabCategoryId") String tabCategoryId);

    @Headers("Accept: application/json")
    @GET("api/v1/sector/invite")
    Call<SectorResponse> getInviteSectors();

}
