package com.jjlee.wedqueen;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class NotificationActivity extends AppCompatActivity {

    private int defaultScreenTimeout;
    private final static int SCREEN_ON_TIME = 1000;
    private boolean shouldFinishOnPause = false;

    private Button cancelButton;
    private Button confirmButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentType( "Activity" )
                .putContentName( "notification" ) );

        if( (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.System.canWrite(this))
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.M ) {
            defaultScreenTimeout = android.provider.Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, -1);
            Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, SCREEN_ON_TIME);
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
                | WindowManager.LayoutParams.FLAG_FULLSCREEN
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        setContentView(R.layout.activity_notification);

        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_OFF);

        final NotificationManager nm = (NotificationManager) MyApplication
                .getGlobalApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        ((TextView) findViewById(R.id.text_to_show)).setText(getIntent().getStringExtra("msg"));
        final PendingIntent pendingIntent = getIntent().getParcelableExtra("pendingIntent");

        View notiWrapperView = findViewById(R.id.noti_wrapper);
        cancelButton = (Button)notiWrapperView.findViewById(R.id.cancel_button);
        confirmButton = (Button)notiWrapperView.findViewById(R.id.confirm_button);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    pendingIntent.send();
                    nm.cancelAll();
                    finish();
                } catch (PendingIntent.CanceledException e) {
                    e.printStackTrace();
                }
            }
        });
    }


//    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
//        super.onWindowFocusChanged(hasFocus);
//        if(!hasFocus) {
//            finish();
//        }
//    }


    @Override
    protected void onResume() {
        super.onResume();
        shouldFinishOnPause = MyApplication.isScreenOn(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(shouldFinishOnPause) {
            finish();
        }
    }

    @Override
    protected void onStop() {
        if( (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && Settings.System.canWrite(this))
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.M ) {
            Settings.System.putInt(getContentResolver(), Settings.System.SCREEN_OFF_TIMEOUT, defaultScreenTimeout);
        }
        super.onStop();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onDestroy() {
        confirmButton.setOnClickListener(null);
        cancelButton.setOnClickListener(null);
        Runtime.getRuntime().gc();
        super.onDestroy();
    }
}
