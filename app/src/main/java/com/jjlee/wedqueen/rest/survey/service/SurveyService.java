package com.jjlee.wedqueen.rest.survey.service;

import com.jjlee.wedqueen.rest.subscription.model.SubscriptionResponse;
import com.jjlee.wedqueen.rest.survey.model.SurveyResponse;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SurveyService {
    @Headers("Accept: application/json")
    @GET("api/v1/survey")
    Call<SurveyResponse> getSurveyAnswer();

    @Headers("Accept: application/json")
    @GET("api/v1/surveys")
    Call<SurveyResponse> getSurveyAnswers();

}
