package com.jjlee.wedqueen.support;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;

import com.jjlee.wedqueen.MainActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.WebviewContainingActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

/**
 * Created by JJLEE on 2016. 4. 5..
 */
public class WebviewJavascriptInterface {

    private final WeakReference<WebviewContainingActivity> mActivity;


    public WebviewJavascriptInterface(WeakReference<WebviewContainingActivity> activity) {
        mActivity = activity;
    }

    @JavascriptInterface
    public void openAndSelectFromGallery(String callbackFunctionName, String requestPageType) {
        mActivity.get().tryOpenGallery(callbackFunctionName, requestPageType, null);
    }

    @JavascriptInterface
    public void notifyUserImageChanged(String userImageUrl) {
        MyApplication.getGlobalApplicationContext().setUserImageUrl( Constants.BASE_URL + userImageUrl);
        ((MainActivity)mActivity.get()).setMypageIcon();
    }

    @JavascriptInterface
    public void moveToEditPage() {
        Log.e( "WQTEST3", "moveToEditPage" );
        ((MainActivity)mActivity.get()).startContentEditActivity();
    }

    @JavascriptInterface
    public void startVideo(String videoId) {
        //TODO: 유툽 비됴...
    }

    @JavascriptInterface
    public void setBadgeCount(String badgeCount) {
        //TODO: 배지 카운드 세팅...
    }

    @JavascriptInterface
    public int getAndroidSDK() {
        return Build.VERSION.SDK_INT;
    }

    @JavascriptInterface
    public void showToolbarItems(String parsableJson) {
        final WebviewContainingActivity thisActivity = mActivity.get();
        try {
            JSONArray jsonArray = new JSONArray(parsableJson);
            thisActivity.toolbarItemsJson = jsonArray;
            for(int i=0; i<jsonArray.length(); i++){
                final JSONObject obj = jsonArray.getJSONObject(i);
                final String item = obj.getString("item");

                thisActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (item.equals("love")) {
                                Boolean loved = obj.getBoolean("loved");
                                if (loved) {
                                    thisActivity.lovedOnButton.setVisibility(View.VISIBLE);
                                } else {
                                    thisActivity.lovedOffButton.setVisibility(View.VISIBLE);
                                }

                            }

                            if (item.equals("delete")) {
                                thisActivity.deleteButton.setVisibility(View.VISIBLE);
                            }

                            if (item.equals("edit")) {
                                thisActivity.editButton.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @JavascriptInterface
    public void openSetting() {
        final WebviewContainingActivity thisActivity = mActivity.get();
        if(thisActivity instanceof MainActivity) {
            thisActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((MainActivity)thisActivity).toggleDrawerLayout();
                }
            });
        }
    }

}
