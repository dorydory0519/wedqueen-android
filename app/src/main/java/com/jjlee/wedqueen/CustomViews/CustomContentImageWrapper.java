package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.jjlee.wedqueen.R;

/**
 * Created by esmac on 2017. 11. 24..
 */

public class CustomContentImageWrapper extends LinearLayout {

    private ImageView contentImg;
    private ImageView btnRemove;
    private ImageView selectedBorder;

    public CustomContentImageWrapper(Context context) {
        super(context);
        initView();
    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate( R.layout.layout_contentimg_wrapper, this, false );

        addView(v);

        contentImg = (ImageView) findViewById(R.id.content_img);
        btnRemove = (ImageView) findViewById(R.id.btn_remove);
        selectedBorder = (ImageView) findViewById(R.id.selected_border);
    }

    public ImageView getContentImg() {
        return contentImg;
    }

    public void setContentImg(ImageView contentImg) {
        this.contentImg = contentImg;
    }

    public ImageView getBtnRemove() {
        return btnRemove;
    }

    public void setBtnRemove(ImageView btnRemove) {
        this.btnRemove = btnRemove;
    }

    public ImageView getSelectedBorder() {
        return selectedBorder;
    }

    public void setSelectedBorder(ImageView selectedBorder) {
        this.selectedBorder = selectedBorder;
    }
}
