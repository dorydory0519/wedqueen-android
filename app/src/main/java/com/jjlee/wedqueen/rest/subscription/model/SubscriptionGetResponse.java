package com.jjlee.wedqueen.rest.subscription.model;

import java.util.List;

/**
 * Created by esmac on 2018. 3. 12..
 */

public class SubscriptionGetResponse {
    private List<SubscriptionDto> users;

    public List<SubscriptionDto> getUsers() {
        return users;
    }

    public void setUsers(List<SubscriptionDto> users) {
        this.users = users;
    }
}
