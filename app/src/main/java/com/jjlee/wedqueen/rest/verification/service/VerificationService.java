package com.jjlee.wedqueen.rest.verification.service;

import com.jjlee.wedqueen.rest.verification.model.VerificationResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface VerificationService {
    @Headers("Accept: application/json")
    @GET("api/v1/verification")
    Call<VerificationResponse> getVerification(@Query("type") String type);
}
