package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jjlee.wedqueen.R;

public class CustomPremiumShopModel extends LinearLayout {

    private long sectorItemId;

    private ImageView premiumShopImg;
    private TextView premiumShopName;
    private TextView premiumShopPoint;

    private String giftImgUrl;
    private String giftBrandName;
    private String giftName;
    private String giftDesc;

    public CustomPremiumShopModel( Context context ) {
        super(context);
        initView();
    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_premium_shops, this, false);
        addView(v);

        premiumShopImg = (ImageView)v.findViewById( R.id.premium_shop_img );
        premiumShopName = (TextView)v.findViewById( R.id.premium_shop_name );
        premiumShopPoint = (TextView)v.findViewById( R.id.premium_shop_point );

    }

    public void setPremiumShopImg( String resUrl ) {
        giftImgUrl = resUrl;
        Glide.with( getContext() ).load( resUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( premiumShopImg );
    }
    public void setPremiumShopName(String resString) {
        giftName = resString;
        premiumShopName.setText( resString );
    }
    public void setPremiumShopPoint(String resString) {
        premiumShopPoint.setText( resString );
    }

    public long getSectorItemId() {
        return sectorItemId;
    }

    public void setSectorItemId(long sectorItemId) {
        this.sectorItemId = sectorItemId;
    }

    public String getGiftImgUrl() {
        return giftImgUrl;
    }

    public void setGiftImgUrl(String giftImgUrl) {
        this.giftImgUrl = giftImgUrl;
    }

    public String getGiftBrandName() {
        return giftBrandName;
    }

    public void setGiftBrandName(String giftBrandName) {
        this.giftBrandName = giftBrandName;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftDesc() {
        return giftDesc;
    }

    public void setGiftDesc(String giftDesc) {
        this.giftDesc = giftDesc;
    }
}
