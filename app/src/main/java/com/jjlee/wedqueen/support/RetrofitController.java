package com.jjlee.wedqueen.support;

import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.rest.test.service.TestService;

import retrofit2.Retrofit;

/**
 * Created by JJLEE on 2016. 4. 1..
 *
 *
 */
public class RetrofitController {

    public static Retrofit RETROFIT;
    public static Retrofit LOGRETROFIT;

    public RetrofitController() {
        RETROFIT = MyApplication.getRetrofit();
        LOGRETROFIT = MyApplication.getLogRetrofit();
    }

}
