package com.jjlee.wedqueen.rest.sector.model;

import java.util.List;

public class SectorItem {
    private long id;
    private String title;
    private int position;
    private boolean visible;
    private String linkUrl;
    private String imageUrl;
    private String type;
    private float imageRatio;
    private List<SectorItemAux> itemAux;
    private Company company;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getImageRatio() {
        return imageRatio;
    }

    public void setImageRatio(float imageRatio) {
        this.imageRatio = imageRatio;
    }

    public List<SectorItemAux> getItemAux() {
        return itemAux;
    }

    public void setItemAux(List<SectorItemAux> itemAux) {
        this.itemAux = itemAux;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
