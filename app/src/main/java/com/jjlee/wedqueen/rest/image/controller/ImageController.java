package com.jjlee.wedqueen.rest.image.controller;

import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.rest.image.model.ImageResponse;
import com.jjlee.wedqueen.rest.image.service.ImageService;
import com.jjlee.wedqueen.support.RetrofitController;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Created by JJLEE on 2016. 5. 23..
 */
public class ImageController extends RetrofitController {
    private ImageService imageService;

    public ImageController() {
        super();
        imageService = RETROFIT.create(ImageService.class);
    }

    public Call<String> postImageToServer(File file, String requestType) {

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("fileData", file.getName(), requestFile);

        return imageService.postImageToServer("image/upload?type="+requestType, body);

    }

    public Call<ImageResponse> postImageUploadToServer(File file, String requestType) {

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("fileData", file.getName(), requestFile);

        return imageService.postImageUploadToServer("api/v1/image/upload?type="+requestType, body);

    }

    public Call<ImageResponse> postImagesUploadToServer( List<File> files, String requestType) {

        List<MultipartBody.Part> parts = new ArrayList<>();

        for( File file : files ) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("fileData", file.getName(), requestFile);
            parts.add( body );
        }

        return imageService.postImagesUploadToServer( "api/v1/image/upload?type="+requestType, parts );

    }

    public Call<ImageResponse> postImageToServerForProfile(File file, String requestType) {

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("fileData", file.getName(), requestFile);

        return imageService.postImageToServerForProfile("api/v1/image/upload?type="+requestType, body);
    }
}
