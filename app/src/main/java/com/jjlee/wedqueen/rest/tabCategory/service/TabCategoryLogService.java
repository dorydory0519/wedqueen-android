package com.jjlee.wedqueen.rest.tabCategory.service;

import com.jjlee.wedqueen.rest.sector.model.SectorItemLog;
import com.jjlee.wedqueen.rest.sector.model.SectorItemLogResponse;
import com.jjlee.wedqueen.rest.tabCategory.model.TabCategoryLog;
import com.jjlee.wedqueen.rest.tabCategory.model.TabCategoryLogResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface TabCategoryLogService {
    @Headers("Accept: application/json")
    @POST("tabcategory/log")
    Call<TabCategoryLogResponse> postTabCategoryLog(@Body TabCategoryLog tabCategoryLog);

}
