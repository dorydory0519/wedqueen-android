package com.jjlee.wedqueen.rest.instance.service;

import com.jjlee.wedqueen.rest.instance.model.InstanceResponse;
import com.jjlee.wedqueen.rest.tutorial.model.TutorialResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface InstanceService {

    @Headers("Accept: application/json")
    @GET("api/v1/instance/{userAgent}")
    Call<InstanceResponse> getInstances(@Path("userAgent") String userAgent);

}
