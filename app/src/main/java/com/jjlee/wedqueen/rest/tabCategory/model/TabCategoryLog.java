package com.jjlee.wedqueen.rest.tabCategory.model;

/**
 * Created by esmac on 2018. 4. 27..
 */

public class TabCategoryLog {
    private Long tabCategoryId;
    private String userAgent;
    private Long userId;

    public TabCategoryLog(Long tabCategoryId, String userAgent, Long userId) {
        this.tabCategoryId = tabCategoryId;
        this.userAgent = userAgent;
        this.userId = userId;
    }

    public Long getTabCategoryId() {
        return tabCategoryId;
    }

    public void setTabCategoryId(Long tabCategoryId) {
        this.tabCategoryId = tabCategoryId;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
