package com.jjlee.wedqueen.rest.content.model;

/**
 * Created by esmac on 2017. 10. 16..
 */

public class Result {
    private int contentId;

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }
}
