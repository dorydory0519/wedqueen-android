package com.jjlee.wedqueen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jjlee.wedqueen.CustomViews.CustomCompanyModel;
import com.jjlee.wedqueen.CustomViews.CustomDetailCompanyModel;
import com.jjlee.wedqueen.CustomViews.CustomDetailPreminumModel;
import com.jjlee.wedqueen.rest.company.controller.CompanyController;
import com.jjlee.wedqueen.rest.company.model.Company;
import com.jjlee.wedqueen.rest.company.model.CompanyResponse;
import com.jjlee.wedqueen.rest.company.model.Content;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.CustomScrollView;
import com.jjlee.wedqueen.utils.ObjectUtils;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by esmac on 2017. 9. 29..
 */

public class DetailCompanyActivity extends AppCompatActivity implements OnMapReadyCallback {

    private RequestManager mGlideRequestManager;

//    private MapView mapView;
    private LinearLayout detailCompanyWrapper;
    private CustomScrollView detailcompanyScrollview;

    private String categoryName;
    private CompanyController companyController;

    private GoogleMap mGoogleMap;

    private boolean isLoadLastPremiums = false;

    private int premiumPage = 1;
    private int companyPage = 1;

    private boolean isLoadingData;

    private Map<String, Marker> companyMarkers = new HashMap<String, Marker>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailcompany);

        Answers.getInstance().logContentView(new ContentViewEvent()
                .putCustomAttribute( "isLogin", MyApplication.getGlobalApplicationContext().isLoggedIn() + "" )
                .putContentType( "Activity" )
                .putContentName( "detail_company_list" ) );

        mGlideRequestManager = Glide.with( this );
        isLoadingData = false;

        categoryName = getIntent().getStringExtra( "categoryName" );
        companyController = new CompanyController();

        detailCompanyWrapper = (LinearLayout)findViewById(R.id.detailcompany_wrapper);

        android.app.FragmentManager fragmentManager = getFragmentManager();
        MapFragment mapFragment = (MapFragment)fragmentManager
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        detailcompanyScrollview = ((CustomScrollView)findViewById(R.id.detailcompany_scrollview));
        detailcompanyScrollview.setOnBottomReachedListener(new CustomScrollView.OnBottomReachedListener() {
            @Override
            public void onBottomReached() {

                // 데이터 로딩 중에는 그 다음페이지를 로딩하지 않음
                if( !isLoadingData ) {
                    // Premium 업체들을 모두 불러왔는지 확인하여 분기
                    if( isLoadLastPremiums ) {
                        getCompaniesFromServer();
                    } else {
                        getPremiumsFromServer();
                    }
                }
            }
        });

//        mapView = (MapView)findViewById(R.id.mapView);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mGoogleMap = googleMap;

        getPremiumsFromServer();

        mGoogleMap.animateCamera( CameraUpdateFactory.zoomTo( 13 ) );
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                for( int i = 0; i < detailCompanyWrapper.getChildCount(); i++ ) {

                    if( detailCompanyWrapper.getChildAt( i ) instanceof ImageView ) {
                        continue;
                    }

                    LinearLayout premiumModelLayout = (LinearLayout) detailCompanyWrapper.getChildAt( i );

                    // Premium Model Layout의 Tag에 CompanyName을 담아놓았기 때문에 꺼내쓰면됨
                    String companyName = (String)premiumModelLayout.getTag();

                    if( marker.getTitle().equals( companyName ) ) {
                        detailcompanyScrollview.smoothScrollTo( 0, premiumModelLayout.getTop() );
                        marker.showInfoWindow();
                        break;
                    }
                }

                return false;
            }
        });

    }

    private void getPremiumsFromServer() {

        isLoadingData = true;

        Call<CompanyResponse> premiumCall = companyController.getPremiums( categoryName, premiumPage++ );
        premiumCall.enqueue(new Callback<CompanyResponse>() {
            @Override
            public void onResponse(Call<CompanyResponse> call, Response<CompanyResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    CompanyResponse companyResponse = response.body();

                    if( !ObjectUtils.isEmpty( companyResponse ) && companyResponse.getCompanies().size() > 0 ) {
                        List<Company> companies = companyResponse.getCompanies();

                        for( Company company : companies ) {
                            if( !ObjectUtils.isEmpty( DetailCompanyActivity.this ) ) {
                                CustomDetailPreminumModel customDetailPreminumModel = new CustomDetailPreminumModel( DetailCompanyActivity.this, mGlideRequestManager );
                                customDetailPreminumModel.setCompanyThumbnail( company.getThumbnailUrl() );
                                customDetailPreminumModel.setCompanyIdx( "" );
                                customDetailPreminumModel.setCompanyName( company.getName() );
                                customDetailPreminumModel.setCompanyDetail( company.getDescription() );
                                customDetailPreminumModel.setCompanyAddress( company.getAddress() );
                                customDetailPreminumModel.setRealWeddingCnt( company.getRealWeddingCount() );
                                customDetailPreminumModel.setSurveyCnt( company.getSurveyCount() );
                                customDetailPreminumModel.setPaymentCnt( company.getPaymentCount() );

                                List<Content> contents = company.getContents();

                                for( Content content : contents ) {
                                    if( !ObjectUtils.isEmpty( DetailCompanyActivity.this ) ) {
                                        CustomCompanyModel customCompanyModel = new CustomCompanyModel( DetailCompanyActivity.this, mGlideRequestManager );
                                        customCompanyModel.setContentImg( content.getImageUrl() );
                                        customCompanyModel.setContentTitle( content.getTitle() );
                                        customCompanyModel.setContentPrice( 0 );
                                        customCompanyModel.setCompanyInfoGone();
                                        customCompanyModel.setTag( content.getUrl() );
                                        customCompanyModel.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                startFullscreenActivity( "" + v.getTag() );
                                            }
                                        });
                                        customDetailPreminumModel.addCompanyContent( customCompanyModel );
                                    }
                                }
                                if( contents.size() > 0 ) {

                                    TextView moreBtnTextView = new TextView( DetailCompanyActivity.this );
                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT );
                                    moreBtnTextView.setLayoutParams( layoutParams );
                                    moreBtnTextView.setGravity( Gravity.CENTER );
                                    moreBtnTextView.setPadding( 60, 15, 60, 15 );
                                    moreBtnTextView.setText( "더\n보\n기" );
                                    moreBtnTextView.setTag( company.getUrl() );
                                    moreBtnTextView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            startFullscreenActivity( v.getTag() + "" );
                                        }
                                    });

                                    customDetailPreminumModel.addCompanyContent( moreBtnTextView );
                                }

                                // 위도, 경도 정보가 없는 경우 GoogleMap에 추가하지 않음
                                if( company.getLatitude() == 0 && company.getLongitude() == 0 ) {
                                    customDetailPreminumModel.getCompanyShowmap().setVisibility( View.GONE );
                                } else {
                                    LatLng latLng = new LatLng( company.getLatitude(), company.getLongitude() );

                                    MarkerOptions markerOptions = new MarkerOptions();
                                    markerOptions.position(latLng);
                                    markerOptions.title( company.getName() );
                                    markerOptions.icon( BitmapDescriptorFactory.fromResource( R.drawable.addr_picker ) );
                                    companyMarkers.put( company.getName(), mGoogleMap.addMarker( markerOptions ) );

                                    mGoogleMap.moveCamera( CameraUpdateFactory.newLatLng( latLng ) );

                                    customDetailPreminumModel.getCompanyPicker().setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Marker marker = companyMarkers.get( v.getTag() );
                                            mGoogleMap.moveCamera( CameraUpdateFactory.newLatLngZoom( marker.getPosition(), 14 ) );
                                            marker.showInfoWindow();
                                        }
                                    });

                                    customDetailPreminumModel.getCompanyShowmap().setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Marker marker = companyMarkers.get( v.getTag() );
                                            mGoogleMap.moveCamera( CameraUpdateFactory.newLatLngZoom( marker.getPosition(), 14 ) );
                                            marker.showInfoWindow();
                                        }
                                    });
                                }

                                customDetailPreminumModel.setLinkUrl( company.getUrl() );
                                customDetailPreminumModel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startFullscreenActivity( ((CustomDetailPreminumModel)v).getLinkUrl() );
                                    }
                                });

                                detailCompanyWrapper.addView( customDetailPreminumModel );
                            }
                        }

                        if( companyResponse.getLastPage() ) {
                            isLoadLastPremiums = true;
                        }
                    }
                } else {
                    Log.e( "WQTEST", "response body is null" );
                }

                isLoadingData = false;
            }

            @Override
            public void onFailure(Call<CompanyResponse> call, Throwable t) {
                t.printStackTrace();
                isLoadingData = false;
            }
        });
    }

    private void getCompaniesFromServer() {

        isLoadingData = true;

        // 첫 Company Model을 Add하기 전에 구분선을 addView
        if( companyPage < 2 ) {
            ImageView sepLineImg = new ImageView( this );
            sepLineImg.setImageResource(R.drawable.sep_line);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams( LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT );
            layoutParams.setMargins( 20, 20, 20, 40 );
            sepLineImg.setLayoutParams( layoutParams );
            sepLineImg.setPadding( 30, 0, 30, 0 );

            detailCompanyWrapper.addView( sepLineImg );
        }

        Call<CompanyResponse> companyCall = companyController.getCompanies( categoryName, companyPage++ );
        companyCall.enqueue(new Callback<CompanyResponse>() {
            @Override
            public void onResponse(Call<CompanyResponse> call, Response<CompanyResponse> response) {
                if( !ObjectUtils.isEmpty( response.body() ) ) {
                    CompanyResponse companyResponse = response.body();

                    if( !ObjectUtils.isEmpty( companyResponse ) && companyResponse.getCompanies().size() > 0 ) {
                        List<Company> companies = companyResponse.getCompanies();

                        for( Company company : companies ) {

                            if( !ObjectUtils.isEmpty( DetailCompanyActivity.this ) ) {
                                CustomDetailCompanyModel customDetailCompanyModel = new CustomDetailCompanyModel( DetailCompanyActivity.this, mGlideRequestManager );
                                customDetailCompanyModel.setCompanyThumbnail( company.getThumbnailUrl() );
                                customDetailCompanyModel.setCompanyName( company.getName() );
                                customDetailCompanyModel.setRealWeddingCnt( company.getRealWeddingCount() );
                                int paymentCnt = company.getPaymentCount();
                                int surveyCnt = company.getSurveyCount();
                                customDetailCompanyModel.setSurveyCnt( surveyCnt );
                                customDetailCompanyModel.setPaymentCnt( paymentCnt );
                                customDetailCompanyModel.setTag( company.getUrl() );
                                customDetailCompanyModel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startFullscreenActivity( "" + v.getTag() );
                                    }
                                });

                                detailCompanyWrapper.addView( customDetailCompanyModel );
                            }
                        }
                    }
                } else {
                    Log.e( "WQTEST", "response body is null" );
                }

                isLoadingData = false;
            }

            @Override
            public void onFailure(Call<CompanyResponse> call, Throwable t) {
                t.printStackTrace();
                isLoadingData = false;
            }
        });
    }

    private void startFullscreenActivity(String url) {

        if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
            Intent intent = new Intent( this, FullscreenActivity.class );
            intent.putExtra( "URL", url );
            startActivityForResult( intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY );
        } else {
            Intent intent = new Intent( this, AccountActivity.class );
            intent.putExtra( Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN );
            startActivity( intent );
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }
}
