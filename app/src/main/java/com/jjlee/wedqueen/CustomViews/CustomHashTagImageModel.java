package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jjlee.wedqueen.R;

/**
 * Created by esmac on 2017. 9. 21..
 */

public class CustomHashTagImageModel extends LinearLayout{

    TextView hashtagText;
    ImageView hashtagRemove;

    public CustomHashTagImageModel(Context context) {
        super(context);
        initView();

    }

    public CustomHashTagImageModel(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        getAttrs(attrs);

    }

    public CustomHashTagImageModel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        initView();
        getAttrs(attrs, defStyle);

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_hashtagimg, this, false);
        addView(v);

        hashtagText = (TextView)findViewById(R.id.hashtag_text);
        hashtagRemove = (ImageView) findViewById(R.id.hashtag_remove);

    }

    private void getAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.HashTagImageModel);
        setTypeArray(typedArray);
    }

    private void getAttrs(AttributeSet attrs, int defStyle) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.HashTagImageModel, defStyle, 0);
        setTypeArray(typedArray);
    }

    private void setTypeArray(TypedArray typedArray) {

        String hashtagText_resID = typedArray.getString(R.styleable.HashTagImageModel_hashtag_text);
        setHashtagText( hashtagText_resID );

        typedArray.recycle();
    }

    public void setHashtagText(String resString) {
        hashtagText.setText( "#" + resString );
    }

    public String getHashtagText() {
        return hashtagText.getText().toString();
    }

    public ImageView getHashtagRemove() {
        return hashtagRemove;
    }

}
