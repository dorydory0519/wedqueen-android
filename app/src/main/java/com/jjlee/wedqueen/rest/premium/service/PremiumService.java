package com.jjlee.wedqueen.rest.premium.service;

import com.jjlee.wedqueen.rest.premium.model.PremiumAd;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface PremiumService {
    @Headers("Accept: application/json")
    @GET("premium/ad/banner")
    Call<PremiumAd> getPremiumAd();

    @Headers("Accept: application/json")
    @GET("premium/ad")
    Call<List<PremiumAd>> getPremiumAds();
}
