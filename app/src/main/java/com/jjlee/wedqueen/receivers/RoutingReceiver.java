package com.jjlee.wedqueen.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MainActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.SplashActivity;
import com.jjlee.wedqueen.rest.account.controller.AccountController;
import com.jjlee.wedqueen.rest.account.model.NotificationCountResponse;
import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.support.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoutingReceiver extends BroadcastReceiver {

    public int getWhichPageToShow(String basePageUrl) {
        if( Constants.CONTENT_FEED_URL.equals(basePageUrl) ) {
            return 0;
        } else if( Constants.WEDDING_TALK_URL.equals(basePageUrl) ) {
            return 1;
        } else if( Constants.CHECKLIST_URL.equals(basePageUrl) ) {
            return 4;
        } else if( Constants.MY_PAGE_URL.equals(basePageUrl) ) {
            return 4;
        } else if( Constants.INQUIRY_URL.equals(basePageUrl) ) {
            return 3;
        } else if( Constants.POINTSHOP_URL.equals(basePageUrl) ) {
            return 3;
        } else if( Constants.SCRAP_URL.equals(basePageUrl) ) {
            return 4;
        } else {
            return -1;
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        String intentMsg = intent.getStringExtra( "MSG" );
        String intentUrl = intent.getStringExtra("URL");
        String basePageUrl = intentUrl.split("\\?")[0];
        //String notiId = intent.getStringExtra("NOTI_ID");
        int pageIndex = getWhichPageToShow(basePageUrl);

        //requestCountDownSingleNoti(notiId);

        if( MainActivity.instance != null ) {
            Intent newIntent;

            String strIsLogin = "익명";

            if( MyApplication.getGlobalApplicationContext().isLoggedIn() ) {
                strIsLogin = "로그인유저";
            }

            Answers.getInstance().logCustom( new CustomEvent( "PushMessage" )
                    .putCustomAttribute( "isLogin", strIsLogin )
                    .putCustomAttribute( "message", intentMsg )
                    .putCustomAttribute( "url", basePageUrl ));

            if(pageIndex == -1) { // should open Fullscreen Activity
                newIntent = new Intent(MainActivity.instance, FullscreenActivity.class);
                newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                newIntent.putExtra("URL", intent.getStringExtra("URL"));
                newIntent.putExtra("NOTI_ID", intent.getStringExtra("NOTI_ID"));
                MainActivity.instance.startActivity(newIntent);
            } else { // should change viewPager page

                // 이건 MainActivity 의 onNewIntent 로 떨어짐.
                newIntent = new Intent(MyApplication.getGlobalApplicationContext(), MainActivity.class);
                newIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                newIntent.putExtra("URL", intent.getStringExtra("URL"));
                newIntent.putExtra("NOTI_ID", intent.getStringExtra("NOTI_ID"));
                newIntent.putExtra("SHOULD_CHANGE_PAGE_TO", pageIndex);
                MainActivity.instance.startActivity(newIntent);
            }

        } else {

            // 이건 MainActivity 의 onresume 으로 떨어짐.
            Intent newIntent = new Intent(MyApplication.getGlobalApplicationContext(), SplashActivity.class);
            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            newIntent.putExtra("URL", intent.getStringExtra("URL"));
            newIntent.putExtra("NOTI_ID", intent.getStringExtra("NOTI_ID"));
            newIntent.putExtra("SHOULD_CHANGE_PAGE_TO", pageIndex);

            MyApplication.getGlobalApplicationContext().startActivity(newIntent);
        }
    }
}
