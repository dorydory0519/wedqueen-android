package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexboxLayout;
import com.jjlee.wedqueen.R;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class CustomRecentRealWeddingModel extends LinearLayout {

    RelativeLayout realweddingContentSection;
    ImageView writerThumbnail;
    TextView writerName;
    ImageView contentImg;

    FlexboxLayout contentHashTagWrapper;
    TextView contentTitle;

    TextView realWeddingCnt;
    TextView followCnt;

    LinearLayout writerInfo;


    public CustomRecentRealWeddingModel(Context context) {
        super(context);
        initView();

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_recent_realwedding, this, false);
        addView(v);

        realweddingContentSection = (RelativeLayout)findViewById(R.id.realwedding_content_section);
        writerThumbnail = (ImageView) findViewById(R.id.writer_thumbnail);
        writerName = (TextView)findViewById(R.id.writer_name);
        contentImg = (ImageView)findViewById(R.id.content_img);
        contentHashTagWrapper = (FlexboxLayout)findViewById(R.id.content_hashTag_wrapper);
        contentTitle = (TextView)findViewById(R.id.content_title);
        realWeddingCnt = (TextView)findViewById(R.id.realwedding_cnt);
        followCnt = (TextView)findViewById(R.id.follow_cnt);

        writerInfo = (LinearLayout)findViewById(R.id.writer_info);
    }


    public void setWriterThumbnail(String resUrl) {
        Glide.with(getContext()).load(resUrl).bitmapTransform( new CropCircleTransformation( getContext() ) ).placeholder( R.drawable.default_user_icon ).into( writerThumbnail );
    }
    public void setWriterName(String resString) {
        writerName.setText( resString );
    }
    public void setContentImg(String resUrl) {
        Glide.with(getContext()).load(resUrl).centerCrop().placeholder( R.color.image_placeholder_color ).into( contentImg );
    }

    public void setContentTitle(String resString) {
        contentTitle.setText( resString );
    }

    public void setRealWeddingCnt(int resInt) {
        if( resInt > 2 ) {
            realWeddingCnt.setVisibility( View.VISIBLE );
            realWeddingCnt.setText( "리얼웨딩 " + resInt );
        }
    }
    public void setFollowCnt(int resInt) {
        if( resInt > 2 ) {
            followCnt.setVisibility( View.VISIBLE );
            followCnt.setText( "구독자 " + resInt );
        }
    }

    public void setWriterInfoGone() {
        writerInfo.setVisibility( View.GONE );
    }

    public RelativeLayout getRealweddingContentSection() {
        return realweddingContentSection;
    }

    public void setContentHashTagWrapper( List<String> hashTags ) {

        for( String hashTag : hashTags ) {

            CustomEventTagModel customEventTagModel = new CustomEventTagModel( getContext() );
            customEventTagModel.setEventTag( hashTag );

            contentHashTagWrapper.addView( customEventTagModel );

        }
    }

}
