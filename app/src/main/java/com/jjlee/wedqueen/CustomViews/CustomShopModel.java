package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jjlee.wedqueen.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by esmac on 2018. 1. 10..
 */

public class CustomShopModel extends LinearLayout {

    private Spinner shopCountSpinner;
    private Long shopId;
    private int shopCount;
    private TextView shopBuyBtn;

    private ImageView shopImg;
    private TextView shopName;
    private TextView shopPoint;
    private TextView shopCompanyName;

    private String giftImgUrl;
    private String giftBrandName;
    private String giftName;
    private String giftDesc;

    public CustomShopModel(Context context ) {
        super(context);
        initView();
    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_shops, this, false);
        addView(v);

        shopCountSpinner = (Spinner)v.findViewById( R.id.shop_count_spinner );

        shopImg = (ImageView)v.findViewById( R.id.shop_img );
        shopName = (TextView)v.findViewById( R.id.shop_name );
        shopPoint = (TextView)v.findViewById( R.id.shop_point );
        shopCompanyName = (TextView)v.findViewById( R.id.shop_company_name );
        shopBuyBtn = (TextView)v.findViewById( R.id.shop_buy_btn );
        setSpinnerAdapter();

    }

    public void setShopImg( String resUrl ) {
        giftImgUrl = resUrl;
        Glide.with( getContext() ).load( resUrl ).centerCrop().placeholder( R.color.image_placeholder_color ).into( shopImg );
    }
    public void setShopName(String resString) {
        giftName = resString;
        shopName.setText( resString );
    }
    public void setShopPoint(String resString) {
        shopPoint.setText( resString );
    }
    public void setShopCompanyName(String resString) {
        giftBrandName = resString;
        shopCompanyName.setText( resString );
    }

    private void setSpinnerAdapter() {
        List<String> spinnerData = new ArrayList<>();
        for( int i = 0; i < 5; i++ ) {
            spinnerData.add( String.valueOf( i + 1 ) );
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>( getContext(), R.layout.custom_spinner_item, spinnerData );
        arrayAdapter.setDropDownViewResource(R.layout.custom_spinner_dropdown_item);
        shopCountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shopCount = Integer.parseInt( shopCountSpinner.getItemAtPosition( position ).toString() );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        shopCountSpinner.setAdapter( arrayAdapter );
    }

    public TextView getShopBuyBtn() {
        return shopBuyBtn;
    }

    public void setShopId(Long shopId) {
        this.shopId = shopId;
    }

    public Long getShopId() {
        return shopId;
    }

    public int getShopCount() {
        return shopCount;
    }

    public String getGiftImgUrl() {
        return giftImgUrl;
    }

    public void setGiftImgUrl(String giftImgUrl) {
        this.giftImgUrl = giftImgUrl;
    }

    public String getGiftBrandName() {
        return giftBrandName;
    }

    public void setGiftBrandName(String giftBrandName) {
        this.giftBrandName = giftBrandName;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftDesc() {
        return giftDesc;
    }

    public void setGiftDesc(String giftDesc) {
        this.giftDesc = giftDesc;
    }
}
