package com.jjlee.wedqueen.rest.content.model;

import java.util.List;

/**
 * Created by LeeJungYi on 2016-04-01.
 */
public class Image {

    private int contentId;
    private String source;
    private String url;
    private List<String> hashTags;

    public int getContentId() {
        return contentId;
    }

    public void setContentId(int contentId) {
        this.contentId = contentId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getHashTags() {
        return hashTags;
    }

    public void setHashTags(List<String> hashTags) {
        this.hashTags = hashTags;
    }
}
