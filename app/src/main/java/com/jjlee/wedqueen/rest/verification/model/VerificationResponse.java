package com.jjlee.wedqueen.rest.verification.model;

public class VerificationResponse {

    private Verification verification;

    public Verification getVerification() {
        return verification;
    }

    public void setVerification(Verification verification) {
        this.verification = verification;
    }
}
