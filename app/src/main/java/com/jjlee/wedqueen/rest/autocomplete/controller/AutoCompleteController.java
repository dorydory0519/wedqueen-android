package com.jjlee.wedqueen.rest.autocomplete.controller;


import com.jjlee.wedqueen.rest.autocomplete.model.AutoCompleteResponse;
import com.jjlee.wedqueen.rest.autocomplete.service.AutoCompleteService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;


public class AutoCompleteController extends RetrofitController {
    private AutoCompleteService autoCompleteService;

    public AutoCompleteController() {
        super();
        autoCompleteService = RETROFIT.create(AutoCompleteService.class);
    }

    public Call<AutoCompleteResponse> getAutoCompletedCompanies(String keyword) {
        return autoCompleteService.getAutoCompletedCompanies( keyword );
    }

}
