package com.jjlee.wedqueen.rest.dday.model;

import com.jjlee.wedqueen.rest.common.model.AppResponse;

public class CategoryResponse extends AppResponse {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
