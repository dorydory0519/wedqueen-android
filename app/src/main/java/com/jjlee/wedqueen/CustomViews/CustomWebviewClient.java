package com.jjlee.wedqueen.CustomViews;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.jjlee.wedqueen.AccountActivity;
import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MainActivity;
import com.jjlee.wedqueen.MyApplication;
import com.jjlee.wedqueen.fragments.FilterableWebviewFragment;
import com.jjlee.wedqueen.fragments.PageFragment;
import com.jjlee.wedqueen.fragments.WebviewFragment;
import com.jjlee.wedqueen.support.Constants;
import com.jjlee.wedqueen.utils.NetworkUtil;
import com.jjlee.wedqueen.utils.SizeConverter;
import com.kakao.kakaolink.AppActionBuilder;
import com.kakao.kakaolink.AppActionInfoBuilder;
import com.kakao.kakaolink.KakaoLink;
import com.kakao.kakaolink.KakaoTalkLinkMessageBuilder;
import com.kakao.util.KakaoParameterException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by JJLEE on 2016. 3. 28..
 */
public class CustomWebviewClient<T extends PageFragment> extends WebViewClient {

    private T pageFragment;
    private Map<String, String> customHeader = new HashMap<>(); // deviceType

    public CustomWebviewClient(T _pageFragment) {
        super();
        customHeader.put("deviceType", "android");
        customHeader.put("version", MyApplication.getGlobalApplicationContext().getVersionName());
        pageFragment = _pageFragment;
    }


    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.equals(Constants.REFRESH_URL)) {
            pageFragment.refresh();
            return true;
        } else if (url.contains(Constants.SIGN_IN_URL)) {
            Intent intent = new Intent(pageFragment.getContext(), AccountActivity.class);
            intent.putExtra(Constants.ACTION_TYPE, Constants.ACTION_TYPE_MANUAL_LOGIN);
            intent.putExtra( Constants.LOGIN_FROM, "action_webview" );
            pageFragment.getActivity().startActivityForResult(intent, Constants.REQ_CODE_LOGIN);
            return true;
        } else if( url.contains(Constants.BASE_URL) ) {
            if(url.equals(Constants.CONTENT_FEED_URL)) { // 탭 이동
                ((MainActivity)pageFragment.getActivity()).changeViewPagerTo(0);
            } else if(url.equals(Constants.WEDDING_TALK_URL)){
                ((MainActivity)pageFragment.getActivity()).changeViewPagerTo(1);
            } else if(url.equals(Constants.CHECKLIST_URL)){
                ((MainActivity)pageFragment.getActivity()).changeViewPagerTo(3);
            } else if(url.equals(Constants.MY_PAGE_URL)) {
                ((MainActivity)pageFragment.getActivity()).changeViewPagerTo(4);
            } else {

                // 일반적이라면 여기에 걸림
                Intent intent = new Intent(pageFragment.getActivity(), FullscreenActivity.class);
                intent.putExtra("URL", url);
                if(url.contains("nobar=1")) {
                    intent.putExtra("NO_TOOL_BAR", true);
                }
                pageFragment.getActivity().startActivityForResult(intent, Constants.REQ_CODE_OPEN_FULLSCREEN_ACTIVITY);
            }
            return true;
        } else if( !url.contains(Constants.BASE_URL) ) {
            if (url != null) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Intent externalIntent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                    Intent existPackage = pageFragment.getContext().getPackageManager().getLaunchIntentForPackage(intent.getPackage());

                    if (existPackage != null) {
                        pageFragment.getContext().startActivity(externalIntent);
                    } else {
                        try {
                            intent.setPackage("com.android.chrome");
                            pageFragment.getActivity().startActivity(intent);
                        } catch (Exception ex) {
                            // Chrome browser presumably not installed so allow user to choose instead
                            intent.setPackage(null);
                            pageFragment.getActivity().startActivity(intent);
                        }
                    }
                } catch (Exception e) {

                }
            }
            return true;

        } else { // 여긴 걸릴 일이 없음
            if(pageFragment instanceof FilterableWebviewFragment) {
                ((FilterableWebviewFragment) pageFragment).setCurrentUrl(url);
            }
            view.loadUrl(url, customHeader);
            return true;
        }
    }

    @SuppressWarnings("deprecated")
    @Override
    public void onReceivedError(WebView view, int errorCode, String description,
                                String failingUrl)
    {
        super.onReceivedError(view, errorCode, description, failingUrl);
        pageFragment.showNetworkError();
        String customErrorPageHtml = "<html></html>";
        view.loadData(customErrorPageHtml, "text/html", null);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError err) {
        super.onReceivedError(view, req, err);
        pageFragment.showNetworkError();
        String customErrorPageHtml = "<html></html>";
        view.loadData(customErrorPageHtml, "text/html", null);
    }

    @Override
    public void onReceivedHttpError(
            WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {

    }


    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        if( !NetworkUtil.CheckInternetConnection(view.getContext()) ) {
            pageFragment.showNetworkError();
        }
    }
}
