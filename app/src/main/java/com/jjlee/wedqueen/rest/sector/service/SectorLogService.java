package com.jjlee.wedqueen.rest.sector.service;

import com.jjlee.wedqueen.rest.sector.model.SectorItemLog;
import com.jjlee.wedqueen.rest.sector.model.SectorItemLogResponse;
import com.jjlee.wedqueen.rest.sector.model.SectorResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface SectorLogService {
    @Headers("Accept: application/json")
    @POST("sectoritem/log")
    Call<SectorItemLogResponse> postSectorItemLog(@Body SectorItemLog sectorItemLog);

}
