package com.jjlee.wedqueen.rest.dday.service;

import com.jjlee.wedqueen.rest.common.model.AppResponse;
import com.jjlee.wedqueen.rest.dday.model.BudgetResponse;
import com.jjlee.wedqueen.rest.dday.model.CategoryResponse;
import com.jjlee.wedqueen.rest.dday.model.DDayFormResponse;
import com.jjlee.wedqueen.rest.dday.model.DDayResponse;
import com.jjlee.wedqueen.rest.dday.model.DDayV2Response;
import com.jjlee.wedqueen.rest.dday.model.PositionDto;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DDayService {
    @Headers("Accept: application/json")
    @GET("api/v1/dday/category/stats/budget")
    Call<BudgetResponse> getBudgetAvg();

    @Headers("Accept: application/json")
    @GET("user/dday")
    Call<DDayResponse> getdDay();

    @Headers("Accept: application/json")
    @GET("user/dday/info")
    Call<DDayFormResponse> getdDayInfo();

    @POST("user/dday")
    @FormUrlEncoded
    Call<AppResponse> createDDay(@FieldMap Map<String, String> map);

    @POST("user/dday/delete")
    Call<AppResponse> deleteDDay();

    @POST("user/dday/category")
    @FormUrlEncoded
    Call<CategoryResponse> saveCategory(@FieldMap Map<String, String> map);

    @POST("user/dday/category/{id}/complete/update")
    Call<AppResponse> updateCategory(@Path("id") Long id
            , @Query("complete") boolean complete );

    @Headers("Accept: application/json")
    @POST("user/dday/position")
    Call<AppResponse> setPosition(@Body PositionDto positionDto);

    @Headers("Accept: application/json")
    @GET("api/v2/dday")
    Call<DDayV2Response> getDDayV2();

}
