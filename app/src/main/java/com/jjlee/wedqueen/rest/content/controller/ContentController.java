package com.jjlee.wedqueen.rest.content.controller;


import com.jjlee.wedqueen.rest.content.model.Content;
import com.jjlee.wedqueen.rest.content.model.ContentImageResponse;
import com.jjlee.wedqueen.rest.content.model.ContentResponse;
import com.jjlee.wedqueen.rest.content.model.GetContentResponse;
import com.jjlee.wedqueen.rest.content.model.ImageResponse;
import com.jjlee.wedqueen.rest.content.model.PostContent;
import com.jjlee.wedqueen.rest.content.model.PostContentResponse;
import com.jjlee.wedqueen.rest.content.model.RealWeddingContentDto;
import com.jjlee.wedqueen.rest.content.service.ContentService;
import com.jjlee.wedqueen.support.RetrofitController;

import java.util.List;

import retrofit2.Call;

/**
 * Created by LeeJungYi on 2016-04-01.
 *
 *
 */

public class ContentController extends RetrofitController {
    private ContentService contentService;

    public ContentController() {
        super();
        contentService = RETROFIT.create(ContentService.class);
    }

    public Call<ContentResponse> getContents(String category, int page, int targetTime) {
        return contentService.getContents(category, page, targetTime);
    }

    public Call<ContentResponse> getContentsBySort(String category, int page, int targetTime, String orderType) {
        return contentService.getContentsBySort(category, page, targetTime, orderType);
    }

    public Call<PostContentResponse> deleteContent(long id) {
        return contentService.deleteContent( id );
    }

    public Call<PostContentResponse> putContentState(long id, String state) {
        return contentService.putContentState(id, state);
    }

    public Call<GetContentResponse> getContent(long id) {
        return contentService.getContent( id );
    }

//    public Call<PostContentResponse> postContent(PostContent postContent) {
//        return contentService.postContent( postContent );
//    }
//
//    public Call<PostContentResponse> putContent(PostContent putContent, long id) {
//        return contentService.putContent( putContent, id );
//    }

    public Call<PostContentResponse> postContentV2(PostContent postContent) {
        return contentService.postContentV2( postContent );
    }

    public Call<PostContentResponse> putContentV2(PostContent putContent, long id) {
        return contentService.putContentV2( putContent, id );
    }

    public Call<ImageResponse> getImages(int page) {
        return contentService.getImages(page);
    }

    public Call<ContentImageResponse> getContentImages(long contentId) {
        return contentService.getContentImages( contentId );
    }

    public Call<ContentResponse> getBestContent() {
        return contentService.getBestContent();
    }

    public Call<List<RealWeddingContentDto>> getV2Contents(String category, int page, int targetTime) {
        return contentService.getV2Contents(category, page, targetTime);
    }

    public Call<List<RealWeddingContentDto>> getPremiums() {
        return contentService.getPremiums();
    }
}
