package com.jjlee.wedqueen.support;

import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;

import com.jjlee.wedqueen.FullscreenActivity;
import com.jjlee.wedqueen.MainActivity;
import com.jjlee.wedqueen.WebviewContainingActivity;
import com.jjlee.wedqueen.utils.ObjectUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by JJLEE on 2016. 4. 5..
 */
public class WebviewJavascriptInterfaceForFullscreenActivity {

    private final WeakReference<FullscreenActivity> mActivity;


    public WebviewJavascriptInterfaceForFullscreenActivity(FullscreenActivity activity) {
        WeakReference<FullscreenActivity> wr = new WeakReference<>(activity);
        mActivity = wr;
    }

    @JavascriptInterface
    public void openAndSelectFromGallery(String callbackFunctionName, String requestPageType) {
        openAndSelectFromGallery(callbackFunctionName, requestPageType, null);
    }

    @JavascriptInterface
    public void openAndSelectFromGallery(String callbackFunctionName, String requestPageType, String mimeType) {
        mActivity.get().tryOpenGallery(callbackFunctionName, requestPageType, mimeType);
    }

    @JavascriptInterface
    public void startVideo(String videoId) {
        //TODO: 유툽 비됴...
    }

    @JavascriptInterface
    public void setBadgeCount(String badgeCount) {
        //TODO: 배지 카운드 세팅...
    }

    @JavascriptInterface
    public int getAndroidSDK() {
        return Build.VERSION.SDK_INT;
    }

    @JavascriptInterface
    public void sendSurvey( String dataJson ) {
        try {
            JSONObject jsonObject = new JSONObject( dataJson );
            String productId = jsonObject.getString( "id" );
            String productName = jsonObject.getString( "title" );

            Log.e( "WQTEST3", "productId : " + productId );
            Log.e( "WQTEST3", "productName : " + productName );

//            IgawCommerceProductModel productModel = new IgawCommerceProductModel()
//                                                            .setProductID( productId )
//                                                            .setProductName( productName )
//                                                            .setExtraAttrs( "contentType", "survey" );
//            IgawAdbrix.Commerce.productView( mActivity.get(), productModel );

        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    @JavascriptInterface
    public void sendSurveyAnswer( String dataJson ) {
        try {
            JSONObject jsonObject = new JSONObject( dataJson );
            String orderId = jsonObject.getString( "surveyAnswerId" );
            String surveyId = jsonObject.getString( "surveyId" );
            String surveyTitle = jsonObject.getString( "surveyTitle" );

            Log.e( "WQTEST3", "orderId : " + orderId );
            Log.e( "WQTEST3", "surveyId : " + surveyId );
            Log.e( "WQTEST3", "surveyTitle : " + surveyTitle );

//            IgawCommerceProductModel productModel = new IgawCommerceProductModel()
//                                                            .setProductID( surveyId )
//                                                            .setProductName( surveyTitle )
//                                                            .setExtraAttrs( "contentType", "survey" );
//            IgawAdbrix.Commerce.purchase( mActivity.get(), orderId, productModel, (double)0, (double)0, IgawCommerce.IgawPaymentMethod.ETC );
//            IgawAdbrix.retention( "survey" );

        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }


    @JavascriptInterface
    public void sendPurchase( String dataJson ) {
        try {

            JSONObject jsonObject = new JSONObject( dataJson );
            String orderId = jsonObject.getString( "contentId" );
//            double discount = jsonObject.getDouble( "discount" );
//            double deliveryCharge = jsonObject.getDouble( "deliveryCharge" );

            String status = jsonObject.getString( "status" );
            String payMethod = jsonObject.getString( "payMethod" );

            JSONObject company = jsonObject.getJSONObject( "company" );
            String companyName = "";
            if( !ObjectUtils.isEmpty( company ) ) {
                companyName = company.getString( "name" );
            }

//            IgawCommerce.IgawPaymentMethod igawPayMethod = null;
//
////            Log.e( "WQTEST3", "status : " + status );
////            Log.e( "WQTEST3", "payMethod : " + payMethod );
//
//            if( !ObjectUtils.isEmpty( payMethod ) ) {
//                if( payMethod.equals( "card" ) ) {
//                    igawPayMethod = IgawCommerce.IgawPaymentMethod.CreditCard;
//                } else if( payMethod.equals( "trans" ) ) {
//                    igawPayMethod = IgawCommerce.IgawPaymentMethod.BankTransfer;
//                } else if( payMethod.equals( "vbank" ) ) {
//                    igawPayMethod = IgawCommerce.IgawPaymentMethod.ETC;
//                } else if( payMethod.equals( "phone" ) ) {
//                    igawPayMethod = IgawCommerce.IgawPaymentMethod.MobilePayment;
//                }
//            }
//
//            List<IgawCommerceProductModel> productModels = new ArrayList<>();
//            JSONArray paymentCosts = jsonObject.getJSONArray( "paymentCosts" );
//            for( int i = 0; i < paymentCosts.length(); i++ ) {
//                JSONObject paymentCost = paymentCosts.getJSONObject( i );
//
//                String productId = paymentCost.getString( "id" );
//                String productName = paymentCost.getString( "name" );
//                if( !ObjectUtils.isEmpty( companyName ) ) {
//                    productName += " ( " + companyName + " )";
//                }
//                int price = paymentCost.getInt( "amount" );
//
//                JSONObject paymentCostOptionDetail = paymentCost.getJSONArray( "paymentCostOptions" ).getJSONObject( 0 )
//                                                            .getJSONArray( "paymentCostOptionDetails" ).getJSONObject( 0 );
//
//                int quantity = paymentCostOptionDetail.getInt( "quantity" );
//                int optionPrice = paymentCostOptionDetail.getInt( "amount" );
//
////                int quantity = paymentCost.getJSONArray( "paymentCostOptions" )
////                        .getJSONObject( 0 ).getJSONArray( "paymentCostOptionDetails" )
////                        .getJSONObject( 0 ).getInt( "quantity" );
//
//                Log.e( "WQTEST3", "productId : " + productId );
//                Log.e( "WQTEST3", "productName : " + productName );
//                Log.e( "WQTEST3", "price : " + price );
//                Log.e( "WQTEST3", "quantity : " + quantity );
//                Log.e( "WQTEST3", "optionPrice : " + optionPrice );
//
//                IgawCommerceProductModel productModel = new IgawCommerceProductModel()
//                                                                .setProductID( productId )
//                                                                .setProductName( productName )
//                                                                .setPrice( price + optionPrice )
//                                                                .setQuantity( quantity )
//                                                                .setCurrency( IgawCommerce.Currency.KR_KRW )
//                                                                .setExtraAttrs( "contentType", "product" );
//
//                productModels.add( productModel );
//
//            }
//
//            if( !ObjectUtils.isEmpty( igawPayMethod )
//                    && !ObjectUtils.isEmpty( status ) && status.equals( "paid" )
//                    && productModels.size() > 0 ) {
//
//                Log.e( "WQTEST3", "orderId : " + orderId );
//
//                IgawAdbrix.Commerce.purchaseBulk( mActivity.get(), orderId, productModels, (double)0, (double)0, igawPayMethod );
//            }

        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    @JavascriptInterface
    public void sendRefund( String dataJson ) {
        try {
            JSONObject jsonObject = new JSONObject( dataJson );
            String orderId = jsonObject.getString( "orderId" );
            double penaltyCharge = jsonObject.getDouble( "penaltyCharge" );

//            IgawAdbrix.Commerce.refund( mActivity.get(), orderId, new IgawCommerceProductModel(), penaltyCharge );
        } catch ( Exception e ) {
        }
    }

    @JavascriptInterface
    public void showToolbarItems(String parsableJson) {
        final FullscreenActivity thisActivity = mActivity.get();
        try {
            JSONArray jsonArray = new JSONArray(parsableJson);
            thisActivity.toolbarItemsJson = jsonArray;
            for(int i=0; i<jsonArray.length(); i++){
                final JSONObject obj = jsonArray.getJSONObject(i);
                final String item = obj.getString("item");

                thisActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (item.equals("share")) {
                                thisActivity.shareButton.setVisibility(View.VISIBLE);
                            }


                            if (item.equals("love")) {
                                Boolean loved = obj.getBoolean("loved");
                                if (loved) {
                                    thisActivity.lovedOnButton.setVisibility(View.VISIBLE);
                                    thisActivity.lovedOffButton.setVisibility(View.GONE);
                                } else {
                                    thisActivity.lovedOnButton.setVisibility(View.GONE);
                                    thisActivity.lovedOffButton.setVisibility(View.VISIBLE);
                                }

                            }

                            if (item.equals("delete")) {
                                thisActivity.deleteButton.setVisibility(View.VISIBLE);
                            }

                            if (item.equals("edit")) {
                                thisActivity.editButton.setVisibility(View.VISIBLE);
                            }

                            if (item.equals("phone")) {
                                thisActivity.phoneButton.setVisibility(View.VISIBLE);
                            }

                            if (item.equals("talk")) {
                                thisActivity.talkButton.setVisibility(View.VISIBLE);
                            }

                            if (item.equals("homepage")) {
                                thisActivity.homepageButton.setVisibility(View.VISIBLE);
                            }

                            if (item.equals("location")) {
                                thisActivity.locationButton.setVisibility(View.VISIBLE);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    @JavascriptInterface
    public void openSetting() {
        final WebviewContainingActivity thisActivity = mActivity.get();
        if(thisActivity instanceof MainActivity) {
            thisActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((MainActivity)thisActivity).toggleDrawerLayout();
                }
            });
        }
    }

    @JavascriptInterface
    public void hideToolbar() {
        (mActivity.get()).hideToolbar();
    }

    @JavascriptInterface
    public void showToolbar() {
        (mActivity.get()).showToolbar();
    }

    @JavascriptInterface
    public void disableRefresh() {
        (mActivity.get()).disableRefresh();
    }

    @JavascriptInterface
    public void enableRefresh() {
        (mActivity.get()).enableRefresh();
    }

    @JavascriptInterface
    public void showInviteFriendPopup() {
        mActivity.get().showInviteFriendPopup();
    }

//    @JavascriptInterface
//    public void setFabButtonUrlAndMakeVisible(String uri) {
//        mActivity.get().setFabButtonUrlAndMakeVisible(uri);
//    }
//
//    @JavascriptInterface
//    public void showFab() {
//        mActivity.get().showFab();
//    }
//
//    @JavascriptInterface
//    public void hideFab() {
//        mActivity.get().hideFab();
//    }

}
