package com.jjlee.wedqueen.CustomViews;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.flexbox.FlexboxLayout;
import com.jjlee.wedqueen.R;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class CustomReservModel extends LinearLayout {

    private ImageView reservCompanyThumb;
    private TextView reservCompanyName;
    private TextView reservTime;

    private LinearLayout visitSection;
    private LinearLayout contractSection;
    private LinearLayout reviewSection;

    private LinearLayout visitMark;
    private LinearLayout visitAuth;
    private LinearLayout contractMark;
    private LinearLayout contractAuth;
    private LinearLayout reviewMark;
    private LinearLayout reviewAuth;

    private TextView reservStatus;

    private TextView visitPoint;
    private TextView contractPoint;
    private TextView reviewPoint;

    private String strContractPoint;
    private String strReviewPoint;

    private LinearLayout reservOn;
    private LinearLayout reservOff;

    private LinearLayout startDetailCom;

    public CustomReservModel(Context context) {
        super(context);
        initView();

    }

    private void initView() {

        String infService = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li = (LayoutInflater) getContext().getSystemService(infService);
        View v = li.inflate(R.layout.layout_reserv, this, false);
        addView(v);

        visitSection = (LinearLayout)v.findViewById( R.id.visit_section );
        contractSection = (LinearLayout)v.findViewById( R.id.contract_section );
        reviewSection = (LinearLayout)v.findViewById( R.id.review_section );

        reservCompanyThumb = (ImageView)v.findViewById( R.id.reserv_company_thumb );
        reservCompanyName = (TextView)v.findViewById( R.id.reserv_company_name );
        reservTime = (TextView)v.findViewById( R.id.reserv_time );
        visitPoint = (TextView)v.findViewById( R.id.visit_point );
        contractPoint = (TextView)v.findViewById( R.id.contract_point );
        reviewPoint = (TextView)v.findViewById( R.id.review_point );

        visitMark = v.findViewById( R.id.visit_mark );
        visitAuth = v.findViewById( R.id.visit_auth );
        contractMark = v.findViewById( R.id.contract_mark );
        contractAuth = v.findViewById( R.id.contract_auth );
        reviewMark = v.findViewById( R.id.review_mark );
        reviewAuth = v.findViewById( R.id.review_auth );

        reservStatus = v.findViewById( R.id.reserv_status );

        reservOn = (LinearLayout)v.findViewById( R.id.reserv_on );
        reservOff = (LinearLayout)v.findViewById( R.id.reserv_off );

        startDetailCom = (LinearLayout) v.findViewById( R.id.start_detailcom );

    }


    public void setReservOnOff( boolean existReserv ) {
        if( existReserv ) {
            reservOn.setVisibility( View.VISIBLE );
            reservOff.setVisibility( View.GONE );
        } else {
            reservOn.setVisibility( View.GONE );
            reservOff.setVisibility( View.VISIBLE );
        }
    }

    public void setReservCompanyThumb( String resUrl ) {
        Glide.with( getContext() ).load( resUrl ).centerCrop().bitmapTransform( new CropCircleTransformation( getContext() ) ).placeholder( R.color.image_placeholder_color ).into( reservCompanyThumb );
    }
    public void setReservCompanyName(String resString) {
        reservCompanyName.setText( resString );
    }
    public void setReservTime(String resString) {
        reservTime.setText( resString );
    }
    public void setReviewParam( int surveyAnswerId, String companyName, String reservTime ) {
        reviewSection.setTag( surveyAnswerId + ";" + companyName + ";" + reservTime );
    }
    public void setReservStatus( String resString ) {
        reservStatus.setText( surveyStatusToString( resString ) );
    }


    public void setVisit( boolean isVisit, String point ) {

        visitPoint.setText( point );

        if( isVisit ) {
//            visitPoint.setText( "지급완료" );
//            visitPoint.setBackgroundColor( getResources().getColor( R.color.paid_point_back_color ) );
            visitMark.setVisibility( VISIBLE );
            visitAuth.setVisibility( GONE );
        } else {
//            visitPoint.setText( point );
//            visitPoint.setBackgroundColor( getResources().getColor( R.color.point_back_color ) );
            visitMark.setVisibility( GONE );
            visitAuth.setVisibility( VISIBLE );
        }
    }

    public void setContract( boolean isContract, String point ) {

        contractPoint.setText( point );

        if( isContract ) {
//            contractPoint.setText( "지급완료" );
//            contractPoint.setBackgroundColor( getResources().getColor( R.color.paid_point_back_color ) );
            contractMark.setVisibility( VISIBLE );
            contractAuth.setVisibility( GONE );
        } else {
//            contractPoint.setText( point );
//            contractPoint.setBackgroundColor( getResources().getColor( R.color.point_back_color ) );
            contractMark.setVisibility( GONE );
            contractAuth.setVisibility( VISIBLE );
        }
    }

    public void setReview( boolean isReview, String point ) {

        reviewPoint.setText( point );

        if( isReview ) {
//            reviewPoint.setText( "지급완료" );
//            reviewPoint.setBackgroundColor( getResources().getColor( R.color.paid_point_back_color ) );
            reviewMark.setVisibility( VISIBLE );
            reviewAuth.setVisibility( GONE );
        } else {
//            reviewPoint.setText( point );
//            reviewPoint.setBackgroundColor( getResources().getColor( R.color.point_back_color ) );
            reviewMark.setVisibility( GONE );
            reviewAuth.setVisibility( VISIBLE );
        }
    }


    public LinearLayout getVisitSection() {
        return visitSection;
    }
    public LinearLayout getContractSection() {
        return contractSection;
    }
    public LinearLayout getReviewSection() {
        return reviewSection;
    }

    public LinearLayout getStartDetailCom() {
        return startDetailCom;
    }
    public String getStrContractPoint() { return strContractPoint; }
    public String getStrReviewPoint() {
        return strReviewPoint;
    }

    private String surveyStatusToString( String status ) {

        String resultStr = "";

        if( status.equals( "neverVisited" ) ) {
            resultStr = "방문취소";
        } else if( status.equals( "visited" ) ) {
            resultStr = "방문완료";
        } else if( status.equals( "contracted" ) ) {
            resultStr = "계약완료";
        } else if( status.equals( "canceled" ) ) {
            resultStr = "계약취소";
        } else if( status.equals( "reservationCanceled" ) ) {
            resultStr = "예약취소";
        } else if( status.equals( "noSelected" ) ) {
            resultStr = "선택안됨";
        } else if( status.equals( "contractByCall" ) ) {
            resultStr = "방문취소";
        }

        return resultStr;
    }
}
