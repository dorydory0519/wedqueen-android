package com.jjlee.wedqueen.rest.dday.model;

import com.jjlee.wedqueen.rest.common.model.AppResponse;

/**
 * Created by esmac on 2017. 5. 29..
 */

public class DDayResponse extends AppResponse {

    private DDayDto dDay;

    public DDayResponse() {}

    public DDayDto getdDay() {
        return dDay;
    }

    public void setdDay(DDayDto dDay) {
        this.dDay = dDay;
    }
}
