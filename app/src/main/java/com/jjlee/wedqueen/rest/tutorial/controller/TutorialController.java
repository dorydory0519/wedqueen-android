package com.jjlee.wedqueen.rest.tutorial.controller;

import com.jjlee.wedqueen.rest.tutorial.model.TutorialResponse;
import com.jjlee.wedqueen.rest.tutorial.service.TutorialService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

public class TutorialController extends RetrofitController {
    private TutorialService tutorialService;

    public TutorialController() {
        super();
        tutorialService = RETROFIT.create(TutorialService.class);
    }

    public Call<TutorialResponse> getTutorial() {
        return tutorialService.getTutorial( "android" );
    }

}
