package com.jjlee.wedqueen.rest.popup.service;

import com.jjlee.wedqueen.rest.popup.model.PopupResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface PopupService {

    @Headers("Accept: application/json")
    @GET("api/v1/popup/{userAgent}")
    Call<PopupResponse> getPopup(@Path("userAgent") String userAgent);

    @Headers("Accept: application/json")
    @GET("api/v2/popup/{userAgent}")
    Call<PopupResponse> getPopupV2(@Path("userAgent") String userAgent);

}
