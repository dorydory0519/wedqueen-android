package com.jjlee.wedqueen.rest.content.model;

/**
 * Created by LeeJungYi on 2016-04-01.
 */
public class ContentFragment {

    private String type;
    private String source;
    private float imageRatio;

    public ContentFragment(String type, String source) {
        this.type = type;
        this.source = source;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public float getImageRatio() {
        return imageRatio;
    }

    public void setImageRatio(float imageRatio) {
        this.imageRatio = imageRatio;
    }
}
