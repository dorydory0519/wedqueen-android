package com.jjlee.wedqueen.rest.weddingtalk.controller;


import com.jjlee.wedqueen.rest.common.model.CommonResponse;
import com.jjlee.wedqueen.rest.weddingtalk.service.WeddingtalkService;
import com.jjlee.wedqueen.support.RetrofitController;

import retrofit2.Call;

/**
 * Created by JJLEE on 2016. 5. 7..
 */
public class WeddingtalkController extends RetrofitController {
    private WeddingtalkService weddingtalkService;

    public WeddingtalkController() {
        super();
        weddingtalkService = RETROFIT.create(WeddingtalkService.class);
    }

    public Call<CommonResponse> deleteWeddingtalk(Long weddingtalkId) {
        return weddingtalkService.deleteWeddingtalk(weddingtalkId, "DELETE");
    }

}
