package com.jjlee.wedqueen.rest.survey.model;

import java.util.List;

/**
 * Created by esmac on 2018. 1. 11..
 */

public class SurveyResponse {
    private String msg;
    private int resultCode;
    private List<SurveyAnswer> surveyAnswers;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public List<SurveyAnswer> getSurveyAnswers() {
        return surveyAnswers;
    }

    public void setSurveyAnswers(List<SurveyAnswer> surveyAnswers) {
        this.surveyAnswers = surveyAnswers;
    }
}
